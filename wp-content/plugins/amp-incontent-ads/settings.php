<?php

if(is_plugin_active( 'amp/amp.php' )){
$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name'              => 'redux_builder_amp', // This is where your data is stored in the database and also becomes your global variable name.
    'display_name'          =>  __( 'AMPforWP Advance AMP Ads Options','accelerated-mobile-pages' ), // Name that appears at the top of your panel
    'menu_type'             => 'menu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu'        => true, // Show the sections below the admin menu item or not
    'menu_title'            => __( 'Advanced AMP Ads', 'accelerated-mobile-pages' ),
    'page_title'            => __('Advanced AMP Ads','accelerated-mobile-pages'),
    'global_variable'       => '', // Set a different name for your global variable other than the opt_name
    'dev_mode'              => false, // Show the time the page took to load, etc
    'customizer'            => false, // Enable basic customizer support,
    'async_typography'      => false, // Enable async for fonts,
    'disable_save_warn'     => true,
    'open_expanded'         => false,
    // OPTIONAL -> Give you extra features
    'page_priority'         => null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent'           => 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions'      => 'manage_options', // Permissions needed to access the options panel.
    'last_tab'              => '', // Force your panel to always open to a specific tab (by id)
    'page_icon'             => 'icon-themes', // Icon displayed in the admin panel next to your menu_title
    'page_slug'             => 'advanced_amp_ads_options', // Page slug used to denote the panel
    'save_defaults'         => true, // On load save the defaults to DB before user clicks save or not
    'default_show'          => false, // If true, shows the default value next to each field that is not the default value.
    'default_mark'          => '', // What to print by the field's title if the value shown is default. Suggested: *
    'admin_bar'             => false,
    'admin_bar_icon'        => 'dashicons-admin-generic', 
    // CAREFUL -> These options are for advanced use only
    'output'                => false, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag'            => false, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    //'domain'              => 'redux-framework', // Translation domain key. Don't change this unless you want to retranslate all of Redux.
    'footer_credit'         => false, // Disable the footer credit of Redux. Please leave if you can help it.
    'footer_text'           => "",
    'show_import_export'    => false,
    'system_info'           => true,

);

    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ahmedkaludi/Accelerated-Mobile-Pages',
        'title' => __('Visit us on GitHub','accelerated-mobile-pages'),
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );


Redux::setArgs( "redux_builder_amp", $args );
}

//my code starts here
function ampforwp_create_controls() {
	$controls = array();

    $controls[] =  array(
                   'id' => 'ads_first_block',
                   'type' => 'section',
                   'title' => __('Global', 'accelerated-mobile-pages'),
                   'indent' => true,
                   'layout_type' => 'accordion',
                   'accordion-open'=> 1,
        );

	$controls[] =  array(
        'id'        =>'ampforwp-data-strategy-loading',
        'type'      => 'switch',
        'title'     => __('Optimize Ads', 'redux-framework-demo'),
        'tooltip-subtitle'	=> __('Optimize for viewability', 'redux-framework-demo'),
        'default'   => 1,
	);	

	// AMP Auto ADs
	if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){
		$controls[] = array(
			'id'        	=> 'ampforwp-amp-auto-ads',
			'type'      	=> 'switch',
			'title'     	=> __('AMP Auto Ads'),
			'default'		=> 0,
		);
	

		$controls[] = array(
			'class' => 'child_opt child_opt_arrow',
			'id'        	=> 'ampforwp-amp-auto-ads-code',
			'type'      	=> 'textarea',
			'title'     	=> __('AMP Auto Ads Code'),							
			'placeholder'	=> '<amp-auto-ads
	type="adsense"
	data-ad-client="ca-pub-5439573510495356">
</amp-auto-ads>',
			'required'		=> array('ampforwp-amp-auto-ads','=','1'),
		);
	}


$controls[] = array(
				    'id'        =>'ampforwp-ad-sponsorship',
				    'type'      => 'switch',
				    'title'     => __('Sponsorship Label', 'redux-framework-demo'),
				    'default'   => 0,
				    'true'      => 'Enabled',
				    'false'     => 'Disabled',
				);
	
		$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',	
		'id'       => 'ampforwp-ad-sponsorship-location',
		'type'     => 'select',
		'title'    => __( 'Location', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Sponsorship Location you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Above the Ads',
			'2'  	=> 'Below the Ads',
					),
		'default'  => '2',
		'required'	=> array('ampforwp-ad-sponsorship','=','1'),
	);


	$controls[] = array(
		            'class' => 'child_opt',
			        'id'        =>'ampforwp-ad-sponsorship-label',
			        'type'      => 'text',
			        'required'  => array('ampforwp-ad-sponsorship', '=' , '1'),
			        'title'     => __('Sponsorship Label Text', 'redux-framework-demo'),
			        'tooltip-subtitle'		=> 'It will display below the all Ads',
			        'default'   => '',
			        'placeholder'=> 'Sponsored'
	);	
    

	// Incontent Ad #1
	$controls[] =  array(
                   'id' => 'ads_second_block',
                   'type' => 'section',
                   'title' => __('Incontent Ads', 'accelerated-mobile-pages'),
                   'indent' => true,
                   'layout_type' => 'accordion',
                   'accordion-open'=> 1,
        );
		$controls[] =  array(
        'id'        =>'ampforwp-incontent-ad-1',
        'type'      => 'switch',
        'title'     => __('Incontent Ad #1', 'redux-framework-demo'),
        'default'   => 0,
        'true'      => 'Enabled',
        'false'     => 'Disabled',
		'default'		=> 0,
	);
	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-incontent-ad-1',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-incontent-ad-1','=','1'),
	);
	// Responsive ad unit incontent-1
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-incontent-1',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
			'default'	=> 0, 
	);
 
	// '1' 	=> 'Adsense',
		$controls[] =  array(
	        'class' => 'child_opt',
			'id'       => 'ampforwp-adsense-ad-position-incontent-ad-1',
			'type'     => 'select',
			'title'    => __( 'Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-adsense-ad-position-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-adsense-ad-position-incontent-ad-1','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
					  array('adsense-rspv-ad-incontent-1','=','0'),
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
					 array('adsense-rspv-ad-incontent-1','=','0'),
				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'     => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','1'),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-doubleclick-ad-position-incontent-ad-1',
			'type'     => 'select',
			'title'    => __( 'DoubleClick incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-doubleclick-ad-position-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-1','=','custom'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
		);

	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-1','=','1'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'default' =>'700x90',
			'required'	=> array('ampforwp-doubleclick-ad-data-multi-size','=','1'),
			  
		);
			$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-1','=','1'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','2'),
			'default'	=> 0, 
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		// '4'  	=> 'Amazon',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-amazon-ad-position-incontent-ad-1',
			'type'     => 'select',
			'title'    => __( 'Amazon incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','4'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-amazon-ad-position-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-1','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-amazon-ad-width-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-1','=','4'),
					  
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-amazon-ad-height-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-1','=','4'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-data-ad-client-incontent-ad-1',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'tooltip-subtitle'      => __('Enter the Data Ad Instance ID (data-ad-instanceid) from the amazon ad code. e.g. feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','4'),
		);

		//MGID

		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-mgid-ad-position-incontent-ad-1',
			'type'     => 'select',
			'title'    => __( 'MGID incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','5'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-mgid-ad-position-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-mgid-ad-position-incontent-ad-1','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-width-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-1','=','5'),
					  
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-height-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '320',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-1','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Publisher-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Data Publisher'),
			'placeholder'	=> 'site.com',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-1','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Widget-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Data Widget'),
			'placeholder'	=> '3XXXXX',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-1','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Container-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Data Container'),
			'placeholder'	=> 'MXXScriptRootCXXXXXX',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-1','=','5'),
 				),
		);
				
	// '3'  	=> 'Custom Advertisement',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-custom-ads-ad-position-incontent-ad-1',
			'type'     => 'select',
			'title'    => __( 'Custom Ads incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-1','=','3'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-custom-ads-ad-position-incontent-ad-1',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-custom-ads-ad-position-incontent-ad-1','=','custom'),
		);
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-incontent-ad-1',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-1','=','3'),
		);
		if(is_plugin_active('accelerated-mobile-pages/accelerated-moblie-pages.php')){
		$controls[] = array(
			'class' => 'child_opt',
		    'id'       => 'fx-checkbox',
		    'type'     => 'checkbox',
		    'title'    => __('Parallax Effect', 'redux-framework-demo'), 
		    'tooltip-subtitle' => __('add parallax effect (amp fx flying carpet) to the ad', 'redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-1','=','1'),
		    'default'  => 0// 1 = on | 0 = off
		);
	}
	// Incontent Ad #1 ENDS

	// Incontent Ad #2
	$controls[] =  array(
        'id'        =>'ampforwp-incontent-ad-2',
        'type'      => 'switch',
        'title'     => __('Incontent Ad #2', 'redux-framework-demo'),
        'default'   => 0,
        'true'      => 'Enabled',
        'false'     => 'Disabled',
		'default'		=> 0,
	);
	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-incontent-ad-2',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'3'  	=> 'Custom Advertisement',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-incontent-ad-2','=','1'),
	);

	// Responsive ad unit incontent-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-incontent-2',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-adsense-ad-position-incontent-ad-2',
			'type'     => 'select',
			'title'    => __( 'Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
		);
		$controls[] =  array(
			'id'       		=> 'ampforwp-custom-adsense-ad-position-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-adsense-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
						array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
						array('adsense-rspv-ad-incontent-2','=','0'),
					),

		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'required'		=> array(
						array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
						array('adsense-rspv-ad-incontent-2','=','0'),
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
		    'tooltip-subtitle'  => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'    => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','1'),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-doubleclick-ad-position-incontent-ad-2',
			'type'     => 'select',
			'title'    => __( 'DoubleClick incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-doubleclick-ad-position-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-ad-2',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-2','=','1'),'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'default' =>'700x90',
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-ad-2','=','1'),
		);

			$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-ad-2',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-2','=','1'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-ad-2','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-amazon-ad-position-incontent-ad-2',
			'type'     => 'select',
			'title'    => __( 'Amazon incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','4'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-amazon-ad-position-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-amazon-ad-width-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-2','=','4'),
					  
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-amazon-ad-height-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-2','=','4'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-data-ad-client-incontent-ad-2',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'tooltip-subtitle'      => __('Enter the Data Ad Instance ID (data-ad-instanceid) from the amazon ad code. e.g. feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','4'),
		);


		//MGID

		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-mgid-ad-position-incontent-ad-2',
			'type'     => 'select',
			'title'    => __( 'MGID incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','5'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-mgid-ad-position-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-mgid-ad-position-incontent-ad-2','=','custom'),
		);
		
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-width-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-2','=','5'),
					  
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-height-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '320',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-2','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Publisher-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Data Publisher'),
			'placeholder'	=> 'site.com',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-2','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Widget-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Data Widget'),
			'placeholder'	=> '3XXXXX',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-2','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Container-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Data Container'),
			'placeholder'	=> 'MXXScriptRootCXXXXXX',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-2','=','5'),
 				),
		);
	// '3'  	=> 'Custom Advertisement',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-custom-ads-ad-position-incontent-ad-2',
			'type'     => 'select',
			'title'    => __( 'Custom Ads incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-2','=','3'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-custom-ads-ad-position-incontent-ad-2',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-custom-ads-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-incontent-ad-2',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-2','=','3'),
		);
	if(is_plugin_active('accelerated-mobile-pages/accelerated-moblie-pages.php')){
		$controls[] = array(
			'class' => 'child_opt',
		    'id'       => 'fx-checkbox-2',
		    'type'     => 'checkbox',
		    'title'    => __('Parallax Effect', 'redux-framework-demo'), 
		    'tooltip-subtitle' => __('add parallax effect (amp fx flying carpet) to the ad', 'redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-2','=','1'),
		    'default'  => 0// 1 = on | 0 = off
		);
	}
	// Incontent Ad #2 ENDS

	// Incontent Ad #3
	$controls[] =  array(
        'id'        =>'ampforwp-incontent-ad-3',
        'type'      => 'switch',
        'title'     => __('Incontent Ad #3', 'redux-framework-demo'),
        'default'   => 0,
        'true'      => 'Enabled',
        'false'     => 'Disabled',
		'default'		=> 0,
	);
	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-incontent-ad-3',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'3'  	=> 'Custom Advertisement',
			'5'  	=> 'MGID',
			
		),
		'default'  => '1',
		'required'	=> array('ampforwp-incontent-ad-3','=','1'),
	);

		// Responsive ad unit incontent-3
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-incontent-3',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-adsense-ad-position-incontent-ad-3',
			'type'     => 'select',
			'title'    => __( 'Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-adsense-ad-position-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-adsense-ad-position-incontent-ad-3','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
				array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
				array('adsense-rspv-ad-incontent-3','=','0'),
			),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
					array('adsense-rspv-ad-incontent-3','=','0'),
				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'   => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'    => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','1'),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-doubleclick-ad-position-incontent-ad-3',
			'type'     => 'select',
			'title'    => __( 'DoubleClick incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-doubleclick-ad-position-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-ad-3',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-3','=','1'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'default' =>'700x90',
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-ad-3','=','1'),
		);
					$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-ad-3',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-3','=','1'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-ad-3','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-amazon-ad-position-incontent-ad-3',
			'type'     => 'select',
			'title'    => __( 'Amazon incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','4'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-amazon-ad-position-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-3','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-amazon-ad-width-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-3','=','4'),
					  
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-amazon-ad-height-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-3','=','4'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-data-ad-client-incontent-ad-3',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'tooltip-subtitle'      => __('Enter the Data Ad Instance ID (data-ad-instanceid) from the amazon ad code. e.g. feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','4'),
		);


		//MGID

		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-mgid-ad-position-incontent-ad-3',
			'type'     => 'select',
			'title'    => __( 'MGID incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','5'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-mgid-ad-position-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-mgid-ad-position-incontent-ad-3','=','custom'),
		);
		
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-width-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-3','=','5'),
					  
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-height-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '320',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-3','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Publisher-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Data Publisher'),
			'placeholder'	=> 'site.com',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-3','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Widget-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Data Widget'),
			'placeholder'	=> '3XXXXX',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-3','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Container-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Data Container'),
			'placeholder'	=> 'MXXScriptRootCXXXXXX',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-3','=','5'),
 				),
		);
	// '3'  	=> 'Custom Advertisement',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-custom-ads-ad-position-incontent-ad-3',
			'type'     => 'select',
			'title'    => __( 'Custom Ads incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-3','=','3'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-custom-ads-ad-position-incontent-ad-3',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-custom-ads-ad-position-incontent-ad-3','=','custom'),
		);
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-incontent-ad-3',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-3','=','3'),
		);
	if(is_plugin_active('accelerated-mobile-pages/accelerated-moblie-pages.php')){
		$controls[] = array(
			'class' => 'child_opt',
		    'id'       => 'fx-checkbox-3',
		    'type'     => 'checkbox',
		    'title'    => __('Parallax Effect', 'redux-framework-demo'), 
		    'tooltip-subtitle' => __('add parallax effect (amp fx flying carpet) to the ad', 'redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-3','=','1'),
		    'default'  => 0// 1 = on | 0 = off
		);
	}
	// Incontent Ad #3 ENDS



	// Incontent Ad #4
	$controls[] =  array(
        'id'        =>'ampforwp-incontent-ad-4',
        'type'      => 'switch',
        'title'     => __('Incontent Ad #4', 'redux-framework-demo'),
        'default'   => 0,
        'true'      => 'Enabled',
        'false'     => 'Disabled',
		'default'		=> 0,
	);
	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-incontent-ad-4',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'3'  	=> 'Custom Advertisement',
			'5'     => 'MGID'
		),
		'default'  => '1',
		'required'	=> array('ampforwp-incontent-ad-4','=','1'),
	);

		// Responsive ad unit incontent-4
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-incontent-4',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-adsense-ad-position-incontent-ad-4',
			'type'     => 'select',
			'title'    => __( 'Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
				
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','1'),
		);
		$controls[] =  array(
		    'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-adsense-ad-position-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-adsense-ad-position-incontent-ad-4','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
				array('ampforwp-advertisement-type-incontent-ad-4','=','1'),
				array('adsense-rspv-ad-incontent-4','=','0'),
			),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '320',
			'required'		=> array(
				array('ampforwp-advertisement-type-incontent-ad-4','=','1'),
				array('adsense-rspv-ad-incontent-4','=','0'),
			),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-incontent-ad-4',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'     => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','1'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-incontent-ad-4',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
	        'tooltip-subtitle'    => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','1'),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-doubleclick-ad-position-incontent-ad-4',
			'type'     => 'select',
			'title'    => __( 'DoubleClick incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','2'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-doubleclick-ad-position-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-2','=','custom'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-incontent-ad-4',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-incontent-ad-4',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-incontent-ad-4',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','2'),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-ad-4',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-4','=','1'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','2'),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-incontent-ad-4',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'default' =>'700x90',
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-ad-4','=','1'),
		);
			$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-ad-4',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-4','=','1'),
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-incontent-ad-4',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-ad-4','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-amazon-ad-position-incontent-ad-4',
			'type'     => 'select',
			'title'    => __( 'Amazon incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','4'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-amazon-ad-position-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-doubleclick-ad-position-incontent-ad-4','=','custom'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-amazon-ad-width-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-4','=','4'),
					  
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-amazon-ad-height-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-4','=','4'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-data-ad-client-incontent-ad-4',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'tooltip-subtitle'      => __('Enter the Data Ad Instance ID (data-ad-instanceid) from the amazon ad code. e.g. feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','4'),
		);

		//MGID

		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-mgid-ad-position-incontent-ad-4',
			'type'     => 'select',
			'title'    => __( 'MGID incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','5'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-mgid-ad-position-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-mgid-ad-position-incontent-ad-4','=','custom'),
		);
		
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-width-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array( 
					array('ampforwp-advertisement-type-incontent-ad-4','=','5'),
					  
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-height-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '320',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-4','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Publisher-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Data Publisher'),
			'placeholder'	=> 'site.com',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-4','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Widget-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Data Widget'),
			'placeholder'	=> '3XXXXX',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-4','=','5'),
 				),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-mgid-ad-Data-Container-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Data Container'),
			'placeholder'	=> 'MXXScriptRootCXXXXXX',
			'required'		=> array(
					array('ampforwp-advertisement-type-incontent-ad-4','=','5'),
 				),
		);
		
	// '3'  	=> 'Custom Advertisement',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       => 'ampforwp-custom-ads-ad-position-incontent-ad-4',
			'type'     => 'select',
			'title'    => __( 'Custom Ads incontent Ads Position', 'redux-framework-demo' ),
			'tooltip-subtitle' => __( 'Select after how many paragraphs your ads should show.', 'redux-framework-demo' ),
			'options'  => array(
					'20-percent' 	=> 'Show Ad after 20% of content',
					'40-percent' 	=> 'Show Ad after 40% of content',
					'50-percent' 	=> 'Show Ad after 50% of content',
					'60-percent' 	=> 'Show Ad after 60% of content',
					'80-percent' 	=> 'Show Ad after 80% of content',
					'1' 					=> '1 Paragraphs',
					'2'  					=> '2 Paragraphs',
					'3'  					=> '3 Paragraphs',
					'4'  					=> '4 Paragraphs',
					'5'  					=> '5 Paragraphs',
					'6'  					=> '6 Paragraphs',
					'7'  					=> '7 Paragraphs',
					'8'  					=> '8 Paragraphs',
					'9'  					=> '9 Paragraphs',
					'10' 					=> '10 Paragraphs',
					'custom' 				=> 'Custom',
				),
			'default'  => '50-percent',
			'required'	=> array('ampforwp-advertisement-type-incontent-ad-4','=','3'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-custom-custom-ads-ad-position-incontent-ad-4',
			'type'     		=> 'text',
			'title'    		=> __('Show ad after Paragraphs', 'redux-framework-demo'),
			'default'  		=> '5',
			'placeholder'	=> '5',
			'required'		=> array('ampforwp-custom-ads-ad-position-incontent-ad-4','=','custom'),
		);
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-incontent-ad-4',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-advertisement-type-incontent-ad-4','=','3'),
		);

		if(is_plugin_active('accelerated-mobile-pages/accelerated-moblie-pages.php')){
		$controls[] = array(
			'class' => 'child_opt',
		    'id'       => 'fx-checkbox-4',
		    'type'     => 'checkbox',
		    'title'    => __('Parallax Effect', 'redux-framework-demo'), 
		    'tooltip-subtitle' => __('add parallax effect (amp fx flying carpet) to the ad', 'redux-framework-demo'),
			'required'	=> array('ampforwp-incontent-ad-4','=','1'),
		    'default'  => 0// 1 = on | 0 = off
		);
	}
	

	// Incontent Ad #4 ENDS

    $controls[] =  array(
                   'id' => 'General',
                   'type' => 'section',
                   'title' => __('General Options', 'accelerated-mobile-pages'),
                   'indent' => true,
                   'layout_type' => 'accordion',
                   'accordion-open'=> 1,
        );
	
	// Ad After Featured Image 
		if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){
		$controls[] = array(
			'id'        	=> 'ampforwp-after-featured-image-ad',
			'type'      	=> 'switch',
			'title'     	=> __('Ad after Featured Image'),
			'default'		=> 0,
		);
	

		$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-after-featured-image-ad-type',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement'
		),
		'default'  => '1',
		'required'	=> array('ampforwp-after-featured-image-ad','=','1'),
	);
			// Responsive ad After Featured Image
		$controls[] = array(
			'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-after-featured-img',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','1'),
			'default'	=> 0, 
	);

		// Adsense
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-after-featured-image-ad-type-1-width',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
				array('ampforwp-after-featured-image-ad-type','=','1'),
				array('adsense-rspv-ad-after-featured-img','=','0'),
			),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-after-featured-image-ad-type-1-height',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'required'		=> array(
				array('ampforwp-after-featured-image-ad-type','=','1'),
				array('adsense-rspv-ad-after-featured-img','=','0'),
			),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-after-featured-image-ad-type-1-data-ad-client',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'     => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','1'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-after-featured-image-ad-type-1-data-ad-slot',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','1'),
		);

		// Double Click
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-after-featured-image-ad-type-2-width',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array('ampforwp-after-featured-image-ad-type','=','2'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-after-featured-image-ad-type-2-height',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'required'		=> array('ampforwp-after-featured-image-ad-type','=','2'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-after-featured-image-ad-type-2-ad-data-slot',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','2'),
		);
		$controls[] = array(
		'class' => 'child_opt',
		'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-ad-after-featured-img',
		'type'		=> 'switch', 
		'title'		=> __('Data Multi Size','redux-framework-demo'),
		'required'	=> array('ampforwp-after-featured-image-ad-type','=','2'),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-after-featured-image-ad-type-2-ad-data-multi-size',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-ad-after-featured-img','=','1'),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-ad-after-featured-img',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-after-featured-image-ad-type-2-ad-data-enable-refresh',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-ad-after-featured-img','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		//Amazon
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-after-featured-image-ad-type-4-width',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array('ampforwp-after-featured-image-ad-type','=','4'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-after-featured-image-ad-type-4-height',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'required'		=> array('ampforwp-after-featured-image-ad-type','=','4'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-after-featured-image-ad-type-4-data-ad-client',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','4'),
		);

//mgid
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-after-featured-image-ad-type-5-width',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'=> '300',
			'required'		=> array('ampforwp-after-featured-image-ad-type','=','5'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-after-featured-image-ad-type-5-height',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'=> '320',
			'required'		=> array('ampforwp-after-featured-image-ad-type','=','5'),
		);
		
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-after-featured-image-ad-type-5-Data-publisher',
			'type'      => 'text',
			'title'     => __('Data Publisher'),
			'placeholder'=> 'site.com',
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','5'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-after-featured-image-ad-type-5-Data-widget',
			'type'      => 'text',
			'title'     => __('Data Widget'),
			'placeholder'=> '3XXXXX',
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','5'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-after-featured-image-ad-type-5-Data-Container',
			'type'      => 'text',
			'title'     => __('Data Container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
			'required'	=> array('ampforwp-after-featured-image-ad-type','=','5'),
		);

		// Custom type

		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-after-featured-image-ad-custom-advertisement',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-after-featured-image-ad-type','=','3'),
		);
		
		//** End Of Ad After Featured Image **//
	}
	

  // Standard Ads - starts
		if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){
	$controls[] =  array(
        'id'        =>'ampforwp-standard-ads-1',
        'type'      => 'switch',
        'title'     => __('Below the Header (SiteWide)', 'redux-framework-demo'),
        'default'   => 0,
	);


  //standard ad 1 - starts here
	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-standard-1',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement'
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-1','=','1'),
	);

		// Responsive ad unit incontent-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-type-standard-1',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-standard-1','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-standard-1',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','1'),
                          array('adsense-rspv-ad-type-standard-1','=','0'),
		));
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-standard-1',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','1'),
                          array('adsense-rspv-ad-type-standard-1','=','0'),
                      )
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-1',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','1')),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-1',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-standard-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-standard-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-1',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','2')),
		);

$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-standard-ad-1',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-standard-1',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-standard-ad-1','=','1'),
		);

		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-1',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-standard-ads-1','=','1'),
			'required'	=> array('ampforwp-advertisement-type-standard-1','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-standard-1',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-1','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
	// Amazon

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-width-standard-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-height-standard-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-amazon-data-ad-client-standard-1',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','4')),
		);

 //mgid
		 $controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-width-standard-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-height-standard-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-ad-data-publisher-standard-1',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-ad-data-widget-standard-1',
			'type'      => 'text',
			'title'     => __('Data Widget'),
			'placeholder'=> '3XXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-ad-data-container-standard-1',
			'type'      => 'text',
			'title'     => __('Data Container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-1','=','1'),
                          array('ampforwp-advertisement-type-standard-1','=','5')),
		);

	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-standard-1',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-1','=','1'),
                    array('ampforwp-advertisement-type-standard-1','=','3')),
		);
	}
    //standard ad -1 ends here
    $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-2',
          'type'      => 'switch',
          'title'     => __('Below the Footer (SiteWide)', 'redux-framework-demo'),
          'default'   => 0,
  	);


  //standard ad 2- starts here
	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-standard-2',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement'
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-2','=','1'),
	);
		// Responsive ad type-standard-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-type-standard-2',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-standard-2','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-standard-2',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'   => '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','1'),
                          array('adsense-rspv-ad-type-standard-2','=','0'),
		));
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-standard-2',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'   => '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','1'),
      					  array('adsense-rspv-ad-type-standard-2','=','0'),
      					),

		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-2',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'     => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','1')),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-2',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'   => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-standard-2',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-standard-2',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-2',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','2')),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-standard-ad-2',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-standard-2',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-standard-ad-2','=','1'),
		);

		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-2',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-standard-ads-2','=','1'),
			'required'	=> array('ampforwp-advertisement-type-standard-2','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-standard-2',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-2','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		// Amazon

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-width-standard-2',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-height-standard-2',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-amazon-data-ad-client-standard-2',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','4')),
		);
		//MGID
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-width-standard-2',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-height-standard-2',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-ad-Data-publishe-standard-2',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','5')),
		);$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-ad-Data-widget-standard-2',
			'type'      => 'text',
			'title'     => __('Data widget'),
			'placeholder'=> '3XXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','5')),
		);$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-ad-Data-Container-standard-2',
			'type'      => 'text',
			'title'     => __('Data Container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-2','=','1'),
                          array('ampforwp-advertisement-type-standard-2','=','5')),
		);

	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-standard-2',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-2','=','1'),
                    array('ampforwp-advertisement-type-standard-2','=','3')),
		);
    //standard ad -2 ends here



if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){

  $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-2-1',
          'type'      => 'switch',
          'title'     => __('Above the Footer (SiteWide)', 'redux-framework-demo'),
          'default'   => 0,
  	);


  //standard ad 2-1- starts here
	$controls[] =  array(
        'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-standard-2-1',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement'
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-2-1','=','1'),
	);

		// Responsive ad unit incontent-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-type-standard-2-1',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-standard-2-1','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-standard-2-1',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'placeholder'	=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','1'),
                          array('adsense-rspv-ad-type-standard-2-1','=','0'),
		));
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-standard-2-1',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'placeholder'	=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','1'),
                          array('adsense-rspv-ad-type-standard-2-1','=','0'),
                      		)
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'     => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','1')),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-standard-2-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'placeholder'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-standard-2-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'placeholder'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','2')),
		);

		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-standardd-2-1',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-standardd-2-1','=','1'),
		);

		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-2-1',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-standard-ads-2-1','=','1'),
			'required'	=> array('ampforwp-advertisement-type-standard-2-1','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-2-1','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		// Amazon

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-width-standard-2-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-height-standard-2-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-amazon-data-ad-client-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','4')),
		);
		//MGID
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-width-standard-2-1',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-height-standard-2-1',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-data-publisher-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-widget-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data widget'),
			'placeholder'=> '3XXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-container-standard-2-1',
			'type'      => 'text',
			'title'     => __('Data Container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-2-1','=','1'),
                          array('ampforwp-advertisement-type-standard-2-1','=','5')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-standard-2-1',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-2-1','=','1'),
                    array('ampforwp-advertisement-type-standard-2-1','=','3')),
		);
    //standard ad -2-1 ends here

}
		if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){
    $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-3',
          'type'      => 'switch',
          'title'     => __('Above the Post Content (Single Post)', 'redux-framework-demo'),
          'default'   => 0,
  	);


  //standard ad 3 - starts here
	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-standard-3',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-3','=','1'),
	);

		// Responsive ad unit incontent-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-type-standard-3',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-standard-3','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',3
  $controls[] =  array(
  	        'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-standard-3',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','1'),
                          array('adsense-rspv-ad-type-standard-3','=','0'),
		));
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-standard-3',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','1'),
                      		 array('adsense-rspv-ad-type-standard-3','=','0'),
                      		)
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-3',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'     => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','1')),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-3',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'    => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-standard-3',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-standard-3',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-3',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','2')),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-standardd-3',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-standard-3',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-standardd-3','=','1'),
		);

		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-3',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-standard-ads-3','=','1'),
			'required'	=> array('ampforwp-advertisement-type-standard-3','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-standard-3',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-3','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		// Amazon

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-width-standard-3',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-height-standard-3',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-amazon-data-ad-client-standard-3',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','4')),
		);
		//mgid

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-width-standard-3',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-height-standard-3',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-publisher-standard-3',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-widget-standard-3',
			'type'      => 'text',
			'title'     => __('Data widget'),
			'placeholder'=> '3XXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-container-standard-3',
			'type'      => 'text',
			'title'     => __('Data Container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-3','=','1'),
                          array('ampforwp-advertisement-type-standard-3','=','5')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-standard-3',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-3','=','1'),
                    array('ampforwp-advertisement-type-standard-3','=','3')),
		);
	}
    //standard ad -3 ends here

		//standard ad - 4 starts here
		if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){
    $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-4',
          'type'      => 'switch',
          'title'     => __('Below the Post Content (Single Post)', 'redux-framework-demo'),
          'default'   => 0,
  	);

	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-standard-4',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement'
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-4','=','1'),
	);

		// Responsive ad unit incontent-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-type-standard-4',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-standard-4','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-standard-4',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','1'),
                          array('adsense-rspv-ad-type-standard-4','=','0'),
		));
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-standard-4',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','1'),
                      	array('adsense-rspv-ad-type-standard-4','=','0'),
                  )
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-4',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'     => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','1')),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-4',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-standard-4',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-standard-4',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-4',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','2')),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-std-4',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-standard-4',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-std-4','=','1'),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-4',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-standard-ads-4','=','1'),
			'required'	=> array('ampforwp-advertisement-type-standard-4','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-standard-4',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-4','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		// Amazon

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-width-standard-4',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-height-standard-4',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-amazon-data-ad-client-standard-4',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','4')),
		);
		//MGID

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-width-standard-4',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-height-standard-4',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-publisher-standard-4',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-widget-standard-4',
			'type'      => 'text',
			'title'     => __('Data widget'),
			'placeholder'=> '3XXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-container-standard-4',
			'type'      => 'text',
			'title'     => __('Data Container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-4','=','1'),
                          array('ampforwp-advertisement-type-standard-4','=','5')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-standard-4',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-4','=','1'),
                    array('ampforwp-advertisement-type-standard-4','=','3')),
		);
	}
    //standard ad -4 ends here

	//standard ad - 5 starts here
		if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){
    $controls[] =  array(
          'id'        =>'ampforwp-standard-ads-5',
          'type'      => 'switch',
          'title'     => __('Below the Title (Single Post)', 'redux-framework-demo'),
          'default'   => 0,
  	);

	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-standard-5',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-5','=','1'),
	);

		// Responsive ad unit incontent-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-type-standard-5',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-standard-5','=','1'),
			'default'	=> 0, 
	);

	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-standard-5',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','1'),
                          array('adsense-rspv-ad-type-standard-5','=','0'),
		));
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-standard-5',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','1'),
                          array('adsense-rspv-ad-type-standard-5','=','0'),
                      )
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-5',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'    => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','1')),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-5',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'     => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-standard-5',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-standard-5',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-5',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','2')),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-std-5',
			'type'		=> 'switch', 
			'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-standard-5',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-std-5','=','1'),
		);
				$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-5',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-standard-ads-5','=','1'),
			'required'	=> array('ampforwp-advertisement-type-standard-5','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-standard-5',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-5','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		// '4'  	=> 'Amazon',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-width-standard-5',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-height-standard-5',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-amazon-ad-data-slot-standard-5',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','4')),
		);
		// '5'  	=> 'mgid',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-width-standard-5',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-height-standard-5',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-ad-data-publisher-standard-5',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-ad-data-widget-standard-5',
			'type'      => 'text',
			'title'     => __('Data widget'),
			'placeholder'=> '3XXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-ad-data-container-standard-5',
			'type'      => 'text',
			'title'     => __('Data Container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-5','=','1'),
                          array('ampforwp-advertisement-type-standard-5','=','5')),
		);
		
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-standard-5',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-5','=','1'),
                    array('ampforwp-advertisement-type-standard-5','=','3')),
		);
	}
    //standard ad -5 ends here

		//standard ad -6 starts here
		if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){
		$controls[] =  array(
					'id'        =>'ampforwp-standard-ads-6',
					'type'      => 'switch',
					'title'     => __('Above Related Posts (Single Post)', 'redux-framework-demo'),
					'default'   => 0,
		);
	
	$controls[] =  array(
		'class' => 'child_opt child_opt_arrow',
		'id'       => 'ampforwp-advertisement-type-standard-6',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement'
		),
		'default'  => '1',
		'required'	=> array('ampforwp-standard-ads-6','=','1'),
	);

		// Responsive ad unit incontent-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-type-standard-6',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-standard-6','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-standard-6',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','1'),
													array('adsense-rspv-ad-type-standard-6','=','0'),
		));
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-standard-6',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','1'),
													array('adsense-rspv-ad-type-standard-6','=','0'),
												)
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-6',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
		  'tooltip-subtitle'     => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','1')),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-6',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'    => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-standard-6',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-standard-6',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
			'required'		=> array(
													array('ampforwp-standard-ads-6','=','1'),
													array('ampforwp-advertisement-type-standard-6','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-6',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array(											array('ampforwp-standard-ads-6','=','1'),
						array('ampforwp-advertisement-type-standard-6','=','2')),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-std-6',
		'type'		=> 'switch', 
		'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'	=> array(
						array('ampforwp-standard-ads-6','=','1'),
						array('ampforwp-advertisement-type-standard-6','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-standard-6',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-std-6','=','1'),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-6',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-standard-ads-6','=','1'),
			'required'	=> array('ampforwp-advertisement-type-standard-6','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-standard-6',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-6','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
	// Amazon
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-width-standard-6',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      		'required'		=> array(
                          array('ampforwp-standard-ads-6','=','1'),
                          array('ampforwp-advertisement-type-standard-6','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-amazon-ad-height-standard-6',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-6','=','1'),
                          array('ampforwp-advertisement-type-standard-6','=','4')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-amazon-data-ad-client-standard-6',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
      'required'		=> array(
                          array('ampforwp-standard-ads-6','=','1'),
                          array('ampforwp-advertisement-type-standard-6','=','4')),
		);
		// mgid
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-width-standard-6',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '280',
      		'required'		=> array(
                          array('ampforwp-standard-ads-6','=','1'),
                          array('ampforwp-advertisement-type-standard-6','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-height-standard-6',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-6','=','1'),
                          array('ampforwp-advertisement-type-standard-6','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-publisher-standard-6',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
      'required'		=> array(
                          array('ampforwp-standard-ads-6','=','1'),
                          array('ampforwp-advertisement-type-standard-6','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-widget-standard-6',
			'type'      => 'text',
			'title'     => __('Data widget'),
			'placeholder'=> '3XXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-6','=','1'),
                          array('ampforwp-advertisement-type-standard-6','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-data-container-standard-6',
			'type'      => 'text',
			'title'     => __('Data container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
      'required'		=> array(
                          array('ampforwp-standard-ads-6','=','1'),
                          array('ampforwp-advertisement-type-standard-6','=','5')),
		);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-standard-6',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
										array('ampforwp-standard-ads-6','=','1'),
										array('ampforwp-advertisement-type-standard-6','=','3')),
		);
	}
		//standard ad -6 ends here


    //standard ad -7 starts here

    if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){
	$controls[] =  array(
        'id'        =>'ampforwp-standard-ads-7',
        'type'      => 'switch',
        'title'     => __('Below the Author Box (Single Post)', 'redux-framework-demo'),
        'tooltip-subtitle' => __( 'Works from 0.9.97.10 and Above', 'ampforwp' ),
        'required' => array('amp-design-selector', '=' , '4')
                    ,
        'default'   => 0,
	);


  //standard ad -7 starts here
	$controls[] =  array(
		'class' 	=> 'child_opt child_opt_arrow',
		'id'      	=> 'ampforwp-advertisement-type-standard-7',
		'type'    	=> 'select',
		'title'    	=> __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement',
		),
		'default'   => '1',
		'required'	=> array('ampforwp-standard-ads-7','=','1'),
	);

		// Responsive ad unit incontent-7
	$controls[] = array(
		   'class' 		=> 'child_opt',
			'id' 		=> 'adsense-rspv-ad-type-standard-7',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-advertisement-type-standard-7','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' 		=> 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-width-standard-7',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','1'),
                          array('adsense-rspv-ad-type-standard-7','=','0'),
		));
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-adsense-ad-height-standard-7',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','1'),
                          array('adsense-rspv-ad-type-standard-7','=','0'),
                      )
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-adsense-ad-data-ad-client-standard-7',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'      => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','1')),
		);
		$controls[] =  array(
            'class' => 'child_opt',	
			'id'        => 'ampforwp-adsense-ad-data-ad-slot-standard-7',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
			'tooltip-subtitle'      => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','1')),
		);
	// '2'  	=> 'DoubleClick',
		$controls[] = 	array(
		    'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-width-standard-7',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','2')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-doubleclick-ad-height-standard-7',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','2')),
		);
		$controls[] = 	array(
	     'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-slot-standard-7',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','2')),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-std-7',
		'type'		=> 'switch', 
		'title'		=> __('Data Multi Size','redux-framework-demo'),
			'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-multi-size-standard-7',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-std-7','=','1'),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-7',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-standard-ads-7','=','1'),
			'required'	=> array('ampforwp-advertisement-type-standard-7','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-doubleclick-ad-data-enable-refresh-standard-7',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-standard-ad-7','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		//mgid
		$controls[] = 	array(
		    'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-width-standard-7',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '300',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','5')),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-mgid-ad-height-standard-7',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','5')),
		);
		$controls[] = 	array(
	     'class' => 'child_opt',
			'id'        => 'ampforwp-mgid-ad-data-publisher-standard-7',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
      'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','5')),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-mgid-ad-data-widget-std-7',
		'type'		=> 'text', 
		'title'		=> __('Data widget','redux-framework-demo'),
		'placeholder'=> '3XXXXX',
			'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','5')),
			'default'	=> 0, 
	);

		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-mgid-ad-data-container-std-7',
		'type'		=> 'text', 
		'title'		=> __('Data Container','redux-framework-demo'),
		'placeholder'=> 'MXXScriptRootCXXXXXX',
			'required'		=> array(
                          array('ampforwp-standard-ads-7','=','1'),
                          array('ampforwp-advertisement-type-standard-7','=','5')),
			'default'	=> 0, 
	);
	// '3'  	=> 'Custom Advertisement'
		$controls[] = array(
		'class' => 'child_opt',
			'id'        	=> 'ampforwp-custom-advertisement-standard-7',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
'required'		=> array(
                    array('ampforwp-standard-ads-7','=','1'),
                    array('ampforwp-advertisement-type-standard-7','=','3')),
		);
	}

 // standard ad -7 ends here
	//Standard Ads ENDS


// Ads in Between Loop
     if(is_plugin_active( 'accelerated-mobile-pages/accelerated-moblie-pages.php')){  
    $controls[] =  array(

        'id'        =>'ampforwp-inbetween-loop',
        'type'      => 'switch',
        'title'     => __('Ads Inbetween Loop', 'redux-framework-demo'),
        'tooltip-subtitle'     => __('Works only with 0.9.68 or higher version of ampforwp core', 'redux-framework-demo'),
        'default'   => 0,
        'true'      => 'Enabled',
        'false'     => 'Disabled',
	);

	$controls[] =  array(
		   'class' => 'child_opt child_opt_arrow',
			'id'       		=> 'ampforwp-inbetween-loop-post-num',
			'type'     		=> 'text',
			'title'    		=> __('After How many posts?', 'redux-framework-demo'),
			'default'  		=> '4',
			'required'		=> array(
						array('ampforwp-inbetween-loop','=','1'),
						 
					),
		);

	$controls[] =  array(
		'class' => 'child_opt',
		'id'       => 'ampforwp-inbetween-type',
		'type'     => 'select',
		'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
		'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
		'options'  	=> array(
			'1' 	=> 'Adsense',
			'2'  	=> 'DoubleClick',
			'4'  	=> 'Amazon',
			'5'  	=> 'MGID',
			'3'  	=> 'Custom Advertisement',
		),
		'default'  => '1',
		'required'	=> array('ampforwp-inbetween-loop','=','1'),
	);

		// Responsive ad unit incontent-2
	$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'adsense-rspv-ad-inbetween-type',
			'type'		=> 'switch', 
			'title'		=> __('Responsive Ad unit','redux-framework-demo'),
			'required'	=> array('ampforwp-inbetween-type','=','1'),
			'default'	=> 0, 
	);
	// '1' 	=> 'Adsense',
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-inbetween-adsense-ad-width',
			'type'     		=> 'text',
			'title'    		=> __('Width', 'redux-framework-demo'),
			'default'  		=> '300',
			'required'		=> array(
						array('ampforwp-inbetween-type','=','1'),
						array('adsense-rspv-ad-inbetween-type','=','0'),
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'       		=> 'ampforwp-inbetween-adsense-ad-height',
			'type'     		=> 'text',
			'title'    		=> __('Height'),
			'default'  		=> '300',
			'required'		=> array(
						array('ampforwp-inbetween-type','=','1'),
						array('adsense-rspv-ad-inbetween-type','=','0'),
					),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-inbetween-adsense-ad-data-ad-client',
			'type'      => 'text',
			'title'     => __('Data AD Client'),
			'tooltip-subtitle'    => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
			'default'   => '',
			'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
			'required'	=> array('ampforwp-inbetween-type','=','1'),
		);
		$controls[] =  array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-inbetween-adsense-ad-data-ad-slot',
			'type'      => 'text',
			'title'     => __('Data AD Slot'),
	  'tooltip-subtitle'     => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
			'default'  => '',
			'placeholder'=> '70XXXXXX12',
			'required'	=> array('ampforwp-inbetween-type','=','1'),
		);
	// '4'  	=> 'Amazon',
		
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-inbetween-amazon-ad-width',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'required'	=> array('ampforwp-inbetween-type','=','4'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-inbetween-amazon-ad-height',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
			'required'	=> array('ampforwp-inbetween-type','=','4'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-inbetween-amazon-ad-data-slot',
			'type'      => 'text',
			'title'     => __('Data AD Instance ID'),
			'placeholder'=> 'feXXXXXX-fXXX-4f8d-8dfb-45ec747632e5',
			'required'	=> array('ampforwp-inbetween-type','=','4'),
		);

		//MGID
		
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-inbetween-mgid-ad-width',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'placeholder'=> '300',
			'required'	=> array('ampforwp-inbetween-type','=','5'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-inbetween-mgid-ad-height',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '300',
			'placeholder'=> '320',
			'required'	=> array('ampforwp-inbetween-type','=','5'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-inbetween-mgid-ad-data-publisher',
			'type'      => 'text',
			'title'     => __('Data publisher'),
			'placeholder'=> 'site.com',
			'required'	=> array('ampforwp-inbetween-type','=','5'),
		);	
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-inbetween-mgid-ad-data-widget',
			'type'      => 'text',
			'title'     => __('Data widget'),
			'placeholder'=> '3XXXXX',
			'required'	=> array('ampforwp-inbetween-type','=','5'),
		);		
		
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-inbetween-mgid-ad-data-container',
			'type'      => 'text',
			'title'     => __('Data container'),
			'placeholder'=> 'MXXScriptRootCXXXXXX',
			'required'	=> array('ampforwp-inbetween-type','=','5'),
		);		
			
		
		// '2'  	=> 'Doubleclick',
		
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-inbetween-doubleclick-ad-width',
			'type'      => 'text',
			'title'     => __('Width'),
			'default'=> '300',
			'required'	=> array('ampforwp-inbetween-type','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        =>'ampforwp-inbetween-doubleclick-ad-height',
			'type'      => 'text',
			'title'     => __('Height'),
			'default'=> '250',
			'required'	=> array('ampforwp-inbetween-type','=','2'),
		);
		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-inbetween-doubleclick-ad-data-slot',
			'type'      => 'text',
			'title'     => __('Data Slot'),
			'placeholder'=> '/41****9/mobile_ad_banner',
			'required'	=> array('ampforwp-inbetween-type','=','2'),
		);
		$controls[] = array(
		'class' => 'child_opt',
		'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-inbtw-loop',
		'type'		=> 'switch', 
		'title'		=> __('Data Multi Size','redux-framework-demo'),
		'required'	=> array('ampforwp-inbetween-type','=','2'),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-inbetween-adsense-ad-data-multi-size',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-inbtw-loop','=','1'),
		);
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-inbtw-loop',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array('ampforwp-inbetween-type','=','2'),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'ampforwp-inbetween-doubleclick-ad-data-enable-refresh',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-inbtw-loop','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
	// '3'  	=> 'Custom Advertisement',

		$controls[] = array(
			'class' => 'child_opt',
			'id'        	=> 'ampforwp-inbetween-custom-advertisement',
			'type'      	=> 'textarea',
			'title'     	=> __('Advertisement Code'),
			'placeholder'	=> ' <amp-ad width=300 height=250 type="XXXX" data-xxx="XXXX""></amp-ad>
OR
<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 alt="advert banner"></amp-img> </a>',
			'required'		=> array('ampforwp-inbetween-type','=','3'),
		);
	}
// End of In Between loop Ads



 $controls[] =  array(
                   'id' => 'Sticky',
                   'type' => 'section',
                   'title' => __('Sticky', 'accelerated-mobile-pages'),
                   'indent' => true,
                   'layout_type' => 'accordion',
                   'accordion-open'=> 1,
        );

          // #Sticky AD
        $controls[] = 	array(

									'id'        =>'ampforwp-sticky-ad',
									'type'      => 'switch',
									'title'     => __('Sticky Ads', 'redux-framework-demo'),
									'tooltip-subtitle' 	=> __('Shows Ad as sticky ad at the bottom of the screen.','redux-framework-demo'),
									'default'   => 0,
									'true'      => 'Enabled',
									'false'     => 'Disabled',
								);
                $controls[] =  array(
                	'class' => 'child_opt child_opt_arrow',
                  'id'       => 'ampforwp-advertisement-sticky-type',
                  'type'     => 'select',
                  'title'    => __( 'Advertisement Type', 'redux-framework-demo' ),
                  'tooltip-subtitle' => __( 'Select Advertisement Type you want to show.', 'ampforwp' ),
                  'options'  	=> array(
                    '1' 	=> 'Adsense',
                    '2'  	=> 'DoubleClick',
                    '4'  	=> 'Amazon',
                    '5'  	=> 'MGID',
                    '3'  	=> 'Custom Advertisement',
                  ),
                  'default'  => '1',
                  'required'	=> array('ampforwp-sticky-ad','=','1'),
                );
              $controls[] = 	array(
                                	'class' => 'child_opt',
									'id'        =>'ampforwp-sticky-ad-width',
									'type'      => 'text',
									'title'     => __('Width', 'redux-framework-demo'),
									'default'   => '300',
									'required'	=> array(
                             array('ampforwp-sticky-ad','=','1'),
									           array('ampforwp-advertisement-sticky-type','=','1'))
								);
              $controls[] = 	array(
                                 	'class' => 'child_opt',
									'id'        =>'ampforwp-sticky-ad-height',
									'type'      => 'text',
									'title'     => __('Height', 'redux-framework-demo'),
									'default'   => '50',
                  'required'	=> array(
                             array('ampforwp-sticky-ad','=','1'),
									           array('ampforwp-advertisement-sticky-type','=','1'))
								);
              $controls[] = array(
              	                    'class' => 'child_opt',
									'id'        =>'ampforwp-sticky-ad-data-ad-client',
									'type'      => 'text',
									'title'     => __('Data AD Client', 'redux-framework-demo'),
									'tooltip-subtitle'    => __('Enter the Data Ad Client (data-ad-client) from the adsense ad code. e.g. ca-pub-2005XXXXXXXXX342', 'redux-framework-demo'),
									'default'   => '',
									'placeholder'=> 'ca-pub-2005XXXXXXXXX342',
                  'required'	=> array(
                             array('ampforwp-sticky-ad','=','1'),
									           array('ampforwp-advertisement-sticky-type','=','1'))
								);
              $controls[] = array(
              	                   'class' => 'child_opt',
									'id'        => 'ampforwp-sticky-ad-data-ad-slot',
									'type'      => 'text',
									'title'     => __('Data AD Slot', 'redux-framework-demo'),
									'tooltip-subtitle'     => __('Enter the Data Ad Slot (data-ad-slot) from the adsense ad code. e.g. 70XXXXXX12', 'redux-framework-demo'),
									'default'  => '',
									'placeholder'=> '70XXXXXX12',
                  'required'	=> array(
                             array('ampforwp-sticky-ad','=','1'),
									           array('ampforwp-advertisement-sticky-type','=','1'))
								);
                // '2'  	=> 'DoubleClick',
              		$controls[] = 	array(
              			'class' => 'child_opt',
              			'id'        =>'sticky-ampforwp-doubleclick-ad-width',
              			'type'      => 'text',
              			'title'     => __('Width'),
              			'default'=> '300',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','2'))
              		);
              		$controls[] = 	array(
              			'class' => 'child_opt',
              			'id'        =>'sticky-ampforwp-doubleclick-ad-height',
              			'type'      => 'text',
              			'title'     => __('Height'),
              			'default'=> '250',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','2'))
              		);
              		$controls[] = 	array(
              			'class' => 'child_opt',
              			'id'        => 'sticky-ampforwp-doubleclick-ad-data-slot',
              			'type'      => 'text',
              			'title'     => __('Data Slot'),
              			'placeholder'=> '/41****9/mobile_ad_banner',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','2'))
              		);
              		$controls[] = array(
		'class' => 'child_opt',
		'id' 		=> 'ampforwp-doubleclick-ad-data-multi-size-sticky-ad',
		'type'		=> 'switch', 
		'title'		=> __('Data Multi Size','redux-framework-demo'),
		'required'	=> array(
        			   array('ampforwp-sticky-ad','=','1'),
  					   array('ampforwp-advertisement-sticky-type','=','2')),
			'default'	=> 0, 
	);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'sticky-ampforwp-doubleclick-ad-data-multi-size',
			'type'      => 'text',
			'title'     => __('Data Multi Size'),
			'placeholder'=> '700x90,700x60,500x60',
			 'required'	=> array('ampforwp-doubleclick-ad-data-multi-size-sticky-ad','=','1'),
		);
		
		$controls[] = array(
		    'class' => 'child_opt',
			'id' 		=> 'ampforwp-doubleclick-ad-data-enable-refresh-sticky-ad',
			'type'		=> 'switch', 
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'required'	=> array(
        			   array('ampforwp-sticky-ad','=','1'),
  					   array('ampforwp-advertisement-sticky-type','=','2')),
			'default'	=> 0, 
		);

		$controls[] = 	array(
			'class' => 'child_opt',
			'id'        => 'sticky-ampforwp-doubleclick-ad-data-enable-refresh',
			'type'      => 'text',
			'title'     => __('Data Enable Refresh'),
			'placeholder'=> '30',
			'title'		=> __('Data Enable Refresh','redux-framework-demo'),
			'default' =>'30',
			'required'	=> array('ampforwp-doubleclick-ad-data-enable-refresh-sticky-ad','=','1'),
			'tooltip-subtitle'     => esc_html__("If this field is empty then Data Enable Refresh will not load please add value.", 'redux-framework-demo'),
		);
		// '5'  	=> 'mgid',
              		$controls[] = 	array(
              			'class' => 'child_opt',
              			'id'        =>'sticky-ampforwp-mgid-ad-width',
              			'type'      => 'text',
              			'title'     => __('Width'),
              			'default'=> '300',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','5'))
              		);
              		$controls[] = 	array(
              			'class' => 'child_opt',
              			'id'        =>'sticky-ampforwp-mgid-ad-height',
              			'type'      => 'text',
              			'title'     => __('Height'),
              			'default'=> '300',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','5'))
              		);
              		$controls[] = 	array(
              			'class' => 'child_opt',
              			'id'        => 'sticky-ampforwp-mgid-ad-Data-Publisher',
              			'type'      => 'text',
              			'title'     => __('Data Publisher'),
              			'placeholder'=> 'site.com',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','5'))
              		);
              		$controls[] = 	array(
              			'class' => 'child_opt',
              			'id'        => 'sticky-ampforwp-mgid-ad-Data-Widget',
              			'type'      => 'text',
              			'title'     => __('Data Widget'),
              			'placeholder'=> '3XXXXX',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','5'))
              		);
              		$controls[] = 	array(
              			'class' => 'child_opt',
              			'id'        => 'sticky-ampforwp-mgid-ad-Data-Container',
              			'type'      => 'text',
              			'title'     => __('Data Container'),
              			'placeholder'=> 'MXXScriptRootCXXXXXX',
                    'required'	=> array(
                               array('ampforwp-sticky-ad','=','1'),
  									           array('ampforwp-advertisement-sticky-type','=','5'))
              		);  		

              	// '3'  	=> 'Custom Advertisement',
              		$controls[] = array(
              			'class' => 'child_opt',
              			'id'        	=> 'sticky-ampforwp-custom-advertisement',
              			'type'      	=> 'textarea',
              			'title'     	=> __('Advertisement Code'),
              			'tooltip-subtitle'   	=> __('Please have a look at the Example and then form your own Sticky-Ad code'),
              			'placeholder'	=> '<a href="https://example.com"><amp-img src="/img/amp.jpg" width=300 height=250 layout="responsive" alt="advert banner"></amp-img> </a>',
              'required'	=> array(
                         array('ampforwp-sticky-ad','=','1'),
                         array('ampforwp-advertisement-sticky-type','=','3'))
              		);

 $controls[] =  array(
                   'id' => 'Advance_options',
                   'type' => 'section',
                   'title' => __('Advance options', 'accelerated-mobile-pages'),
                   'indent' => true,
                   'layout_type' => 'accordion',
                   'accordion-open'=> 1,
        );

 // Incontent Ads on custom front page

		$controls[] = array(
			'id'        	=> 'ampforwp-custom-front-page-ad',
			'type'      	=> 'switch',
			'title'     	=> __('Show Incontent Ads on custom front page'),
			'default'		=> 1,
			'tooltip-subtitle' => __('Works only when any of the above Incontent AD is enabled', 'redux-framework-demo'),		);
		
	// Incontent Ads on custom front page ENDS
      

	return $controls;
}

if ( ! function_exists( 'ampforwp_ads_section' ) ) {
	function ampforwp_ads_section($sections){

	    $sections[] = array(
	        'title' => __('Advanced AMP ADS', 'redux-framework-demo'),
	        'icon' => 'el el-th-large',
					'desc'  => ' ',
			'id'	=> 'ampforwp-incontent-ads-subsection',
	        'fields' =>  ampforwp_create_controls(),
	        );
	    return $sections;
	}
}

 add_filter("redux/options/redux_builder_amp/sections", 'ampforwp_ads_section');
if(is_plugin_active('accelerated-mobile-pages/accelerated-moblie-pages.php') && class_exists('Redux')){ 
global $opt_name;
Redux::removeSection( $opt_name,'amp-ads');
}
?>