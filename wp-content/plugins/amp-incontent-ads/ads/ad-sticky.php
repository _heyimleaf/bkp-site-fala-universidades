<?php
// Sticky Ad code.
add_action('amp_post_template_head','ampforwp_sticky_ads_script', 12);
if ( ! function_exists('ampforwp_sticky_ads_script' ) ) {
	function ampforwp_sticky_ads_script() {
		$skip_ad = "";
		$skip_ad = apply_filters('ampforwp_skip_ad_filter', $skip_ad );

		if ( true === $skip_ad ) {
			return ;
		}
		

	}
}		
add_action('amp_post_template_footer','ampforwp_insert_sticky_ads_code', 12);

function ampforwp_insert_sticky_ads_code() {
	echo ampforwp_generate_sticky_ads_code();
}

function ampforwp_generate_sticky_ads_code() {
	$skip_ad = "";
	$skip_ad = apply_filters('ampforwp_skip_ad_filter', $skip_ad );
	$dboc ='';
	if(function_exists('ampforwp_get_data_consent') && ampforwp_get_data_consent()){
		$dboc = ampforwp_get_data_consent();
	}
	if($dboc == 1){
		$dboc = 'data-block-on-consent';
	}
	if ( true === $skip_ad ) {
		return ;
	}
		
	global $redux_builder_amp;
	if ( $redux_builder_amp['ampforwp-advertisement-sticky-type'] == '1' && $redux_builder_amp['ampforwp-sticky-ad'] == '1' ) {
			if(isset($redux_builder_amp['ampforwp-data-strategy-loading']) && $redux_builder_amp['ampforwp-data-strategy-loading']==1){
				$optimize = 'data-loading-strategy="prefer-viewability-over-views"';
			}
			else{
				$optimize = '';
			}
	

		$advert_width  	= $redux_builder_amp['ampforwp-sticky-ad-width'];
		$advert_height 	= $redux_builder_amp['ampforwp-sticky-ad-height'];
		$advert_client	= $redux_builder_amp['ampforwp-sticky-ad-data-ad-client'];
		$advert_slot	= $redux_builder_amp['ampforwp-sticky-ad-data-ad-slot'];


		$output  = '<amp-sticky-ad layout="nodisplay">';
			$output	.=	'<amp-ad '.$dboc.' class="amp-sticky-ads"
						type="adsense"'.$optimize.'
						width='. $advert_width .'
						height='. $advert_height . '
						data-ad-client="'. $advert_client .'"
						data-ad-slot="'.  $advert_slot .'">';
			$output	.=	'</amp-ad>';
		$output	.= '</amp-sticky-ad>';
		return $output;

	} elseif ( $redux_builder_amp['ampforwp-advertisement-sticky-type'] == '2'  && $redux_builder_amp['ampforwp-sticky-ad'] == '1' ) {

		$advert_width  	= $redux_builder_amp['sticky-ampforwp-doubleclick-ad-width'];
		$advert_height 	= $redux_builder_amp['sticky-ampforwp-doubleclick-ad-height'];
		$advert_slot	= $redux_builder_amp['sticky-ampforwp-doubleclick-ad-data-slot'];
		$advert_multi_size	= $redux_builder_amp['sticky-ampforwp-doubleclick-ad-data-multi-size'];
		$data_enable_refresh = $redux_builder_amp['sticky-ampforwp-doubleclick-ad-data-enable-refresh'];
		if(isset($redux_builder_amp['sticky-ampforwp-doubleclick-ad-data-multi-size']) && $redux_builder_amp['sticky-ampforwp-doubleclick-ad-data-multi-size'] == true) {
         $data_multisize = ' data-multi-size="'.  $advert_multi_size .'"';
         }else{
         $data_multisize = "";
         }

		$output  = '<amp-sticky-ad layout="nodisplay">';
			$output	.=	'<amp-ad '.$dboc.' class="amp-sticky-ads" 
			width='. $advert_width .' height='. $advert_height . '  
			type="doubleclick"'.$optimize.' data-slot="'.  $advert_slot .'" '. $data_multisize .' '.((isset($redux_builder_amp['ampforwp-doubleclick-ad-data-enable-refresh-sticky-ad']) && $redux_builder_amp['ampforwp-doubleclick-ad-data-enable-refresh-sticky-ad']== true)? ' 
                data-enable-refresh="'. $data_enable_refresh .'" ' :"") .' >';
			$output	.=	'</amp-ad>';
		$output	.= '</amp-sticky-ad>';

		return $output;

	} 
elseif ( $redux_builder_amp['ampforwp-advertisement-sticky-type'] == '4'  && $redux_builder_amp['ampforwp-sticky-ad'] == '1' ) {

		$advert_width  	= $redux_builder_amp['sticky-ampforwp-amazon-ad-width'];
		$advert_height 	= $redux_builder_amp['sticky-ampforwp-amazon-ad-height'];
		$advert_client	= $redux_builder_amp['sticky-ampforwp-amazon-ad-data-slot'];
		
		$output  = '<amp-sticky-ad layout="nodisplay">';
			$output	.=	'<amp-ad '.$dboc.' class="amp-sticky-ads" 
			width='. $advert_width .' height='. $advert_height . '  
			type="a9"'.$optimize.' data-adinstanceid="'. $advert_client .'"
				data-recomtype="sync"'.$optimize.'
				data-amzn_assoc_ad_mode="auto"'.$optimize.'
			>';
			$output	.=	'</amp-ad>';
		$output	.= '</amp-sticky-ad>';

		return $output;
}
//mgid

elseif ( $redux_builder_amp['ampforwp-advertisement-sticky-type'] == '5'  && $redux_builder_amp['ampforwp-sticky-ad'] == '1' ) {

	$advert_width  	= $redux_builder_amp['sticky-ampforwp-mgid-ad-width'];
	$advert_height 	= $redux_builder_amp['sticky-ampforwp-mgid-ad-height'];
	$DataPublisher	= $redux_builder_amp['sticky-ampforwp-mgid-ad-Data-Publisher'];
	$DataWidget	= $redux_builder_amp['sticky-ampforwp-mgid-ad-Data-Widget'];
	$DataContainer	= $redux_builder_amp['sticky-ampforwp-mgid-ad-Data-Container'];
		
		
		$output  = '<amp-sticky-ad layout="nodisplay">';
			$output	.=	'<amp-ad '.$dboc.' 
			class="amp-sticky-ads" 
			width="'. $advert_width .'" 
			height="'. $advert_height .'"  
			type="mgid"'.$optimize.' 
			data-publisher="'. $DataPublisher .'"
			data-widget="'.$DataWidget.'"
			data-container="'.$DataContainer.'"
			>';
			$output	.=	'</amp-ad>';
		$output	.= '</amp-sticky-ad>';

		return $output;
}
	elseif ( $redux_builder_amp['ampforwp-advertisement-sticky-type'] == '3' && $redux_builder_amp['ampforwp-sticky-ad'] == '1' ) {
		 $output = '<div class="ampforwp-sticky-custom-ad amp-sticky-ads">
		              
						 <div class="amp-sticky-ad-custom"  id="sticky-custom-ads">
						 <button on="tap:sticky-custom-ads.hide" class="close-btn">x</button>';
if( (isset($redux_builder_amp['ampforwp-ad-sponsorship']) && $redux_builder_amp['ampforwp-ad-sponsorship'] =='1') &&  (isset($redux_builder_amp['ampforwp-ad-sponsorship-location']) && $redux_builder_amp['ampforwp-ad-sponsorship-location'] =='1') )  {

				$output	.= ampforwp_sponsorship_ad();
			}

		 
				$output .= $redux_builder_amp['sticky-ampforwp-custom-advertisement'];	

 if( (isset($redux_builder_amp['ampforwp-ad-sponsorship']) && $redux_builder_amp['ampforwp-ad-sponsorship'] =='1') &&  (isset($redux_builder_amp['ampforwp-ad-sponsorship-location']) && $redux_builder_amp['ampforwp-ad-sponsorship-location'] =='2') ) {
			$output	.= ampforwp_sponsorship_ad();
 		}

			$output	.= '</div></div>';
		return $output;
	}
}

// added extra css to improve user experiance for sticky ads
add_action( 'amp_post_template_css', 'ampforwp_additional_css_styles' );
function ampforwp_additional_css_styles( $amp_template ) {
global $redux_builder_amp; ?>
	amp-sticky-ad {
		z-index: 9999
	}
	.ampforwp-custom-banner-ad {
		text-align : center
	}<?php if(is_plugin_active('accelerated-mobile-pages/accelerated-moblie-pages.php')){?>
	.amp-ad-wrapper {
		padding-bottom: 15px;
	}<?php } ?>
	.amp_ad_2,
	.amp_ad_3,
	.amp_ad_4 {
		margin-top: 15px;
	}
<?php if($redux_builder_amp['ampforwp-advertisement-sticky-type'] == '3' && $redux_builder_amp['ampforwp-sticky-ad'] == '1'){ ?>
	.ampforwp-sticky-custom-ad{
		position: relative;
		bottom:0;
		text-align: center;
		left: 0;
		width: 100%;
		z-index: 11;
		max-height: 100px;
		box-sizing: border-box;
		opacity: 1;
		background-image: none;
		background-color: #fff;
		box-shadow: 0 0 5px 0 rgba(0,0,0,.2);
		margin-bottom: 0;
		 }
.amp-sticky-ad-custom {bottom: 0px;position:fixed;text-align: center;left: 0;width: 100%;height: 71px;z-index: 11;max-height: 150px;box-sizing: border-box;opacity: 1;background-image: none;background-color: #fff;box-shadow: 0 0 5px 0 rgba(0,0,0,0.2);margin-bottom: 0;
}

.amp-sticky-ad-custom amp-img {width: 350px;height: 50px;left: 40%;top:10px;
}
@media (max-width:675px){
	.amp-sticky-ad-custom amp-img {left: 0;width: 100%;height: 70px;top:9px;
	}
}
.close-btn {position: absolute;width: 28px;height: 28px;top: -28px;right: 0;background: white;box-shadow: 0 -1px 1px 0 rgba(0,0,0,0.2);border: none;border-radius: 12px 0 0 0;font-size: 20px;
}
	body{
		padding-bottom: 40px;
	}	
<?php }  
if(isset($redux_builder_amp['amp-rtl-select-option']) && 1 == $redux_builder_amp['amp-rtl-select-option']) { ?>
.amp-ad-wrapper amp-ad, .amp-ad-wrapper iframe{ direction: ltr; }
<?php }
}