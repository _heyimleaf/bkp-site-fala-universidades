=== Advanced AMP ADS ===
Contributors:  Mohammed Kaludi, Mohammed Khaled
Tags: adsense, amp, dfp, banner, plugin
Requires at least: 3.0
Tested up to: 5.2
Stable tag: 1.19
License: GPLv2

Advertisement addon for Accelerated Mobile Pages plugin. This plugin will Auto Insert Advertisements in the content and many more features.

== Description ==

Advertisement addon for Accelerated Mobile Pages plugin. This plugin will Auto Insert Advertisements in the content and many more features.


== Changelog ==

= 1.19 (27 December 2019) =
* Bug Fixed: =
* Same AD loading twice due to another Plugin, issue resolved.

= 1.18 (13 December 2019) =
* Bug Fixed: =
* !important tag causing validation errors in amp-sticky-ad with Custom advertisement issue resolved.

= 1.17 (06 Sep 2019) =
* Bug Fixed: =
* amp-sticky-ad loading more than once Validation error resolved.

= 1.16 (31 Aug 2019) =
* Feature Added =
* 'data-enable-refresh' Attribute Feature Added.

= 1.15 (23 Aug 2019) =
* Feature Added =
* Enable multi-size ad requests Feature Added.
* Bug Fixed =
* Hide ADS Option not working with Core Advertisment Section ADS resolved. 

= 1.14 (09 Aug 2019) =
* Feature Added =
* MGID Advertisement Support Added.
* Bugs Fixed =
* Sticky Ads not working in the Custom Advertisement Section resolved.
 * Notices fixed.

= 1.13 (02 Aug 2019) =
* Bug Fixed =
* Redux Class not found Fatal Error resolved.

== 1.12 (19 July 2019) ==
== Bugs Fixed ==
  * Sticky AD not working issue resolved.
  * amp-sticky-ad Script not loading when using Custom Sticky AD resolved.

=1.11 (2 July 2019) =
= Bug Fixed =
* Fatal error fixed and Validation error for sticky AD resolved.

=1.10 (29 June 2019) =
= Bug Fixed =
* Parallax Option not working resolved.

=1.9.11 (14 June 2019) =
= Features Added =
* Amazon Native AD Support Added.

=1.9.10.3 (17 May 2019) =

* Validation error fixed.

=1.9.10.2 (10 May 2019) =

=Features Added:

* Option added for Incontent ADS on Front Page. 
* Multiple Positions for ADS added.
* Data-multi-size attribute added.
* Max width increased for Responsive ADS.
* ADS aligned in center when responsive is enabled.

=Bugs Fixed:

* Notices fixed.

= 1.9.10.1 (3 Oct 2018) =
* Ads are not centered issue fixed.

= 1.9.10 (29 Sept 2018) =
* Sponsorship Label option added (Above the AD).
* To show Ads Below the Author Box option Added ( It works only in Swift Theme).
* Restructured the Advanced Amp Ad Panel.
* Responsive ads unit issue fixed.

= 1.9.9 (14 July 2018) =
*  GDPR Proper code added. 

= 1.9.8 (26 May 2018) =
*  Ads getting cutoff when RTL is enable issue fixed. 

= 1.9.7 (4 May 2018) =
*  GDPR Compliancy 

= 1.9.6 (2 May 2018) =
*  Section ID Added

= 1.9.5 (20 April 2018) =
*  Upgrade Constants

= 1.9.4 (18 April 2018) =
*  Change some 'Update Proces' Code for better enhancement.

= 1.9.3 (14 April 2018) =
*  Activation Improved
*  Sponsored Label added
*  Swift theme loop ad compatibility
*  many other improvements 

= 1.9.2 (12 April 2018) =
*  Better Update Process Integrated
