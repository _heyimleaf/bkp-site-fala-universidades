<?php

/*------------------------------------------------------------
 * Widget:
 * Hygge: Image Banner
 *------------------------------------------------------------*/

add_action( 'widgets_init', 'register_widget_hygge_image_banner' );
function register_widget_hygge_image_banner() {
	register_widget( 'Hygge_Image_Banner' );
}

class Hygge_Image_Banner extends WP_Widget {



	/* Register widget with WordPress
	 *------------------------------------------------------------*/

	function __construct() {
		parent::__construct(
			'hygge_image_banner', // Base ID
			esc_html__( 'Hygge: Image Banner', 'hygge' ), // Name
			array( 'description' => esc_html__( 'Feature single Category, Tag, Page, Post or create custom banner.', 'hygge' ), ) // Args
		);

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}


	/**
	 * Enqueue scripts.
	 *
	 * @since 1.0
	 *
	 * @param string $hook_suffix
	 */
	public function enqueue_scripts( $hook_suffix ) {
		if ( 'widgets.php' !== $hook_suffix ) {
			return;
		}

		// wp_enqueue_script( 'underscore' );
		wp_enqueue_style( 'dashicons');
		wp_enqueue_style( 'thickbox');
		wp_enqueue_script( 'thickbox');
		wp_enqueue_script( 'media-upload');
	}



	/* Front-end display of widget
	 *------------------------------------------------------------*/

	public function widget( $args, $instance ) {

		$theme_settings = hygge_theme_settings();

		echo $args["before_widget"];

		$node = $link = $title = $target = $img = $loader_img = $color = '';

		$node = $instance["node"];

		if( !$node || $node == 'false' ){
			return;
		}

		if( $node ){
			if( $node == 'custom_banner' ){
				$img = $instance["img"];
				$title = $instance["title"];
				$link = $instance["link"];
				$target = $instance["target"];
			}else{
				$node = explode('-', $node);
				$node[1] = (int)$node[1];

				// Categories and Tags
				if( $node[0] == 'cat' || $node[0] == 'tag' ){
					$obj = get_term( $node[1] );

					$link = get_term_link( $obj );
					$title = $obj->name;
					$color = hygge_get_category_color( $node[1], true );

					if( function_exists('get_field') ){
						$img = get_field( 'image', $obj ) ? get_field( 'image', $obj ) : '';
						$loader_img = hygge_get_loader_img_url( $node[1], true );
					}
				}

				// Pages & Posts
				if( $node[0] == 'page' || $node[0] == 'post' ){
					$obj = get_post( $node[1] );
					$link = get_permalink( $node[1] );
					$title = $obj->post_title;
					$color = hygge_get_post_color( $node[1], true );
					$img = get_post_thumbnail_id($node[1]);
					$loader_img = hygge_get_loader_img_url( $node[1] );
				}

				if( $img ){
					$img = wp_get_attachment_image_src( $img, 'hygge-horizontal-m' );
					$img = $img[0];
				}
			}
		}
?>
		<a class="item <?php echo hygge_is_color_dark($color, true, true); ?>" href="<?php echo esc_url( $link ); ?>" title="<?php echo esc_attr( $title ); ?>" data-loader-img="<?php echo esc_attr($loader_img); ?>">
			<?php if( $img ): ?>
				<div class="item__media element-modular-loading hygge-media-hover" style="<?php echo esc_attr( $color ); ?>">
					<img class="hygge-media-hover__img" src="<?php echo esc_attr($img); ?>" title="<?php echo esc_attr($title); ?>" alt="<?php echo esc_attr($title); ?>">
					<div class="hygge-media-hover__hover" style="background-color:<?php echo esc_attr( $color );?>"></div>
					<h5 class="item__text ellipsis"><?php echo esc_html( $title ); ?></h5>
				</div>
			<?php else: ?>
				<h5 class="item__text ellipsis" style="background-color:<?php echo esc_attr( $color );?>"><?php echo esc_html( $title ); ?></h5>
			<?php endif; ?>
		</a>
<?php
		echo $args['after_widget'];
	}



	/* Back-end widget form
	 *------------------------------------------------------------*/

	public function form( $instance ){
		$theme_settings = hygge_theme_settings();
		$nodes = hygge_collect_nodes();

		echo '<div class="hygge-widget-featured-banners">';
			echo '<p></p>';

			// Node Selection
			$node = !empty( $instance['node'] ) ? $instance['node'] : 'false';
			echo '<select class="widefat featured-banners-node" id="'.$this->get_field_id( 'node' ).'" name="'.$this->get_field_name( 'node' ).'">';
				echo '<option value="false">'.esc_html__( '-- Select --', 'hygge' ).'</option>';
				echo '<optgroup label="──────────">';
				$selected = esc_attr( $node ) == 'custom_banner' ? 'selected' : '';
				echo '<option value="custom_banner"'.$selected.'>'.esc_html__( 'Custom Banner', 'hygge' ).'</option>';
				echo '<optgroup label="──────────">';
				echo '<optgroup label="'.esc_html__('Category', 'hygge').'">';
				foreach( $nodes['cats_id'] as $item ){
					foreach( $item as $id => $name ){
						$selected = esc_attr( $node ) == $id ? 'selected' : '';
						echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
					}
				}
				echo '<optgroup label="'.esc_html__('Page', 'hygge').'">';
				foreach( $nodes['pages_id'] as $item ){
					foreach( $item as $id => $name ){
						$selected = esc_attr( $node ) == $id ? 'selected' : '';
						echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
					}
				}
				echo '<optgroup label="'.esc_html__('Tag', 'hygge').'">';
				foreach( $nodes['tags_id'] as $item ){
					foreach( $item as $id => $name ){
						$selected = esc_attr( $node ) == $id ? 'selected' : '';
						echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
					}
				}
				echo '<optgroup label="'.esc_html__('Post', 'hygge').'">';
				foreach( $nodes['posts_id'] as $item ){
					foreach( $item as $id => $name ){
						$selected = esc_attr( $node ) == $id ? 'selected' : '';
						echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
					}
				}
			echo '</select>';

			echo '<div class="hygge-custom-banner">';

				// Image
				$img = !empty( $instance['img'] ) ? $instance['img'] : '';
				echo '<p>';
					echo '<button type="button" class="button hygge-upload-image-button"><span class="dashicons dashicons-admin-media"></span> ' . esc_html__('Choose Image', 'hygge') . '</button>';
					echo '<a class="remove js-remove"><i class="hygge-icon-close"></i></a>';
					echo '<input class="url" id="'.$this->get_field_id( 'img' ).'" name="'.$this->get_field_name( 'img' ).'" type="hidden" placeholder="'.esc_html__( 'http://', 'hygge' ).'" value="'.esc_attr( $img ).'">';
					echo '<img class="hygge-image-preview" src="'.$img.'">';
				echo '</p>';

				// Title
				$title = !empty( $instance['title'] ) ? $instance['title'] : '';
				$link = !empty( $instance['link'] ) ? $instance['link'] : '';
				echo '<p class="custom-button">';
					echo '<input id="'.$this->get_field_id( 'title' ).'" name="'.$this->get_field_name( 'title' ).'" type="text" placeholder="'.esc_html__( 'Title', 'hygge' ).'" value="'.esc_attr( $title ).'">';
					echo '<input id="'.$this->get_field_id( 'link' ).'" name="'.$this->get_field_name( 'link' ).'" type="text" placeholder="'.esc_html__( 'http://', 'hygge' ).'" value="'.esc_attr( $link ).'">';
				echo '</p>';

				// Target
				$target = !empty( $instance['target'] ) ? $instance['target'] : 'false';
				echo '<p>';
				echo '<input id="'.$this->get_field_id( 'target' ).'" name="'.$this->get_field_name( 'target' ).'" type="checkbox"' . checked( $target, 'on', false ) . '>';
				echo '<label for="'.$this->get_field_id( $target ).'">'.esc_html__('Open link in new tab (_blank)', 'hygge').'</label>';
				echo '</p>';

			echo '</div>';

		echo '<p></p>';
		echo '</div>';

	}



	/* Sanitize widget form values as they are saved
	 *------------------------------------------------------------*/

	public function update( $new_instance, $old_instance ){

		$instance = array();

		$instance['node'] = !empty( $new_instance['node'] ) ? esc_html(strip_tags( $new_instance['node'] )) : 'false';
		$instance['img'] = !empty( $new_instance['img'] ) ? esc_html(strip_tags( $new_instance['img'] )) : '';
		$instance['title']= !empty( $new_instance['title'] ) ? esc_html(strip_tags( $new_instance['title'] )) : '';
		$instance['link']= !empty( $new_instance['link'] ) ? esc_html(strip_tags( $new_instance['link'] )) : '';
		$instance['target']= !empty( $new_instance['target'] ) ? esc_html(strip_tags( $new_instance['target'] )) : 'false';

		return $instance;
	}

} // class Hygge_Image_Banner
