msgid ""
msgstr ""
"Project-Id-Version: hygge.1.0.0\n"
"POT-Creation-Date: 2017-11-21 14:49+0100\n"
"PO-Revision-Date: 2017-11-21 14:49+0100\n"
"Last-Translator: \n"
"Language-Team: korra.io\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;esc_attr__;esc_attr_e;"
"esc_html__;esc_html_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../widget-social.php:23
msgid "Hygge: Social"
msgstr ""

#: ../widget-social.php:24
msgid "Social Links with icons and/or labels."
msgstr ""

#: ../widget-social.php:77 ../widget-about.php:110
#: ../widget-blog_filmstrip.php:75 ../widget-blog_list.php:68
#: ../widget-blog_slider.php:68
msgid "Title:"
msgstr ""

#: ../widget-social.php:78 ../widget-about.php:111
#: ../widget-blog_filmstrip.php:76 ../widget-blog_list.php:69
#: ../widget-blog_slider.php:69
msgid "Widget Title"
msgstr ""

#: ../widget-social.php:84
msgid "Social Media Links:"
msgstr ""

#: ../widget-social.php:111 ../widget-social.php:122 ../widget-social.php:135
#: ../widget-blog_filmstrip.php:122 ../widget-image_banner.php:190
#: ../widget-blog_list.php:117 ../widget-featured_banners.php:242
#: ../widget-featured_banners.php:243 ../widget-blog_slider.php:117
msgid "Title"
msgstr ""

#: ../widget-social.php:112 ../widget-social.php:123 ../widget-social.php:136
#: ../widget-about.php:102 ../widget-about.php:135
#: ../widget-image_banner.php:182 ../widget-image_banner.php:191
#: ../widget-blog_list.php:154 ../widget-featured_banners.php:235
#: ../widget-featured_banners.php:257
msgid "http://"
msgstr ""

#: ../widget-social.php:129
msgid "Add new social media"
msgstr ""

#: ../widget-social.php:140
msgid "Generated shortcode:"
msgstr ""

#: ../widget-social.php:141
msgid "Use this shortcode for post content or author social links field:"
msgstr ""

#: ../widget-about.php:23
msgid "Hygge: About"
msgstr ""

#: ../widget-about.php:24
msgid "Information about the blog to be used in sidebar."
msgstr ""

#: ../widget-about.php:59
msgid "author image"
msgstr ""

#: ../widget-about.php:98
msgid "Image:"
msgstr ""

#: ../widget-about.php:100
msgid "Add Image"
msgstr ""

#: ../widget-about.php:117
msgid "Description:"
msgstr ""

#: ../widget-about.php:118
msgid "Description Text"
msgstr ""

#: ../widget-about.php:124
msgid "Social Shortcode:"
msgstr ""

#: ../widget-about.php:125
msgid "[hygge_social]"
msgstr ""

#: ../widget-about.php:126
msgid ""
"<small>Enter the social shortcode here.<br><strong>Hygge: Social</strong> "
"widget provides generator for the social shortcode.</small>"
msgstr ""

#: ../widget-about.php:133 ../widget-blog_list.php:152
msgid "Custom Button:"
msgstr ""

#: ../widget-about.php:134 ../widget-blog_list.php:153
msgid "Button Text"
msgstr ""

#: ../widget-blog_filmstrip.php:23
msgid "Hygge: Blog Filmstrip"
msgstr ""

#: ../widget-blog_filmstrip.php:24
msgid "Fullwidth Blog Filmstrip to be used in the Below Page Content Area."
msgstr ""

#: ../widget-blog_filmstrip.php:70
msgid ""
"This Widget is designed to be placed in the Below Page Content Area. It "
"takes full screen width and will not display correctly in smaller footer "
"widget areas."
msgstr ""

#: ../widget-blog_filmstrip.php:82 ../widget-blog_list.php:77
#: ../widget-blog_slider.php:77
msgid "Number of posts:"
msgstr ""

#: ../widget-blog_filmstrip.php:91 ../widget-blog_list.php:86
#: ../widget-blog_slider.php:86
msgid "Category:"
msgstr ""

#: ../widget-blog_filmstrip.php:93 ../widget-blog_list.php:88
#: ../widget-blog_slider.php:88
msgid "All categories"
msgstr ""

#: ../widget-blog_filmstrip.php:106 ../widget-blog_list.php:101
#: ../widget-blog_slider.php:101
msgid "Tag:"
msgstr ""

#: ../widget-blog_filmstrip.php:108 ../widget-blog_list.php:103
#: ../widget-blog_slider.php:103
msgid "All tags"
msgstr ""

#: ../widget-blog_filmstrip.php:119 ../widget-blog_list.php:114
#: ../widget-blog_slider.php:114
msgid "Date"
msgstr ""

#: ../widget-blog_filmstrip.php:120 ../widget-blog_list.php:115
#: ../widget-blog_slider.php:115
msgid "Date modified"
msgstr ""

#: ../widget-blog_filmstrip.php:121 ../widget-blog_list.php:116
#: ../widget-blog_slider.php:116
msgid "Comment count"
msgstr ""

#: ../widget-blog_filmstrip.php:123 ../widget-blog_list.php:118
#: ../widget-blog_slider.php:118
msgid "Random"
msgstr ""

#: ../widget-blog_filmstrip.php:127 ../widget-blog_list.php:122
#: ../widget-blog_slider.php:122
msgid "Order by:"
msgstr ""

#: ../widget-blog_filmstrip.php:139 ../widget-blog_list.php:134
#: ../widget-blog_slider.php:134
msgid "Descending"
msgstr ""

#: ../widget-blog_filmstrip.php:140 ../widget-blog_list.php:135
#: ../widget-blog_slider.php:135
msgid "Ascending"
msgstr ""

#: ../widget-blog_filmstrip.php:144 ../widget-blog_list.php:139
#: ../widget-blog_slider.php:139
msgid "Order:"
msgstr ""

#: ../widget-blog_filmstrip.php:156
msgid "Animation interval:"
msgstr ""

#: ../widget-blog_filmstrip.php:157
msgid "Defaults to 0 (off). Use number in ms (3000 = 3s)."
msgstr ""

#: ../widget-image_banner.php:23
msgid "Hygge: Image Banner"
msgstr ""

#: ../widget-image_banner.php:24
msgid "Feature single Category, Tag, Page, Post or create custom banner."
msgstr ""

#: ../widget-image_banner.php:140 ../widget-featured_banners.php:193
msgid "-- Select --"
msgstr ""

#: ../widget-image_banner.php:143 ../widget-featured_banners.php:196
msgid "Custom Banner"
msgstr ""

#: ../widget-image_banner.php:145 ../widget-featured_banners.php:198
msgid "Category"
msgstr ""

#: ../widget-image_banner.php:152 ../widget-featured_banners.php:205
msgid "Page"
msgstr ""

#: ../widget-image_banner.php:159 ../widget-featured_banners.php:212
msgid "Tag"
msgstr ""

#: ../widget-image_banner.php:166 ../widget-featured_banners.php:219
msgid "Post"
msgstr ""

#: ../widget-image_banner.php:180 ../widget-featured_banners.php:233
msgid "Choose Image"
msgstr ""

#: ../widget-image_banner.php:198 ../widget-featured_banners.php:264
msgid "Open link in new tab (_blank)"
msgstr ""

#: ../widget-blog_list.php:23
msgid "Hygge: Blog List"
msgstr ""

#: ../widget-blog_list.php:24
msgid "Custom blog list intended to be used in Sidebar."
msgstr ""

#: ../widget-featured_banners.php:23
msgid "Hygge: Featured Banners"
msgstr ""

#: ../widget-featured_banners.php:24
msgid "Feature multiple Category, Tag, Page or Post as banners."
msgstr ""

#: ../widget-featured_banners.php:170
msgid ""
"Display up to 4 featured banners that can be linked to either internal links "
"(category, tag, page, post), or custom external link. Advised to use in "
"larger widget areas, like Area Before Page Content."
msgstr ""

#: ../widget-featured_banners.php:176
msgid "Make widget fullwidth"
msgstr ""

#: ../widget-featured_banners.php:181
msgid "Featured Banner "
msgstr ""

#: ../widget-featured_banners.php:249 ../widget-featured_banners.php:250
msgid "Description"
msgstr ""

#: ../widget-featured_banners.php:256
msgid "Link"
msgstr ""

#: ../widget-blog_slider.php:23
msgid "Hygge: Blog Slider"
msgstr ""

#: ../widget-blog_slider.php:24
msgid "Custom blog slider intended to be used in Sidebar."
msgstr ""
