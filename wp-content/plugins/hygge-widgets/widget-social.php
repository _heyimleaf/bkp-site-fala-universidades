<?php

/*------------------------------------------------------------
 * Widget:
 * Hygge: Social
 *------------------------------------------------------------*/

add_action( 'widgets_init', 'register_widget_hygge_social_links' );
function register_widget_hygge_social_links() {
	register_widget( 'Hygge_Social_Links' );
}

class Hygge_Social_Links extends WP_Widget {



	/* Register widget with WordPress
	 *------------------------------------------------------------*/

	function __construct() {
		parent::__construct(
			'hygge_social_icons', // Base ID
			esc_html__( 'Hygge: Social', 'hygge' ), // Name
			array( 'description' => esc_html__( 'Social Links with icons and/or labels.', 'hygge' ), ) // Args
		);
	}



	/* Front-end display of widget
	 *------------------------------------------------------------*/

	public function widget( $args, $instance ) {

		echo $args['before_widget'];
		if( !empty( $instance['title'] ) ){
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

		// Set defaults for init add on WP customizer
		$instance['data'] = !empty( $instance['data'] ) ? $instance['data'] : '';

		// Content
		$data = explode( ' ', $instance['data'] );
		$socialString = '';

		foreach( $data as $social ){

			if( empty($social) ){
				continue;
			}

			$social = explode( '|', $social );

			if( ($social[0] || $social[1]) && $social[2] ){
				$socialString .= " {$social[0]}|{$social[1]}|{$social[2]}";
			}
		}

		if( !empty($socialString) && shortcode_exists('hygge_social') ){
			echo do_shortcode( '[hygge_social ' . $socialString . ']' );
		}

		echo $args['after_widget'];
	}



	/* Back-end widget form
	 *------------------------------------------------------------*/

	public function form( $instance ){

		// Title
		$title = !empty( $instance['title'] ) ? $instance['title'] : '';
		echo '<p>';
		echo '<label for="'.$this->get_field_id( 'title' ).'">'.esc_html__( 'Title:', 'hygge' ).'</label>';
		echo '<input class="widefat" id="'.$this->get_field_id( 'title' ).'" name="'.$this->get_field_name( 'title' ).'" type="text" placeholder="'.esc_html__( 'Widget Title', 'hygge' ).'" value="'.esc_attr( $title ).'">';
		echo '</p>';



		// Social Picker Wrap Start
		echo '<label>'.esc_html__( 'Social Media Links:', 'hygge' ).'</label>';

		echo '<div class="hygge-widget-social-picker">';

		// Hidden field with serialized data
		$data = !empty( $instance['data'] ) ? $instance['data'] : '';
		echo '<input type="hidden" class="value-joined" id="'.$this->get_field_id( 'data' ).'" name="'.$this->get_field_name( 'data' ).'" value="'.esc_attr( $data ).'">';

		// Infinite form
		echo '<div class="form">';

			$shortcode = '[hygge_social ' . esc_attr( $data ) . ']';

			// Create fields from existing value
			$socialList = explode( ' ', esc_attr( $data ) );

			if( !empty( $socialList[0] ) ){
				foreach( $socialList as $social ){

					$social = str_replace( '%20', ' ', $social );
					$social = explode( '|', $social);

					if( ($social[0] || $social[1]) && $social[2] ){

						echo '<div class="form-row">';
							echo '<div class="icon"><input type="hidden" name="icon" value="'.$social[0].'"><i class="hygge-icon-'.$social[0].'"></i></div>';
							echo '<a class="remove js-remove"><i class="hygge-icon-close"></i></a>';
							echo '<div class="title"><input type="text" name="title" placeholder="'.esc_html__('Title', 'hygge').'" value="'.$social[1].'"></div>';
							echo '<div class="url"><input type="text" name="url" placeholder="'.esc_html__('http://', 'hygge').'" value="'.$social[2].'"></div>';
						echo '</div>';
					}
				}
			}

			// Default empty item
			echo '<div class="form-row">';
				echo '<div class="icon"><input type="hidden" name="icon"><i></i></div>';
				echo '<a class="remove js-remove"><i class="hygge-icon-close"></i></a>';
				echo '<div class="title"><input type="text" name="title" placeholder="'.esc_html__('Title', 'hygge').'"></div>';
				echo '<div class="url"><input type="text" name="url" placeholder="'.esc_html__('http://', 'hygge').'"></div>';
			echo '</div>';


		echo '</div>';

		echo '<a class="button right js-add-new-social">'.esc_html__( 'Add new social media', 'hygge' ).'</a>';

		// original empty social item used for copying
		echo '<div class="form-row">';
			echo '<div class="icon"><input type="hidden" name="icon"><i></i></div>';
			echo '<a class="remove js-remove"><i class="hygge-icon-close"></i></a>';
			echo '<div class="title"><input type="text" name="title" placeholder="'.esc_html__('Title', 'hygge').'"></div>';
			echo '<div class="url"><input type="text" name="url" placeholder="'.esc_html__('http://', 'hygge').'"></div>';
		echo '</div>';

		echo '<p class="shortcode">';
		echo '<label>' . esc_html__( 'Generated shortcode:', 'hygge' ).'</label>';
		echo '<br><small>' . esc_html__('Use this shortcode for post content or author social links field:', 'hygge') . '</small>';
		echo '<input type="text" id="shortcode" onfocus="this.select();" readonly="readonly" class="large-text code" value="'. $shortcode .'">';
		echo '</p>';

		echo '</div>';
	}



	/* Sanitize widget form values as they are saved
	 *------------------------------------------------------------*/

	public function update( $new_instance, $old_instance ){

		$instance = array();
		$instance['title'] = !empty( $new_instance['title'] ) ? esc_html(strip_tags( $new_instance['title'] )) : '';
		$instance['data'] = !empty( $new_instance['data'] ) ? esc_html(strip_tags( $new_instance['data'] )) : '';

		return $instance;
	}

} // class Hygge_Social_Links
