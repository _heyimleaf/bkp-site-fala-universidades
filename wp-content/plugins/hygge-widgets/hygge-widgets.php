<?php
/*

Plugin Name: Hygge Widgets
Plugin URI:
Description: Bundled widgets for KORRA's Hygge theme. Contains only the functional part. Styles and front-end behavior is covered by the theme.
Version: 1.0.2
Author: KORRA
Author URI: http://korra.io
Text Domain: hygge

Copyright 2018 KORRA

*/



/*------------------------------------------------------------
 * Include external shortcode files
 *------------------------------------------------------------*/

include_once( plugin_dir_path( __FILE__ ) . '/widget-about.php' );
include_once( plugin_dir_path( __FILE__ ) . '/widget-blog-list.php' );
include_once( plugin_dir_path( __FILE__ ) . '/widget-blog-slider.php' );
include_once( plugin_dir_path( __FILE__ ) . '/widget-blog-filmstrip.php' );
include_once( plugin_dir_path( __FILE__ ) . '/widget-featured-banners.php' );
include_once( plugin_dir_path( __FILE__ ) . '/widget-image-banner.php' );
include_once( plugin_dir_path( __FILE__ ) . '/widget-social.php' );
