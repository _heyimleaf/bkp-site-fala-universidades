<?php

/*------------------------------------------------------------
 * Widget:
 * Hygge: Blog List
 *------------------------------------------------------------*/

add_action( 'widgets_init', 'register_widget_hygge_blog_list' );
function register_widget_hygge_blog_list() {
	register_widget( 'Hygge_Widget_Blog_List' );
}

class Hygge_Widget_Blog_List extends WP_Widget {



	/* Register widget with WordPress
	 *------------------------------------------------------------*/

	function __construct() {
		parent::__construct(
			'hygge_blog_list', // Base ID
			esc_html__( 'Hygge: Blog List', 'hygge' ), // Name
			array( 'description' => esc_html__( 'Custom blog list intended to be used in Sidebar.', 'hygge' ), ) // Args
		);
	}



	/* Front-end display of widget
	 *------------------------------------------------------------*/

	public function widget( $args, $instance ) {

		// Check if shortcode exists
		if( !shortcode_exists( 'hygge_blog' ) ){
			return;
		}

		// Set defaults
		$instance['posts_num'] = !empty( $instance['posts_num'] ) ? $instance['posts_num'] : '5';
		$instance['category'] = !empty( $instance['category'] ) ? $instance['category'] : '';
		$instance['tag'] = !empty( $instance['tag'] ) ? $instance['tag'] : '';
		$instance['order_by'] = !empty( $instance['order_by'] ) ? $instance['order_by'] : 'date';
		$instance['order'] = !empty( $instance['order'] ) ? $instance['order'] : 'DESC';

		echo $args['before_widget'];

		if( !empty( $instance['title'] ) ){
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

		echo do_shortcode('[hygge_blog posts="'.$instance['posts_num'].'" style="widget-list" category="'.$instance['category'].'" tag="'.$instance['tag'].'" sort_by="'.$instance['order_by'].'" sort_order="'.$instance['order'].'" link_text="'.$instance['button_text'].'" link_url="'.$instance['button_link'].'"]');

		echo $args['after_widget'];
	}



	/* Back-end widget form
	 *------------------------------------------------------------*/

	public function form( $instance ){

		// Title
		$title = !empty( $instance['title'] ) ? $instance['title'] : '';
		echo '<p>';
		echo '<label for="'.$this->get_field_id( 'title' ).'">'.esc_html__( 'Title:', 'hygge' ).'</label>';
		echo '<input class="widefat" id="'.$this->get_field_id( 'title' ).'" name="'.$this->get_field_name( 'title' ).'" type="text" placeholder="'.esc_html__( 'Widget Title', 'hygge' ).'" value="'.esc_attr( $title ).'">';
		echo '</p>';



		// Number of posts
		$posts_num = !empty( $instance['posts_num'] ) ? $instance['posts_num'] : '5';
		echo '<p>';
		echo '<label for="'.$this->get_field_id( 'posts_num' ).'">'.esc_html__( 'Number of posts:', 'hygge' ).'</label>';
		echo '<input class="widefat" id="'.$this->get_field_id( 'posts_num' ).'" name="'.$this->get_field_name( 'posts_num' ).'" type="text" value="'.esc_attr( $posts_num ).'">';
		echo '</p>';

		// Category
		$category = !empty( $instance['category'] ) ? $instance['category'] : '';
		$category_list = get_categories();

		echo '<p>';
		echo '<label for="'.$this->get_field_id( 'category' ).'">'.esc_html__( 'Category:', 'hygge' ).'</label>';
		echo '<select class="widefat" id="'.$this->get_field_id( 'category' ).'" name="'.$this->get_field_name( 'category' ).'">';
			echo '<option value="">'.esc_html__( 'All categories', 'hygge' ).'</option>';
			foreach( $category_list as $cat ){
				$selected = esc_attr( $category ) == $cat->slug ? 'selected' : '';
				echo '<option value="'.$cat->slug.'" '.$selected.'>'.$cat->name.'</option>';
			}
		echo '</select>';
		echo '</p>';

		// Tag
		$tag = !empty( $instance['tag'] ) ? $instance['tag'] : '';
		$tag_list = get_categories( array('taxonomy' => 'post_tag') );

		echo '<p>';
		echo '<label for="'.$this->get_field_id( 'tag' ).'">'.esc_html__( 'Tag:', 'hygge' ).'</label>';
		echo '<select class="widefat" id="'.$this->get_field_id( 'tag' ).'" name="'.$this->get_field_name( 'tag' ).'">';
			echo '<option value="">'.esc_html__( 'All tags', 'hygge' ).'</option>';
			foreach( $tag_list as $tag_item ){
				$selected = esc_attr( $tag ) == $tag_item->slug ? 'selected' : '';
				echo '<option value="'.$tag_item->slug.'" '.$selected.'>'.$tag_item->name.'</option>';
			}
		echo '</select>';
		echo '</p>';

		// Order By
		$order_by = !empty( $instance['order_by'] ) ? $instance['order_by'] : 'date';
		$order_by_list = array(
			'date' => esc_html__('Date', 'hygge'),
			'modified' => esc_html__('Date modified', 'hygge'),
			'comment_count' => esc_html__('Comment count', 'hygge'),
			'title' => esc_html__('Title', 'hygge'),
			'rand' => esc_html__('Random', 'hygge'),
		);

		echo '<p>';
		echo '<label for="'.$this->get_field_id( 'order_by' ).'">'.esc_html__( 'Order by:', 'hygge' ).'</label>';
		echo '<select class="widefat" id="'.$this->get_field_id( 'order_by' ).'" name="'.$this->get_field_name( 'order_by' ).'">';
			foreach( $order_by_list as $key => $val ){
				$selected = esc_attr( $order_by ) == $key ? 'selected' : '';
				echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
			}
		echo '</select>';
		echo '</p>';

		// Order
		$order = !empty( $instance['order'] ) ? $instance['order'] : 'DESC';
		$order_list = array(
			'DESC' => esc_html__('Descending', 'hygge'),
			'ASC' => esc_html__('Ascending', 'hygge'),
		);

		echo '<p>';
		echo '<label for="'.$this->get_field_id( 'order' ).'">'.esc_html__( 'Order:', 'hygge' ).'</label>';
		echo '<select class="widefat" id="'.$this->get_field_id( 'order' ).'" name="'.$this->get_field_name( 'order' ).'">';
			foreach( $order_list as $key => $val ){
				$selected = esc_attr( $order ) == $key ? 'selected' : '';
				echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
			}
		echo '</select>';
		echo '</p>';

		// Button
		$button_text = !empty( $instance['button_text'] ) ? $instance['button_text'] : '';
		$button_link = !empty( $instance['button_link'] ) ? $instance['button_link'] : '';
		echo '<p class="custom-button">';
			echo '<label class="hygge-big" for="'.$this->get_field_id( 'button_text' ).'">'. esc_html__( 'Custom Button:', 'hygge' ).'</label>';
			echo '<input id="'.$this->get_field_id( 'button_text' ).'" name="'.$this->get_field_name( 'button_text' ).'" type="text" placeholder="'. esc_html__( 'Button Text', 'hygge' ).'" value="'.esc_attr( $button_text ).'">';
			echo '<input id="'.$this->get_field_id( 'button_link' ).'" name="'.$this->get_field_name( 'button_link' ).'" type="text" placeholder="'. esc_html__( 'http://', 'hygge' ).'" value="'.esc_attr( $button_link ).'">';
		echo '</p>';

}



	/* Sanitize widget form values as they are saved
	 *------------------------------------------------------------*/

	public function update( $new_instance, $old_instance ){

		$instance = array();
		$instance['title'] = !empty( $new_instance['title'] ) ? esc_html(strip_tags( $new_instance['title'] )) : '';
		$instance['posts_num'] = !empty( $new_instance['posts_num'] ) ? esc_html(strip_tags( $new_instance['posts_num'] )) : '5';
		$instance['category'] = !empty( $new_instance['category'] ) ? esc_html(strip_tags( $new_instance['category'] )) : '';
		$instance['tag'] = !empty( $new_instance['tag'] ) ? esc_html(strip_tags( $new_instance['tag'] )) : '';
		$instance['order_by'] = !empty( $new_instance['order_by'] ) ? esc_html(strip_tags( $new_instance['order_by'] )) : 'date';
		$instance['order'] = !empty( $new_instance['order'] ) ? esc_html(strip_tags( $new_instance['order'] )) : 'DESC';
		$instance['button_text']= !empty( $new_instance['button_text'] ) ? esc_html(strip_tags( $new_instance['button_text'] )) : '';
		$instance['button_link']= !empty( $new_instance['button_link'] ) ? esc_html(strip_tags( $new_instance['button_link'] )) : '';


		return $instance;
	}

} // class Hygge_Widget_Blog_List
