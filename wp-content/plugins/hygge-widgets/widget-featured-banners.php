<?php

/*------------------------------------------------------------
 * Widget:
 * Hygge: Featured Banners
 *------------------------------------------------------------*/

add_action( 'widgets_init', 'register_widget_hygge_featured_banners' );
function register_widget_hygge_featured_banners() {
	register_widget( 'Hygge_Featured_Banners' );
}

class Hygge_Featured_Banners extends WP_Widget {



	/* Register widget with WordPress
	 *------------------------------------------------------------*/

	function __construct() {
		parent::__construct(
			'hygge_featured_banners', // Base ID
			esc_html__( 'Hygge: Featured Banners', 'hygge' ), // Name
			array( 'description' => esc_html__( 'Feature multiple Category, Tag, Page or Post as banners.', 'hygge' ), ) // Args
		);

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}


	/**
	 * Enqueue scripts.
	 *
	 * @since 1.0
	 *
	 * @param string $hook_suffix
	 */
	public function enqueue_scripts( $hook_suffix ) {
		if ( 'widgets.php' !== $hook_suffix ) {
			return;
		}

		// wp_enqueue_script( 'underscore' );
		wp_enqueue_style( 'dashicons');
		wp_enqueue_style( 'thickbox');
		wp_enqueue_script( 'thickbox');
		wp_enqueue_script( 'media-upload');
	}



	/* Front-end display of widget
	 *------------------------------------------------------------*/

	public function widget( $args, $instance ) {

		$theme_settings = hygge_theme_settings();

		echo $args["before_widget"];

		if( $instance['fullwidth'] ){
			echo "<div class='hygge-fullwidth'>";
		}

		echo "<div class='hygge-featured-banners'>";

		for( $i = 1; $i <= 4; $i++ ){

			$node = $link = $title = $description = $target = $img = $loader_img = $color = '';

			$node = $instance["node{$i}"];

			if( $node == 'false' ){
				continue;
			}else{
				if( $node == 'custom_banner' ){
					$img = $instance["img{$i}"];
					$title = $instance["title{$i}"];
					$description = $instance["description{$i}"];
					$link = $instance["link{$i}"];
					$target = $instance["target{$i}"];

					if( !$title || !$link ){
						continue;
					}

				}else{

					$node = explode('-', $node);
					$node[1] = (int)$node[1];

					// Categories and Tags
					if( $node[0] == 'cat' || $node[0] == 'tag' ){
						$obj = get_term( $node[1] );

						$link = get_term_link( $obj );
						$title = $obj->name;
						$description = $obj->description;
						$color = hygge_get_category_color( $node[1] );

						if( function_exists('get_field') ){
							$img = get_field( 'image', $obj ) ? get_field( 'image', $obj ) : '';
							$loader_img = hygge_get_loader_img_url( $node[1], true );
						}
					}

					// Pages & Posts
					if( $node[0] == 'page' || $node[0] == 'post' ){
						$obj = get_post( $node[1] );
						$link = get_permalink( $node[1] );
						$title = $obj->post_title;
						$description = get_post_meta( $node[1], 'cover_description', true );
						$color = hygge_get_post_color( $node[1], true );
						$img = get_post_thumbnail_id($node[1]);
						$loader_img = hygge_get_loader_img_url( $node[1] );
					}

					if( $img ){
						$img = wp_get_attachment_image_src( $img, 'hygge-horizontal-l' );
						$img = $img[0];
					}

				}

				if( $img ){
					$img = "background-image: url({$img});";
				}else{
					$img = $color;
				}
			}
?>

			<a class="item element-modular-loading hygge-media-hover" href="<?php echo esc_url( $link ); ?>" title="<?php echo esc_attr( $title ); ?>" data-loader-img="<?php echo $loader_img; ?>">
				<div class="item__media hygge-media-hover__img" style="<?php echo esc_attr( $img ); ?>">
				</div>
				<div class="hygge-media-hover__hover" style="background-color:<?php echo $color; ?>;"></div>
				<?php $bg_style = $color ? "" : ""; ?>
				<div class="item__text <?php echo hygge_is_color_dark($color, true)?>">
					<div class="item__text__inwrap">
						<div class="item__text__bg" style="background-color: <?php echo $color ?>;"></div>
						<h4 class="item__title"><?php echo esc_html( $title ); ?></h4>
						<p class="item__description label--italic"><?php echo esc_html( $description ); ?></p>
					</div>
				</div>
			</a>

<?php
		}

		echo "</div>";

		if( $instance['fullwidth'] ){
			echo "</div>";
		}

		echo $args['after_widget'];
	}



	/* Back-end widget form
	 *------------------------------------------------------------*/

	public function form( $instance ){
		$theme_settings = hygge_theme_settings();
		$nodes = hygge_collect_nodes();

		echo '<div class="hygge-widget-featured-banners">';

		echo '<p>'.esc_html__('Display up to 4 featured banners that can be linked to either internal links (category, tag, page, post), or custom external link. Advised to use in larger widget areas, like Area Before Page Content.', 'hygge').'</p>';

		// Fullscreen
		$fullwidth = !empty( $instance['fullwidth'] ) ? $instance['fullwidth'] : false;
		echo '<p>';
		echo '<input id="'.$this->get_field_id( 'fullwidth' ).'" name="'.$this->get_field_name( 'fullwidth' ).'" type="checkbox"' . checked( $fullwidth, 'on', false ) . '>';
		echo '<label for="'.$this->get_field_id( 'fullwidth' ).'">'.esc_html__('Make widget fullwidth', 'hygge').'</label>';
		echo '</p>';

		for( $i = 1; $i <= 4; $i++ ){

			echo '<label class="hygge-big">'.esc_html__( 'Featured Banner ', 'hygge' ).$i.':</label>';

			$nodeI = 'node'.$i;
			$imgI = 'img'.$i;
			$titleI = 'title'.$i;
			$descriptionI = 'description'.$i;
			$linkI = 'link'.$i;
			$targetI = 'target'.$i;

			// Node Selection
			$node = !empty( $instance[$nodeI] ) ? $instance[$nodeI] : 'false';
			echo '<select class="widefat featured-banners-node" id="'.$this->get_field_id( $nodeI ).'" name="'.$this->get_field_name( $nodeI ).'">';
				echo '<option value="false">'.esc_html__( '-- Select --', 'hygge' ).'</option>';
				echo '<optgroup label="──────────">';
				$selected = esc_attr( $node ) == 'custom_banner' ? 'selected' : '';
				echo '<option value="custom_banner"'.$selected.'>'.esc_html__( 'Custom Banner', 'hygge' ).'</option>';
				echo '<optgroup label="──────────">';
				echo '<optgroup label="'.esc_html__('Category', 'hygge').'">';
				foreach( $nodes['cats_id'] as $item ){
					foreach( $item as $id => $name ){
						$selected = esc_attr( $node ) == $id ? 'selected' : '';
						echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
					}
				}
				echo '<optgroup label="'.esc_html__('Page', 'hygge').'">';
				foreach( $nodes['pages_id'] as $item ){
					foreach( $item as $id => $name ){
						$selected = esc_attr( $node ) == $id ? 'selected' : '';
						echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
					}
				}
				echo '<optgroup label="'.esc_html__('Tag', 'hygge').'">';
				foreach( $nodes['tags_id'] as $item ){
					foreach( $item as $id => $name ){
						$selected = esc_attr( $node ) == $id ? 'selected' : '';
						echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
					}
				}
				echo '<optgroup label="'.esc_html__('Post', 'hygge').'">';
				foreach( $nodes['posts_id'] as $item ){
					foreach( $item as $id => $name ){
						$selected = esc_attr( $node ) == $id ? 'selected' : '';
						echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
					}
				}
			echo '</select>';

			echo '<div class="hygge-custom-banner">';

				// Image
				$img = !empty( $instance[$imgI] ) ? $instance[$imgI] : '';
				echo '<p>';
					echo '<button type="button" class="button hygge-upload-image-button"><span class="dashicons dashicons-admin-media"></span> ' . esc_html__('Choose Image', 'hygge') . '</button>';
					echo '<a class="remove js-remove"><i class="hygge-icon-close"></i></a>';
					echo '<input class="url" id="'.$this->get_field_id( $imgI ).'" name="'.$this->get_field_name( $imgI ).'" type="hidden" placeholder="'.esc_html__( 'http://', 'hygge' ).'" value="'.esc_attr( $img ).'">';
					echo '<img class="hygge-image-preview" src="'.$img.'">';
				echo '</p>';

				// Title
				$title = !empty( $instance[$titleI] ) ? $instance[$titleI] : '';
				echo '<p>';
					echo '<label>' . esc_html__( 'Title', 'hygge' ) . '</label>';
					echo '<input class="widefat" id="'.$this->get_field_id( $titleI ).'" name="'.$this->get_field_name( $titleI ).'" type="text" placeholder="'.esc_html__( 'Title', 'hygge' ).'" value="'.esc_attr( $title ).'">';
				echo '</p>';

				// Description
				$description = !empty( $instance[$descriptionI] ) ? $instance[$descriptionI] : '';
				echo '<p>';
					echo '<label>' . esc_html__( 'Description', 'hygge' ) . '</label>';
					echo '<input class="widefat" id="'.$this->get_field_id( $descriptionI ).'" name="'.$this->get_field_name( $descriptionI ).'" type="text" placeholder="'.esc_html__( 'Description', 'hygge' ).'" value="'.esc_attr( $description ).'">';
				echo '</p>';

				// Link
				$link = !empty( $instance[$linkI] ) ? $instance[$linkI] : '';
				echo '<p>';
					echo '<label>' . esc_html__( 'Link', 'hygge' ) . '</label>';
					echo '<input class="widefat" id="'.$this->get_field_id( $linkI ).'" name="'.$this->get_field_name( $linkI ).'" type="text" placeholder="'.esc_html__( 'http://', 'hygge' ).'" value="'.esc_attr( $link ).'">';
				echo '</p>';

				// Target
				$target = !empty( $instance[$targetI] ) ? $instance[$targetI] : 'false';
				echo '<p>';
				echo '<input id="'.$this->get_field_id( $targetI ).'" name="'.$this->get_field_name( $targetI ).'" type="checkbox"' . checked( $target, 'on', false ) . '>';
				echo '<label for="'.$this->get_field_id( $targetI ).'">'.esc_html__('Open link in new tab (_blank)', 'hygge').'</label>';
				echo '</p>';

			echo '</div>';

			echo '<hr class="hygge-big">';

		}

		echo '</div>';

	}



	/* Sanitize widget form values as they are saved
	 *------------------------------------------------------------*/

	public function update( $new_instance, $old_instance ){

		$instance = array();
		$instance['fullwidth'] = !empty( $new_instance['fullwidth'] ) ? strip_tags( $new_instance['fullwidth'] ) : false;


		for( $i = 1; $i <= 4; $i++ ){

			$nodeI = 'node'.$i;
			$imgI = 'img'.$i;
			$titleI = 'title'.$i;
			$descriptionI = 'description'.$i;
			$linkI = 'link'.$i;
			$targetI = 'target'.$i;

			$instance[$nodeI] = !empty( $new_instance[$nodeI] ) ? esc_html(strip_tags( $new_instance[$nodeI] )) : 'false';
			$instance[$imgI] = !empty( $new_instance[$imgI] ) ? esc_html(strip_tags( $new_instance[$imgI] )) : '';
			$instance[$titleI]= !empty( $new_instance[$titleI] ) ? esc_html(strip_tags( $new_instance[$titleI] )) : '';
			$instance[$descriptionI]= !empty( $new_instance[$descriptionI] ) ? esc_html(strip_tags( $new_instance[$descriptionI] )) : '';
			$instance[$linkI]= !empty( $new_instance[$linkI] ) ? esc_html(strip_tags( $new_instance[$linkI] )) : '';
			$instance[$targetI]= !empty( $new_instance[$targetI] ) ? esc_html(strip_tags( $new_instance[$targetI] )) : 'false';
		}

		return $instance;
	}

} // class Hygge_Featured_Banners
