<?php

/*------------------------------------------------------------
 * Widget:
 * Hygge: About
 *------------------------------------------------------------*/

add_action( 'widgets_init', 'register_widget_hygge_about' );
function register_widget_hygge_about() {
	register_widget( 'Hygge_About_Author' );
}

class Hygge_About_Author extends WP_Widget {



	/* Register widget with WordPress
	 *------------------------------------------------------------*/

	function __construct() {
		parent::__construct(
			'hygge_about', // Base ID
			 esc_html__( 'Hygge: About', 'hygge' ), // Name
			array( 'description' => esc_html__( 'Information about the blog to be used in sidebar.', 'hygge' ), ) // Args
		);

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}


	/**
	 * Enqueue scripts.
	 *
	 * @since 1.0
	 *
	 * @param string $hook_suffix
	 */
	public function enqueue_scripts( $hook_suffix ) {
		if ( 'widgets.php' !== $hook_suffix ) {
			return;
		}

		// wp_enqueue_script( 'underscore' );
		wp_enqueue_style( 'dashicons');
		wp_enqueue_style( 'thickbox');
		wp_enqueue_script( 'thickbox');
		wp_enqueue_script( 'media-upload');
	}


	/* Front-end display of widget
	 *------------------------------------------------------------*/

	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		if( !empty( $instance['img'] ) ){
			echo '<img src="' . esc_url( $instance['img'] ) . '" title="' . esc_attr( $instance['title'] ) . '" alt="'. esc_html__('author image', 'hygge').'" />';
		}

		echo '<div class="widget-text">';

			if( !empty( $instance['title'] ) ){
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
			}

			if( !empty( $instance['description'] ) ){
				echo wpautop( do_shortcode( esc_html( $instance['description'] ) ) );
			}

			if( !empty( $instance['social'] ) ){
				echo do_shortcode( $instance['social'] );
			}

			if( !empty( $instance['button_text'] ) && !empty( $instance['button_link'] ) ){
				echo "<br><a class='label link--underline' href='" . esc_url( $instance['button_link'] ) . "' title='" . esc_attr( $instance['button_text'] ) . "'>" . esc_html( $instance['button_text'] ) . "</a>";
			}

		echo '</div>';

		echo $args['after_widget'];
	}



	/* Back-end widget form
	 *------------------------------------------------------------*/

	public function form( $instance ){
		$theme_settings = hygge_theme_settings();

		echo '<div class="hygge-widget-about">';

		// Image
		$img = !empty( $instance['img'] ) ? $instance['img'] : '';
		echo '<p>';
			echo '<label class="hygge-big" for="'.$this->get_field_id( 'img' ).'">'. esc_html__( 'Image:', 'hygge' ).'</label>';

			echo '<button type="button" class="button hygge-upload-image-button"><span class="dashicons dashicons-admin-media"></span> ' . esc_html__('Add Image', 'hygge') . '</button>';
			echo '<a class="remove js-remove"><i class="hygge-icon-close"></i></a>';
			echo '<input class="url" id="'.$this->get_field_id( 'img' ).'" name="'.$this->get_field_name( 'img' ).'" type="hidden" placeholder="'. esc_html__( 'http://', 'hygge' ).'" value="'.esc_attr( $img ).'">';
			echo '<img class="hygge-image-preview" src="'.$img.'">';

		echo '</p>';

		// Title
		$title = !empty( $instance['title'] ) ? $instance['title'] : '';
		echo '<p>';
		echo '<label class="hygge-big" for="'.$this->get_field_id( 'title' ).'">'. esc_html__( 'Title:', 'hygge' ).'</label>';
		echo '<input class="widefat" id="'.$this->get_field_id( 'title' ).'" name="'.$this->get_field_name( 'title' ).'" type="text" placeholder="'. esc_attr__( 'Widget Title', 'hygge' ).'" value="'.esc_attr( $title ).'">';
		echo '</p>';

		// Description
		$description = !empty( $instance['description'] ) ? $instance['description'] : '';
		echo '<p>';
			echo '<label class="hygge-big" for="'.$this->get_field_id( 'description' ).'">'. esc_html__( 'Description:', 'hygge' ).'</label>';
			echo '<textarea class="widefat" rows="6" id="'.$this->get_field_id( 'description' ).'" name="'.$this->get_field_name( 'description' ).'" placeholder="'. esc_html__( 'Description Text', 'hygge' ).'">'. $description .'</textarea>';
		echo '</p>';

		// Social shortcode
		$social = !empty( $instance['social'] ) ? $instance['social'] : '';
		echo '<p>';
		echo '<label class="hygge-big" for="'.$this->get_field_id( 'social' ).'">'. esc_html__( 'Social Shortcode:', 'hygge' ).'</label>';
		echo '<input class="widefat" id="'.$this->get_field_id( 'social' ).'" name="'.$this->get_field_name( 'social' ).'" type="text" placeholder="'. esc_html__( '[hygge_social]', 'hygge' ).'" value="'.esc_attr( $social ).'">';
		echo wp_kses( __( '<small>Enter the social shortcode here.<br><strong>Hygge: Social</strong> widget provides generator for the social shortcode.</small>', 'hygge' ), array( 'small' => array(), 'br' => array(), 'strong' => array() ) );
		echo '</p>';

		// Button
		$button_text = !empty( $instance['button_text'] ) ? $instance['button_text'] : '';
		$button_link = !empty( $instance['button_link'] ) ? $instance['button_link'] : '';
		echo '<p class="custom-button">';
			echo '<label class="hygge-big" for="'.$this->get_field_id( 'button_text' ).'">'. esc_html__( 'Custom Button:', 'hygge' ).'</label>';
			echo '<input id="'.$this->get_field_id( 'button_text' ).'" name="'.$this->get_field_name( 'button_text' ).'" type="text" placeholder="'. esc_html__( 'Button Text', 'hygge' ).'" value="'.esc_attr( $button_text ).'">';
			echo '<input id="'.$this->get_field_id( 'button_link' ).'" name="'.$this->get_field_name( 'button_link' ).'" type="text" placeholder="'. esc_html__( 'http://', 'hygge' ).'" value="'.esc_attr( $button_link ).'">';
		echo '</p>';

		echo '</div>';

	}



	/* Sanitize widget form values as they are saved
	 *------------------------------------------------------------*/

	public function update( $new_instance, $old_instance ){

		$instance = array();
		$instance['img'] = !empty( $new_instance['img'] ) ? esc_html(strip_tags( $new_instance['img'] )) : '';
		$instance['title'] = !empty( $new_instance['title'] ) ? esc_html(strip_tags( $new_instance['title'] )) : '';
		$instance['description']= !empty( $new_instance['description'] ) ? esc_html(strip_tags( $new_instance['description'] )) : '';
		$instance['social'] = !empty( $new_instance['social'] ) ? esc_html(strip_tags( $new_instance['social'] )) : '';
		$instance['button_text']= !empty( $new_instance['button_text'] ) ? esc_html(strip_tags( $new_instance['button_text'] )) : '';
		$instance['button_link']= !empty( $new_instance['button_link'] ) ? esc_html(strip_tags( $new_instance['button_link'] )) : '';

		return $instance;
	}

} // class Hygge_About_Author
