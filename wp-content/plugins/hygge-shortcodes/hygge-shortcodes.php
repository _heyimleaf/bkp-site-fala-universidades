<?php
/*

Plugin Name: Hygge Shortcodes
Plugin URI:
Description: Bundled shortcodes for KORRA's Hygge theme. Contains only the functional part. Styles and front-end behavior is covered by the theme.
Version: 1.0.2
Author: KORRA
Author URI: http://korra.io
Text Domain: hygge

Copyright 2018 KORRA

*/



/*------------------------------------------------------------
 * Include external shortcode files
 *------------------------------------------------------------*/

include_once( plugin_dir_path( __FILE__ ) . 'blog.php' );



/*------------------------------------------------------------
 * Shortcode: Small Separator
 *------------------------------------------------------------*/

/*
[hygge_small_separator]
*/

if( !function_exists('hygge_small_separator') ){
function hygge_small_separator($atts, $content = null) {

	return '<hr class="hygge-separator--small">';
}
}

add_shortcode('hygge_small_separator', 'hygge_small_separator');



/*------------------------------------------------------------
 * Shortcode: Icon
 *------------------------------------------------------------*/

/*
[hygge_icon icon="ICON_NAME"]

@icon
	Icon class name without the theme-prefix
*/

if( !function_exists('hygge_icon') ){
function hygge_icon($atts, $content = null) {

	extract( shortcode_atts( array(
		'icon' => ''
	), $atts ) );

	// support older icon format: [hygge_icon]NAME[/icon]
	if( !$icon && $content ){
		$icon = $content;
	}

	return $icon ? "<i class='hygge-icon-{$icon}'></i>" : false;
}
}

add_shortcode('hygge_icon', 'hygge_icon');






/*------------------------------------------------------------
 * Shortcode: Social Links
 *------------------------------------------------------------*/

/*
[hygge_social icon|title|url icon2|title2|url2 ]

@icon - icon to use without 'hygge-icon-'
@title - textual label of network
@url - full url to profile
*/

if( !function_exists('hygge_social') ){
function hygge_social($atts, $content = null) {

	if( count( $atts ) == 0 ){
		return;
	}



	$output = '';
	$output .= '<span class="social-nav">';

	foreach ($atts as $att) {

		$att = str_replace( '%20', ' ', $att );
		$att = explode( '|', $att);

		$att[0] = strtolower($att[0]);
		$class = '';
		if( $att[0] ){
			$class .= ' icon--true';
		}
		$output .= "<a class='item label js-skip-ajax {$class}' target='_blank'  href='{$att[2]}'>";
			if( $att[0] ){
				$output .= "<i class='hygge-icon-{$att[0]}'></i>";
			}
			if( $att[1] ){
				$output .= "<span>{$att[1]}</span>";
			}
		$output .= '</a>';
	}

	$output .= '</span>';

	return $output;
}
}

add_shortcode('hygge_social', 'hygge_social');






/*------------------------------------------------------------
 * Shortcode: Empty Spacing
 *------------------------------------------------------------*/

/*
[hygge_spacing height="15"]

@height
	Height for the spacing. Defaults to 15
*/

if( !function_exists('hygge_spacing') ){
function hygge_spacing($atts) {

	extract( shortcode_atts( array(
		'height' => '0'
	), $atts ) );

	return '<hr class="hygge-spacing" style="height:' . esc_attr( $height ) . 'px;">';
}
}

add_shortcode('hygge_spacing', 'hygge_spacing');





/*------------------------------------------------------------
 * Shortcode: Hygge Scroll-To Anchor
 *------------------------------------------------------------*/

/*
[hygge-anchor id="marker-1"]

@id
	#ID to link to for scrolling
*/

if( !function_exists('hygge_anchor') ){
function hygge_anchor($atts) {

	extract( shortcode_atts( array(
		'id' => ''
	), $atts ) );

	// Ommit # on start if user input it
	if( substr($id, 0, 1) === '#' ){
		$id = substr($id, 1);
	}

	if( !$id ){
		return;
	}

	return "<hr class='hygge-anchor' id='" . esc_attr( $id ) . "'>";
}
}

add_shortcode('hygge_anchor', 'hygge_anchor');







/*------------------------------------------------------------
 * Shortcode: Ad Widget
 *------------------------------------------------------------*/

/*
[hygge_ad_widget type="square"]

@type
	What widget area to use.
	- square [default]
	- wide
*/

if( !function_exists('hygge_ad_widget') ){
function hygge_ad_widget($atts) {

	extract( shortcode_atts( array(
		'type' => 'square'
	), $atts ) );

	$output = '';

	if( is_active_sidebar("widget-area-ad-{$type}") ){
		ob_start();
		dynamic_sidebar("widget-area-ad-{$type}");
		$output = ob_get_clean();
	}

	return $output;
}
}

add_shortcode('hygge_ad_widget', 'hygge_ad_widget');





/*------------------------------------------------------------
 * Shortcode: Rich Image
 *------------------------------------------------------------*/

/*
[hygge_rich_image image="" title="" description=""]
*/

if( !function_exists('hygge_rich_image') ){
function hygge_rich_image($atts) {

	extract( shortcode_atts( array(
		'title' => '',
		'description' => '',
		'image' => '',
	), $atts ) );

	$output = '';

	if( !$image ){
		return false;
	}

	$attr = "";
	$image = explode( "|", $image );

	if( array_key_exists(1, $image) ){
		$attr .= ' width="' . esc_attr( $image[1] ) . '" ';
	}
	if( array_key_exists(2, $image) ){
		$attr .= ' height="' . esc_attr( $image[2] ) . '" ';
	}



	$output .= "<div class='rich-image'>";
	$output .= "<div class='rich-image__inwrap'>";

		$output .= "<img src='" . esc_url($image[0]) . "' alt='" . esc_attr($title) . " " . esc_attr($description) . "' " . $attr . ">";

		if( $title || $description ){
			$output .= "<div class='rich-image__text grey-mode'>";
				if( $title ){
					$output .= "<h4 class='description-heading'>" . esc_html( $title ) . "</h4>";
				}
				if( $description ){
					$output .= "<p>" . esc_html( $description ) . "</p>";
				}
			$output .= "</div>";
		}

	$output .= "</div>";
	$output .= "</div>";

	return $output;
}
}

add_shortcode('hygge_rich_image', 'hygge_rich_image');






/*------------------------------------------------------------
 * Shortcode: Custom Banner
 *------------------------------------------------------------*/

/*
[hygge_custom_banner image="" title="" description=""]
*/

if( !function_exists('hygge_custom_banner') ){
function hygge_custom_banner($atts) {

	extract( shortcode_atts( array(
		'title' => '',
		'title_label' => '',
		'description' => '',
		'image' => '',
		'height' => '',
		'link_text' => '',
		'link_url' => '',
		'link_target' => '',
	), $atts ) );

	$output = '';

	if( !$image ){
		return false;
	}

	$attr = "";
	$image = explode( "|", $image );
	if( array_key_exists(1, $image) ){
		$attr .= " width={$image[1]} ";
	}
	if( array_key_exists(2, $image) ){
		$attr .= " height={$image[2]} ";
	}

	$target = $link_target == "on" ? "target='_blank'" : "";
	$height = $height && is_numeric($height) ? "height:{$height}px;" : "";

	$output .= "<div class='custom-banner js-hygge-even-height' style='background-image: url({$image[0]});'>";
	$output .= "<div class='custom-banner__inwrap'>";

		$output .= "<img src='" . esc_url($image[0]) . "' alt='" . esc_attr($title) . " " . esc_attr($description) . "' style='" . esc_attr($height) . "' " . esc_attr($attr) . ">";

		if( $title || $description || ( $link_text && $link_url ) ){
			$output .= "<div class='custom-banner__text js-hygge-even-height'>";
			$output .= "<div class='custom-banner__text__inwrap'>";

				if( $title_label ){
					$output .= "<div class='label--italic'>" . esc_html( $title_label ) . "</div>";
				}

				if( $title ){
					$output .= "<h3>" . esc_html( $title ) . "</h3>";
				}
				if( $description ){
					$output .= "<p>" . esc_html( $description ) . "</p>";
				}
				if( $link_text && $link_url ){
					$output .= "<a class='label link--underline' href='" . esc_url($link_url) . "' {$target}>" . esc_html( $link_text ) . "</a>";
				}
			$output .= "</div>";
			$output .= "</div>";
		}

	$output .= "</div>";
	$output .= "</div>";

	return $output;
}
}

add_shortcode('hygge_custom_banner', 'hygge_custom_banner');






/*------------------------------------------------------------
 * Shortcode: Note
 *------------------------------------------------------------*/

/*
[hygge_sideblock]Note Content[/hygge_sideblock]

@content
	Content of the sideblock
*/

if( !function_exists('hygge_sideblock') ){
function hygge_sideblock($atts, $content = null) {

	return '<div class="hygge-sideblock">'. do_shortcode($content) .'</div>';
}
}

add_shortcode('hygge_sideblock', 'hygge_sideblock');



/*------------------------------------------------------------
 * Shortcode: Full Width
 *------------------------------------------------------------*/

/*
[hygge_fullwidth bg=""]Full Width Content[/fullwidth]

@content
	Custom content
@bg
	background css string
*/

if( !function_exists('hygge_fullwidth') ){
function hygge_fullwidth($atts, $content = null) {

	extract( shortcode_atts( array(
		'bg' => ''
		// ,'bg_light' => ''
	), $atts ) );

	$style = '';
	$class = '';

	if ($bg) {
		$style .= 'background:'.$bg.';';
	}

	// if ( $bg_light == 'false' ) {
	// 	$class .= ' dark-mode';
	// }

	return '<div class="hygge-fullwidth ' . $class . '" style="' . $style . '">' . do_shortcode( $content) . '</div>';

}
}

add_shortcode('hygge_fullwidth', 'hygge_fullwidth');





/*------------------------------------------------------------
 * Shortcode: Taxonomy Blog
 * Customize the outter layout a bit with Title Sideblock
 * and pass to standard [hygge_blog] shortcode
 * Prioritize category, then tag.
 * If none exist, fallback to standard blog
 *------------------------------------------------------------*/

/*
[hygge_taxonomy_blog posts="8" style="" category="" tag="" sort_by="date" sort_order="DESC" extra_class=""]

@posts:
	- number
	- -1: unlimited / all
	- empty: [default] WP setting for posts per page
@style:
	-list
	-list_highlight
@category:
	-category slug: show only chosen categories, separated by coma
	-empty: all categories selected
@tag:
	-tag slug: show only chosen tags, separated by coma
	-empty: all categories selected
@sort_by:
	-date [default]
	-comment_count
	-[all worpdress order & orderby params supported]
@sort_order:
	-ASC
	-DESC [default]
@extra_class:
	-extra class to add on loop-container
*/

if( !function_exists('hygge_taxonomy_blog') ){
function hygge_taxonomy_blog($atts, $content = null) {

	extract( shortcode_atts( array(
		'posts' => get_option('posts_per_page')
		,'style' => ''
		,'category' => ''
		,'tag' => ''
		,'sort_by' => ''
		,'sort_order' => ''
		,'extra_class' => ''
	), $atts ) );

	$output = '';

	$tax = '';
	if( $category ){
		$tax = get_term_by('slug', $category, 'category');
	}elseif( $tag ){
		$tax = get_term_by('slug', $tag, 'post_tag');
	}
	$subtitle = $tax->taxonomy == 'category' ? esc_html__('Category', 'hygge') : esc_html__('Tag', 'hygge');
	$color = hygge_get_category_color($tax->term_id);

	if( $tax ){
		$output .= "<div class='hygge-sideblock hygge-sideblock--tax-title dark-mode' style='background-color:{$color};'>";
			$output .= "<h2>{$tax->name}<span class='title--striped'>{$subtitle}</span></h2>";
			if( $tax->description ){
				$output .= "<p class='description-heading'>{$tax->description}</p>";
			}
		$output .= "</div>";
	}


	// Check if necessary and display more link
	$posts = $posts ? $posts : get_option('posts_per_page');
	$taxLink = '';
	if( $posts > -1 && $posts < $tax->count ){
		$taxLink = get_category_link( $tax->term_id );
	}

	$output .= do_shortcode("[hygge_blog posts='{$posts}' style='{$style}' category='{$category}' tag='{$tag}' sort_by='{$sort_by}' sort_order='{$sort_order}' extra_class='{$extra_class}' more_link='{$taxLink}']");

	return $output;
}
}

add_shortcode('hygge_taxonomy_blog', 'hygge_taxonomy_blog');
