<?php

/*------------------------------------------------------------
 * Add Shortcode
 *------------------------------------------------------------*/

add_shortcode('hygge_blog', 'hygge_blog');



/*------------------------------------------------------------
 * Shortcode: Blog
 * Currently returns loop-templates via output buffering.
 *------------------------------------------------------------*/

/*
[hygge_blog title="" posts="8" style="" category="" tag="" featured_exclude="" sort_by="date" sort_order="DESC" interval="4000" extra_class=""]

@posts:
	- number
	- -1: unlimited / all
	- empty: [default] WP setting for posts per page
@style:
	-list
	-list-highlights
	-grid
	-grid-3-1
	-grid-3-2
	-masonry
	-widget-list
	-widget-slider
	-filmstrip
	-featured
	-related
@category:
	-category slug: show only chosen categories, separated by coma
	-empty: all categories selected
@tag:
	-tag slug: show only chosen tags, separated by coma
	-empty: all categories selected
@featured_exclude:
	-on: exclude
	-off: don't exclude
@sort_by:
	-date [default]
	-comment_count
	-[all worpdress order & orderby params supported]
@sort_order:
	-ASC
	-DESC [default]
@interval:
	-n
@extra_class:
	-extra class to add on loop-container

*/

function hygge_blog($atts, $content = null) {
	$theme_settings = hygge_theme_settings();

	extract( shortcode_atts( array(
		 'title' => ''
		,'posts' => get_option('posts_per_page')
		,'style' => 'grid'
		,'category' => ''
		,'tag' => ''
		,'featured_exclude' => ''
		,'sort_by' => ''
		,'sort_order' => ''
		,'interval' => '0'
		,'extra_class' => ''
		,'link_text' => ''
		,'link_url' => '#'
		,'ads' => '0'
	), $atts ) );

	$output = '';
	$display_style = $style;
	$ads_i = 0;
	$ads_type = 'wide';

	$posts = $posts ? $posts : get_option('posts_per_page');

	$meta_key = '';

	// Must contain featured images
	if( $display_style == 'featured' || $display_style == 'carousel' || $display_style == 'widget-slider' || $display_style == 'filmstrip' ){
		$meta_key = '_thumbnail_id';
	}

	// Exclude Featured Posts
	$featured_post_ids = '';
	if( $featured_exclude == 'on' ) {
		// WPML convert to translated tag ID
		$featured_tag = apply_filters( 'wpml_object_id', $theme_settings['featured_tag'], 'post_tag', TRUE );

		$featured_post_ids = get_posts( array(
			'numberposts'	=> $theme_settings['featured_count'],
			'tag_id'			=> $featured_tag,
			'meta_key'		=> '_thumbnail_id', // contain featured image
			'fields'			=> 'ids', // Only get post IDs
		));
	}


	$query = new WP_Query( array(
		 'posts_per_page' => $posts
		,'category_name' => $category
		,'tag' => $tag
		,'orderby' => $sort_by
		,'order' => $sort_order
		,'ignore_sticky_posts' => 1
		,'meta_key' => $meta_key
		,'post__not_in' => $featured_post_ids
	));

	// d($query);



	/* Loop Classes
	 *------------------------------------------------------------*/

	$loop_classes = '';
	$loop_classes .= ' loop-container--style-'.$display_style;

	// add classes for masonry
	if( $display_style == 'masonry' || $display_style == 'masonry_image' ){
		$loop_classes .= ' loop-container--masonry js-loop-is-masonry';
	}

	if( $display_style == 'carousel' ){
		$loop_classes .= ' hygge-carousel js-hygge-carousel';
	}

	$loop_classes .= ' '.$extra_class;



	// Ads: set ads type
	if( $display_style == 'masonry' || $display_style == 'grid' || $display_style == 'grid-3-2' || $display_style == 'grid-3-1' || $display_style == 'widget-list' || $display_style == 'widget-slider' ){

		$ads_type = 'square';
	}




	/*	Loop Data
	 *------------------------------------------------------------*/

	$loop_data = '';
	$loop_data .= ' data-interval="'.$interval.'"';

	if( $display_style == 'slider' || $display_style == 'widget-slider' ){
		$loop_classes .= ' hygge-slider hygge-slider--blog';
	}





/*------------------------------------------------------------*
 *	Print it!
 *------------------------------------------------------------*/

	if( $query->have_posts() ):


		$output .= '<div class="hygge-blog hygge-blog--style-'.$display_style.'">';

		if( $title ){
			$output .= "<div class='hygge-blog__title'>";
				$output .= ( do_shortcode("<h4>{$title}</h4>") );
				$output .= "<hr class='hygge-separator--small' />";
			$output .= "</div>";
		}

		// Filmstrip Wrapper
		if( $display_style == 'filmstrip' ){
			$output .= '<div class="js-filmstrip blog-filmstrip" data-interval="'.$interval.'">';
			$output .= '<div class="blog-filmstrip-wrapper">';
		}

		$output .= '<div class="loop-container '.$loop_classes.'" '.$loop_data.'>';



		/*	Slider / Carosuel: Item Wrapper Start
		 *------------------------------------------------------------*/

		if( $display_style == 'carousel' ){
			$output .= '<div class="hygge-carousel__inwrap">';
			$output .= '<div class="hygge-carousel__items">';
			$output .= '<div class="hygge-carousel__items-group">';
		}

		if( $display_style == 'slider' || $display_style == 'widget-slider' ){
			$output .= '<div class="hygge-slider__items">';
		}



		/*	Query Posts
		 *------------------------------------------------------------*/


		while( $query->have_posts() ) : $query->the_post();

			ob_start();

			get_template_part( 'loop-item', $display_style );

			$ads_i++;
			if( $ads && $ads_i % $ads == 0 ){
				if( is_active_sidebar("widget-area-ad-{$ads_type}") ){
					echo '<article class="widget-area--blog hygge-animate-appearance post">';
					echo '<div class="post__inwrap">';
					dynamic_sidebar("widget-area-ad-{$ads_type}");
					echo '</div>';
					echo '</article>';
				}
			}

			$output .= ob_get_clean();

		endwhile;



		/*	Carousel: Item Wrapper End
		 *------------------------------------------------------------*/

		if( $display_style == 'carousel' ){
			$output .= '</div>';
			$output .= '</div>';
			$output .= '</div>';
		}

		if( $display_style == 'slider' || $display_style == 'widget-slider'){
			$output .= '</div>';
		}

		/* Pager: Carousel, Slider, Widget-Slider
		 *------------------------------------------------------------*/

		if( ( $display_style == 'carousel' || $display_style == 'slider' || $display_style == 'widget-slider' ) && $query->found_posts > 1 && $posts > 1 ){
			$output .= '<div class="hygge-ui-pager">';

			$i = 0;
			foreach( $query->posts as $post){
				$active_class = $i === 0 ? 'active' : '';

				$output .= '<div class="hygge-ui-pager__item js-hygge-ui-pager-item '.$active_class.'" data-id="'.$i.'"></div>';

				$i++;
			}

			$output .= '<div class="hygge-ui-pager__item hygge-ui-pager__active-marker"></div>';

			$output .= '</div>';
		}



		/* Arrows: Slider, Widget-Slider
		 *------------------------------------------------------------*/
		//
		// if( ( $display_style == 'slider' || $display_style == 'widget-slider' ) && $query->found_posts > 1 && $posts > 1 ){
		// 	$output .= '<div class="hygge-ui-arrows dark-mode">';
		// 	$output .= '<a class="hygge-ui-arrow hygge-ui-arrow--prev js-hygge-ui-arrow--prev" href="#"><i class="hygge-icon-arrow-left"></i></a>';
		// 	$output .= '<a class="hygge-ui-arrow hygge-ui-arrow--next js-hygge-ui-arrow--next" href="#"><i class="hygge-icon-arrow-right"></i></a>';
		// 	$output .= '</div>';
		// }



		$output .= '</div>'; // /.loop-container

		// Filmstrip Wrapper
		if( $display_style == 'filmstrip' ){
			$output .= '</div>';
			$output .= '</div>';
		}

		if( $link_text ){
			$output .= '<div class="hygge-blog__more">';
				$output .= "<a class='label link--underline' href='{$link_url}' title='{$link_text}'>{$link_text}</a>";
			$output .= '</div>';
		}

		$output .= '</div>'; // /.hygge-blog


	endif;


	wp_reset_postdata();
	return $output;
}
