<?php
/*

Plugin Name: Hygge Share
Plugin URI:
Description: Bundled Share for KORRA's Hygge theme. Contains only the functional part. Styles and front-end behavior is covered by the theme.
Version: 1.0.2
Author: KORRA
Author URI: http://korra.io
Text Domain: hygge

Copyright 2018 KORRA

*/


if( !function_exists('hygge_share') ){
function hygge_share() {

$theme_settings = hygge_theme_settings();

if( $theme_settings['sharing_count'] ):

if( has_post_thumbnail() ){
	$share_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'hygge-xl' );
	$share_image = $share_image[0];
	$share_image_portrait = wp_get_attachment_image_src( get_post_thumbnail_id(), 'hygge-portrait-m' );
	$share_image_portrait = $share_image_portrait[0];
}else{
	$share_image = '';
	$share_image_portrait = '';
}

$share_excerpt = esc_html(strip_tags( get_the_excerpt(), '<b><i><strong><a>' ));


?>

<div class="post__share">

	<div class="social-nav social-nav--count-<?php echo $theme_settings['sharing_count']; ?>">
		<?php if( $theme_settings['sharing']['email'] ): ?>
				<a title="<?php esc_html_e( 'Share on Email', 'hygge' ); ?>" class="item label js-skip-ajax" href="mailto:?subject=<?php echo ( rawurlencode( get_the_title() ) ); ?>&amp;body=<?php echo ( rawurlencode ( $share_excerpt . ' ' . get_the_permalink() ) ); ?>">
					<i class="hygge-icon-envelope-o"></i>
					<span><?php esc_html_e('Email', 'hygge'); ?></span>
				</a>
		<?php endif;

		if( $theme_settings['sharing']['facebook'] ): ?>
				<a title="<?php esc_html_e( 'Share on Facebook', 'hygge' ); ?>" class="item label js-sharer js-skip-ajax" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo( rawurlencode( get_the_permalink() ) ); ?>">
					<i class="hygge-icon-facebook"></i>
					<span><?php esc_html_e('Facebook', 'hygge'); ?></span>
				</a>
		<?php endif;

		if( $theme_settings['sharing']['twitter'] ): ?>
				<a title="<?php esc_html_e( 'Share on Twitter', 'hygge' ); ?>" class="item label js-sharer js-skip-ajax" target="_blank"  href="http://twitter.com/intent/tweet?text=<?php echo( rawurlencode( get_the_title() ) ); ?>&amp;url=<?php echo( rawurlencode( get_the_permalink() ) ); ?>">
					<i class="hygge-icon-twitter"></i>
					<span><?php esc_html_e('Twitter', 'hygge'); ?></span>
				</a>
		<?php endif;

		if( $theme_settings['sharing']['pinterest'] ): ?>
				<a title="<?php esc_html_e( 'Share on Pinterest', 'hygge' ); ?>" class="item label js-sharer js-skip-ajax" target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php echo( rawurlencode( get_the_permalink() ) ); ?>&amp;media=<?php echo ( rawurlencode( $share_image_portrait ) ); ?>&amp;description=<?php echo( rawurlencode( get_the_title() ) ); ?>">
					<i class="hygge-icon-pinterest-p"></i>
					<span><?php esc_html_e('Pinterest', 'hygge'); ?></span>
				</a>
		<?php endif;

		if( $theme_settings['sharing']['google'] ): ?>
				<a title="<?php esc_html_e( 'Share on Google+', 'hygge' ); ?>" class="item label js-sharer js-skip-ajax" target="_blank" href="https://plus.google.com/share?url=<?php echo( rawurlencode( get_the_permalink() ) ); ?>">
					<i class="hygge-icon-google-plus"></i>
					<span><?php esc_html_e('Google+', 'hygge'); ?></span>
				</a>
		<?php endif;

		if( $theme_settings['sharing']['linkedin'] ): ?>
				<a title="<?php esc_html_e( 'Share on LinkedIn', 'hygge' ); ?>" class="item label js-sharer js-skip-ajax" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo( rawurlencode( get_the_permalink() ) ); ?>&amp;title=<?php echo( rawurlencode( get_the_title() ) ); ?>&amp;summary=<?php echo ( rawurlencode ( $share_excerpt ) );?>&amp;source=<?php echo ( rawurlencode( get_bloginfo('name') ) );?>">
					<i class="hygge-icon-linkedin"></i>
					<span><?php esc_html_e('LinkedIn', 'hygge'); ?></span>
				</a>
		<?php endif;

		if( $theme_settings['sharing']['tumblr'] ): ?>
				<a title="<?php esc_html_e( 'Share on Tumblr', 'hygge' ); ?>" class="item label js-sharer js-skip-ajax" target="_blank" href="
					http://tumblr.com/widgets/share/tool?
					canonicalUrl=<?php echo( rawurlencode( get_the_permalink() ) ); ?>&amp;posttype=photo&amp;content=<?php echo( rawurlencode( $share_image ) ); ?>&amp;title=<?php echo( rawurlencode( get_the_title() ) ); ?>&amp;caption=<?php echo( rawurlencode( $share_excerpt ) );?>&amp;source=<?php echo( rawurlencode( get_bloginfo('name') ) );?>&amp;via=true ">
					<i class="hygge-icon-tumblr"></i>
					<span><?php esc_html_e('Tumblr', 'hygge'); ?></span>
				</a>
		<?php endif; ?>

	</div>
</div>

<?php
	endif;
}
}
?>
