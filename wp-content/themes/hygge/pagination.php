<?php

$theme_settings = hygge_theme_settings();

// if called from custom page_blog query, use $blog_pagination defined there
if( isset($wp_query->hygge_page_blog_pagination) ){
	$pagination_type = $wp_query->hygge_page_blog_pagination;
// if index
}elseif( is_home() ){
	$pagination_type = esc_attr( $theme_settings['blog_home_pagination'] );
// if archive pages
}elseif( is_archive() || is_search() ){
	$pagination_type = $theme_settings['archive_pagination'];
}



if( $pagination_type == 'off' ){
	return;
}


/* Classic Pagination
 *------------------------------------------------------------*/

if( $pagination_type == 'pagination' ){
	echo '<div class="pagination pagination__classic label">';
		$pagination = hygge_wp_pagination();
		if( $pagination ){
			echo "<div>{$pagination}</div>";
		}
	echo '</div>';
}



/* Load More Button
 *------------------------------------------------------------*/

if( $pagination_type == 'load_more' ){
	echo '<div class="pagination pagination__load-more">';
		hygge_wp_load_more();
	echo '</div>';
}



/* Custom Button Link
 *------------------------------------------------------------*/

if( $pagination_type == 'custom_link' ){
	$link_text = $wp_query->hygge_more_button_text;
	$link_url = $wp_query->hygge_more_button_link;

	if( $link_text ){
		echo '<div class="hygge-blog__more">';
			echo "<a class='label link--underline' href='{$link_url}' title='{$link_text}'>{$link_text}</a>";
		echo '</div>';
	}
}
