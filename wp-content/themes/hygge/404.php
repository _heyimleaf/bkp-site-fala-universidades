<?php
	$theme_settings = hygge_theme_settings();
	get_header();

	$post_classes = '';
	$post_classes .= ' article-single article-single--page article--widget-area article-single--404';
?>

<article class="<?php echo esc_attr($post_classes); ?>" role="main">

	<div class="main-content">

		<div class="post__content">
			<div class="hygge-contentwidth tac">
				<?php
					if( is_active_sidebar('page-404-area') ){
						echo '<div class="widget-area--content widget-area--main-content">';
							dynamic_sidebar('page-404-area');
						echo '</div>';
					}else{
						echo '<p class="description-heading">' . esc_html__( 'Unfortunately, this page doesn\'t exist.', 'hygge' ) . '</p>';
						get_template_part('searchform');
					}
				?>
			</div>

		</div>

	</div>

</article>



<?php get_footer(); ?>
