<?php

/*------------------------------------------------------------
* Customized Style
* Fallback when WP-LESS missing
* Get settings from WP Customizer, compile styles and return them
* Function is called from functions.php as wp_add_inline_style()
*------------------------------------------------------------*/

function hygge_customized_style() {
$theme_settings = hygge_theme_settings();
ob_start();


if( (int)$theme_settings['sidebar_width'] != 360 ): ?>
@media only screen and (min-width: 961px) {
  .hygge-sidebar--true .hygge-loader--body {
    margin-left: calc(-<?php echo esc_html( (int)$theme_settings['sidebar_width'] ); ?>px/2 - 35px);
  }

	.hygge-layout-sidebar {
		flex-basis: <?php echo esc_html( (int)$theme_settings['sidebar_width'] ); ?>px;
	}

	.hygge-layout-sidebar .sidebar__content {
		width: calc(<?php echo esc_html( (int)$theme_settings['sidebar_width'] ); ?>px - 60px);
	}

	.hygge-sidebar--true .post__share--sticky.is_stuck {
		<?php echo hygge_get_rtl('right') ?>: <?php echo esc_html( (int)$theme_settings['sidebar_width'] ); ?>px;
	}
}
<?php endif;



if( (int)$theme_settings['sideblock_width'] != 360 ): ?>
.post__meta--single small {
  max-width: <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px;
}

.hygge-lightbox .hygge-loader {
  margin-left: calc( <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px/2 + 120px/2 - 15px/2);
}
.hygge-lightbox__info {
  width: <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px;
}

@media only screen and (min-width: 961px) {
	.rich-image__text {
		width: calc( <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px + 50px );
	}

	.hygge-sidebar--true .hygge-fullwidth {
		margin-<?php echo hygge_get_rtl('left') ?>: calc( 50% - 50vw + <?php echo esc_html( (int)$theme_settings['sidebar_width'] ); ?>px/2 );
	}
	.post__content .hygge-fullwidth {
		width: calc( 100% - 50% + 50vw - <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px/2 );
		margin-<?php echo hygge_get_rtl('left') ?>: calc( 50% - 50vw - <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px/2 );
	}
	.hygge-sidebar--true .post__content .hygge-fullwidth {
		width: calc( 100% - 50% + 50vw + <?php echo esc_html( (int)$theme_settings['sidebar_width'] ); ?>px/2 - <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px/2 );
		margin-<?php echo hygge_get_rtl('left') ?>: calc( 50% - 50vw + <?php echo esc_html( (int)$theme_settings['sidebar_width'] ); ?>px/2 - <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px/2 );
	}

}

@media only screen and (min-width: 1400px) {
	.post__content,
	.post__comments__inwrap {
		padding-<?php echo hygge_get_rtl('left') ?>: calc( <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px + 60px );
	}

	.hygge-sideblock,
  .hygge-sideblock + .hygge-sideblock-placeholder {
    width: calc( <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px );
    margin-<?php echo hygge_get_rtl('left') ?>: calc( -<?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px - 60px );
  }

	.hygge-contentwidth {
		margin-<?php echo hygge_get_rtl('left') ?>: calc( -<?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px - 60px );
	}
}

@media only screen and (min-width: 1650px) {
	.post__content,
	.post__comments__inwrap {
		padding-<?php echo hygge_get_rtl('left') ?>: calc( <?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px + 100px );
	}

	.hygge-sideblock,
	.hygge-sideblock + .hygge-sideblock-placeholder {
		margin-<?php echo hygge_get_rtl('left') ?>: calc( -<?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px - 100px );
	}

	.hygge-contentwidth {
		margin-<?php echo hygge_get_rtl('left') ?>: calc( -<?php echo esc_html( (int)$theme_settings['sideblock_width'] ); ?>px - 100px );
	}
}
<?php endif;



if( $theme_settings['bg_body'] != '#ffffff' ): ?>
body {
	background: <?php echo hygge_split_bg_setting($theme_settings['bg_body'],'bg') ?>;
	-webkit-background-size: <?php echo hygge_split_bg_setting($theme_settings['bg_body'],'bg_size') ?>;
	background-size: <?php echo hygge_split_bg_setting($theme_settings['bg_body'],'bg_size') ?>;
}
<?php endif;

if( $theme_settings['bg_header'] != '#f7f7f7' ): ?>
.header,
.hygge-nav--classic .sub-menu {
	background: <?php echo hygge_split_bg_setting($theme_settings['bg_header'],'bg') ?>;
	-webkit-background-size: <?php echo hygge_split_bg_setting($theme_settings['bg_header'],'bg_size') ?>;
	background-size: <?php echo hygge_split_bg_setting($theme_settings['bg_header'],'bg_size') ?>;
}
<?php endif;

if( $theme_settings['bg_sidebar'] != '#f7f7f7' ): ?>
.sidebar {
	background: <?php echo hygge_split_bg_setting($theme_settings['bg_sidebar'],'bg') ?>;
	-webkit-background-size: <?php echo hygge_split_bg_setting($theme_settings['bg_sidebar'],'bg_size') ?>;
	background-size: <?php echo hygge_split_bg_setting($theme_settings['bg_sidebar'],'bg_size') ?>;
}
<?php endif;

if( $theme_settings['bg_footer'] != '#f7f7f7' ): ?>
.footer__bottom {
	background: <?php echo hygge_split_bg_setting($theme_settings['bg_footer'],'bg') ?>;
	-webkit-background-size: <?php echo hygge_split_bg_setting($theme_settings['bg_footer'],'bg_size') ?>;
	background-size: <?php echo hygge_split_bg_setting($theme_settings['bg_footer'],'bg_size') ?>;
}
<?php endif;



if( strtolower( $theme_settings['colors']['color_primary']) != strtolower('#71bc91') ): ?>
/* Colors: Primary
*------------------------------------------------------------*/

a:hover,
h1 a:hover,
h2 a:hover,
h3 a:hover,
h4 a:hover,
h5 a:hover,
h6 a:hover,
.social-nav .item:hover,
.on.hygge-dropdown .hygge-dropdown__handle,
.hygge-dropdown .hygge-dropdown__handle:hover,
.hygge-dropdown .hygge-dropdown__menu ul li a[selected]:after,
.hygge-nav--mobile-btn:hover,
.header__search__toggle.on,
.header__search__toggle:hover,
#comments .comment__content a:hover,
.editor-content p a:not(.button),
.editor-content .sideblock a:hover,
.loop-container--style-widget-list article a:hover .post__title,
.loop-container--style-related .post a:hover .post__title,
.pagination__classic a:hover,
.tagcloud a:hover,
.woocommerce-loop .products .product .price ins,
.woocommerce-loop .products .product a:hover .woocommerce-loop-product__title,
.single-woocommerce .summary .price ins,
.single-woocommerce .summary .woocommerce-review-link:hover,
.shop_attributes a:hover,
.variations a:hover,
.shop_table a:hover,
#reviews .commentlist .comment .comment-text a:hover,
#reviews #respond .comment-form-rating .stars span:hover a,
#reviews #respond .comment-form-rating .stars.selected a,
.product_list_widget a:hover .post__title,
.product_list_widget .price ins,
.widget_shopping_cart .remove:hover,
.widget_shopping_cart .total .label,
.widget_rating_filter li.chosen a .star--full,
.widget_rating_filter a:hover .star--full,
.cart_totals .shipping-calculator-button,
.woocommerce-checkout-review-order-table .shipping-calculator-button
{
	color: <?php echo esc_html($theme_settings['colors']['color_primary']); ?>;
}

.hygge-ui-arrows .hygge-ui-arrow:hover,
.hygge-media-hover .hygge-media-hover__hover,
.blockquote-wrapper,
.button,
.button--black:hover,
.button--grey:hover,
input[type="submit"],
button[type="submit"],
input[type="button"],
button[type="button"],
.mejs-container .mejs-controls .mejs-button.mejs-playpause-button.mejs-pause,
.hygge-nav--classic > .menu > ul > li > a:after,
#comments .comment.bypostauthor > .comment-body .comment__image img,
#comments .pingback.bypostauthor > .comment-body .comment__image img,
#comments .comment__links a:hover,
.post__category .items a,
.null-instagram-feed p a,
.widget_hygge_image_banner .item__text,
.hygge-lightbox__close:hover,
.hygge-product-category-menu > ul > li > a:after,
.wc-tabs-wrapper .wc-tabs li a:after,
.price_slider_wrapper .button:hover,
.cart_totals .wc-proceed-to-checkout .checkout-button:hover,
.woocommerce-checkout-review-order-table .wc-proceed-to-checkout,
form.woocommerce-checkout #place_order:hover,
.hygge-ui-arrows .hygge-ui-arrow:hover,
.hygge-media-hover .hygge-media-hover__hover
{
	background-color: <?php echo esc_html($theme_settings['colors']['color_primary']); ?>;
}

<?php endif; ?>



/* Typography
*------------------------------------------------------------*/

/* Body font */
html {
	font-family: "<?php echo esc_html($theme_settings['typo']['font_body'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_body'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_body'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_body'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_body'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_body'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_body'][5]); ?>em;
}

.font-size--reset { <?php echo esc_html($theme_settings['typo']['font_body'][2]); ?>px; }

input[type="text"],
input[type="textfield"],
input[type="search"],
input[type="email"],
input[type="password"],
input[type="date"],
input[type="tel"],
input[type="url"],
textarea,
select
{
	font-family: <?php echo esc_html($theme_settings['typo']['font_body'][0]); ?>;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_body'][1],'weight') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_body'][2]); ?>px;
}



/* Navigation */
.font--navigation,
.hygge-nav--mobile .menu,
.hygge-nav--classic
{
	font-family: "<?php echo esc_html($theme_settings['typo']['font_navigation'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_navigation'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_navigation'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_navigation'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_navigation'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_navigation'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_navigation'][5]); ?>em;
}



/* Label */
.label,
.button,
.comment .comment-respond small,
.header__search input[type="text"]::-webkit-input-placeholder,
.header__search input[type="search"]::-webkit-input-placeholder,
.hygge-form-label label,
.pagination__classic,
.widget_nav_menu ul,
.widget_pages ul,
.widget_archive ul,
.widget_categories ul,
.widget_meta ul,
.widget_recent_comments ul,
.widget_recent_entries ul,
.widget_upcoming_events_widget ul,
.widget_authors ul,
.widget_product_categories ul,
.widget_rating_filter ul,
.widget_layered_nav ul,
.widget_layered_nav_filters ul,
.widget_calendar thead th,
.widget_calendar tfoot td,
.tagcloud,
.wc-tabs-wrapper .wc-tabs li a,
.woocommerce-message,
.widget_rating_filter .chosen a:before,
.widget_layered_nav .chosen a:before,
.widget_layered_nav_filters .chosen a:before,
.price_slider_wrapper .price_label,
.theme-hygge #lang_sel,
.theme-hygge #lang_sel_click,
#comments .comment__meta,
form label,
.hygge-select-wrap select,
.focused.hygge-form-label label,
.loop-container article.sticky .post__media:after,
.post__content ol > li:before,
.wp-caption-text,
.gallery-caption,
cite,
table,
.null-instagram-feed p a,
.woocommerce-loop .products .product .price,
.onsale,
.single-woocommerce .summary .price .woocommerce-price-suffix,
.shop_attributes th,
.shop_attributes td,
.variations th,
.variations td,
.shop_table th,
.shop_table td,
#reviews .commentlist .comment .meta,
.product_list_widget .price
{
	font-family: "<?php echo esc_html($theme_settings['typo']['font_label'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_label'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_label'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_label'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_label'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_label'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_label'][5]); ?>em;
}

.hygge-select-wrap select {
	text-transform: <?php echo esc_html($theme_settings['typo']['font_label'][4]); ?> !important;
}

form.woocommerce-checkout #billing_address_2_field {
	padding-top: <?php echo ((int)$theme_settings['typo']['font_label'][2] * 0.9 * (int)$theme_settings['typo']['font_label'][3]); ?>;
}



/* Label Italic */
.label--italic,
.comment-respond .logged-in-as,
.cart_totals .shipping-calculator-button,
.woocommerce-checkout-review-order-table .shipping-calculator-button
{
	font-family: "<?php echo esc_html($theme_settings['typo']['font_label_italic'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_label_italic'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_label_italic'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_label_italic'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_label_italic'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_label_italic'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_label_italic'][5]); ?>em;

}



h1, .h1 {
	font-family: "<?php echo esc_html($theme_settings['typo']['font_h1'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_h1'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_h1'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_h1'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_h1'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_h1'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_h1'][5]); ?>em;
}

@media only screen and (max-width: 599px) {
  h1, .h1 {
    font-size: <?php echo esc_html((int)$theme_settings['typo']['font_h1'][2] * 0.6); ?>px;
  }
}

.dropcap {
	font-family: "<?php echo esc_html($theme_settings['typo']['font_h1'][0]); ?>",sans-serif;
	font-size: <?php echo esc_html($theme_settings['typo']['font_h1'][2]); ?>px;
}



h2, .h2 {
	font-family: "<?php echo esc_html($theme_settings['typo']['font_h2'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_h2'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_h2'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_h2'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_h2'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_h2'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_h2'][5]); ?>em;
}

@media only screen and (max-width: 599px) {
  h2, .h2 {
    font-size: <?php echo esc_html((int)$theme_settings['typo']['font_h2'][2] * 0.6); ?>px;
  }
}




h3, .h3,
.single-woocommerce .summary .product_title {
	font-family: "<?php echo esc_html($theme_settings['typo']['font_h3'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_h3'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_h3'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_h3'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_h3'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_h3'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_h3'][5]); ?>em;
}

@media only screen and (max-width: 599px) {
  h3, .h3 {
    font-size: <?php echo esc_html((int)$theme_settings['typo']['font_h3'][2] * 0.6); ?>px;
  }
}



h4, .h4,
.hygge-wc-categories-banners li .woocommerce-loop-category__title,
#reviews #respond .comment-reply-title,
form.woocommerce-checkout h3
{
	font-family: "<?php echo esc_html($theme_settings['typo']['font_h4'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_h4'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_h4'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_h4'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_h4'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_h4'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_h4'][5]); ?>em;
}

@media only screen and (max-width: 599px) {
  h4, .h4 {
    font-size: <?php echo esc_html((int)$theme_settings['typo']['font_h4'][2] * 0.7); ?>px;
  }
}


.null-instagram-feed .widget__title ~ p a {
	top: calc( 60px + <?php echo esc_html($theme_settings['typo']['font_h4'][2]); ?>px );
}



h5, .h5,
.shop_attributes td.product-name,
.variations td.product-name,
.shop_table td.product-name,
.cart_totals .shop_table th,
.cart_totals.shop_table th,
.woocommerce-checkout-review-order-table .shop_table th,
.woocommerce-checkout-review-order-table.shop_table th,
.cart_totals .shop_table .order-total strong,
.cart_totals.shop_table .order-total strong,
.woocommerce-checkout-review-order-table .shop_table .order-total strong,
.woocommerce-checkout-review-order-table.shop_table .order-total strong
{
	font-family: "<?php echo esc_html($theme_settings['typo']['font_h5'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_h5'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_h5'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_h5'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_h5'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_h5'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_h5'][5]); ?>em;
}

@media only screen and (max-width: 1170px) {
	.post__navigation .post__navigation__item .post__navigation__text .post__title
	{
		font-family: "<?php echo esc_html($theme_settings['typo']['font_h5'][0]); ?>",sans-serif;
		font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_h5'][1],'weight') ?>;
		font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_h5'][1],'style') ?>;
		font-size: <?php echo esc_html($theme_settings['typo']['font_h5'][2]); ?>px;
		line-height: <?php echo esc_html($theme_settings['typo']['font_h5'][3]); ?>em;
		text-transform: <?php echo esc_html($theme_settings['typo']['font_h5'][4]); ?>;
		letter-spacing: <?php echo esc_html($theme_settings['typo']['font_h5'][5]); ?>em;
	}
}



h6, .h6 {
	font-family: "<?php echo esc_html($theme_settings['typo']['font_h6'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_h6'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_h6'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_h6'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_h6'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_h6'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_h6'][5]); ?>em;
}



/* Description */
.description-heading,
.highlighted-p,
.widget_calendar caption,
.single-woocommerce .summary .price
{
	font-family: "<?php echo esc_html($theme_settings['typo']['font_description'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_description'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_description'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_description'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_description'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_description'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_description'][5]); ?>em;
}

@media only screen and (max-width: 599px) {
	.description-heading,
	.highlighted-p,
	.widget_calendar caption,
	.single-woocommerce .summary .price {
    font-size: <?php echo esc_html((int)$theme_settings['typo']['font_description'][2] * 0.7); ?>px;
  }
}




/* Blockquote */
blockquote {
	font-family: "<?php echo esc_html($theme_settings['typo']['font_blockquote'][0]); ?>",sans-serif;
	font-weight: <?php echo hygge_split_font_style($theme_settings['typo']['font_blockquote'][1],'weight') ?>;
	font-style: <?php echo hygge_split_font_style($theme_settings['typo']['font_blockquote'][1],'style') ?>;
	font-size: <?php echo esc_html($theme_settings['typo']['font_blockquote'][2]); ?>px;
	line-height: <?php echo esc_html($theme_settings['typo']['font_blockquote'][3]); ?>em;
	text-transform: <?php echo esc_html($theme_settings['typo']['font_blockquote'][4]); ?>;
	letter-spacing: <?php echo esc_html($theme_settings['typo']['font_blockquote'][5]); ?>em;
}



h1 + .hygge-separator--small {
	margin-top: -<?php echo ((int)$theme_settings['typo']['font_h1'][2] * 0.5); ?>px;
}
h2 + .hygge-separator--small {
	margin-top: -<?php echo ((int)$theme_settings['typo']['font_h2'][2] * 0.4); ?>px;
}
h3 + .hygge-separator--small {
	margin-top: -<?php echo ((int)$theme_settings['typo']['font_h3'][2] * 0.3); ?>px;
}
h4 + .hygge-separator--small {
	margin-top: -<?php echo ((int)$theme_settings['typo']['font_h4'][2] * 0.1); ?>px;
}
h5 + .hygge-separator--small {
	margin-top: -<?php echo ((int)$theme_settings['typo']['font_h5'][2] * 0.1); ?>px;
}
h6 + .hygge-separator--small {
	margin-top: -<?php echo ((int)$theme_settings['typo']['font_h6'][2] * 0.1); ?>px;
}

@media only screen and (min-width: 600px) {
	h1 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h1'][2] * 0.8); ?>px;
	}
	h2 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h2'][2] * 0.7); ?>px;
	}
	h3 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h3'][2] * 0.6); ?>px;
	}
	h4 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h4'][2] * 0.2); ?>px;
	}
	h5 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h5'][2] * 0.2); ?>px;
	}
	h6 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h6'][2] * 0.2); ?>px;
	}

	<!-- Hygge Sideblock override  -->

	.hygge-sideblock h1 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h1'][2] * 0.5); ?>px;
	}
	.hygge-sideblock h2 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h2'][2] * 0.4); ?>px;
	}
	.hygge-sideblock h3 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h3'][2] * 0.3); ?>px;
	}
	.hygge-sideblock h4 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h4'][2] * 0.1); ?>px;
	}
	.hygge-sideblock h5 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h5'][2] * 0.1); ?>px;
	}
	.hygge-sideblock h6 + .hygge-separator--small {
		margin-top: -<?php echo ((int)$theme_settings['typo']['font_h6'][2] * 0.1); ?>px;
	}
}



<?php
	return ob_get_clean();
}
