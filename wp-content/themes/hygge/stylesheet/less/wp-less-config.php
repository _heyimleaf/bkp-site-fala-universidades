<?php

function hygge_wp_less_config(){
	$theme_settings = hygge_theme_settings();

	$sidebar_width = $theme_settings['sidebar_width'];
	$sideblock_width = $theme_settings['sideblock_width'];

	$color_primary = $theme_settings['colors']['color_primary'];

	$color_white = $theme_settings['colors']['color_white'];
	$color_grey_pale = $theme_settings['colors']['color_grey_pale'];
	$color_grey_light = $theme_settings['colors']['color_grey_light'];
	$color_grey_label = $theme_settings['colors']['color_grey_label'];
	$color_grey = $theme_settings['colors']['color_grey'];
	$color_black = $theme_settings['colors']['color_black'];

	$bg_body = hygge_split_bg_setting($theme_settings['bg_body'],'bg');
	$bg_body_size	= hygge_split_bg_setting($theme_settings['bg_body'],'bg_size');
	$bg_header = hygge_split_bg_setting($theme_settings['bg_header'],'bg');
	$bg_header_size = hygge_split_bg_setting($theme_settings['bg_header'],'bg_size');
	$bg_sidebar = hygge_split_bg_setting($theme_settings['bg_sidebar'],'bg');
	$bg_sidebar_size = hygge_split_bg_setting($theme_settings['bg_sidebar'],'bg_size');
	$bg_footer = hygge_split_bg_setting($theme_settings['bg_footer'],'bg');
	$bg_footer_size = hygge_split_bg_setting($theme_settings['bg_footer'],'bg_size');

	$typo_body_font = $theme_settings['typo']['font_body'][0];
	$typo_body_weight = hygge_split_font_style($theme_settings['typo']['font_body'][1],'weight');
	$typo_body_style = hygge_split_font_style($theme_settings['typo']['font_body'][1],'style');
	$typo_body_size = $theme_settings['typo']['font_body'][2];
	$typo_body_line = $theme_settings['typo']['font_body'][3];
	$typo_body_transform = $theme_settings['typo']['font_body'][4];
	$typo_body_letter = $theme_settings['typo']['font_body'][5];

	$typo_navigation_font = $theme_settings['typo']['font_navigation'][0];
	$typo_navigation_weight = hygge_split_font_style($theme_settings['typo']['font_navigation'][1],'weight');
	$typo_navigation_style = hygge_split_font_style($theme_settings['typo']['font_navigation'][1],'style');
	$typo_navigation_size = $theme_settings['typo']['font_navigation'][2];
	$typo_navigation_line = $theme_settings['typo']['font_navigation'][3];
	$typo_navigation_transform = $theme_settings['typo']['font_navigation'][4];
	$typo_navigation_letter = $theme_settings['typo']['font_navigation'][5];

	$typo_label_font = $theme_settings['typo']['font_label'][0];
	$typo_label_weight = hygge_split_font_style($theme_settings['typo']['font_label'][1],'weight');
	$typo_label_style = hygge_split_font_style($theme_settings['typo']['font_label'][1],'style');
	$typo_label_size = $theme_settings['typo']['font_label'][2];
	$typo_label_line = $theme_settings['typo']['font_label'][3];
	$typo_label_transform = $theme_settings['typo']['font_label'][4];
	$typo_label_letter = $theme_settings['typo']['font_label'][5];

	$typo_label_italic_font = $theme_settings['typo']['font_label_italic'][0];
	$typo_label_italic_weight = hygge_split_font_style($theme_settings['typo']['font_label_italic'][1],'weight');
	$typo_label_italic_style = hygge_split_font_style($theme_settings['typo']['font_label_italic'][1],'style');
	$typo_label_italic_size = $theme_settings['typo']['font_label_italic'][2];
	$typo_label_italic_line = $theme_settings['typo']['font_label_italic'][3];
	$typo_label_italic_transform = $theme_settings['typo']['font_label_italic'][4];
	$typo_label_italic_letter = $theme_settings['typo']['font_label_italic'][5];

	$typo_h1_font = $theme_settings['typo']['font_h1'][0];
	$typo_h1_weight = hygge_split_font_style($theme_settings['typo']['font_h1'][1],'weight');
	$typo_h1_style = hygge_split_font_style($theme_settings['typo']['font_h1'][1],'style');
	$typo_h1_size = $theme_settings['typo']['font_h1'][2];
	$typo_h1_line = $theme_settings['typo']['font_h1'][3];
	$typo_h1_transform = $theme_settings['typo']['font_h1'][4];
	$typo_h1_letter = $theme_settings['typo']['font_h1'][5];

	$typo_h2_font = $theme_settings['typo']['font_h2'][0];
	$typo_h2_weight = hygge_split_font_style($theme_settings['typo']['font_h2'][1],'weight');
	$typo_h2_style = hygge_split_font_style($theme_settings['typo']['font_h2'][1],'style');
	$typo_h2_size = $theme_settings['typo']['font_h2'][2];
	$typo_h2_line = $theme_settings['typo']['font_h2'][3];
	$typo_h2_transform = $theme_settings['typo']['font_h2'][4];
	$typo_h2_letter = $theme_settings['typo']['font_h2'][5];

	$typo_h3_font = $theme_settings['typo']['font_h3'][0];
	$typo_h3_weight = hygge_split_font_style($theme_settings['typo']['font_h3'][1],'weight');
	$typo_h3_style = hygge_split_font_style($theme_settings['typo']['font_h3'][1],'style');
	$typo_h3_size = $theme_settings['typo']['font_h3'][2];
	$typo_h3_line = $theme_settings['typo']['font_h3'][3];
	$typo_h3_transform = $theme_settings['typo']['font_h3'][4];
	$typo_h3_letter = $theme_settings['typo']['font_h3'][5];

	$typo_h4_font = $theme_settings['typo']['font_h4'][0];
	$typo_h4_weight = hygge_split_font_style($theme_settings['typo']['font_h4'][1],'weight');
	$typo_h4_style = hygge_split_font_style($theme_settings['typo']['font_h4'][1],'style');
	$typo_h4_size = $theme_settings['typo']['font_h4'][2];
	$typo_h4_line = $theme_settings['typo']['font_h4'][3];
	$typo_h4_transform = $theme_settings['typo']['font_h4'][4];
	$typo_h4_letter = $theme_settings['typo']['font_h4'][5];

	$typo_h5_font = $theme_settings['typo']['font_h5'][0];
	$typo_h5_weight = hygge_split_font_style($theme_settings['typo']['font_h5'][1],'weight');
	$typo_h5_style = hygge_split_font_style($theme_settings['typo']['font_h5'][1],'style');
	$typo_h5_size = $theme_settings['typo']['font_h5'][2];
	$typo_h5_line = $theme_settings['typo']['font_h5'][3];
	$typo_h5_transform = $theme_settings['typo']['font_h5'][4];
	$typo_h5_letter = $theme_settings['typo']['font_h5'][5];

	$typo_h6_font = $theme_settings['typo']['font_h6'][0];
	$typo_h6_weight = hygge_split_font_style($theme_settings['typo']['font_h6'][1],'weight');
	$typo_h6_style = hygge_split_font_style($theme_settings['typo']['font_h6'][1],'style');
	$typo_h6_size = $theme_settings['typo']['font_h6'][2];
	$typo_h6_line = $theme_settings['typo']['font_h6'][3];
	$typo_h6_transform = $theme_settings['typo']['font_h6'][4];
	$typo_h6_letter = $theme_settings['typo']['font_h6'][5];

	$typo_description_font = $theme_settings['typo']['font_description'][0];
	$typo_description_weight = hygge_split_font_style($theme_settings['typo']['font_description'][1],'weight');
	$typo_description_style = hygge_split_font_style($theme_settings['typo']['font_description'][1],'style');
	$typo_description_size = $theme_settings['typo']['font_description'][2];
	$typo_description_line = $theme_settings['typo']['font_description'][3];
	$typo_description_transform = $theme_settings['typo']['font_description'][4];
	$typo_description_letter = $theme_settings['typo']['font_description'][5];

	$typo_blockquote_font = $theme_settings['typo']['font_blockquote'][0];
	$typo_blockquote_weight = hygge_split_font_style($theme_settings['typo']['font_blockquote'][1],'weight');
	$typo_blockquote_style = hygge_split_font_style($theme_settings['typo']['font_blockquote'][1],'style');
	$typo_blockquote_size = $theme_settings['typo']['font_blockquote'][2];
	$typo_blockquote_line = $theme_settings['typo']['font_blockquote'][3];
	$typo_blockquote_transform = $theme_settings['typo']['font_blockquote'][4];
	$typo_blockquote_letter = $theme_settings['typo']['font_blockquote'][5];



	$config = array(
		'sidebar_width' => $sidebar_width.'px',
		'sideblock_width' => $sideblock_width.'px',
		'sideblock_width_small' => '280px',

		'color_primary' => $color_primary,
		'color_white' => $color_white,
		'color_grey_pale' => $color_grey_pale,
		'color_grey_light' => $color_grey_light,
		'color_grey_label' => $color_grey_label,
		'color_grey' => $color_grey,
		'color_black' => $color_black,

		'bg_body' => $bg_body,
		'bg_body_size' => $bg_body_size,
		'bg_header' => $bg_header,
		'bg_header_size' => $bg_header_size,
		'bg_sidebar' => $bg_sidebar,
		'bg_sidebar_size' => $bg_sidebar_size,
		'bg_footer' => $bg_footer,
		'bg_footer_size' => $bg_footer_size,

		'typo_body_font'  => '"'.$typo_body_font.'",sans-serif',
		'typo_body_weight' => $typo_body_weight,
		'typo_body_style' => $typo_body_style,
		'typo_body_size'  => $typo_body_size,
		'typo_body_line'  => $typo_body_line.'em',
		'typo_body_transform' => $typo_body_transform,
		'typo_body_letter' => $typo_body_letter.'em',

		'typo_navigation_font' => '"'.$typo_navigation_font.'",sans-serif',
		'typo_navigation_weight' => $typo_navigation_weight,
		'typo_navigation_style' => $typo_navigation_style,
		'typo_navigation_size' => $typo_navigation_size,
		'typo_navigation_line' => $typo_navigation_line.'em',
		'typo_navigation_transform' => $typo_navigation_transform,
		'typo_navigation_letter' => $typo_navigation_letter.'em',

		'typo_label_font' => '"'.$typo_label_font.'",sans-serif',
		'typo_label_weight' => $typo_label_weight,
		'typo_label_style' => $typo_label_style,
		'typo_label_size' => $typo_label_size,
		'typo_label_line' => $typo_label_line.'em',
		'typo_label_transform' => $typo_label_transform,
		'typo_label_letter' => $typo_label_letter.'em',

		'typo_label_italic_font' => '"'.$typo_label_italic_font.'",sans-serif',
		'typo_label_italic_weight' => $typo_label_italic_weight,
		'typo_label_italic_style' => $typo_label_italic_style,
		'typo_label_italic_size' => $typo_label_italic_size,
		'typo_label_italic_line' => $typo_label_italic_line.'em',
		'typo_label_italic_transform' => $typo_label_italic_transform,
		'typo_label_italic_letter' => $typo_label_italic_letter.'em',

		'typo_h1_font' => '"'.$typo_h1_font.'",sans-serif',
		'typo_h1_weight' => $typo_h1_weight,
		'typo_h1_style' => $typo_h1_style,
		'typo_h1_size' => $typo_h1_size,
		'typo_h1_line' => $typo_h1_line.'em',
		'typo_h1_transform' => $typo_h1_transform,
		'typo_h1_letter' => $typo_h1_letter.'em',

		'typo_h2_font' => '"'.$typo_h2_font.'",sans-serif',
		'typo_h2_weight' => $typo_h2_weight,
		'typo_h2_style' => $typo_h2_style,
		'typo_h2_size' => $typo_h2_size,
		'typo_h2_line' => $typo_h2_line.'em',
		'typo_h2_transform' => $typo_h2_transform,
		'typo_h2_letter' => $typo_h2_letter.'em',

		'typo_h3_font' => '"'.$typo_h3_font.'",sans-serif',
		'typo_h3_weight' => $typo_h3_weight,
		'typo_h3_style' => $typo_h3_style,
		'typo_h3_size' => $typo_h3_size,
		'typo_h3_line' => $typo_h3_line.'em',
		'typo_h3_transform' => $typo_h3_transform,
		'typo_h3_letter' => $typo_h3_letter.'em',

		'typo_h4_font' => '"'.$typo_h4_font.'",sans-serif',
		'typo_h4_weight' => $typo_h4_weight,
		'typo_h4_style' => $typo_h4_style,
		'typo_h4_size' => $typo_h4_size,
		'typo_h4_line' => $typo_h4_line.'em',
		'typo_h4_transform' => $typo_h4_transform,
		'typo_h4_letter' => $typo_h4_letter.'em',

		'typo_h5_font' => '"'.$typo_h5_font.'",sans-serif',
		'typo_h5_weight' => $typo_h5_weight,
		'typo_h5_style' => $typo_h5_style,
		'typo_h5_size' => $typo_h5_size,
		'typo_h5_line' => $typo_h5_line.'em',
		'typo_h5_transform' => $typo_h5_transform,
		'typo_h5_letter' => $typo_h5_letter.'em',

		'typo_h6_font' => '"'.$typo_h6_font.'",sans-serif',
		'typo_h6_weight' => $typo_h6_weight,
		'typo_h6_style' => $typo_h6_style,
		'typo_h6_size' => $typo_h6_size,
		'typo_h6_line' => $typo_h6_line.'em',
		'typo_h6_transform' => $typo_h6_transform,
		'typo_h6_letter' => $typo_h6_letter.'em',

		'typo_description_font' => '"'.$typo_description_font.'",sans-serif',
		'typo_description_weight' => $typo_description_weight,
		'typo_description_style' => $typo_description_style,
		'typo_description_size' => $typo_description_size,
		'typo_description_line' => $typo_description_line.'em',
		'typo_description_transform' => $typo_description_transform,
		'typo_description_letter' => $typo_description_letter.'em',

		'typo_blockquote_font' => '"'.$typo_blockquote_font.'",sans-serif',
		'typo_blockquote_weight' => $typo_blockquote_weight,
		'typo_blockquote_style' => $typo_blockquote_style,
		'typo_blockquote_size' => $typo_blockquote_size,
		'typo_blockquote_line' => $typo_blockquote_line.'em',
		'typo_blockquote_transform' => $typo_blockquote_transform,
		'typo_blockquote_letter' => $typo_blockquote_letter.'em',

	);
	return $config;

}
