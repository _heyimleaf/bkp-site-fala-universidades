<form class="search-form" method="get" action="<?php echo  esc_url( home_url() ) ; ?>">


<div class="hygge-form-label"><label><?php esc_html_e('Search', 'hygge'); ?></label><input type="text" class="textfield" name="s" required="required" placeholder="<?php esc_attr_e('Search', 'hygge');?>"></div>


	<a href="#" class="close">×</a>
	<i class="hygge-icon-search"></i>
	<input type="submit" class="submit">
</form>
