<?php $theme_settings = hygge_theme_settings();

$meta_class = $theme_settings['post_meta_author'] && get_the_author_meta('description') ? 'author--true' : 'author--false';

 ?>

<div class="post__meta--single <?php echo $meta_class; ?>">
	<?php if ( $theme_settings['post_cover_style'] != 'notebook' && hygge_category() ) { ?>
		<div class="post__meta__item">
			<?php echo hygge_category(20); ?>
		</div>
	<?php } ?>

	<div class="post__meta__item">
		<div class="post__meta__label label--italic post__meta__date"><?php esc_html_e('Published', 'hygge')?></div>
		<div class="post__meta__data label">
			<?php echo get_the_date(); ?>
		</div>
	</div>



	<?php if(
		get_post_meta( get_the_ID(), 'comments_use', true ) != '0' // comments are not disabled on post MUST
		&& ( comments_open() || get_comments_number() > 0 ) // comments are open or count > 0 MUST
		):?>
		<div class="post__meta__item">
			<div class="post__meta__label label--italic post__meta__comments"><?php esc_html_e('Comments', 'hygge')?></div>
			<div class="post__meta__data label">
				<?php comments_popup_link( esc_html__( '0 Comments', 'hygge' ), esc_html__( '1 Comment', 'hygge' ), esc_html__( '% Comments', 'hygge' )); ?>
			</div>
		</div>
	<?php endif; ?>


	<?php if( $theme_settings['post_meta_author'] && get_the_author_meta('description') ): ?>
		<div class="post__meta__item">
			<?php
				$img = get_avatar( get_the_author_meta( 'ID' ), 80 );
				if( $img ){
					echo "<div class='author-img'>{$img}</div>";
				}
			?>

			<div class="author-text">
				<div class="post__meta__label label--italic post__meta__author"><?php esc_html_e('Author', 'hygge')?></div>
				<div class="post__meta__data label">
					<?php the_author_posts_link(); ?>
				</div>

				<?php if( get_the_author_meta('description') ): ?>
					<?php echo wpautop( do_shortcode(get_the_author_meta('description') ) ); ?>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

</div>
