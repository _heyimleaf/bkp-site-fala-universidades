<?php
	$color = hygge_get_post_color();
	$colorOpacity = $color ? '' : 'color-opacity-low';
?>

<article <?php post_class(); ?>>
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
	<div class="hygge-media-hover has-thumbnail-<?php echo ( has_post_thumbnail() ? 'true' : 'false' ); ?>">

		<div class="post__media">
			<div class="hygge-media-hover">
				<div class="hygge-media-hover__img">
					<?php if( has_post_thumbnail() ){
						the_post_thumbnail( 'hygge-horizontal-s' );
					}else{
						echo hygge_get_dropcap_media('horizontal');
					}?>
				</div>
				<div class="hygge-media-hover__hover <?php echo esc_attr($colorOpacity); ?>" style="background-color:<?php echo esc_attr($color);?>;"></div>
			</div>
		</div>

		<div class="post__text">
			<h6 class="post__title">
				<?php echo wp_kses( get_the_title(), hygge_allowed_kses_tags() ); ?>
			</h6>

			<div class="label--italic">
				<?php echo '<div class="item">' . get_the_date() . '</div>'; ?>
			</div>

		</div>
	</div>
</a>
</article>
