<?php
	$theme_settings = hygge_theme_settings();
	get_header();

	if (have_posts()): while (have_posts()) : the_post();

	$use_comments = false;
	if(
		get_post_meta( get_the_ID(), 'comments_use', true ) != '0' // comments are not disabled on post MUST
		&& ( comments_open() || get_comments_number() > 0 ) // comments are open or count > 0 MUST
		&& !post_password_required()
	){
		$use_comments = true;
	}

	$page_blog_use = get_post_meta( get_the_ID(), 'blog_use', true );

	$post_classes = '';
	$post_classes .= ' article-single article-single--page';

	if( !$use_comments && $page_blog_use ){
		$post_classes .= ' article-single--post-blog-last';
	}
?>

<!-- article -->
<div id="post-<?php the_ID(); ?>" <?php post_class( $post_classes ); ?>>

	<div class="main-content">

		<div class="post__content <?php echo get_the_content() ? '' : 'empty'; ?>">
		<div class="editor-content">
			<?php
				// Show MANUAL excerpt if enabled in theme settings
				if( $theme_settings['post_excerpt'] && has_excerpt() ){
					echo "<p class='description-heading'>" . get_the_excerpt() . "</p>";
					echo "<hr class='hygge-separator--small' />";
				}

				// If Gallery shown as cover, exclude it from content
				if( get_post_meta( get_the_ID(), 'cover_media', true ) == 'gallery' ){
					$output_content = hygge_match_gallery( get_the_content(), true );
					echo apply_filters( 'the_content', $output_content );
				}else{
					the_content();
				}
			?>
		</div>
		</div>

		<?php hygge_link_split_pages(); ?>

		<?php
		if( get_post_meta( get_the_ID(), 'blog_use', true ) ){
			get_template_part('page-blog');
		}
		?>

	</div>


	<?php if( $use_comments ){
		echo '<div class="hygge-fullwidth">';
		echo '<div class="grey-mode">';
		echo '<div class="post__comments js-masonry-comments">';
		comments_template();
		echo '</div>';
		echo '</div>';
		echo '</div>';
	}?>

</div>



<?php endwhile; endif;

get_footer();
