<?php

/*------------------------------------------------------------
 * This is loop-item-filmstrip template.
 * Style: filmstrip
 * Title: Filmstrip
 *------------------------------------------------------------*/

$theme_settings = hygge_theme_settings();
$post_classes = '';
$color = hygge_get_post_color();
$colorOpacity = $color ? '' : 'color-opacity-low';
$post_classes .= ' element-modular-loading';
?>



<article <?php post_class( $post_classes ); ?>>
	<div class="post__inwrap">

		<div class="hygge-media-hover has-thumbnail-<?php echo ( has_post_thumbnail() ? 'true' : 'false' ); ?> <?php echo hygge_is_color_dark($color, true, has_post_thumbnail()) ?>">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" data-loader-img="<?php echo hygge_get_loader_img_url(); ?>">
				<div class="hygge-media-hover__img">
					<?php if( has_post_thumbnail() ){
						the_post_thumbnail( 'hygge-horizontal-l' );
					}else{
						echo hygge_get_dropcap_media('horizontal');
					}?>
				</div>
			</a>
			<div class="hygge-media-hover__hover <?php echo esc_attr($colorOpacity); ?>" style="background-color:<?php echo esc_attr($color); ?>;"></div>

			<div class="post__text dark-mode">

				<div class="label--italic italic-meta">
					<?php get_template_part('post-meta-loop'); ?>
				</div>

				<h3 class="post__title">
					<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" data-loader-img="<?php echo hygge_get_loader_img_url(); ?>">
						<?php echo wp_kses( get_the_title(), hygge_allowed_kses_tags() ); ?>
					</a>
				</h3>

			</div>
		</div>

	</div>
</article>
