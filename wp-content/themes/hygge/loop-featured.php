<?php

/*------------------------------------------------------------*
 * Loop for displaying only featured items
 *------------------------------------------------------------*/

$theme_settings = hygge_theme_settings();

$featured_tag_id = $theme_settings['featured_tag'];
$featured_tag = '';

if( $featured_tag_id ){
	$featured_tag = get_tag($featured_tag_id);
	$featured_tag = $featured_tag->slug;
}

$featured_count = (int)$theme_settings['featured_count'];
$featured_interval = (int)$theme_settings['featured_interval'];

// 0 posts = off
if( $featured_count == 0 ){
	return;
}

// Check if shortcode exists
if( !shortcode_exists( 'hygge_blog' ) ){
	return;
}

echo '<div class="featured-posts">';
echo do_shortcode('[hygge_blog posts="' . $featured_count . '" style="carousel" category="" tag="' . $featured_tag . '" sort_by="date" sort_order="DESC" interval="' . $featured_interval . '" extra_class="featured"]
');
echo '</div>';
