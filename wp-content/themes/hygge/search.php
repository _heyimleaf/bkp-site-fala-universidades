<?php
$theme_settings = hygge_theme_settings();
get_header();

echo '<div class="main-content">';

	get_template_part('loop');

	if( !have_posts() ): ?>
		<div class="article-single article-single--page article--widget-area article-single--search-no-results">

			<div class="post__content">
				<div class="hygge-contentwidth tac">
					<?php
						if( is_active_sidebar('search-results') ){
							echo '<div class="widget-area--content widget-area--main-content">';
								dynamic_sidebar('search-results');
							echo '</div>';
						}else{
							echo '<p class="description-heading">' . esc_html__( 'Unfortunately, no results have been found.', 'hygge' ) . '</p>';
							get_template_part('searchform');
						}
					?>
				</div>
			</div>

		</div>

	<?php endif;

echo '</div>';

get_footer();
