/*	Icon Picker Dialog Script
/*------------------------------------------------------------*/

(function($){
"use strict";
$(document).ready(function(){

	var $el, id, input, source;
	var $lightbox = $('.hygge-lightbox--icon-picker');
	var $icon = $lightbox.find('.hygge-icon-picker-list .item input');
	var $filter = $lightbox.find('.filter input');
	var $statusIcon = $lightbox.find('.status i');
	var $statusTitle = $lightbox.find('.status .js-title');

	// fix for css problem with misbehaving fixed div inside absolute div
	$lightbox.appendTo('body');



	// on TinyMCE button clicked
	$(document).on( 'hygge:tinymceGetIconClick', function( event, data ){
		// pass the argument
		openLightbox(data.preset);
		// set save button for TinyMCE variation
		$lightbox.find('.js-save').get(0).dataEditor = data.editor;
		$lightbox.find('.js-save').get(0).dataElement = data.icon_element;
		$lightbox.find('.js-save').get(0).dataPreset = data.preset;

	});



	// on Widget Social picker click
	$('body').on( 'click', '.hygge-widget-social-picker .form .icon', function(){
		source = 'widget-social-picker';
		$el = $(this);

		openLightbox();
	});



	// close bg control lightbox
	$lightbox.on('click', '.js-close', function(){
		$lightbox.fadeOut();
		$('body').removeClass('hygge-lightbox-active');
	});



	// save bg control and exit lightbox
	$lightbox.on('click', '.js-save', function(){

		var value = getIconValue();

		// If in TinyMCE
		if( this.dataEditor ){

			// if dataElement get whole dataElement
			if( this.dataElement === true ){
				// this.dataEditor.insertContent( '[hygge_icon icon="'+value+'"]' );
				// Inserting direct icon works, but content has dificulties with further editing
				// because of the empty italic tag
				// this.dataEditor.insertContent( '<i class="'+value+'"></i>' );


				// Use same approach as Shortcode-Generator to replace current shortcode
		    if( this.dataPreset ){
		      var node = this.dataEditor.selection.getNode();
		      this.dataEditor.selection.select(node);
		    }

				if( value ){
			    this.dataEditor.insertContent('<p>[hygge_icon icon="'+value+'"]</p>');
				}else{
					this.dataEditor.insertContent('');
				}

			}else{
				// insert icon class name
				this.dataEditor.insertContent( value );
			}
		}

		// If Widget Social Picker
		if( source == 'widget-social-picker' ){
			$el.find('input').val( value );
			$el.find('i').addClass( 'hygge-icon-'+value );
		}


		$(document).trigger('hygge:iconPicked');
		$lightbox.fadeOut();
		$('body').removeClass('hygge-lightbox-active');

	});



	// open lightbox function
	function openLightbox(preset){
		// Select icon if preset is sent
		if(preset) {
			$icon.filter('[value="'+preset+'"]').prop( 'checked', true );
		}else{
			$icon.prop( 'checked', false );
		}

		$lightbox.fadeIn(300, function(){
			// focus search input field
			$filter.focus();
		});

		$( 'body' ).addClass( 'hygge-lightbox-active' );

	}



	// collect and return value from Icon Picker Lightbox
	function getIconValue() {
		var value = $icon.filter(':checked').val();
		return value;
	}



	// on icon select
	$icon.change(function(){
		var value = getIconValue();
		$statusIcon.removeClass().addClass( value );
		$statusTitle.html( value );
	});



	// on filter
	$filter.keyup(function(){
		var term = $(this).val();

		$icon.each(function(){

			if( $(this).val().toLowerCase().indexOf(term) == -1 ){
				$(this).parent().hide();
			}else{
				$(this).parent().show();
			}

		});
	});



}); // doc ready end
})(jQuery);
