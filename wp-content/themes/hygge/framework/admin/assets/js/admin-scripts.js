/*------------------------------------------------------------
 * Admin Scripts
 *------------------------------------------------------------*/

var hyggeAdmin = {};

(function($){
"use strict";



/*------------------------------------------------------------
 * Events
 *------------------------------------------------------------*/

$(document).ready(function(){
	hyggeUploadImageButton();
	hyggeWidgetFeaturedBanners();
	hyggeWidgetSocialPicker();
	hyggeGallerySettings();
});

$(document).ajaxStop( function(){
	hyggeUploadImageButton();
	hyggeWidgetFeaturedBanners();
	hyggeWidgetSocialPicker();
});

$('.widgets-sortables').on( 'sortstop', function(){
	hyggeWidgetFeaturedBanners();
	hyggeWidgetSocialPicker();
});


/* Hygge Upload Image Button
 * used in Widgets and Shortcode Generator
 *------------------------------------------------------------*/

function hyggeUploadImageButton(){
	$(document).off('.hyggeUploadImageButton');
	// Button Clicked
	$(document).on("click.hyggeUploadImageButton", ".hygge-upload-image-button", function() {

		var $btn = $(this);
		var $urlField = $btn.nextAll('input.url');
		var $preview = $btn.nextAll('.hygge-image-preview');

		$urlField.on('change keyup blur', function(){
			var url = $(this).val().split('|')[0];
			$preview.attr('src', url);
		});

		window.send_to_editor = function(html) {
			var imgurl = $(html).attr('src');
			imgurl = imgurl ? imgurl : $(html).find('img').attr('src');

			if( $btn.is('.with-dimensions') ){
				var imgwidth = $(html).attr('width');
				imgwidth = imgwidth ? imgwidth : $(html).find('img').attr('width');
				var imgheight = $(html).attr('height');
				imgheight = imgheight ? imgheight : $(html).find('img').attr('height');

				imgurl = imgurl + '|' + imgwidth + '|' + imgheight;
			}

			$urlField.val( imgurl ).change();
			tb_remove();
		};

		tb_show('', 'media-upload.php?type=image&tab=library&TB_iframe=true');
		return false;
	});

	$(document).on("click", ".hygge-upload-image-button ~ .js-remove", function() {
		$(this).nextAll('input.url').val('');
		$(this).nextAll('.hygge-image-preview').attr('src', '');
	});

}

$('.widgets-sortables').one( 'sortstop', hyggeUploadImageButton);





/* Hygge Widget Featured Banners
 *------------------------------------------------------------*/

function hyggeWidgetFeaturedBanners(){

	$('select.featured-banners-node').each(function(){

		var $el = $(this);
		update($el.val());

		$el.change( function(){
			update($el.val());
		});


		function update(val){
			if( val == 'custom_banner' ){
				$el.nextAll('.hygge-custom-banner').first().show();
			}else{
				$el.nextAll('.hygge-custom-banner').first().hide();
			}
		}

	});

}





/* Hygge Widget Social Picker
 *------------------------------------------------------------*/

function hyggeWidgetSocialPicker(){

	$('.hygge-widget-social-picker').not('.processed').each(function(){

		var $el = $(this);
		var $input = $el.find('input.value-joined');
		var $origin = $el.children('.form-row');
		var $form = $el.find('.form');
		var $btn = $el.find('.js-add-new-social');
		var $shortcode = $el.find('input#shortcode');

		// If not activated instance of widget, skip it
		// this happens when widget is still in #available-widgets
		// or when added in active-widgets, but still not processed with the unique ID
		if( $input.attr('id').indexOf('__i__') > -1 ){
			return;
		}



		// Add new item
		$btn.click( function(){
			$form.append( $origin.clone() );
		});

		// Remove Item
		$form.on( 'click', '.js-remove', function(){
			$(this).parent('.form-row').remove();
			updateValue();
		});



		// Update value on form changes
		$form.on({
			change: updateValue,
			keyup: updateValue,
		}, 'input');
		$(document).on( 'hygge:iconPicked', updateValue );



		// Update value
		function updateValue(){

			var valueJoined = [];
			var i = 1;
			$form.find('.form-row').each( function(){
				var icon = $(this).find('[name="icon"]').val().trim();
				var title = $(this).find('[name="title"]').val().trim();
				var url = $(this).find('[name="url"]').val().trim();

				if( icon || title || url ){
					valueJoined.push( (icon+'|'+title+'|'+url).split(' ').join('%20') );
				}

			});

			valueJoined = valueJoined.join(' ');
			$input.val( valueJoined ).keyup();
			$shortcode.val( '[hygge_social '+valueJoined+']' );

		}



		// Mark as js processed
		$el.addClass('processed');

	});

}





function hyggeGallerySettings(){
	if ( typeof _ !== 'undefined' && 'media' in wp && 'gallery' in wp.media) {
		_.extend(wp.media.gallery.defaults, {
			hygge_style: 'fluid',
			hygge_height: '',
			hygge_interval: '0',
		});

		wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
			template: function(view){
				return wp.media.template('gallery-settings')(view)
				+ wp.media.template('hygge-gallery-settings')(view);
			}
		});
	}
}



})(jQuery);
