/* Shortcode Generator
 * works only with self-enclosing shortcodes
 * if no attributes, shortcode should not be generated
/*------------------------------------------------------------*/

(function($){
	"use strict";
	$(document).ready(function(){

		var $el, id, input;
		var $lightbox = $('.hygge-lightbox--sc-gen');
		var $lightboxContent = $lightbox.find('.hygge-lightbox-content');

		// fix for css problem with misbehaving fixed div inside absolute div
		// or hidden div on tag/category
		$lightbox.appendTo('body');



		// on TinyMCE button clicked
		$(document).on( 'hygge:tinymceShortcodeGenerator', function( event, data ){

			// Get currently selected node, and check if it is
			// the same shortcode as clicked to edit the current
			var edNode = data.editor.selection.getNode();
			var regString = '\\['+data.shortcode+'(\\s[^\\]]*)\\]';
			var match = edNode.innerHTML.match(regString);
			var atts = {};

			// If editing the current shortcode, extract attributes for passing to form
			// Set generator to override current node in tinymce
			if(match){
				var getKeyValue = function(a,b,c) { atts[b] = c; };
				match[1].replace(/\s*([^=]*)=["|']([^"']*)["|']/gm, getKeyValue);
				$lightbox.find('.js-save').get(0).isEdit = true;
			}else{
				$lightbox.find('.js-save').get(0).isEdit = false;
			}

			$lightbox.find('.js-save').get(0).editor = data.editor;

			// For Icon shortcode, redirect to Icon-Picker script
			// and exit shortcode generator
			// icon picker will return value to editor
			if( data.shortcode == 'hygge_icon' ){
				jQuery(document).trigger('hygge:tinymceGetIconClick', {
					editor: data.editor,
					icon_element: data.icon_element,
					preset: atts.icon
				});
				return;
			}

			openLightbox(data, atts);
		});



		// open lightbox function
		function openLightbox(data, currentAtts){

			var edit = _.isEmpty(currentAtts) ? false : true;

			$lightbox.find('.hygge-lightbox-head .title .shortcode').html('['+data.shortcode+']');
			if( edit ){
				$lightbox.find('.hygge-lightbox-head .title .action').html('Edit');
			}else{
				$lightbox.find('.hygge-lightbox-head .title .action').html('Generate');
			}

			$lightboxContent.find('.info').html('');
			if( data.info ){
				$lightboxContent.find('.info').html(data.info);
			}
			// Generate fields
			var formContent = '';

			// Shortcode attributes exposed
			_.each(data.attributes, function(attribute){
				formContent += '<div class="form-item">';
				if( attribute.title ){
					formContent += '<label class="title">'+attribute.title+'</label>';
				}
				if( attribute.description ){
					formContent += '<small class="description">'+attribute.description+'</small>';
				}

				if( typeof(attribute.placeholder) === 'undefined' ){ attribute.placeholder = ''; }

				var value = currentAtts[attribute.name] ? currentAtts[attribute.name] : '';

				// text
				if( attribute.type == 'text' ){
					formContent += '<input type="text" name="'+attribute.name+'" placeholder="'+attribute.placeholder+'" value="'+value+'">';
				}

				// textarea
				if( attribute.type == 'textarea' ){
					formContent += '<textarea name="'+attribute.name+'" placeholder="'+attribute.placeholder+'">'+value+'</textarea>';
				}

				// checkbox
				if( attribute.type == 'checkbox' ){
					var checked = value == 'on' ? 'checked' : '';
					formContent += '<input type="checkbox" name="'+attribute.name+'" ' + checked + '> '+attribute.title;
				}


				// select
				if( attribute.type == 'select' ){
					formContent += '<select name="'+attribute.name+'">';

					if( typeof(attribute.default) === 'undefined' ){
						formContent += '<option value="">-- Select --</option>';
					}

					if( edit && value ){
						attribute.default = value;
					}

					_.each(attribute.values, function(value, key){
						// If object is passed instead of straight key:value
						// extract it's values
						if( _.isObject(value) ){
							key = Object.keys(value)[0];
							value = value[key];
						}

						if( key == 'hyggeOptgroup' ){
							formContent += '<optgroup label="'+value+'">';
						}else{
							var selected = attribute.default == key ? 'selected': '';
							formContent += '<option value="'+key+'" '+selected+'>'+value+'</option>';
						}
					});

					formContent += '</select>';
				}

				// Image URL
				if( attribute.type == 'image-url' ){
					formContent += '<button type="button" class="button hygge-upload-image-button with-dimensions"><span class="dashicons dashicons-admin-media"></span> '+hyggeLocalize.chooseImage+'</button>';
					formContent += '<a class="remove js-remove"><i class="hygge-icon-close"></i></a>';
					formContent += '<input class="url" type="hidden" name="'+attribute.name+'" placeholder="'+attribute.placeholder+'" value="'+value+'">';

					var url = value.split('|')[0];
					formContent += '<img class="hygge-image-preview" src="'+url+'">';
				}



				formContent += '</div>';
			});

			// Shortcode name in a hidden field
			formContent += '<input type="hidden" name="shortcode_name"" value="'+data.shortcode+'">';

			$lightboxContent.find('.form').html(formContent);

			$lightbox.fadeIn();
			$('body').addClass('hygge-lightbox-active');
		}



		// Collect and return values from BG Control Lightbox
		function generateShortcode() {
			var output = '';
			var attr = '';

			$lightbox.find('.form-item input, .form-item select, .form-item textarea').each(function(){
				if( $(this).is('[type="checkbox"]') ){
					if( $(this).is(':checked') ){
						attr += ' ' + $(this).attr('name') + '="on"';
					}
				}else if( $(this).val() ){
					attr += ' ' + $(this).attr('name');
					attr += '="' + $(this).val() + '"';
				}
			});

			// Don't generate shortcode without attributes
			if( !attr ){
				return false;
			}

			output += '[';
			output += $lightbox.find('[name="shortcode_name"]').val();
			output += attr;
			output += ']';

			return output;
		}


		// close lightbox
		function closeLightbox(){
			$lightbox.fadeOut();
			$('body').removeClass('hygge-lightbox-active');
		}


		$lightbox.on('click', '.js-close', closeLightbox);
		$(document).keydown(function(e){
			if( e.which == 27 ) {
				closeLightbox();
			}
		});



		// save and exit lightbox
		// add content to editor
		$lightbox.on('click', '.js-save', function(){

			var shortcode = generateShortcode();

			if( this.isEdit ){
				var node = this.editor.selection.getNode();
				this.editor.selection.select(node);
			}

			if( shortcode ){
				this.editor.insertContent('<p>'+shortcode+'</p>');
			}else{
				// if empty shortcode, make sure to erase the existing if selected
				this.editor.insertContent('');
			}


			$lightbox.fadeOut();
			$('body').removeClass('hygge-lightbox-active');
		});



	}); // doc ready end
})(jQuery);
