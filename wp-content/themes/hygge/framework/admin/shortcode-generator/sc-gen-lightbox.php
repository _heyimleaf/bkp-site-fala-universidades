<!-- Shortcode Generator Lightbox -->

<div class="hygge-lightbox hygge-lightbox--sc-gen" style="display:none;">
	<div class="hygge-lightbox-mask js-close"></div>
	<div class="hygge-lightbox-modal">

		<div class="hygge-lightbox-head">
			<div class="title"><span class="action">Generate</span> Shortcode - <span class="shortcode"></span></div>
			<div class="js-close close-icon"></div>
		</div>

    <div class="hygge-lightbox-content">
			<div class="info"></div>
			<div class="form"></div>
		</div>

		<div class="hygge-lightbox-actions">
			<a class="button button-primary js-save">Save</a>
			<a class="button js-close">Cancel</a>
		</div>

	</div>
</div>
<!-- / Shortcode Generator Lightbox -->
