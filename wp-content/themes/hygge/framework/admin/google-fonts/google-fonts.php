<?php

/*
Font Fetcher.
Gets google font list per API
https://developers.google.com/fonts/docs/developer_api

Call:
https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key=AIzaSyCdnrWzjePQ4KvZyFwiMm8eLc7jRgL-U6Q

If unsuccessful [max-quote, error] uses manually provided list.
*/


if( !function_exists( 'hygge_get_google_fonts' ) ){
	function hygge_get_google_fonts() {

		/*------------------------------------------------------------
		 *	Get Google Fonts by API call and store in global var
		 *------------------------------------------------------------*/

		static $hygge_google_fonts;

		if( $hygge_google_fonts ) {
			return $hygge_google_fonts;
		}

		// Prepare filesystem
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		global $wp_filesystem;
		WP_Filesystem();

		// cached file location
		$hygge_cached_file = HYGGE_ADMIN . '/google-fonts/google-fonts.txt';
		// Total time the file will be cached in seconds, set to a 28days
		$cachetime = 86400 * 28;

		// Check if cached file is available and up to date to use
		if(
			file_exists($hygge_cached_file)
			&& filesize($hygge_cached_file) != 0
			&& $cachetime > time()-filemtime($hygge_cached_file)
		){

			$hygge_google_fonts = $wp_filesystem->get_contents( $hygge_cached_file );

		}else{
			// Get fresh google fonts from API and cache response
			$theme_settings = hygge_theme_settings();

			if ($theme_settings['google_api']) {
				$hygge_google_fonts_request = wp_remote_get( esc_url_raw( 'https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key='.$theme_settings['google_api'] ) );

				// If request is successful cache it and use fonts file
				if(
					!is_wp_error( $hygge_google_fonts_request ) &&
					$hygge_google_fonts_request['response']['code'] == 200
				){

					// save cache
					$wp_filesystem->put_contents($hygge_cached_file, $hygge_google_fonts_request['body']);

					$hygge_google_fonts = $hygge_google_fonts_request['body'];
			}


			}else{

				// If everything fails, load the original cached file
				$hygge_google_fonts = $wp_filesystem->get_contents( $hygge_cached_file );

			}
		}

		$hygge_google_fonts = json_decode( $hygge_google_fonts );
		$hygge_google_fonts = isset($hygge_google_fonts) ? $hygge_google_fonts->items : array();



		/* Add Web Safe fonts to the bottom
		 *------------------------------------------------------------*/

		$hygge_websafe_fonts_families = array('Arial', 'Arial Black', 'Helvetica Neue', 'Verdana', 'Tahoma', 'Century Gothic', 'Gill Sans', 'Times New Roman', 'Baskerville', 'Palatino', 'Georgia', 'Courier New', 'Lucida Sans Typewriter' );
		$hygge_websafe_fonts_variants = array('300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic', '800', '800italic');

		foreach( $hygge_websafe_fonts_families as $family ){
			$font = new StdClass();
			$font->family = $family;
			$font->variants = $hygge_websafe_fonts_variants;
			$hygge_google_fonts[] = $font;
		}



		return $hygge_google_fonts;
	}
}
