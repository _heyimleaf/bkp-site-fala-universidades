<?php if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_page-settings',
		'title' => 'Page Settings',
		'fields' => array (
			array (
				'key' => 'field_54f5b39b58f82',
				'label' => 'Layout & Title',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_593c02f8e5c1c',
				'label' => 'Use Sidebar',
				'name' => 'use_sidebar',
				'type' => 'select',
				'instructions' => 'Override the setting for Use Sidebar set in Customizer > Layout.',
				'choices' => array (
					'true' => 'Yes (Override Layout Setting)',
					'false' => 'No (Override Layout Setting)',
				),
				'default_value' => '',
				'allow_null' => 1,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5832c84ee5ae6',
				'label' => 'What is displayed as Cover?',
				'name' => 'cover_media',
				'type' => 'select',
				'instructions' => 'Choose what is displayed as Cover. Defaults to Featured Image.',
				'choices' => array (
					'featured' => 'Featured Image',
					'gallery' => 'Gallery (first in content)',
					'video' => 'Video (first in content)',
					'custom' => 'Custom Cover Image',
					'title' => 'Only Title (no media)',
					'none' => 'Hide Cover',
				),
				'default_value' => 'featured',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5832c8d7670f3',
				'label' => 'Custom Cover Image',
				'name' => 'custom_cover_image',
				'type' => 'background',
				'instructions' => 'Customize cover image to be used instead of the featured image on the page.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5832c84ee5ae6',
							'operator' => '==',
							'value' => 'custom',
						),
					),
					'allorany' => 'all',
				),
				'bg' => '',
			),
			array (
				'key' => 'field_58caa1678bf9c',
				'label' => 'Cover Description',
				'name' => 'cover_description',
				'type' => 'textarea',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5832c84ee5ae6',
							'operator' => '!=',
							'value' => 'none',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 2,
				'formatting' => 'br',
			),
			array (
				'key' => 'field_54f5b2f754fbb',
				'label' => 'Enable Comments',
				'name' => 'comments_use',
				'type' => 'true_false',
				'instructions' => 'Uncheck to completely disable comments, including previously posted.',
				'message' => 'Enable Comments',
				'default_value' => 0,
			),
			array (
				'key' => 'field_54f5b40258f83',
				'label' => 'Blog',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_55b208ac1c0f1',
				'label' => 'Display Featured Posts',
				'name' => 'use_featured',
				'type' => 'true_false',
				'instructions' => 'Check this field to display the featured posts on top of the page. Featured Posts are configured in Theme Customizer > Blog Layouts & Posts > Featured Posts.',
				'message' => 'Display Featured Slider',
				'default_value' => 0,
			),
			array (
				'key' => 'field_54f5b6de6a3c4',
				'label' => 'Use Blog',
				'name' => 'blog_use',
				'type' => 'true_false',
				'message' => 'Use Blog',
				'default_value' => 0,
			),
			array (
				'key' => 'field_58de504bcbd78',
				'label' => 'Blog Title',
				'name' => 'blog_title',
				'type' => 'text',
				'instructions' => 'Add custom title for the blog. Leave empty for no title.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => 'Blog Title',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54f5b75f6a3c6',
				'label' => 'Blog Display Style',
				'name' => 'blog_display_style',
				'type' => 'select',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'choices' => array (
					'list' => 'List',
					'list-highlights' => 'List with Highlights',
					'grid' => 'Grid',
					'grid-3-1' => 'Grid 3-1',
					'grid-3-2' => 'Grid 3-2',
					'masonry' => 'Masonry',
				),
				'default_value' => 'grid',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_54f5bc066a3c7',
				'label' => 'Blog Category',
				'name' => 'blog_category',
				'type' => 'taxonomy',
				'instructions' => 'Choose blog category. All unselected for all categories.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'taxonomy' => 'category',
				'field_type' => 'checkbox',
				'allow_null' => 0,
				'load_save_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),
			array (
				'key' => 'field_54ff065b5d345',
				'label' => 'Blog Category Exclude',
				'name' => 'blog_category_exclude',
				'type' => 'taxonomy',
				'instructions' => 'Choose blog category to exclude from blog list.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'taxonomy' => 'category',
				'field_type' => 'checkbox',
				'allow_null' => 0,
				'load_save_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),
			array (
				'key' => 'field_55b20e1f47f68',
				'label' => 'Blog Featured Exclude',
				'name' => 'blog_featured_exclude',
				'type' => 'true_false',
				'instructions' => 'Check this to exclude the featured posts from the blog.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'message' => 'Exclude featured posts',
				'default_value' => 1,
			),
			array (
				'key' => 'field_54f5bc984b5a7',
				'label' => 'Blog Posts per Page',
				'name' => 'blog_count',
				'type' => 'number',
				'instructions' => 'Defaults to blog posts number set in Settings->Reading. For no limit use \' -1 \'.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => 8,
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_54f5bcd04b5a8',
				'label' => 'Blog Pagination',
				'name' => 'blog_pagination',
				'type' => 'select',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'choices' => array (
					'pagination' => 'Classic Pagination',
					'load_more' => 'Load More Button',
					'custom_link' => 'Custom Link Button',
					'off' => 'No Pagination',
				),
				'default_value' => 'pagination',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_587777a14c7ac',
				'label' => 'Pagination: Custom More-Button Text',
				'name' => 'more_button_text',
				'type' => 'text',
				'instructions' => 'Add text for your custom more-button.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5bcd04b5a8',
							'operator' => '==',
							'value' => 'custom_link',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => 'See More Posts',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_587778214c7ad',
				'label' => 'Pagination: Custom More-Button Link',
				'name' => 'more_button_link',
				'type' => 'text',
				'instructions' => 'Add link for your custom more-button.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5bcd04b5a8',
							'operator' => '==',
							'value' => 'custom_link',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => 'http://',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_581351e41db6c',
				'label' => 'Blog Ads',
				'name' => 'blog_ads',
				'type' => 'select',
				'instructions' => 'Choose after how many posts the \'Ad Area in Blog\' Widget is displayed (repeating). <br>Configure this widget in Appearance -> Widgets.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'choices' => array (
					0 => '0 - Off',
					1 => 1,
					2 => 2,
					3 => 3,
					4 => 4,
					5 => 5,
					6 => 6,
					7 => 7,
					8 => 8,
					'' => '',
				),
				'default_value' => 0,
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_hygge_page_enable_sticky_posts',
				'label' => 'Enable Sticky Posts',
				'name' => 'enable_sticky_posts',
				'type' => 'true_false',
				'instructions' => 'Check this to show sticky posts at the top.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f5b6de6a3c4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'message' => 'Enable Sticky Posts',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_post-settings',
		'title' => 'Post Settings',
		'fields' => array (
			array (
				'key' => 'field_post_setting_use_sidebar',
				'label' => 'Use Sidebar',
				'name' => 'use_sidebar',
				'type' => 'select',
				'instructions' => 'Override the setting for Use Sidebar set in Customizer > Layout.',
				'choices' => array (
					'true' => 'Yes (Override Layout Setting)',
					'false' => 'No (Override Layout Setting)',
				),
				'default_value' => '',
				'allow_null' => 1,
				'multiple' => 0,
			),
			array (
				'key' => 'field_54f2ef49a6166',
				'label' => 'What is displayed as Cover?',
				'name' => 'cover_media',
				'type' => 'select',
				'instructions' => 'Choose what is displayed as Cover. Defaults to Featured Image.',
				'choices' => array (
					'featured' => 'Featured Image',
					'gallery' => 'Gallery (first in content)',
					'video' => 'Video (first in content)',
					'custom' => 'Custom Cover Image',
					'title' => 'Only Title (no media)',
					'none' => 'Hide Cover',
				),
				'default_value' => 'featured',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5944520bba9dc',
				'label' => 'Custom Cover Image',
				'name' => 'custom_cover_image',
				'type' => 'background',
				'instructions' => 'Customize cover image to be used instead of the featured image on the page.',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_54f2ef49a6166',
							'operator' => '==',
							'value' => 'custom',
						),
					),
					'allorany' => 'all',
				),
				'bg' => '',
			),
			array (
				'key' => 'field_59445228ba9dd',
				'label' => 'Cover Description',
				'name' => 'cover_description',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 2,
				'formatting' => 'br',
			),
			array (
				'key' => 'field_58e9131e9f34f',
				'label' => 'Post Meta is Sticky',
				'name' => 'post_meta_sticky',
				'type' => 'true_false',
				'instructions' => 'Uncheck to make the first Sideblock with Post Meta details not stick.',
				'message' => 'Post Meta Sideblock is sticky',
				'default_value' => 1,
			),
			array (
				'key' => 'field_54f2f028a6167',
				'label' => 'Enable Comments',
				'name' => 'comments_use',
				'type' => 'true_false',
				'instructions' => 'Uncheck to completely disable comments, including previously posted.',
				'message' => 'Enable Comments',
				'default_value' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_taxonomy-settings',
		'title' => 'Taxonomy Settings',
		'fields' => array (
			array (
				'key' => 'field_575c4161f4a35',
				'label' => 'Image',
				'name' => 'image',
				'type' => 'image',
				'instructions' => 'Choose featured image for this taxonomy',
				'save_format' => 'id',
				'preview_size' => 'medium',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'category',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_category-settings',
		'title' => 'Category Settings',
		'fields' => array (
			array (
				'key' => 'field_576c307355460',
				'label' => 'Color',
				'name' => 'color',
				'type' => 'color_picker',
				'instructions' => 'Choose category color.',
				'default_value' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'category',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));
}
 ?>
