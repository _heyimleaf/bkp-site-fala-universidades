<?php

add_action('init', 'hygge_tinymce_extend');

if( !function_exists('hygge_tinymce_extend') ){
function hygge_tinymce_extend() {
	if( get_user_option('rich_editing') == 'true' ) {
		add_filter( 'mce_external_plugins', 'hygge_mce_add_plugin' );
		add_filter( 'mce_buttons', 'hygge_mce_register_buttons' );
		add_filter( 'mce_buttons_2', 'hygge_mce_include_hidden_buttons' );
		add_filter( 'tiny_mce_before_init', 'hygge_mce_customization' );
	}
}
}



if( !function_exists('hygge_mce_add_plugin') ){
function hygge_mce_add_plugin( $plugin_array ) {
	$plugin_array['hygge_tinymce_extend'] =  HYGGE_FRAMEWORK_URI . '/tinymce-extend/tinymce-shortcodes.js';
	return $plugin_array;
}
}



if( !function_exists('hygge_mce_register_buttons') ){
function hygge_mce_register_buttons( $buttons ) {
	// add hidden formats button
	array_push( $buttons, "styleselect" );
	// add custom buttons
	array_push( $buttons, "hygge_elements" );
	array_push( $buttons, "hygge_sideblock" );
	array_push( $buttons, "hygge_fullwidth" );
	return $buttons;
}
}




if( !function_exists('hygge_mce_include_hidden_buttons') ){
function hygge_mce_include_hidden_buttons($buttons) {
	// Add in a core button that's disabled by default
	$buttons[] = 'fontsizeselect';
	return $buttons;
}
}


// Customize mce editor font sizes
if( !function_exists('hygge_mce_customization') ){
function hygge_mce_customization( $initArray ){

	// Font Sizes
	$initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 20px 21px 22px 24px 28px 32px 36px 50px 70px 120px";

	// Colors
	$default_colours = ' "000000", "Black", "993300", "Burnt orange", "333300", "Dark olive", "003300", "Dark green", "003366", "Dark azure", "000080", "Navy Blue", "333399", "Indigo", "333333", "Very dark gray", "800000", "Maroon", "FF6600", "Orange", "808000", "Olive", "008000", "Green", "008080", "Teal", "0000FF", "Blue", "666699", "Grayish blue", "808080", "Gray", "FF0000", "Red", "FF9900", "Amber", "99CC00", "Yellow green", "339966", "Sea green", "33CCCC", "Turquoise", "3366FF", "Royal blue", "800080", "Purple", "999999", "Medium gray", "FF00FF", "Magenta", "FFCC00", "Gold", "FFFF00", "Yellow", "00FF00", "Lime", "00FFFF", "Aqua", "00CCFF", "Sky blue", "993366", "Brown", "C0C0C0", "Silver", "FF99CC", "Pink", "FFCC99", "Peach", "FFFF99", "Light yellow", "CCFFCC", "Pale green", "CCFFFF", "Pale cyan", "99CCFF", "Light sky blue", "CC99FF", "Plum", "FFFFFF", "White" ';

  $theme_settings = hygge_theme_settings();
	$custom_colours = '
		"'.substr( $theme_settings['colors']['color_primary'], 1) .'", "Custom Primary Color",
		"'.substr( $theme_settings['colors']['color_white'], 1) .'", "Custom White",
		"'.substr( $theme_settings['colors']['color_grey_pale'], 1) .'", "Custom Grey Pale",
		"'.substr( $theme_settings['colors']['color_grey_light'], 1) .'", "Custom Grey Light",
		"'.substr( $theme_settings['colors']['color_grey'], 1) .'", "Custom Grey",
		"'.substr( $theme_settings['colors']['color_black'], 1) .'", "Custom Black",
	';

	$initArray['textcolor_map'] = '['.$default_colours.','.$custom_colours.']';
	$initArray['textcolor_rows'] = 6;



	// Add Hygge styles in Formats button
	$hygge_styles = array(
		array(
			'title'		=> esc_html__( 'Wrap in Sideblock', 'hygge' ),
			'inline' 	=> '',
			'block' 	=> 'div',
			'selector'=> '',
			'classes'	=> 'hygge-sideblock',
			'wrapper'	=> true
		),
		array(
			'title'		=> esc_html__( 'Dropcap', 'hygge' ),
			'inline' 	=> 'span',
			'block' 	=> '',
			'selector'	=> '',
			'classes'	=> 'dropcap'
		),
		array(
			'title'		=> esc_html__( 'Description Heading', 'hygge' ),
			'inline' 	=> '',
			'block' 	=> 'p',
			'selector'	=> '',
			'classes'	=> 'description-heading'
		),
		array(
			'title'		=> esc_html__( 'Cite (for Blockquote)', 'hygge' ),
			'inline' 	=> 'cite',
			'block' 	=> '',
			'selector'=> '',
			'attributes' => array (
				'data-author' => esc_html__( 'Author', 'hygge' )
			),
			'classes'	=> ''
		),
		array(
			'title'		=> esc_html__( 'Label Value', 'hygge' ),
			'inline' 	=> 'span',
			'block' 	=> '',
			'selector'	=> '',
			'classes'	=> 'label'
		),
		array(
			'title'	=> esc_html__( 'Hygge Buttons', 'hygge' ),
			'items' => array(
				array(
					'title'		=> esc_html__( 'Underlined link', 'hygge' ),
					'selector'	=> 'a',
					'classes'	=> 'link--underline'
				),
				array(
					'title'		=> esc_html__( 'Black Button', 'hygge' ),
					'selector'	=> 'a',
					'classes'	=> 'button button--black'
				),
				array(
					'title'		=> esc_html__( 'White Button', 'hygge' ),
					'selector'	=> 'a',
					'classes'	=> 'button button--white'
				),
				array(
					'title'		=> esc_html__( 'Small Black Button', 'hygge' ),
					'selector'	=> 'a',
					'classes'	=> 'button button--black button--small'
				),
				array(
					'title'		=> esc_html__( 'Small White Button', 'hygge' ),
					'selector'	=> 'a',
					'classes'	=> 'button button--white button--small'
				),
			),
		),
	);

	// Merge old & new styles
	// Just uncomment the following line to display the original Formats as well
	// $initArray['style_formats_merge'] = true;

	// Add new styles
	$initArray['style_formats'] = json_encode( $hygge_styles );



	return $initArray;
}
}
