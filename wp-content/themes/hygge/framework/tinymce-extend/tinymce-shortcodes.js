
/* Shortcodes List
 *------------------------------------------------------------*/

(function() {
"use strict";

tinymce.PluginManager.add('hygge_tinymce_extend', function(editor, url) {

	// Adds a dropdown menu with typography elements
	editor.addButton('hygge_elements', {
		type: 'menubutton',
		text: 'Elements',
		icon: false,
		menu: [
			{
				text: 'Rich Image',
				onclick: function() {
					jQuery(document).trigger('hygge:tinymceShortcodeGenerator', {
						editor: editor,
						shortcode: 'hygge_rich_image',
						info: 'Displays image with large description over it. Best used for larger images, and within <code>Full Width</code> for full effect.',
						attributes: [
							{
								'name': 'title',
								'title': 'Title',
								'description': '',
								'placeholder': 'Image Title',
								'type': 'text'
							},
							{
								'name': 'image',
								'title': 'Image',
								'description': '',
								'placeholder': 'http://',
								'type': 'image-url'
							},
							{
								'name': 'description',
								'title': 'Description',
								'description': 'A bit longer text providing additional information. Will be displayed below the title.',
								'placeholder': 'Description text',
								'type': 'textarea'
							},
						]
					});
				}
			},
			{
				text: 'Custom Banner',
				onclick: function() {
					jQuery(document).trigger('hygge:tinymceShortcodeGenerator', {
						editor: editor,
						shortcode: 'hygge_custom_banner',
						info: 'Displays image with large description over it. Best used for larger images, and within <code>Full Width</code>.',
						attributes: [
							{
								'name': 'title',
								'title': 'Title',
								'description': '',
								'placeholder': 'Banner Title',
								'type': 'text'
							},
							{
								'name': 'title_label',
								'title': 'Title Label (above title)',
								'description': '',
								'placeholder': 'Banner Title label',
								'type': 'text'
							},
							{
								'name': 'description',
								'title': 'Description',
								'description': 'A bit longer text providing additional information. Will be displayed below the title.',
								'placeholder': 'Description text',
								'type': 'textarea'
							},
							{
								'name': 'image',
								'title': 'Image',
								'description': '',
								'placeholder': 'http://',
								'type': 'image-url'
							},
							{
								'name': 'height',
								'title': 'Height',
								'description': 'Set fixed height for the banner, in px. Leave empty to automatically set height based on the image height.',
								'placeholder': '400',
								'type': 'text'
							},
							{
								'name': 'link_text',
								'title': 'Button Text:',
								'description': 'Create custom button for the banner.',
								'placeholder': 'Button Text',
								'type': 'text'
							},
							{
								'name': 'link_url',
								'title': 'Button URL:',
								'description': 'This is URL for the custom banner\'s button.',
								'placeholder': 'http://',
								'type': 'text'
							},
							{
								'name': 'link_target',
								'title': 'Open link in new tab',
								'type': 'checkbox',
								'default': 'off'
							},
						]
					});
				}
			},
			{
				text: 'Blog',
				icon: false,
				onclick: function() {
					var options_category = _.clone(hygge.nodes.cats_slug) || [];
					options_category.unshift({'':'-- All --'});
					var options_tag = _.clone(hygge.nodes.tags_slug) || [];
					options_tag.unshift({'':'-- All --'});

					jQuery(document).trigger('hygge:tinymceShortcodeGenerator', {
						editor: editor,
						shortcode: 'hygge_blog',
						info: 'Customize and display blog posts.',
						attributes: [
							{
								'name': 'title',
								'title': 'Blog Title',
								'description': 'Add custom title for the blog. Leave empty for no title.',
								'placeholder': 'Blog Title',
								'type': 'text'
							},
							{
								'name': 'posts',
								'title': 'Number of posts',
								'description': 'Defaults to WordPress\' setting. To display all posts use -1',
								'placeholder': '8',
								'type': 'text'
							},
							{
								'name': 'style',
								'title': 'Layout Style',
								'type': 'select',
								'values': {
									'list': 'List',
									'list-highlights': 'List with Highlights',
									'grid': 'Grid',
									'grid-3-1': 'Grid 3-1',
									'grid-3-2': 'Grid 3-2',
									'masonry': 'Masonry',
									// 'gallery': 'Gallery',
									// 'archive_gallery': 'Archive Gallery',
									'filmstrip': 'Filmstrip',
									'carousel': 'Carousel',
									'widget-list': 'Widget List'
								},
								'default': 'list'
							},
							{
								'name': 'category',
								'title': 'Category',
								'type': 'select',
								'values': options_category,
								'default': ''
							},
							{
								'name': 'tag',
								'title': 'Tag',
								'type': 'select',
								'values': options_tag,
								'default': ''
							},
							{
								'name': 'sort_by',
								'title': 'Sort By:',
								'type': 'select',
								'values': {
									'date': 'Date',
									'modified': 'Modified date',
									'comment_count': 'Comment count',
									'title': 'Title',
									'name': 'Post name (slug)',
									'ID': 'Post ID',
									'type': 'Post type',
									'author': 'Author',
									'rand': 'Random',
									'none': 'No order',
								},
								'default': 'date'
							},
							{
								'name': 'sort_order',
								'title': 'Sort Order:',
								'type': 'select',
								'values': {
									'DESC': 'Descending',
									'ASC': 'Ascending',
								},
								'default': 'DESC'
							},
							{
								'name': 'link_text',
								'title': 'More-Button Text:',
								'description': 'Create a custom link below the posts. This is text of the button. Leave empty to not use the button.',
								'placeholder': 'See All Articles',
								'type': 'text'
							},
							{
								'name': 'link_url',
								'title': 'More-Button URL:',
								'description': 'This is URL for the custom More button.',
								'placeholder': 'http://',
								'type': 'text'
							},
							{
								'name': 'ads',
								'title': 'Ads:',
								'description': 'Choose after how many posts the \'Ad Area in Blog\' Widget is displayed (repeating). Configure this widget in Appearance -> Widgets.',
								'type': 'select',
								'values': {
									'0': '0 - Off',
									'1': '1',
									'2': '2',
									'3': '3',
									'4': '4',
									'5': '5',
									'6': '6',
									'7': '7',
									'8': '8'
								},
								'default': '0'
							},
							{
								'name': 'featured_exclude',
								'title': 'Exclude Featured Posts',
								'type': 'checkbox',
								'default': 'off'
							},
							{
								'name': 'interval',
								'title': 'Animation interval',
								'description': 'Used for filmstrip to specify auto-animation frequency in milliseconds (ms). Defaults to 0 (auto-animation is off).',
								'placeholder': '4000',
								'type': 'text'
							},
						]
					});
				}
			},
			{
				text: 'Ad Widget',
				icon: false,
				onclick: function() {
					jQuery(document).trigger('hygge:tinymceShortcodeGenerator', {
						editor: editor,
						shortcode: 'hygge_ad_widget',
						info: 'Displays your ad widgets from Ad Area - Square/Skyscraper/Wide.',
						attributes: [
							{
								'name': 'type',
								'title': 'Ad Area to use:',
								'description': 'For Sideblock use Square ad; for Full Width section use Wide ad.',
								'type': 'select',
								'values': {
									'square': 'Ad Area - Square',
									'skyscraper': 'Ad Area - Skyscraper',
									'wide': 'Ad Area - Wide',
								},
								'default': 'square'
							},
						]
					});
				}
			},
			{
				text: 'Icon',
				onclick: function() {
					jQuery(document).trigger('hygge:tinymceShortcodeGenerator', {
						editor: editor,
						shortcode: 'hygge_icon',
						icon_element: true
					});
					// jQuery(document).trigger('hygge:tinymceGetIconClick', {
					// 	editor: editor,
					// 	icon_element: true
					// });
				}
			},
			{
				text: 'Scroll-To Anchor',
				icon: false,
				onclick: function() {
					jQuery(document).trigger('hygge:tinymceShortcodeGenerator', {
						editor: editor,
						shortcode: 'hygge_anchor',
						info: 'Invisible anchor element that can be easily linked to.<br>Example: link with URL <code>#anchor-1</code> will scroll to anchor with ID <code>anchor-1</code> on the same page.<br>Can be used from other pages by adding the anchor link at the end of the url: <code>http://website.com/page#anchor-1</code>.',
						attributes: [
							{
								'name': 'id',
								'title': 'Anchor #ID',
								'description': 'Anchor ID name to link to for scrolling. ',
								'placeholder': 'anchor-1',
								'type': 'text'
							}
						]
					});
				}
			},
			{
				text: 'Small Separator',
				onclick: function() {
					editor.insertContent('<hr class="hygge-separator--small" />');
				}
			},
			{
				text: 'Alignment Clear',
				onclick: function() {
					editor.insertContent('<hr class="cl">');
				}
			},
			{
				text: 'Invisible Spacing',
				icon: false,
				onclick: function() {
					jQuery(document).trigger('hygge:tinymceShortcodeGenerator', {
						editor: editor,
						shortcode: 'hygge_spacing',
						info: 'Vertical spacing, if in dire need of one.',
						attributes: [
							{
								'name': 'height',
								'title': 'Height',
								'description': 'Custom height in pixels for empty spacing element.',
								'placeholder': '15',
								'type': 'text'
							}
						]
					});
				}
			}
		]
	});

	editor.addButton('hygge_sideblock', {
		type: 'splitbutton',
		text: 'Sideblock',
		icon: false,
		onclick: function() {
			editor.insertContent('<div class="hygge-sideblock"><p>&nbsp;</p></div>');
		},
		menu: [
			{
				text: 'Non-Sticky Sideblock',
				onclick: function() {
					editor.insertContent('<div class="hygge-sideblock hygge-sideblock--non-sticky"><p>&nbsp;</p></div>');
				}
			},
			{
				text: 'Border to unstick previous sideblock',
				onclick: function() {
					editor.insertContent('<hr class="sideblock-clear">');
				}
			},
		]
	});

	editor.addButton('hygge_fullwidth', {
		type: 'splitbutton',
		text: 'Full Width',
		icon: false,
		onclick: function() {
			editor.insertContent('<div class="hygge-fullwidth"><p>&nbsp;</p></div>');
		},
		menu: [
			{
				text: 'Content Width',
				onclick: function() {
					editor.insertContent('<div class="hygge-contentwidth"><p>&nbsp;</p></div>');
				}
			},
			// {
			// 	text: 'Light Background',
			// 	onclick: function() {
			// 		jQuery(document).trigger('hygge:tinymceGetBgClick', {
			// 			editor: editor,
			// 			element: 'fullwidth'
			// 		});
			// 	}
			// },
			// {
			// 	text: 'Dark Background',
			// 	onclick: function() {
			// 		jQuery(document).trigger('hygge:tinymceGetBgClick', {
			// 			editor: editor,
			// 			element: 'fullwidth',
			// 			dark: true
			// 		});
			// 	}
			// }
		]
	});

});

})();
