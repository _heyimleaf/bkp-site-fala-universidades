<?php

/*------------------------------------------------------------
 * Add Action
 *------------------------------------------------------------*/

add_action('init', 'hygge_register_nav_menu');



/*------------------------------------------------------------
 * Register menus locations
 *------------------------------------------------------------*/

function hygge_register_nav_menu() {
	register_nav_menus(array(
		'main-menu' => esc_html__('Main Menu', 'hygge'),
		'mobile-menu' => esc_html__('Mobile Menu', 'hygge')
	));
}



/*------------------------------------------------------------
 * Navigation Menus
 *------------------------------------------------------------*/

function hygge_nav_menu_header() {
	wp_nav_menu(array(
		'theme_location'  => 'main-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'hygge_main_menu_fallback',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => new hygge_megamenu_walker
	));
}

function hygge_nav_menu_mobile() {
	wp_nav_menu(array(
		'theme_location'  => 'mobile-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'hygge_nav_menu_header',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
	));
}



/*------------------------------------------------------------
 * Main Menu Fallback
 * http://wordpress.stackexchange.com/questions/64515/fall-back-for-main-menu/64526#64526
 *------------------------------------------------------------*/

function hygge_main_menu_fallback( $args ){

		extract( $args );

		$link = $link_before . '<a href="' .  esc_url( home_url('/') )  . '">' . $before . 'Home' . $after . '</a>' . $link_after;

		// We have a list
		if( FALSE !== stripos( $items_wrap, '<ul' ) or FALSE !== stripos( $items_wrap, '<ol' ) ){
			$link = "<li>$link</li>";
		}

		$output = sprintf( $items_wrap, $menu_id, $menu_class, $link );
		if( !empty( $container ) ){
			$output  = "<$container class='$container_class' id='$container_id'>$output</$container>";
		}

		if( $echo ){
			echo $output;
		}

		return $output;
}
