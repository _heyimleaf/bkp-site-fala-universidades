<?php

/*------------------------------------------------------------
 * Define Sidebars
 *------------------------------------------------------------*/

if (function_exists('register_sidebar')) {

	register_sidebar(array(
		'name' => esc_html__('Sidebar Area', 'hygge'),
		'description' => esc_html__('Main Sidebar Area.', 'hygge'),
		'id' => 'widget-area-sidebar-area',
		'before_widget' => '<div id="%1$s" class="widget widget--sidebar %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Header Area', 'hygge'),
		'description' => esc_html__('Main Header Area.', 'hygge'),
		'id' => 'widget-area-header-area',
		'before_widget' => '<div id="%1$s" class="widget widget--header %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Above Page Content Area', 'hygge'),
		'description' => esc_html__('This area is located below header, before the main page content starts. It is created mainly for Featured Banners and Ads, though it can be used for any other widget.', 'hygge'),
		'id' => 'widget-area-before-content',
		'before_widget' => '<div id="%1$s" class="widget widget--before-content %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Below Page Content Area', 'hygge'),
		'description' => esc_html__('This area is located after the main page content ends. It is created as an Ad container, though it can be used for any other widget.', 'hygge'),
		'id' => 'widget-area-after-content',
		'before_widget' => '<div id="%1$s" class="widget widget--after-content %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Footer Copyright Area', 'hygge'),
		'description' => esc_html__('Widget area in the right part of the footer, next to copyright.', 'hygge'),
		'id' => 'footer-area',
		'before_widget' => '<div id="%1$s" class="widget widget--footer %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Footer Instagram Area', 'hygge'),
		'description' => esc_html__('Widget area in footer used specifically for Instagram widget. You need to install the recommended Instagram plugin, and assign it\'s widget in this area.', 'hygge'),
		'id' => 'footer-area-instagram',
		'before_widget' => '<div id="%1$s" class="widget widget--footer-area-instagram %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Ad Area - Square', 'hygge'),
		'description' => esc_html__('Ad Area used in appropriate blog layouts (smaller, square-like) and in Sideblock. Blog ad can be enabled in the appropriate blog settings - Theme Customizer for homepage/archives, Blog settings in dedicated page blog, or Blog shortcode. Sideblock ad can be added via content editor. Use appropriate ad dimensions here (300x250, 250x250, 336x280).', 'hygge'),
		'id' => 'widget-area-ad-square',
		'before_widget' => '<div id="%1$s" class="widget widget--ad widget--ad-square %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Ad Area - Skyscraper', 'hygge'),
		'description' => esc_html__('Ad Area appropriate for usage in Sideblock. Sideblock ad can be added via content editor. Use appropriate ad dimensions here (300x600).', 'hygge'),
		'id' => 'widget-area-ad-skyscraper',
		'before_widget' => '<div id="%1$s" class="widget widget--ad widget--ad-skyscraper %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Ad Area - Wide', 'hygge'),
		'description' => esc_html__('Ad Area used in appropriate blog layouts (wide, full-width list) and in Full Width section. Blog ad can be enabled in the appropriate blog settings - Theme Customizer for homepage/archives, Blog settings in dedicated page blog, or Blog shortcode. Full Width ad can be added via content editor. Use appropriate ad dimensions here (728x90).', 'hygge'),
		'id' => 'widget-area-ad-wide',
		'before_widget' => '<div id="%1$s" class="widget widget--ad widget--ad-wide %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('404 Area', 'hygge'),
		'description' => esc_html__('404 Area is used as main content on 404 Error Page.', 'hygge'),
		'id' => 'page-404-area',
		'before_widget' => '<div id="%1$s" class="widget widget--404 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

	register_sidebar(array(
		'name' => esc_html__('Search Results Area', 'hygge'),
		'description' => esc_html__('This area is used as main content if search provides 0 results.', 'hygge'),
		'id' => 'search-results',
		'before_widget' => '<div id="%1$s" class="widget widget--search-results %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget__title"><span>',
		'after_title' => '</span></h4>'
	));

}
