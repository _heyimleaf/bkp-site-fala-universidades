<?php

/*------------------------------------------------------------
* Enhance Gallery with custom settings
*------------------------------------------------------------*/

if( !function_exists( 'hygge_gallery_custom_settings' ) ){
	function hygge_gallery_custom_settings(){
		add_action('print_media_templates', function(){
			?>
			<script type="text/html" id="tmpl-hygge-gallery-settings">
				<label class="setting">
					<span><?php esc_html_e('Gallery Style', 'hygge'); ?></span>
					<select data-setting="hygge_style">
						<option value="fluid">Fluid</option>
						<option value="masonry">Masonry</option>
						<option value="filmstrip">Filmstrip</option>
						<option value="slider">Image Slider</option>
					</select>
				</label>
				<label class="setting">
					<span style="text-align: left;"><?php esc_html_e('Max height for filmstrip,slider or fluid row.', 'hygge'); ?></span>
					<input type="text" style="float:left;" value="" data-setting="hygge_height"><span> px</span>
				</label>
				<label class="setting">
					<span style="text-align: left;"><?php esc_html_e('Animation frequency for filmstrip/slider in milliseconds. Use 0 to disable automatic animation.', 'hygge'); ?></span>
					<input type="text" style="float:left;" value="0" data-setting="hygge_interval"><span> ms</span>
				</label>
			</script>
			<?php
		});
	}
}

hygge_gallery_custom_settings();





/*------------------------------------------------------------
* Change WP Gallery shortcode output
* Base code copied from wp-includes/media.php
*------------------------------------------------------------*/

add_filter( 'post_gallery', 'hygge_post_gallery', 10, 2 );

if( !function_exists( 'hygge_post_gallery' ) ){
	function hygge_post_gallery( $output = '', $attr, $content = false, $tag = false ) {


		$theme_settings = hygge_theme_settings();
		$post = get_post();

		static $instance = 0;
		$instance++;

		if ( ! empty( $attr['ids'] ) ) {
			// 'ids' is explicitly ordered, unless you specify otherwise.
			if ( empty( $attr['orderby'] ) ) {
				$attr['orderby'] = 'post__in';
			}
			$attr['include'] = $attr['ids'];
		}


		// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
		if ( isset( $attr['orderby'] ) ) {
			$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
			if ( ! $attr['orderby'] ) {
				unset( $attr['orderby'] );
			}
		}

		$html5 = current_theme_supports( 'html5', 'gallery' );
		$atts = shortcode_atts( array(
			'order'      => 'ASC',
			'orderby'    => 'menu_order ID',
			'id'         => $post ? $post->ID : 0,
			'itemtag'    => $html5 ? 'figure'     : 'dl',
			'icontag'    => $html5 ? 'div'        : 'dt',
			'captiontag' => $html5 ? 'figcaption' : 'dd',
			'columns'    => 3,
			'size'       => 'hygge-portrait-m',
			'include'    => '',
			'exclude'    => '',
			'link'       => '',
			'hygge_style' => 'fluid',
			'hygge_height' => '',
			'hygge_interval' => '0'
		), $attr, 'gallery' );


		if($atts['hygge_interval'] === ''){
			$atts['hygge_interval'] = 0;
		}

		// change image preset depending on number of columns
		// columns are only effective on masonry
		if( $atts['hygge_style'] == 'masonry' ){

			if( (int)$atts['columns'] == 1 ){}
			if( (int)$atts['columns'] == 2 ){
				if( $atts['size'] == 'hygge-horizontal-m' ){ $atts['size'] = 'hygge-horizontal-l'; }
				if( $atts['size'] == 'hygge-square-m' ){ $atts['size'] = 'hygge-square-l'; }
			}
			if( (int)$atts['columns'] == 3 ){
				if( $atts['size'] == 'hygge-horizontal-m' ){ $atts['size'] = 'hygge-horizontal-l'; }
				if( $atts['size'] == 'hygge-square-m' ){ $atts['size'] = 'hygge-square-l'; }
			}
			if( (int)$atts['columns'] == 4 ){
				if( $atts['size'] == 'hygge-horizontal-m' ){ $atts['size'] = 'hygge-horizontal-l'; }
				if( $atts['size'] == 'hygge-square-m' ){ $atts['size'] = 'hygge-square-l'; }
			}
			if( (int)$atts['columns'] > 3 ){}
			if( (int)$atts['columns'] > 4 ){
				if( $atts['size'] == 'hygge-portrait-m' ){ $atts['size'] = 'hygge-portrait-s'; }
			}

		}

		$id = intval( $atts['id'] );
		if ( 'RAND' == $atts['order'] ) {
			$atts['orderby'] = 'none';
		}

		if ( ! empty( $atts['include'] ) ) {
			$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

			$attachments = array();
			foreach ( $_attachments as $key => $val ) {
				$attachments[$val->ID] = $_attachments[$key];
			}
		} elseif ( ! empty( $atts['exclude'] ) ) {
			$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
		} else {
			$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
		}

		if ( empty( $attachments ) ) {
			return '';
		}

		if ( is_feed() ) {
			$output = "\n";
			foreach ( $attachments as $att_id => $attachment ) {
				$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
			}
			return $output;
		}

		$itemtag = tag_escape( $atts['itemtag'] );
		$captiontag = tag_escape( $atts['captiontag'] );
		$icontag = tag_escape( $atts['icontag'] );
		$valid_tags = wp_kses_allowed_html( 'post' );
		if ( ! isset( $valid_tags[ $itemtag ] ) ) {
			$itemtag = 'dl';
		}
		if ( ! isset( $valid_tags[ $captiontag ] ) ) {
			$captiontag = 'dd';
		}
		if ( ! isset( $valid_tags[ $icontag ] ) ) {
			$icontag = 'dt';
		}

		$columns = intval( $atts['columns'] );
		$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
		$float = is_rtl() ? 'right' : 'left';

		$selector = "gallery-{$instance}";

		$gallery_style = '';


		$size_class = sanitize_html_class( $atts['size'] );

		$gallery_classes = 'gallery--'.$atts['hygge_style'];
		$gallery_data = '';

		if( $atts['hygge_style'] == 'masonry' && $columns > 1 ){
			$gallery_classes .= ' js-gallery-masonry';
		}

		$gallery_data .= " data-interval='".$atts['hygge_interval']."' data-height='".$atts['hygge_height']."'";

		if( $atts['hygge_style'] == 'filmstrip' ){
			$gallery_classes .= ' js-filmstrip';
		}

		if( $atts['hygge_style'] == 'slider' ){
			$gallery_classes .= ' hygge-slider hygge-slider--images';
		}

		if( $atts['link'] === 'file' ){
			$gallery_classes .= ' gallery--lightbox';
		}

		$slider_inwrap_class = $atts['hygge_style'] == 'slider' ? 'hygge-slider__items' : '';
		$gallery_div = "<div id='{$selector}' class='gallery {$gallery_classes} galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}' {$gallery_data}><div class='gallery__inwrap'><div class='gallery__items {$slider_inwrap_class}'>";

		$pager = '<ul class="hygge-ui-pager">';

		/**
		* Filter the default gallery shortcode CSS styles.
		*
		* @since 2.5.0
		*
		* @param string $gallery_style Default gallery shortcode CSS styles.
		* @param string $gallery_div   Opening HTML div container for the gallery shortcode output.
		*/
		$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

		foreach ( $attachments as $id => $attachment ) {
			if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
				$image_output = wp_get_attachment_link( $id, $atts['size'], false, 'alt' );
			} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
				$image_output = wp_get_attachment_image( $id, $atts['size'], false, 'alt' );
			} else {
				$image_output = wp_get_attachment_link( $id, $atts['size'], true, 'alt' );
			}

			$img = wp_get_attachment_image_src( $id, $atts['size'] );

			$gallery_icon_class = '';
			$gallery_icon_style = '';

			if( $atts['hygge_style'] == 'slider' ){
				$gallery_icon_class .= ' hygge-slider__item-inwrap';
				$gallery_icon_style .= 'background-image: url('.$img[0].')';
			}

			if ( isset( $img[2], $img[1] ) ) {
				$gallery_icon_class .= ( $img[2] > $img[1] ) ? ' portrait' : ' landscape';
			}

			$lightbox_icon = '';

			$item_class = $attachment->post_excerpt ? 'wp-caption' : '';
			$item_class .= $atts['hygge_style'] == 'slider' ? ' hygge-slider__item' : '';
			$output .= "<{$itemtag} class='gallery-item {$item_class}' data-item-title='" . esc_attr($attachment->post_title) . "' data-item-caption='" . esc_attr($attachment->post_excerpt) . "' data-item-description='" . esc_attr($attachment->post_content) . "' data-w='{$img[1]}' data-h='{$img[2]}'>";
			$output .= "
			<{$icontag} class='gallery-icon {$gallery_icon_class}' style='{$gallery_icon_style}'>
			$image_output
			$lightbox_icon
			</{$icontag}>";
			if ( $captiontag && trim($attachment->post_excerpt) ) {
				$output .= "
				<{$captiontag} class='wp-caption-text'>
				" . wptexturize($attachment->post_excerpt). "
				</{$captiontag}>";
			}
			$output .= "</{$itemtag}>";

			$pager .= "<li class='hygge-ui-pager__item js-hygge-ui-pager-item'></li>";

		}

		$pager .= '<li class="hygge-ui-pager__item hygge-ui-pager__active-marker"></li>';
		$pager .= '</ul>';

		$arrows = '';
		$arrows .= '<div class="hygge-ui-arrows label">';
		$arrows .= '<span class="hygge-ui-arrow hygge-ui-arrow--prev js-hygge-ui-arrow--prev"><i class="hygge-icon-chevron-left"></i></span>';
		$arrows .= '<span class="hygge-ui-arrow hygge-ui-arrow--next js-hygge-ui-arrow--next"><i class="hygge-icon-chevron-right"></i></span>';
		$arrows .= '</div>';

		$output .= "</div></div>";

		if( count($attachments) > 1 ){
			// NOTE: Pager currently not in use
			// $output .= $atts['hygge_style'] == 'slider' ? $pager : '';
			$output .= $atts['hygge_style'] == 'slider' ? $arrows : '';
		}

		$output .= "</div>\n";

		return $output;
	}
}
