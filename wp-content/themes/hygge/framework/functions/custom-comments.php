<?php

/*------------------------------------------------------------
 * Custom Comments Callback
 *------------------------------------------------------------*/



function hygge_comments($comment, $args, $depth) {

	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}

?>
	<<?php echo esc_attr($tag) ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">

	<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>



		<div class="comment__image">
			<?php echo get_avatar( $comment, 160 ); ?>
		</div>



		<div class="comment__content">

			<div class="comment__meta">
				<?php echo '<div class="comment__author">' . get_comment_author_link() . '</div>'; ?>

				<?php
					echo '<div class="comment__date">';
					// echo get_comment_time();
					echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ' . esc_html__('ago', 'hygge');
					echo '</div>';
				?>

				<?php if ($comment->comment_approved == '0') : ?>
					<div class="comment__moderated"><?php esc_html_e('Your comment is awaiting moderation.', 'hygge') ?></div>
				<?php endif; ?>
			</div>

			<div class="comment__links label">
				<?php edit_comment_link(esc_html__('Edit', 'hygge'),'  ','' );	?>
				<?php comment_reply_link( array_merge( $args, array(
					'add_below' => $add_below,
					'depth' => $depth,
					'max_depth' => $args['max_depth'],
					'reply_text' => esc_html__('Reply', 'hygge'),
					'login_text' => esc_html__('Log in to leave a comment', 'hygge'),
				))); ?>
			</div>

			<div class="comment__text">
				<?php comment_text();?>
			</div>

		</div>




	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>

<?php }
