<?php

/*------------------------------------------------------------
 * Theme Functions
 *------------------------------------------------------------*/



// Remove invalid rel attribute values in the categorylist
add_filter('the_category', 'hygge_remove_category_rel_from_category_list');

if( !function_exists( 'hygge_remove_category_rel_from_category_list' ) ){
	function hygge_remove_category_rel_from_category_list($thelist) {
		return str_replace('rel="category tag"', 'rel="tag"', $thelist);
	}
}



/*------------------------------------------------------------
 * Body Classes
 *------------------------------------------------------------*/

add_filter('body_class', 'hygge_add_body_classes');

if( !function_exists( 'hygge_add_body_classes' ) ){
	function hygge_add_body_classes($classes) {
		global $post;
		$theme_settings = hygge_theme_settings();

		// add theme-name class
		$classes[] = sanitize_html_class('theme-hygge');

		// page slug to body class
		if (is_home()) {
			$key = array_search('blog', $classes);
			if ($key > -1) {
				unset($classes[$key]);
			}
		} elseif (is_page()) {
			$classes[] = sanitize_html_class($post->post_name);
		} elseif (is_singular()) {
			$classes[] = sanitize_html_class($post->post_name);
		}

		// ajax page load true/false
		if( $theme_settings['ajax_load_pages'] ){
			$classes[] = 'js-ajax-load-pages';
		}else{
			$classes[] = 'js-ajax-load-pages--false';
		}

		// featured posts ON, assume visible for js processing
		if( (int)$theme_settings['featured_count'] !== 0 ){
			$classes[] = 'hygge-featured-visible--true';
		}

		// notification bar true/false
		if( $theme_settings['notif_use'] ){
			$classes[] = 'notif--true';
		}else{
			$classes[] = 'notif--false';
		}

		// Sticky Header Menu true/false
		if( $theme_settings['sticky_header_menu'] ){
			$classes[] = 'hygge-sticky-header--true';
		}else{
			$classes[] = 'hygge-sticky-header--false';
		}

		// Sidebar
		if( $theme_settings['use_sidebar'] ){
			$classes[] = 'hygge-sidebar--true';
		}else{
			$classes[] = 'hygge-sidebar--false';
		}

		return $classes;
	}
}



/*------------------------------------------------------------
 * WP Posts: Pagination
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_wp_pagination' ) ){
	function hygge_wp_pagination() {
		global $wp_query;

		$max_num_pages = $wp_query->max_num_pages;
		$current_page = get_query_var('paged') > 1 ? get_query_var('paged') : 1;

		// create classic pagination
		return paginate_links(array(
			'base' => str_replace(999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ),
			'format' => '?paged=%#%',
			'current' => max(1, $current_page),
			'total' => $max_num_pages,
			'prev_text' => '<i class="hygge-icon-arrow-left2"></i>',
			'next_text' => '<i class="hygge-icon-arrow-right2"></i>',
			'before_page_number' => '<b>',
			'after_page_number' => '</b>',
		));
	}
}



/*------------------------------------------------------------
 * WP Posts: Load More
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_wp_load_more' ) ){
	function hygge_wp_load_more(){
		global $wp_query;

		$max_num_pages = $wp_query->max_num_pages;
		$current_page = get_query_var('paged') > 1 ? get_query_var('paged') : 1;

		// create load more button with universal markup for AJAX load
		$link = explode('"', get_next_posts_link());

		if( count($link)>1 ){
			echo '<a class="js-skip-ajax js-load-more--wp-loop button button--grey button--semiwide" href="'. esc_url( $link[1] ) .'" data-max="'. esc_attr( $max_num_pages ) .'" data-current="'. esc_attr( $current_page ) .'">' . esc_html__('Load More', 'hygge') . '</a>';
			echo '<div class="hygge-loader pagination__loader"></div>';
			echo '<div class="pagination__message label">'. esc_html__('No More Posts', 'hygge').'</div>';
		}
	}
}



/*------------------------------------------------------------
 * Custom Excerpts
 *------------------------------------------------------------*/

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'hygge_custom_wp_trim_excerpt', 10, 2);

if( !function_exists( 'hygge_custom_wp_trim_excerpt' ) ){
	function hygge_custom_wp_trim_excerpt($hygge_excerpt, $post=null) {
		$raw_excerpt = $hygge_excerpt;
		$post = !$post ? get_post() : $post;

		if ( $hygge_excerpt == '' ) {

			$hygge_excerpt = get_post_field('post_content', $post->ID);
			$hygge_excerpt = strip_shortcodes( $hygge_excerpt );
			$hygge_excerpt = apply_filters('the_content', $hygge_excerpt);
			$hygge_excerpt = str_replace(']]>', ']]&gt;', $hygge_excerpt);
			$hygge_excerpt = esc_html(strip_tags($hygge_excerpt));

			//Set the excerpt word count and only break after sentence is complete.
			global $hyggeExcerptLength;
			$excerpt_length = apply_filters('excerpt_length', $hyggeExcerptLength);
			$tokens = array();
			$excerptOutput = '';
			$count = 0;

			// Divide the string into tokens; HTML tags, or words, followed by any whitespace
			preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $hygge_excerpt, $tokens);

			foreach ($tokens[0] as $token) {

				if ($count >= $excerpt_length && preg_match('/[\;\:\?\.\!]\s*$/uS', $token)) {
				// Limit reached, continue until ; ? . or ! occur at the end
					$excerptOutput .= trim($token);
					break;
				}

				// Add words to complete sentence
				$count++;

				// Append what's left of the token
				$excerptOutput .= $token;
			}

			$hygge_excerpt = trim(force_balance_tags($excerptOutput));
			return $hygge_excerpt;
		}

		return esc_html(apply_filters('hygge_custom_wp_trim_excerpt', $hygge_excerpt, $raw_excerpt));
	}
}



// Hygge custom excerpt function
// Define global var for setting excerpt length
$hyggeExcerptLength;
function hygge_wp_excerpt( $excerpt_length = 20 ){
	global $hyggeExcerptLength;
	$hyggeExcerptLength = $excerpt_length;

	$output = get_the_excerpt();

	// Add read-more link if title is missing
	// if( !get_the_title() ){
	// 	$output .= '<br>';
	// 	$output .= hygge_continue_reading( true, false, false );
	// }

	echo '<p class="excerpt">' . $output . '</p>';
}



// Custom View Article link to Post
add_filter('excerpt_more', 'hygge_view_article');
add_filter('the_content_more_link', 'hygge_view_article');

if( !function_exists( 'hygge_view_article' ) ){
	function hygge_view_article($more) {
		return '';
	}
}



/* Threaded Comments
 *------------------------------------------------------------*/

add_action('get_header', 'hygge_enable_threaded_comments');

if( !function_exists( 'hygge_enable_threaded_comments' ) ){
	function hygge_enable_threaded_comments() {
		if(
			!is_admin()
			&& is_singular()
			&& comments_open()
			&& ( get_option('thread_comments') == 1 )
		){
			wp_enqueue_script('comment-reply');
		}
	}
}



/*------------------------------------------------------------
 * If featured posts active, remove them from main loop on homepage
 *------------------------------------------------------------*/

add_action( 'pre_get_posts', 'hygge_remove_featured_posts_from_homepage_loop' );

if( !function_exists( 'hygge_remove_featured_posts_from_homepage_loop' ) ){
	function hygge_remove_featured_posts_from_homepage_loop($query){
		$theme_settings = hygge_theme_settings();

		if (
			$theme_settings['featured_exclude']
			&& $theme_settings['featured_count'] // not 0 ( 0 = off)
			&& $query->is_home()
			&& $query->is_main_query()
		){

			// WPML convert to translated tag ID
			$featured_tag = apply_filters( 'wpml_object_id', $theme_settings['featured_tag'], 'post_tag', TRUE );

			// Get the exact posts displayed as featured
			$featured_post_ids = get_posts( array(
					'numberposts'	=> $theme_settings['featured_count'],
					'tag_id'			=> $featured_tag,
					'meta_key'		=> '_thumbnail_id', // contain featured image
					'fields'			=> 'ids', // Only get post IDs
			));

			// Exclude the exact featured posts
			$query->set( 'post__not_in', $featured_post_ids );
		}
	}
}



/*------------------------------------------------------------
 * Split background setting
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_split_bg_setting' ) ){
	function hygge_split_bg_setting($string, $attr){
		$matches = explode('; background-size:', $string);
		$bg = $matches[0];
		if(count($matches)>1){
			$bg_size = $matches[1];
		}else{
			$bg_size = 'auto,auto';
		}

		if($attr == 'bg'){
			// if bg empty, return transparent for valid css
			$bg = $bg == '' ? 'transparent' : $bg;
			return $bg;
		}

		if($attr == 'bg_size'){
			return $bg_size;
		}
	}
}



/*------------------------------------------------------------
 * Split font style into weight and style
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_split_font_style' ) ){
	function hygge_split_font_style($string, $attr){
		$matches = preg_split('#(?<=\d)(?=[a-z])#i', $string);

		if(count($matches)>1){
			$weight = $matches[0];
			$style = $matches[1];
		}else{
			if(($matches[0]=='italic')||($matches[0]=='regular')){
				$weight = '400';
				$style = $matches[0];
			}else{
				$weight = $matches[0];
				$style = 'regular';
			}
		}

		if($attr == 'weight'){
			return $weight;
		}

		if($attr == 'style'){
			if($style=='regular'){
				$style = 'normal';
			}
			return $style;
		}
	}
}



/*------------------------------------------------------------
 * Combine google font settings into URL for enqueue
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_get_google_fonts_embed_url' ) ){
	function hygge_get_google_fonts_embed_url(){
		$theme_settings = hygge_theme_settings();
		$request_url = '';
		$request_url .= '//fonts.googleapis.com/css?family=';

		foreach( $theme_settings['typo'] as $key => $value ){

			if( !$value[0] ){ continue; }

			// Skip web-safe fonts manually added to font collection
			if( in_array( $value[0], array('Arial','Arial Black','Helvetica Neue','Verdana','Tahoma','Century Gothic','Gill Sans','Times New Roman','Baskerville','Palatino','Georgia','Courier New','Lucida Sans Typewriter') ) ){
				continue;
			}

			$request_url .= str_replace(' ', '+', $value[0]);
			$request_url .= ':';
			$request_url .= $value[1];

			// on body font add additional styles for general usage
			if($key == 'font_body'){
				$request_url .= ',100,100italic,200,200italic,300,300italic,regular,italic,400,400italic,700,700italic';
			}

			$request_url .= '|';
		}

		$request_url .= hygge_google_font_subset();
		return $request_url;
	}
}

/* Customize google font character sets
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_google_font_subset' ) ){
	function hygge_google_font_subset(){
		$subset = '';

		// new theme option: set character sets
		$theme_settings = hygge_theme_settings();
		$subset .= str_replace( ' ', '', '&subset='. esc_attr( $theme_settings['character_sets'] ) );

		return $subset;
	}
}



/*------------------------------------------------------------
 * Add ID Column in admin panel and style it
 *------------------------------------------------------------*/

add_filter('manage_posts_columns', 'hygge_posts_columns_id', 10);
add_action('manage_posts_custom_column', 'hygge_posts_custom_id_columns', 10, 2);
add_filter('manage_pages_columns', 'hygge_posts_columns_id', 10);
add_action('manage_pages_custom_column', 'hygge_posts_custom_id_columns', 10, 2);
add_filter('manage_media_columns', 'hygge_posts_columns_id', 10);
add_action('manage_media_custom_column', 'hygge_posts_custom_id_columns', 10, 2);
add_action('admin_head', 'hygge_posts_columns_id_style');

if( !function_exists( 'hygge_posts_columns_id' ) ){
	function hygge_posts_columns_id($columns){
		return array_merge( $columns, array('post_id' => esc_html__('ID', 'hygge')) );
	}
}

if( !function_exists( 'hygge_posts_custom_id_columns' ) ){
	function hygge_posts_custom_id_columns($column_name, $post_id){
		if($column_name === 'post_id'){
			echo esc_html($post_id);
		}
	}
}

if( !function_exists( 'hygge_posts_columns_id_style' ) ){
	function hygge_posts_columns_id_style() {
		echo '<style type="text/css">th#post_id{width:60px;}</style>';
	}
}



/*------------------------------------------------------------
 * Wrap oEmbed into hygge custom container for responsive embeds
 *------------------------------------------------------------*/

add_filter('embed_oembed_html', 'hygge_wrap_oembed', 10, 3);

if( !function_exists( 'hygge_wrap_oembed' ) ){
	function hygge_wrap_oembed($html, $url, $attr) {

		preg_match( "/^(?:https?)?(?::?\/\/)?(?:(?:www)?\.?)(.*)\.(?:com|io|net|org|de|uk|tv|co)/", $url, $matches );

		/*$class = $matches[1] ? 'domain-'.$matches[1] : '';*/
		$class = 'dodomain-';
		return '<div class="hygge-embed-container '.$class.'">'.$html.'</div>';
	}
}



/*------------------------------------------------------------
 * Support Empty Widget Title
 *------------------------------------------------------------*/

add_filter('widget_title','hygge_empty_widget_title');

if( !function_exists( 'hygge_empty_widget_title' ) ){
	function hygge_empty_widget_title($title){
		$titleNew = trim($title);
		if( !$titleNew || $titleNew == ''){
			return null;
		}else{
			return $title;
		}
	}
}





/*------------------------------------------------------------
 * Return correct left/right based on is_rtl()
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_get_rtl' ) ){
	function hygge_get_rtl( $dir ) {

		if( is_rtl() ){
			if( $dir == 'left' ){
				$dir = 'right';
			}elseif( $dir == 'right' ){
				$dir = 'left';
			}
		}

		return $dir;
	}
}



/*------------------------------------------------------------
 * Match and return first <blockquote></blockquote> in a content string
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_match_blockquote' ) ){
	function hygge_match_blockquote( $content, $invert = false ) {

		if( !$invert ){
			preg_match( '/<blockquote.*?<\/blockquote>/uis', $content, $match );
		}else{
			$match = preg_replace( '/<blockquote.*?<\/blockquote>/uis', '', $content, 1);
		}

		if( !empty( $match ) ){
			return is_array($match) ? $match[0] : $match;
		}else{
			return false;
		}
	}
}

/*------------------------------------------------------------
 * Match and return first embeded element from content string
 * Tags: 'audio', 'video', 'object', 'embed', 'iframe'
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_match_first_embed' ) ){
	function hygge_match_first_embed( $content, $invert = false ) {

		$content = do_shortcode( apply_filters( 'the_content', $content ) );

		if( !$invert ){
			preg_match( '/<audio.*?<\/audio>|<video.*?<\/video>|<object.*?<\/object>|<embed.*?<\/embed>|<iframe.*?<\/iframe>/uis', $content, $match );
		}else{
			$match = preg_replace( '/<audio.*?<\/audio>|<video.*?<\/video>|<object.*?<\/object>|<embed.*?<\/embed>|<iframe.*?<\/iframe>/uis', '', $content, 1);
		}

		if( !empty( $match ) ){
			return is_array($match) ? $match[0] : $match;
		}else{
			return false;
		}
	}
}


/*------------------------------------------------------------
 * Match and return first [gallery] shortcode in a content string
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_match_gallery' ) ){
	function hygge_match_gallery( $content, $invert = false ) {

		if( !$invert ){
			preg_match( '/\[gallery.*?\]/uis', $content, $match );
		}else{
			$match = preg_replace( '/\[gallery.*?\]/uis', '', $content, 1);
		}

		if( !empty( $match ) ){
			return is_array($match) ? $match[0] : $match;
		}else{
			return false;
		}
	}
}





/*------------------------------------------------------------
 * Collect categories, tags, pages and posts
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_collect_nodes' ) ){
	function hygge_collect_nodes(){

		// Object's properties are pushed as arrays to force the given order
		// When ID's are used as keys, key is prefixed with the type,
		// to differentiate same ID numbers
		$cat_list = get_categories();
		$tag_list = get_categories( array('taxonomy' => 'post_tag') );
		$all = array();

		foreach( $cat_list as $cat ){
			$all['cats_id'][]['cat-'.$cat->term_id] = $cat->cat_name;
			$all['cats_slug'][][$cat->slug] = $cat->cat_name;
		}
		foreach( $tag_list as $tag ){
			$all['tags_id'][]['tag-'.$tag->term_id] = $tag->cat_name;
			$all['tags_slug'][][$tag->slug] = $tag->cat_name;
		}

		$page_list = get_pages();
		foreach( $page_list as $page ){
			$all['pages_id'][]['page-'.$page->ID] = $page->post_title;
		}

		$post_list = get_posts( array('posts_per_page' => -1) );
		foreach( $post_list as $post ){
			$all['posts_id'][]['post-'.$post->ID] = $post->post_title;
		}

		return $all;
	}
}





/*------------------------------------------------------------
 * Split Pages Link
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_link_split_pages' ) ){
	function hygge_link_split_pages(){
		wp_link_pages(
			array(
				'before' => '<div class="pagination pagination__classic pagination__split">',
				'after' => '</div>',
				'next_or_number'   => 'next',
				'nextpagelink'     => esc_html__( 'Next page', 'hygge' ),
				'previouspagelink' => esc_html__( 'Previous page', 'hygge' )
			)
		);
	}
}





/*------------------------------------------------------------
 * Continue Reading
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_continue_reading' ) ){
	function hygge_continue_reading( $text = true, $icon = true, $echo = true ){

		$output = '';
		$output .= '<a href="' . get_the_permalink() . '" title="' . esc_attr( get_the_title() ) . '" class="read-more label link--underline" data-loader-img="' . hygge_get_loader_img_url() . '">';
			if( $text ){
				$output .= '<span>' . esc_html__( 'Read More', 'hygge' ) . ' </span>';
			}
			if( $icon ){
				$output .= '<span class="read-more__plus"></span>';
			}
		$output .= '</a>';

		if( $echo ){
			echo $output;
		}else{
			return $output;
		}
	}
}




/*------------------------------------------------------------
 * Convert # HEX into RGB color
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_hex_to_rgb' ) ){
	function hygge_hex_to_rgb( $hex = false ){

		if( !$hex ){
			return;
		}

		list($r, $g, $b) = sscanf( $hex, "#%02x%02x%02x");

		return "{$r},{$g},{$b}";
	}
}



/*------------------------------------------------------------
 * Hygge Category - custom color and count limits
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_category' ) ){
	function hygge_category( $count = 99, $id = null ){

		$theme_settings = hygge_theme_settings();

		if( !$id ){
			$id = get_the_ID();
		}

		$cats = wp_get_post_categories($id);

		// if no categories or
		// if only one category and it's Uncategorized ( ID = 1 )
		if(
			count($cats) === 0 ||
			( count($cats) === 1 && $cats[0] === 1 )
		){
			return;
		}

		$output = '';
		$output .= "<div class='post__category js-hygge-even-height'>";
		$output .= "<div class='items'>";



		$i = 0;
		foreach( $cats as $catId ){

			// exclude 'Uncategorized' category
			if( $catId === 1 ){
				continue;
			}

			$cat = get_category( $catId );
			$link = get_category_link( $catId );
			$color = hygge_get_category_color( $catId );
			$img = '';

			if( function_exists('get_field') && $theme_settings['ajax_loader_post_thumbs'] ){
				$img = hygge_get_loader_img_url( $catId, true );
			}

			$output .= "<div class='item'>";
			$output .= "<a class='label' href='$link' rel='tag' style='background-color:{$color};' data-loader-img='{$img}'>$cat->name</a>";
			$output .= "</div>";

			$i++;
			if( $i >= $count ){
				break;
			}
		}


		$output .= "</div>";
		$output .= "</div>";

		return $output;
	}
}



/*------------------------------------------------------------
 * Get Category Color by an tax ID
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_get_category_color' ) ){
 	function hygge_get_category_color( $cat_id = false ){

		// Default color fallback should be provided by through css.

	 	if( !$cat_id ){
			return '';
		}

		$cat = get_category( $cat_id );

		if( function_exists('get_field') ){
			$color = get_field( 'color', $cat ) ? get_field( 'color', $cat ) : false;

			return $color ? $color : false;
		}
 	}
}



/*------------------------------------------------------------
 * Get Post's color based on a category
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_get_post_color' ) ){
	function hygge_get_post_color( $post_id = false ){

		// Default color fallback should be provided through css.

		// use targeted post or fallback to current post
		$post_id = $post_id ? $post_id : get_the_ID();
		$color = '';

		$cats = wp_get_post_categories( $post_id );

		if ( $cats ){
			$color = hygge_get_category_color( $cats[0] );
		}

		return $color;
	}
}



/*------------------------------------------------------------
 * Get loader image url
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_get_loader_img_url' ) ){
	function hygge_get_loader_img_url( $post_id = false, $category = false ){

		$theme_settings = hygge_theme_settings();

		if( !$theme_settings['ajax_loader_post_thumbs'] ){
			return false;
		}

		// To get category loader_img, category ID must be passed
		if( $category && $post_id ){
			$catObj = get_category( $post_id );
			$loader_img = wp_get_attachment_image_src( get_field( 'image', $catObj), 'hygge-loader' );

			if( !empty( $loader_img ) ){
				return esc_attr( $loader_img[0] );
			}
		}

		// use targeted post or fallback to current post
		$post_id = $post_id ? $post_id : get_the_ID();

		if( has_post_thumbnail( $post_id ) ){
			$imgID = get_post_thumbnail_id( $post_id );
			$loader_img = wp_get_attachment_image_src( $imgID, 'hygge-loader' );

			if( !empty( $loader_img ) ){
				return esc_attr( $loader_img[0] );
			}
		}
	}
}



/*------------------------------------------------------------
 * Get dropcap media
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_get_dropcap_media' ) ){
	function hygge_get_dropcap_media( $ratio = 'square', $id = false, $letter = false ){

		// $ratio is expected to be: horizontal, vertical or square
		$output = '';
		$color = false;

		if( !$letter ){
			$color = hygge_get_post_color( $id );
			$title = get_the_title();
			$letter = $title != '' ? substr( $title, 0, 1 ) : '/';
		}

		$output .= "<div class='hygge-dropcap-media ratio-{$ratio} " . hygge_is_color_dark($color, true) . " js-hygge-even-height' style='background-color:{$color};'>";
		$output .= "<div class='h1 letter'>{$letter}</div>";
		$output .= "</div>";

		return $output;
	}
}



/*------------------------------------------------------------
 * Hygge is color dark
 * checks hex values for luminance
 * returns true/false based on the threshold
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_is_color_dark' ) ){
	function hygge_is_color_dark( $color = false, $class = false, $default = false ){
		if( !$color ){
			if ( $default ){
				return $class ? 'dark-mode' : true;
			}
			return;
		}

		$rgb = hygge_hex_to_rgb( $color );
		$rgb = explode(',', $rgb);

		if( empty($rgb) ){
			if ( $default == 'dark' ){
				return $class ? 'dark-mode' : true;
			}
			return;
		}

		$lum = 0.2126 * $rgb[0] + 0.7152 * $rgb[1] + 0.0722 * $rgb[2]; // per ITU-R BT.709

		if( $class ){
			return $lum < 215 ? 'dark-mode' : '';
		}else{
			return $lum < 215;
		}
	}
}

/*------------------------------------------------------------
 * GIF as featured image support
 * Filters all post_thumbnail calls and checks if image is .gif
 * If .gif it returns full size instead of thumbnail
 *------------------------------------------------------------*/

add_filter( 'wp_get_attachment_image_src', 'hygge_thumbnail_gif_support', 10, 5);

if( !function_exists( 'hygge_thumbnail_gif_support' ) ){
	function hygge_thumbnail_gif_support( $image, $attachment_id, $size, $icon ){

		preg_match( '/.*\.gif/', $image[0], $matches_gif );

		// Don't get caught in the infinite loop!
		if( !empty( $matches_gif ) && $size !== 'full' ){
			return wp_get_attachment_image_src( $attachment_id, 'full' );
		}

		return $image;
	}
}



/*------------------------------------------------------------
 * WooCommerce Support
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_woocommerce' ) ){
	function hygge_woocommerce(){
		remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
		remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
	}
}



/*------------------------------------------------------------
 * Get array of allowed KSES tags for titles
 *------------------------------------------------------------*/

if( !function_exists( 'hygge_allowed_kses_tags' ) ){
	function hygge_allowed_kses_tags(){
		return array(
			'small' => array(),
			'br' => array(),
			'strong' => array(),
			'b' => array(),
			'em' => array(),
			'i' => array()
		);
	}
}
