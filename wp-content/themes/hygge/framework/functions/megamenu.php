<?php

/*------------------------------------------------------------
* Augment Menu to have additional fields
*------------------------------------------------------------*/



/* Add custom fields to $item nav object
* in order to be used in custom Walker
*------------------------------------------------------------*/
function hygge_megamenu_add_fields( $menu_item ) {
	$menu_item->megamenu = get_post_meta( $menu_item->ID, '_menu_item_megamenu', true );
	$menu_item->megamenu_img_hide = get_post_meta( $menu_item->ID, '_menu_item_megamenu_img_hide', true );
	return $menu_item;
}

add_filter( 'wp_setup_nav_menu_item', 'hygge_megamenu_add_fields');



/* Save menu custom fields
*------------------------------------------------------------*/
function hygge_megamenu_update_menu_item( $menu_id, $menu_item_db_id, $args ) {
	// Check if element is properly sent and save it
	$megamenu_value = isset($_REQUEST['menu-item-megamenu'][$menu_item_db_id]) ? $_REQUEST['menu-item-megamenu'][$menu_item_db_id] : false;
	update_post_meta( $menu_item_db_id, '_menu_item_megamenu', $megamenu_value );
	$megamenu_img_hide_value = isset($_REQUEST['menu-item-megamenu_img_hide'][$menu_item_db_id]) ? $_REQUEST['menu-item-megamenu_img_hide'][$menu_item_db_id] : false;
	update_post_meta( $menu_item_db_id, '_menu_item_megamenu_img_hide', $megamenu_img_hide_value );
}

add_action( 'wp_update_nav_menu_item', 'hygge_megamenu_update_menu_item', 10, 3 );



/* Define new Walker edit
*------------------------------------------------------------*/
function hygge_megamenu_edit_walker($walker,$menu_id) {
	return 'Walker_Nav_Menu_Edit_Custom';
}

add_filter( 'wp_edit_nav_menu_walker', 'hygge_megamenu_edit_walker', 10, 2 );





/*------------------------------------------------------------
* Edit Custom Walker
* Admin fields display
*------------------------------------------------------------*/

/**
*  /!\ This is a copy of Walker_Nav_Menu_Edit class in core
*
* Navigation Menu API: Walker_Nav_Menu_Edit class
*
* @package WordPress
* @subpackage Administration
* @since 4.4.0
*/

/**
* Create HTML list of nav menu input items.
*
* @package WordPress
* @since 3.0.0
* @uses Walker_Nav_Menu
*/
class Walker_Nav_Menu_Edit_Custom extends Walker_Nav_Menu {
	/**
	* Starts the list before the elements are added.
	*
	* @see Walker_Nav_Menu::start_lvl()
	*
	* @since 3.0.0
	*
	* @param string $output Passed by reference.
	* @param int    $depth  Depth of menu item. Used for padding.
	* @param array  $args   Not used.
	*/
	public function start_lvl( &$output, $depth = 0, $args = array() ) {}

		/**
		* Ends the list of after the elements are added.
		*
		* @see Walker_Nav_Menu::end_lvl()
		*
		* @since 3.0.0
		*
		* @param string $output Passed by reference.
		* @param int    $depth  Depth of menu item. Used for padding.
		* @param array  $args   Not used.
		*/
		public function end_lvl( &$output, $depth = 0, $args = array() ) {}

			/**
			* Start the element output.
			*
			* @see Walker_Nav_Menu::start_el()
			* @since 3.0.0
			*
			* @global int $_wp_nav_menu_max_depth
			*
			* @param string $output Passed by reference. Used to append additional content.
			* @param object $item   Menu item data object.
			* @param int    $depth  Depth of menu item. Used for padding.
			* @param array  $args   Not used.
			* @param int    $id     Not used.
			*/
			public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
				global $_wp_nav_menu_max_depth;
				$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

				ob_start();
				$item_id = esc_attr( $item->ID );
				$removed_args = array(
					'action',
					'customlink-tab',
					'edit-menu-item',
					'menu-item',
					'page-tab',
					'_wpnonce',
				);

				$original_title = '';
				if ( 'taxonomy' == $item->type ) {
					$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
					if ( is_wp_error( $original_title ) )
					$original_title = false;
				} elseif ( 'post_type' == $item->type ) {
					$original_object = get_post( $item->object_id );
					$original_title = get_the_title( $original_object->ID );
				} elseif ( 'post_type_archive' == $item->type ) {
					$original_object = get_post_type_object( $item->object );
					if ( $original_object ) {
						$original_title = $original_object->labels->archives;
					}
				}

				$classes = array(
					'menu-item menu-item-depth-' . $depth,
					'menu-item-' . esc_attr( $item->object ),
					'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
				);

				$title = $item->title;

				if ( ! empty( $item->_invalid ) ) {
					$classes[] = 'menu-item-invalid';
					/* translators: %s: title of menu item which is invalid */
					$title = sprintf( esc_html__( '%s (Invalid)', 'hygge' ), $item->title );
				} elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
					$classes[] = 'pending';
					/* translators: %s: title of menu item in draft status */
					$title = sprintf( esc_html__( '%s (Pending)', 'hygge' ), $item->title );
				}

				$title = ( ! isset( $item->label ) || '' == $item->label ) ? $title : $item->label;

				$submenu_text = '';
				if ( 0 == $depth )
				$submenu_text = 'style="display: none;"';

				?>
				<li id="menu-item-<?php echo esc_attr( $item_id ); ?>" class="<?php echo implode(' ', $classes ); ?>">
					<div class="menu-item-bar">
						<div class="menu-item-handle">
							<span class="item-title"><span class="menu-item-title"><?php echo esc_html( $title ); ?></span> <span class="is-submenu" <?php echo esc_attr( $submenu_text ); ?>><?php esc_html_e( 'sub item', 'hygge' ); ?></span></span>
							<span class="item-controls">
								<span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
								<span class="item-order hide-if-js">
									<a href="<?php
									echo wp_nonce_url(
										add_query_arg(
											array(
												'action' => 'move-up-menu-item',
												'menu-item' => $item_id,
											),
											remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
										),
										'move-menu_item'
									);
									?>" class="item-move-up" aria-label="<?php esc_attr_e( 'Move up', 'hygge' ) ?>">&#8593;</a>
									|
									<a href="<?php
									echo wp_nonce_url(
										add_query_arg(
											array(
												'action' => 'move-down-menu-item',
												'menu-item' => $item_id,
											),
											remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
										),
										'move-menu_item'
									);
									?>" class="item-move-down" aria-label="<?php esc_attr_e( 'Move down', 'hygge' ) ?>">&#8595;</a>
								</span>
								<a class="item-edit" id="edit-<?php echo esc_attr( $item_id ); ?>" href="<?php
								echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
								?>" aria-label="<?php esc_attr_e( 'Edit menu item', 'hygge' ); ?>"><?php esc_html_e( 'Edit', 'hygge' ); ?></a>
							</span>
						</div>
					</div>

					<div class="menu-item-settings wp-clearfix" id="menu-item-settings-<?php echo esc_attr( $item_id ); ?>">
						<?php if ( 'custom' == $item->type ) : ?>
							<p class="field-url description description-wide">
								<label for="edit-menu-item-url-<?php echo esc_attr( $item_id ); ?>">
									<?php esc_html_e( 'URL', 'hygge' ); ?><br />
									<input type="text" id="edit-menu-item-url-<?php echo esc_attr( $item_id ); ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
								</label>
							</p>
						<?php endif; ?>
						<p class="description description-wide">
							<label for="edit-menu-item-title-<?php echo esc_attr( $item_id ); ?>">
								<?php esc_html_e( 'Navigation Label', 'hygge' ); ?><br />
								<input type="text" id="edit-menu-item-title-<?php echo esc_attr( $item_id ); ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
							</label>
						</p>
						<p class="field-title-attribute field-attr-title description description-wide">
							<label for="edit-menu-item-attr-title-<?php echo esc_attr( $item_id ); ?>">
								<?php esc_html_e( 'Title Attribute', 'hygge' ); ?><br />
								<input type="text" id="edit-menu-item-attr-title-<?php echo esc_attr( $item_id ); ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
							</label>
						</p>
						<p class="field-link-target description">
							<label for="edit-menu-item-target-<?php echo esc_attr( $item_id ); ?>">
								<input type="checkbox" id="edit-menu-item-target-<?php echo esc_attr( $item_id ); ?>" value="_blank" name="menu-item-target[<?php echo esc_attr( $item_id ); ?>]"<?php checked( $item->target, '_blank' ); ?> />
								<?php esc_html_e( 'Open link in a new tab', 'hygge' ); ?>
							</label>
						</p>
						<p class="field-css-classes description description-thin">
							<label for="edit-menu-item-classes-<?php echo esc_attr( $item_id ); ?>">
								<?php esc_html_e( 'CSS Classes (optional)', 'hygge' ); ?><br />
								<input type="text" id="edit-menu-item-classes-<?php echo esc_attr( $item_id ); ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
							</label>
						</p>
						<p class="field-xfn description description-thin">
							<label for="edit-menu-item-xfn-<?php echo esc_attr( $item_id ); ?>">
								<?php esc_html_e( 'Link Relationship (XFN)', 'hygge' ); ?><br />
								<input type="text" id="edit-menu-item-xfn-<?php echo esc_attr( $item_id ); ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
							</label>
						</p>
						<p class="field-description description description-wide">
							<label for="edit-menu-item-description-<?php echo esc_attr( $item_id ); ?>">
								<?php esc_html_e( 'Description', 'hygge' ); ?><br />
								<textarea id="edit-menu-item-description-<?php echo esc_attr( $item_id ); ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo esc_attr( $item_id ); ?>]"><?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
								<span class="description"><?php esc_html_e( 'The description will be displayed in the menu if the current theme supports it.', 'hygge' ); ?></span>
							</label>
						</p>

						<?php
						/* New fields insertion starts here */

						if( $depth == 0 ):
							?>
							<p class="field-custom description description-wide">
								<label for="edit-menu-item-megamenu-<?php echo esc_attr( $item_id ); ?>">
									<input type="checkbox" id="edit-menu-item-megamenu-<?php echo esc_attr( $item_id ); ?>" value="megamenu" name="menu-item-megamenu[<?php echo esc_attr( $item_id ); ?>]"<?php checked( $item->megamenu, 'megamenu' ); ?> />
									<?php esc_html_e( 'Display as MegaMenu', 'hygge' ); ?>
								</label>
							</p>
							<p class="field-custom description description-wide">
								<label for="edit-menu-item-megamenu_img_hide-<?php echo esc_attr( $item_id ); ?>">
									<input type="checkbox" id="edit-menu-item-megamenu_img_hide-<?php echo esc_attr( $item_id ); ?>" value="megamenu_img_hide" name="menu-item-megamenu_img_hide[<?php echo esc_attr( $item_id ); ?>]"<?php checked( $item->megamenu_img_hide, 'megamenu_img_hide' ); ?> />
									<?php esc_html_e( 'Hide featured images from MegaMenu', 'hygge' ); ?>
								</label>
							</p>
							<?php
						endif;

						if( $depth != 0 && ( $item->type == 'taxonomy' || $item->type == 'post_type' ) ):
							// here goes the field for the mega-menu sub-items
						endif;

						?>

						<p class="field-move hide-if-no-js description description-wide">
							<label>
								<span><?php esc_html_e( 'Move', 'hygge' ); ?></span>
								<a href="#" class="menus-move menus-move-up" data-dir="up"><?php esc_html_e( 'Up one', 'hygge' ); ?></a>
								<a href="#" class="menus-move menus-move-down" data-dir="down"><?php esc_html_e( 'Down one', 'hygge' ); ?></a>
								<a href="#" class="menus-move menus-move-left" data-dir="left"></a>
								<a href="#" class="menus-move menus-move-right" data-dir="right"></a>
								<a href="#" class="menus-move menus-move-top" data-dir="top"><?php esc_html_e( 'To the top', 'hygge' ); ?></a>
							</label>
						</p>

						<div class="menu-item-actions description-wide submitbox">
							<?php if ( 'custom' != $item->type && $original_title !== false ) : ?>
								<p class="link-to-original">
									<?php printf( esc_html__( 'Original: %s', 'hygge' ), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
								</p>
							<?php endif; ?>
							<a class="item-delete submitdelete deletion" id="delete-<?php echo esc_attr( $item_id ); ?>" href="<?php
							echo wp_nonce_url(
								add_query_arg(
									array(
										'action' => 'delete-menu-item',
										'menu-item' => $item_id,
									),
									admin_url( 'nav-menus.php' )
								),
								'delete-menu_item_' . $item_id
							); ?>"><?php esc_html_e( 'Remove', 'hygge' ); ?></a> <span class="meta-sep hide-if-no-js"> | </span> <a class="item-cancel submitcancel hide-if-no-js" id="cancel-<?php echo esc_attr( $item_id ); ?>" href="<?php echo esc_url( add_query_arg( array( 'edit-menu-item' => $item_id, 'cancel' => time() ), admin_url( 'nav-menus.php' ) ) );
							?>#menu-item-settings-<?php echo esc_attr( $item_id ); ?>"><?php esc_html_e( 'Cancel', 'hygge' ); ?></a>
						</div>

						<input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item_id ); ?>" />
						<input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
						<input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
						<input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
						<input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
						<input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo esc_attr( $item_id ); ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
					</div><!-- .menu-item-settings-->
					<ul class="menu-item-transport"></ul>
					<?php
					$output .= ob_get_clean();
				}

			} // Walker_Nav_Menu_Edit




			/*------------------------------------------------------------
			* Custom Walker
			* Front-end display of the menu
			*------------------------------------------------------------*/

			/**
			* Custom Walker
			*
			* @access      public
			* @since       1.0
			* @return      void
			*/
			class hygge_megamenu_walker extends Walker_Nav_Menu
			{
				function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0)
				{
					global $wp_query;
					$theme_settings = hygge_theme_settings();
					$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

					$class_names = $value = '';

					$classes = empty( $item->classes ) ? array() : (array) $item->classes;

					// Hygge megamenu for depth-0 items
					if( $depth == 0 && $item->megamenu ){
						$classes[] = 'hygge-megamenu';
					}
					if( $depth == 0 && $item->megamenu_img_hide ){
						$classes[] = 'hygge-megamenu_img_hide';
					}

					// Get Loader img url
					$loader_img = '';
					if( $theme_settings['ajax_loader_post_thumbs'] ){

						// Taxonomies
						if( $item->type == 'taxonomy' && function_exists('get_field') ){
							$loader_img = hygge_get_loader_img_url( $item->object_id, true );
						}

						// Post/Page
						if( $item->type == 'post_type' ){
							$loader_img = hygge_get_loader_img_url( $item->object_id );
						}
					}

					// Hygge show image
					$image_ID = '';
					$color = '';
					if( $depth != 0 && ( $item->type == 'taxonomy' || $item->type == 'post_type' ) ){

						// Taxonomies
						if( $item->type == 'taxonomy' && function_exists('get_field') ){
							$term_obj = get_term( $item->object_id );
							$image_ID = get_field( 'image', $term_obj ) ? get_field( 'image', $term_obj ) : $image_ID;
							$color = hygge_get_category_color( $item->object_id );
						}

						// Page and Post
						if( $item->type == 'post_type' ){
							$image_ID = get_post_thumbnail_id( $item->object_id );
							$color = hygge_get_post_color( $item->object_id );
						}
					}

					$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
					$class_names .= ' menu-item-'. $item->ID;
					$class_names = ' class="'. esc_attr( $class_names ) . '"';

					$output .= $indent . '<li ' . $value . $class_names .'>';

					$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
					$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
					$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
					$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

					$prepend = '<strong>';
					$append = '</strong>';
					// $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';
					$description = '';

					if($depth != 0){
						$description = "";
					}

					$item_output = $args->before;
					$linkClass = $image_ID ? 'has-image' : '';
					$hasColorClass = $color ? 'color--true' : 'color--false';
					$item_output .= '<a'. $attributes .' class="'.$linkClass.'" data-loader-img="'. $loader_img .'">';

					if( $image_ID ){
						$image = wp_get_attachment_image_src( $image_ID, 'hygge-horizontal-m' );
						$item_output .= "<div class='hygge-media-hover {$hasColorClass}'><div class='hygge-media-hover__img'><img src='{$image[0]}' alt='{$item->title}'></div>";
						$item_output .= "<div class='hygge-media-hover__hover' style='background-color:{$color};'></div>";
						$item_output .= "</div>";
					}

					$item_output .= $args->link_before . $prepend . wp_kses( apply_filters( 'the_title', $item->title, $item->ID ), hygge_allowed_kses_tags() ). $append;
					$item_output .= $description . $args->link_after;

					$item_output .= '<i class="has-dropdown-icon"></i></a>';
					$item_output .= $args->after;

					$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
				}
			}
