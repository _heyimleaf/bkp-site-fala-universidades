/*	Custom Customize Control Scripts
/*------------------------------------------------------------*/

(function($){
"use strict";
$(document).ready(function(){



/*------------------------------------
 * Font Control
 *------------------------------------*/

$('.hygge-font-control-field').each(function(){
	var $el 		= $(this);
	var $input 		= $el.find('[name="font-control"]');
	var $family 	= $el.find('[name="font-family"]');
	var $style 		= $el.find('[name="font-style"]');
	var $size 		= $el.find('[name="font-size"]');
	var $line 		= $el.find('[name="line-height"]');
	var $transform 	= $el.find('[name="text-transform"]');
	var $letter		= $el.find('[name="letter-spacing"]');

	// on family change, update styles list
	$family.change(function(){
		var styles = $('option:selected',this).attr('data-styles');
		styles = styles.split(',');

		$style.html('');
		$.each( styles, function(key,value) {
			var selected = value == 'regular' ? 'selected' : '';
			$style.append('<option value="'+value+'" '+selected+'>'+value+'</option>');
		});

		updateValue();
	});

	// on any field change, trigger update main customize field
	$style.change(updateValue);
	$size.change(updateValue);
	$line.change(updateValue);
	$size.keyup(updateValue);
	$line.keyup(updateValue);
	$transform.change(updateValue);
	$letter.change(updateValue);
	$letter.keyup(updateValue);

	// collect values into string and add to customize field
	function updateValue(){
		$input.val( $family.val()+'|'+$style.val()+'|'+$size.val()+'|'+$line.val()+'|'+$transform.val()+'|'+$letter.val() );
		// trigger WP Customize field change
		$input.change();
	}
});



/*------------------------------------
 * Enable chosen for selects
 *------------------------------------*/

$('.chosen').chosen();


}); // doc ready end
})(jQuery);
