<?php

// get font list
include_once( HYGGE_ADMIN.'/google-fonts/google-fonts.php' );

class Font_Control extends WP_Customize_Control {
	public function render_content() {
	?>
		<span class="customize-control-title">
			<?php echo esc_html( $this->label ); ?>
		</span>

		<span class="description customize-control-description">
			<?php echo esc_html( $this->description ); ?>
		</span>

		<div class="hygge-font-control-field">



			<!-- Whole Font setting in string -->
			<input name="font-control" type="hidden" value="<?php echo esc_attr($this->value()); ?>" <?php $this->link(); ?>>

			<?php
				$value = explode( '|', $this->value() );

				$active_font_styles = array();

				if ( !isset($value[2]) ) { $value[2] = 16; }
				if ( !isset($value[3]) ) { $value[3] = 1; }
				if ( !isset($value[4]) ) { $value[4] = 'none'; }
				if ( !isset($value[5]) ) { $value[5] = ''; }
			?>



			<!-- Font Family -->
			<?php $google_fonts =  hygge_get_google_fonts(); ?>
			<select name="font-family" class="family chosen">
				<?php foreach ( $google_fonts as $font ) {

					// get font styles
					$styles = '';
					$i = 1;

					$active_font = $value[0] == $font->family ? true : false;

					foreach($font->variants as $variant){

						// get styles string for family data
						if($i > 1){
							$styles .= ',';
						}
						$i++;
						$styles .= $variant;

						// get styles of active family as array for default font-style field
						if($active_font){
							$active_font_styles[] = $variant;
						}
					}

					// print full font family option
					echo '<option class="font-control-option font-control-option--family" value="'.$font->family.'"'.selected($value[0], $font->family).' data-styles="'.$styles.'">'.$font->family.'</option>';
					}
				?>
			</select>



			<!-- Font Style -->
			<select name="font-style" class="style">
				<?php
					foreach($active_font_styles as $style){
						$selected = $value[1] == $style ? 'selected' : '';
						echo '<option value="'.$style.'" '.$selected.'>'.$style.'</option>';
					}
				?>
			</select>



			<!-- Font Size -->
			<input type="text" class="size" name="font-size" placeholder="Font Size" value="<?php echo esc_attr($value[2]); ?>"/>

			<!-- Line Height -->
			<input type="text" class="line" name="line-height" placeholder="Line Height" value="<?php echo esc_attr($value[3]); ?>"/> px/em



			<!-- Text Transform -->
			<select name="text-transform" class="transform">
				<?php

					$text_transform_options = array(
						'none' => esc_html__( 'No transform', 'hygge'),
						'capitalize' => esc_html__( 'Capitalize', 'hygge'),
						'uppercase' => esc_html__( 'Uppercase', 'hygge'),
						'lowercase' => esc_html__( 'Lowercase', 'hygge')
					);

					foreach($text_transform_options as $key => $name){
						$selected = $value[4] == $key ? 'selected' : '';
						echo '<option value="'.$key.'" '.$selected.'>'.$name.'</option>';
					}
				?>
			</select>

			<!-- Letter Spacing -->
			<input type="text" class="letter-spacing" name="letter-spacing" placeholder="Kerning" value="<?php echo esc_attr($value[5]); ?>"/> em



		</div>
	<?php
	}
}
?>
