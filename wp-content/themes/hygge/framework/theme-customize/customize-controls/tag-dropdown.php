<?php
if (class_exists('WP_Customize_Control')) {

	class Tag_Dropdown_Control extends WP_Customize_Control {
		public function render_content() {
		?>
			<span class="customize-control-title">
				<?php echo esc_html( $this->label ); ?>
			</span>
			<select <?php $this->link(); ?> class="chosen">
			<option value="0"><?php esc_attr_e( '--Select Tag--', 'hygge' ); ?></option>
				<?php
					$args = array( 'taxonomy' => 'post_tag' );
					$tags = get_categories($args);

					foreach ( $tags as $tag ) {
						echo '<option value="'.$tag->term_id.'"'.selected($this->value(), $tag->term_id).'>'.$tag->name.'</option>';
					}
				?>
			</select>
		<?php
		}
	}
}
