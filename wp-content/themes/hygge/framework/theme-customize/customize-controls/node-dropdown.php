<?php
if (class_exists('WP_Customize_Control')) {

	class Node_Dropdown_Control extends WP_Customize_Control {
		public function render_content() {
		?>
			<span class="customize-control-title">
				<?php echo esc_html( $this->label ); ?>
			</span>
			<select <?php $this->link(); ?> class="chosen">
			<option value=""><?php esc_attr_e( '-- Select --', 'hygge' ); ?></option>
				<?php
					$nodes = hygge_collect_nodes();

					echo "<optgroup label='".esc_html__('Categories', 'hygge')."'>";
					foreach ( $nodes['cats_id'] as $node ) {
						foreach( $node as $id => $name ){
							echo '<option value="'.$id.'"'.selected($this->value(), $id).'>'.$name.'</option>';
						}
					}

					echo "<optgroup label='".esc_html__('Pages', 'hygge')."'>";
					foreach ( $nodes['pages_id'] as $node ) {
						foreach( $node as $id => $name ){
							echo '<option value="'.$id.'"'.selected($this->value(), $id).'>'.$name.'</option>';
						}
					}

					echo "<optgroup label='".esc_html__('Posts', 'hygge')."'>";
					foreach ( $nodes['posts_id'] as $node ) {
						foreach( $node as $id => $name ){
							echo '<option value="'.$id.'"'.selected($this->value(), $id).'>'.$name.'</option>';
						}
					}

					echo "<optgroup label='".esc_html__('Tags', 'hygge')."'>";
					foreach ( $nodes['tags_id'] as $node ) {
						foreach( $node as $id => $name ){
							echo '<option value="'.$id.'"'.selected($this->value(), $id).'>'.$name.'</option>';
						}
					}
				?>
			</select>
		<?php
		}
	}
}
