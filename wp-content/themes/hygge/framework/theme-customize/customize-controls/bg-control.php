<?php
if (class_exists('WP_Customize_Control')) {

	class Bg_Control extends WP_Customize_Control {
		public function render_content() {
		?>
			<label data-el="<?php echo esc_attr($this->id); ?>">
				<span class="customize-control-title">
					<?php echo esc_html( $this->label ); ?>
				</span>
				<div class="hygge-bg-control-field" style="background:<?php echo esc_attr($this->value()); ?>;">
					<input type="hidden" class="bg-control" value="<?php echo esc_attr($this->value()); ?>" <?php $this->link(); ?> />
					<a class="button button-primary js-bg-control-btn" data-id="<?php echo esc_attr($this->id); ?>">Change Background</a>
				</div>
			</label>
		<?php
		include_once( HYGGE_BG_CONTROL . '/bg-control-lightbox.php' );

		// Include Icon Picker files here
		include_once( HYGGE_ICONS . '/icons.php' );
		include_once( HYGGE_ICON_PICKER . '/icon-picker-lightbox.php' );
		}
	}
}
?>
