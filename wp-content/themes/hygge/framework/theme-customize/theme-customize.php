<?php

/*------------------------------------
 * Add link for customize.php in admin bar
 * Currently WP only adds it on the front-end, not in the WP-Admin
 *------------------------------------*/

if( current_user_can('edit_theme_options') && is_admin() ){
	add_action( 'admin_bar_menu', 'hygge_add_customize_link', 40 );
}

function hygge_add_customize_link($admin_bar) {
	$admin_bar->add_menu( array(
		'id'    => 'customize',
		'title' => esc_html__('Customize', 'hygge'),
		'href'  => admin_url().'customize.php',
		'meta'  => array(
			'class' => 'hide-if-no-customize',
		),
	));
}



/*------------------------------------
 * Enqueue additional styles and scripts
 *------------------------------------*/

add_action('customize_controls_enqueue_scripts', 'hygge_load_customize_controls_assets');

function hygge_load_customize_controls_assets($hook_suffix){

	// Chosen jQuery
	wp_enqueue_style('hygge-customize-control-chosen', HYGGE_ADMIN_URI . '/assets/chosen/chosen.min.css', array(), hygge_version(), 'all');

	wp_enqueue_script('hygge-customize-control-chosen', HYGGE_ADMIN_URI . '/assets/chosen/chosen.jquery.min.js', array('jquery'), hygge_version(), 'all', true);

	// Hygge custom style
	wp_enqueue_style('hygge-customize-control-custom', HYGGE_CUSTOMIZE_URI . '/assets/css/customize-control-custom-style.css', array(), hygge_version(), 'all');

	wp_enqueue_media();

	wp_enqueue_script('hygge-customize-control-custom', HYGGE_CUSTOMIZE_URI . '/assets/js/customize-control-custom-script.js', array('jquery'), hygge_version(), 'all', true);

	wp_enqueue_style( 'wp-color-picker' );

}



/*------------------------------------
 * Define Theme Settings
 *------------------------------------*/

add_action( 'customize_register', 'hygge_theme_customize' );

function hygge_theme_customize($wp_customize) {



/*------------------------------------
 * Include custom controls
 *------------------------------------*/

include_once( HYGGE_CUSTOMIZE . '/customize-controls/category-dropdown.php');
include_once( HYGGE_CUSTOMIZE . '/customize-controls/tag-dropdown.php');
include_once( HYGGE_CUSTOMIZE . '/customize-controls/node-dropdown.php');
include_once( HYGGE_CUSTOMIZE . '/customize-controls/bg-control.php');
include_once( HYGGE_CUSTOMIZE . '/customize-controls/font-control.php');



/*------------------------------------
 * Title Tagline - Default section used as General Settings
 *------------------------------------*/

$wp_customize->add_section( 'title_tagline', array(
	'title'    => esc_html__( 'General', 'hygge' ),
	'priority' => 20,
));



// Logo
$wp_customize->add_setting( 'logo', array(
	'default' => HYGGE_IMG_URI . '/logo.png',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_url_raw'
));

$wp_customize->add_control(
	new WP_Customize_Image_Control($wp_customize,
		'logo',
		array(
			'label' => esc_html__( 'Main Logo', 'hygge' ),
			'section' => 'title_tagline',
			'settings' => 'logo',
			'priority' => 60,
		)
	)
);

// Logo Width
$wp_customize->add_setting( 'logo_width', array(
	'default' => '',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_number'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'logo_width',
		array(
			'label' => esc_html__( 'Logo Width (2x smaller to get retina)', 'hygge' ),
			'section' => 'title_tagline',
			'settings' => 'logo_width',
			'priority' => 65
		)
	)
);


// AJAX Load Pages
$wp_customize->add_setting( 'ajax_load_pages', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'ajax_load_pages',
		array(
			'label' => esc_html__( 'Load Pages with Ajax', 'hygge' ),
			'section' => 'title_tagline',
			'settings' => 'ajax_load_pages',
			'type' => 'checkbox',
			'priority' => 65,
		)
	)
);



// Ajax loader uses post thumbnails
$wp_customize->add_setting( 'ajax_loader_post_thumbs', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'ajax_loader_post_thumbs',
		array(
			'label' => esc_html__( 'Ajax loader uses post thumbnails when possible', 'hygge' ),
			'section' => 'title_tagline',
			'settings' => 'ajax_loader_post_thumbs',
			'type' => 'checkbox',
			'priority' => 68,
		)
	)
);



// Default ajax loader image
$wp_customize->add_setting( 'ajax_loader_img', array(
	'default' => HYGGE_IMG_URI . '/loader.gif',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_url_raw'
));

$wp_customize->add_control(
	new WP_Customize_Image_Control($wp_customize,
		'ajax_loader_img',
		array(
			'label' => esc_html__( 'Default Ajax Loader Image', 'hygge' ),
			'section' => 'title_tagline',
			'settings' => 'ajax_loader_img',
			'priority' => 70,
		)
	)
);



// Copyright
$wp_customize->add_setting( 'copyright', array(
	'default' => 'Hygge WordPress Theme by KORRA',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'copyright',
		array(
			'label' => esc_html__( 'Copyright', 'hygge' ),
			'section' => 'title_tagline',
			'settings' => 'copyright',
			'type' => 'textarea',
			'priority' => 85,
		)
	)
);





/*------------------------------------
 * Layout Settings
 *------------------------------------*/

$wp_customize->add_section( 'layout_settings', array(
	'title' => esc_html__('Layout', 'hygge'),
	'priority' => 40
));

// Sticky Header Menu
$wp_customize->add_setting( 'sticky_header_menu', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sticky_header_menu',
		array(
			'label' => esc_html__( 'Enable sticky header menu - it appears when you start scrolling top.', 'hygge' ),
			'section' => 'layout_settings',
			'settings' => 'sticky_header_menu',
			'type' => 'checkbox',
			'priority' => 10,
		)
	)
);



// Sidebar
$wp_customize->add_setting( 'use_sidebar', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'use_sidebar',
		array(
			'label' => esc_html__( 'Enable Sidebar', 'hygge' ),
			'section' => 'layout_settings',
			'settings' => 'use_sidebar',
			'type' => 'checkbox',
			'priority' => 15,
		)
	)
);

// Sidebar width
$wp_customize->add_setting( 'sidebar_width', array(
	'default' => '360',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_number'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sidebar_width',
		array(
			'label' => esc_html__( 'Sidebar width on desktop', 'hygge' ),
			'section' => 'layout_settings',
			'settings' => 'sidebar_width',
			'priority' => 17
		)
	)
);

// Sideblock width
$wp_customize->add_setting( 'sideblock_width', array(
	'default' => '360',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_number'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sideblock_width',
		array(
			'label' => esc_html__( 'Sideblock width on desktop', 'hygge' ),
			'section' => 'layout_settings',
			'settings' => 'sideblock_width',
			'priority' => 20
		)
	)
);





/*------------------------------------
 * Style Settings
 *------------------------------------*/

$wp_customize->add_panel( 'style_settings', array(
	'title' => esc_html__('Style', 'hygge'),
	'priority' => 45
));



/* Colors
 *------------------------------------*/

$wp_customize->add_section( 'color_settings', array(
	'title' => esc_html__('Colors', 'hygge'),
	'panel' => 'style_settings',
	'priority' => 10,
	'description' => esc_html__('In order to get advanced color settings (white, greys, black), you need to install the WP-LESS plugin. If Live Preview doesn\'t react to color changes, save new settings and reload to see updates.', 'hygge')
));

// Primary Color
$wp_customize->add_setting( 'color_primary', array(
	'default' => '#55b97a',
	'transport' => 'postMessage',
	'sanitize_callback' => 'sanitize_hex_color'
));

$wp_customize->add_control(
	new WP_Customize_Color_Control($wp_customize,
		'color_primary',
		array(
			'label' => esc_html__( 'Accent', 'hygge' ),
			'description' => esc_html__('Used as primary accent color for links and accented details.', 'hygge'),
			'section' => 'color_settings',
			'settings' => 'color_primary',
			'priority' => 10
		)
	)
);



if ( class_exists('WPLessPlugin') ){

	// White
	$wp_customize->add_setting( 'color_white', array(
		'default' => '#FFFFFF',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color'
	));

	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,
			'color_white',
			array(
				'label' => esc_html__( 'White', 'hygge' ),
				'description' => esc_html__('Everything white: button text, elements over the images.', 'hygge'),
				'section' => 'color_settings',
				'settings' => 'color_white',
				'priority' => 20
			)
		)
	);

	// Grey Pale
	$wp_customize->add_setting( 'color_grey_pale', array(
		'default' => '#f7f7f7',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color'
	));

	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,
			'color_grey_pale',
			array(
				'label' => esc_html__( 'Pale Grey', 'hygge' ),
				'description' => esc_html__('Used as background on form elements, code blocks, highlighted text, navigation dropdowns, dropcaps.', 'hygge'),
				'section' => 'color_settings',
				'settings' => 'color_grey_pale',
				'priority' => 25
			)
		)
	);

	// Grey Light
	$wp_customize->add_setting( 'color_grey_light', array(
	'default' => '#eeeeee',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color'
	));

	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,
			'color_grey_light',
			array(
				'label' => esc_html__( 'Light Grey', 'hygge' ),
				'description' => esc_html__('Used whenever a slightly lighter version of grey is needed. Borders, meta-info, widget titles, and textual labels.', 'hygge'),
				'section' => 'color_settings',
				'settings' => 'color_grey_light',
				'priority' => 30
			)
		)
	);

	// Grey Label
	$wp_customize->add_setting( 'color_grey_label', array(
		'default' => '#bababa',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color'
	));

	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,
			'color_grey_label',
			array(
				'label' => esc_html__( 'Grey Label', 'hygge' ),
				'description' => esc_html__('Used for post-meta labels.', 'hygge'),
				'section' => 'color_settings',
				'settings' => 'color_grey_label',
				'priority' => 35
			)
		)
	);

	// Grey
	$wp_customize->add_setting( 'color_grey', array(
		'default' => '#777777',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color'
	));

	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,
			'color_grey',
			array(
				'label' => esc_html__( 'Grey', 'hygge' ),
				'description' => esc_html__('Main body-font color. Used mostly for typography.', 'hygge'),
				'section' => 'color_settings',
				'settings' => 'color_grey',
				'priority' => 40
			)
		)
	);

	// Black
	$wp_customize->add_setting( 'color_black', array(
		'default' => '#333333',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color'
	));

	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,
			'color_black',
			array(
				'label' => esc_html__( 'Black', 'hygge' ),
				'description' => esc_html__('Everything black: titles color, dark-black elements, secondary-links, notification background, underlaying borders and hovers.', 'hygge'),
				'section' => 'color_settings',
				'settings' => 'color_black',
				'priority' => 50
			)
		)
	);

} // class_exists('WPLessPlugin')




/* Backgrounds
 *------------------------------------*/

$wp_customize->add_section( 'bg_settings', array(
	'title' => esc_html__('Backgrounds', 'hygge'),
	'panel' => 'style_settings',
	'priority' => 20
));


// Body Bg
$wp_customize->add_setting( 'bg_body', array(
	'default' => '#ffffff',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_attr'
));

$wp_customize->add_control(
	new Bg_Control($wp_customize,
		'bg_body',
		array(
			'label' => esc_html__( 'Body', 'hygge' ),
			'section' => 'bg_settings',
			'settings' => 'bg_body',
			'priority' => 10
		)
	)
);

// Header Bg
$wp_customize->add_setting( 'bg_header', array(
	'default' => '#f7f7f7',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_attr'
));

$wp_customize->add_control(
	new Bg_Control($wp_customize,
		'bg_header',
		array(
			'label' => esc_html__( 'Header', 'hygge' ),
			'section' => 'bg_settings',
			'settings' => 'bg_header',
			'priority' => 20
		)
	)
);

// Header Bg Dark
$wp_customize->add_setting( 'bg_header_dark', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'bg_header_dark',
		array(
			'label' => esc_html__( 'Header Background is Dark', 'hygge' ),
			'section' => 'bg_settings',
			'settings' => 'bg_header_dark',
			'type' => 'checkbox',
			'priority' => 25,
		)
	)
);

// Sidebar Bg
$wp_customize->add_setting( 'bg_sidebar', array(
	'default' => '#f7f7f7',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_attr'
));

$wp_customize->add_control(
	new Bg_Control($wp_customize,
		'bg_sidebar',
		array(
			'label' => esc_html__( 'Sidebar', 'hygge' ),
			'section' => 'bg_settings',
			'settings' => 'bg_sidebar',
			'priority' => 30
		)
	)
);

// Sidebar Bg Dark
$wp_customize->add_setting( 'bg_sidebar_dark', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'bg_sidebar_dark',
		array(
			'label' => esc_html__( 'Sidebar Background is Dark', 'hygge' ),
			'section' => 'bg_settings',
			'settings' => 'bg_sidebar_dark',
			'type' => 'checkbox',
			'priority' => 35,
		)
	)
);

// Footer Bg
$wp_customize->add_setting( 'bg_footer', array(
	'default' => '#f7f7f7',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_attr'
));

$wp_customize->add_control(
	new Bg_Control($wp_customize,
		'bg_footer',
		array(
			'label' => esc_html__( 'Footer', 'hygge' ),
			'section' => 'bg_settings',
			'settings' => 'bg_footer',
			'priority' => 40
		)
	)
);

// Footer Bg Dark
$wp_customize->add_setting( 'bg_footer_dark', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'bg_footer_dark',
		array(
			'label' => esc_html__( 'Footer Background is Dark', 'hygge' ),
			'section' => 'bg_settings',
			'settings' => 'bg_footer_dark',
			'type' => 'checkbox',
			'priority' => 45,
		)
	)
);



/*------------------------------------
 * Typography Settings
 *------------------------------------*/

$wp_customize->add_section( 'typography_settings', array(
	'title' => esc_html__('Typography', 'hygge'),
	'panel' => 'style_settings',
	'priority' => 30,
	'description' => esc_html__('Choose Font Family, style, font size / line height, text tranform and kerning(letter-spacing).', 'hygge')
));



// Font: Body
$wp_customize->add_setting( 'font_body', array(
	'default' => 'Open Sans|regular|16|2|none|',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_body',
		array(
			'label' => esc_html__( 'Main Body Font', 'hygge' ),
			'section' => 'typography_settings',
			'description' => esc_html__('Font used for the main content and paragraphs.', 'hygge'),
			'settings' => 'font_body',
			'priority' => 10,
		)
	)
);

// Font: Navigation
$wp_customize->add_setting( 'font_navigation', array(
	'default' => 'Poppins|regular|13|1.2|uppercase|0.15',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_navigation',
		array(
			'label' => esc_html__( 'Navigation', 'hygge' ),
			'description' => esc_html__('Font used for header navigation.', 'hygge'),
			'section' => 'typography_settings',
			'settings' => 'font_navigation',
			'priority' => 15,
		)
	)
);

// Font: Label
$wp_customize->add_setting( 'font_label', array(
	'default' => 'Poppins|500|13|1.8|uppercase|0.16',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_label',
		array(
			'label' => esc_html__( 'Label', 'hygge' ),
			'description' => esc_html__('Font used for buttons, post-meta and tag-like labels.', 'hygge'),
			'section' => 'typography_settings',
			'settings' => 'font_label',
			'priority' => 20,
		)
	)
);

// Font: Label Italic
$wp_customize->add_setting( 'font_label_italic', array(
	'default' => 'Alegreya|italic|14|1.6|none|0.13',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_label_italic',
		array(
			'label' => esc_html__( 'Description Label', 'hygge' ),
			'description' => esc_html__('Font used for smaller description labels, lighter in color.', 'hygge'),
			'section' => 'typography_settings',
			'settings' => 'font_label_italic',
			'priority' => 30,
		)
	)
);

// Font: H1
$wp_customize->add_setting( 'font_h1', array(
	'default' => 'Poppins|600|50|1.1|uppercase|0.08',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_h1',
		array(
			'label' => esc_html__( 'H1', 'hygge' ),
			'section' => 'typography_settings',
			'settings' => 'font_h1',
			'priority' => 40,
		)
	)
);

// Font: H2
$wp_customize->add_setting( 'font_h2', array(
	'default' => 'Poppins|500|44|1.1|uppercase|0.08',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_h2',
		array(
			'label' => esc_html__( 'H2', 'hygge' ),
			'section' => 'typography_settings',
			'settings' => 'font_h2',
			'priority' => 50,
		)
	)
);

// Font: H3
$wp_customize->add_setting( 'font_h3', array(
	'default' => 'Poppins|500|33|1.1|uppercase|0.08',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_h3',
		array(
			'label' => esc_html__( 'H3', 'hygge' ),
			'description' => esc_html__('Also used for widget titles.', 'hygge'),
			'section' => 'typography_settings',
			'settings' => 'font_h3',
			'priority' => 60,
		)
	)
);

// Font: H4
$wp_customize->add_setting( 'font_h4', array(
	'default' => 'Poppins|600|21|1.2|uppercase|0.15',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_h4',
		array(
			'label' => esc_html__( 'H4', 'hygge' ),
			'section' => 'typography_settings',
			'settings' => 'font_h4',
			'priority' => 70,
		)
	)
);

// Font: H5
$wp_customize->add_setting( 'font_h5', array(
	'default' => 'Poppins|500|18|1.2|uppercase|0.14',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_h5',
		array(
			'label' => esc_html__( 'H5', 'hygge' ),
			'section' => 'typography_settings',
			'settings' => 'font_h5',
			'priority' => 80,
		)
	)
);

// Font: H6
$wp_customize->add_setting( 'font_h6', array(
	'default' => 'Poppins|500|12|1.2|uppercase|0.15',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_h6',
		array(
			'label' => esc_html__( 'H6', 'hygge' ),
			'description' => esc_html__('Also used for image captions.', 'hygge'),
			'section' => 'typography_settings',
			'settings' => 'font_h6',
			'priority' => 90,
		)
	)
);

// Font: Description Heading
$wp_customize->add_setting( 'font_description', array(
	'default' => 'Poppins|regular|22|1.4|uppercase|0.16',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_description',
		array(
			'label' => esc_html__( 'Description Heading', 'hygge' ),
			'description' => esc_html__('Used for widget titles. In slightly smaller size used for Description Heading format, Highlighted paragraphs. In slightly larger size used for section titles like "Related Posts" and "Comments".', 'hygge'),
			'section' => 'typography_settings',
			'settings' => 'font_description',
			'priority' => 95,
		)
	)
);

// Font: Blockquote
$wp_customize->add_setting( 'font_blockquote', array(
	'default' => 'Open Sans|300italic|24|1.6|none|',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new Font_Control($wp_customize,
		'font_blockquote',
		array(
			'label' => esc_html__( 'Blockquote', 'hygge' ),
			'section' => 'typography_settings',
			'settings' => 'font_blockquote',
			'priority' => 100,
		)
	)
);

// Character Set for Google Fonts
$wp_customize->add_setting( 'character_sets', array(
	'default' => 'latin',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'character_sets',
		array(
			'label' => esc_html__( 'Character Sets', 'hygge' ),
			'description' => esc_html__( 'Comma separated list of font character sets to be included in the font-embedding. <strong>Example: latin,latin-ext,greek,vietnamese</strong>', 'hygge' ),
			'section' => 'typography_settings',
			'settings' => 'character_sets',
			'type' => 'text',
			'priority' => 110,
		)
	)
);



/*------------------------------------
 * Posts and Blogging Settings
 *------------------------------------*/

$wp_customize->add_panel( 'blogging_settings', array(
	'title' => esc_html__('Blog Layouts & Posts', 'hygge'),
	'priority' => 60,
));

$wp_customize->add_section( 'blogging_settings_home', array(
	'title' => esc_html__('Homepage', 'hygge'),
	'description'=> esc_html__('Settings for Latest Posts page.', 'hygge'),
	'panel'	 	=> 'blogging_settings',
	'priority' => 10,
));



// Use Sidebar
$wp_customize->add_setting( 'blog_home_use_sidebar', array(
	'default' => 'default',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'blog_home_use_sidebar',
		array(
			'label' => esc_html__( 'Use Sidebar', 'hygge' ),
			'section' => 'blogging_settings_home',
			'settings' => 'blog_home_use_sidebar',
			'type' => 'select',
			'choices' => array(
				'default' => esc_html__( 'Default (Use Layout Setting)', 'hygge' ),
				'true' => esc_html__( 'Yes (Override Layout Setting)', 'hygge' ),
				'false' => esc_html__( 'No (Override Layout Setting)', 'hygge' )
			),
			'priority' => 5,
		)
	)
);

// Posts Display Type
$wp_customize->add_setting( 'blog_home_display_style', array(
	'default' => 'grid',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'blog_home_display_style',
		array(
			'label' => esc_html__( 'Display Style', 'hygge' ),
			'section' => 'blogging_settings_home',
			'settings' => 'blog_home_display_style',
			'type' => 'select',
			'choices' => array(
				'list' => esc_html__( 'List', 'hygge'),
				'list-highlights' => esc_html__( 'List with Highlights', 'hygge'),
				'grid' => esc_html__( 'Grid', 'hygge'),
				'grid-3-1' => esc_html__( 'Grid 3-1', 'hygge'),
				'grid-3-2' => esc_html__( 'Grid 3-2', 'hygge'),
				'masonry' => esc_html__( 'Masonry', 'hygge' ),
			),
			'priority' => 10,
		)
	)
);

// Posts Pagination
$wp_customize->add_setting( 'blog_home_pagination', array(
	'default' => 'load_more',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'blog_home_pagination',
		array(
			'label' => esc_html__( 'Pagination Type', 'hygge' ),
			'section' => 'blogging_settings_home',
			'settings' => 'blog_home_pagination',
			'type' => 'select',
			'choices' => array(
				'pagination'=> esc_html__( 'Classic Pagination', 'hygge'),
				'load_more' => esc_html__( 'Load More Button', 'hygge'),
				'off' => esc_html__( 'No Pagination', 'hygge' )
			),
			'priority' => 20,
		)
	)
);

// Ads
$wp_customize->add_setting( 'blog_home_ads', array(
	'default' => 0,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'blog_home_ads',
		array(
			'label' => esc_html__( 'Ads', 'hygge' ),
			'description' => esc_html__( 'Choose after how many posts the \'Ad Area in Blog\' Widget is displayed (repeating). Configure this widget in Appearance -> Widgets.', 'hygge' ),
			'section' => 'blogging_settings_home',
			'settings' => 'blog_home_ads',
			'type' => 'select',
			'choices' => array(
				0 => esc_html__( '0 - Off', 'hygge'),
				1 => esc_html__( '1', 'hygge'),
				2 => esc_html__( '2', 'hygge' ),
				3 => esc_html__( '3', 'hygge' ),
				4 => esc_html__( '4', 'hygge' ),
				5 => esc_html__( '5', 'hygge' ),
				6 => esc_html__( '6', 'hygge' ),
				7 => esc_html__( '7', 'hygge' ),
				8 => esc_html__( '8', 'hygge' )
			),
			'priority' => 30,
		)
	)
);





/* Archives
 *------------------------------------*/

$wp_customize->add_section( 'blogging_settings_archive', array(
	'title' => esc_html__('Archives', 'hygge'),
	'description'=> esc_html__('Settings for Tag, Category, Date and Author pages, and for search results.', 'hygge'),
	'panel' => 'blogging_settings',
	'priority' => 20,
));

// Use Sidebar
$wp_customize->add_setting( 'archive_use_sidebar', array(
	'default' => 'default',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'archive_use_sidebar',
		array(
			'label' => esc_html__( 'Use Sidebar', 'hygge' ),
			'section' => 'blogging_settings_archive',
			'settings' => 'archive_use_sidebar',
			'type' => 'select',
			'choices' => array(
				'default' => esc_html__( 'Default (Use Layout Setting)', 'hygge' ),
				'true' => esc_html__( 'Yes (Override Layout Setting)', 'hygge' ),
				'false' => esc_html__( 'No (Override Layout Setting)', 'hygge' )
			),
			'priority' => 5,
		)
	)
);

// Archive Display Type
$wp_customize->add_setting( 'archive_display_style', array(
	'default' => 'grid',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'archive_display_style',
		array(
			'label' => esc_html__( 'Display Style', 'hygge' ),
			'section' => 'blogging_settings_archive',
			'settings' => 'archive_display_style',
			'type' => 'select',
			'choices' => array(
				'list' => esc_html__( 'List', 'hygge'),
				'list-highlights' => esc_html__( 'List with Highlights', 'hygge'),
				'grid' => esc_html__( 'Grid', 'hygge'),
				'grid-3-1' => esc_html__( 'Grid 3-1', 'hygge'),
				'grid-3-2' => esc_html__( 'Grid 3-2', 'hygge'),
				'masonry' => esc_html__( 'Masonry', 'hygge' )
			),
			'priority' => 20,
		)
	)
);

// Archive Pagination
$wp_customize->add_setting( 'archive_pagination', array(
	'default' => 'pagination',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'archive_pagination',
		array(
			'label' => esc_html__( 'Pagination Type', 'hygge' ),
			'section' => 'blogging_settings_archive',
			'settings' => 'archive_pagination',
			'type' => 'select',
			'choices' => array(
				'pagination'=> esc_html__( 'Classic Pagination', 'hygge' ),
				'load_more' => esc_html__( 'Load More Button', 'hygge' ),
				'off' => esc_html__( 'No Pagination', 'hygge' )
			),
			'priority' => 30,
		)
	)
);

// Ads
$wp_customize->add_setting( 'archive_ads', array(
	'default' => 0,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'archive_ads',
		array(
			'label' => esc_html__( 'Ads', 'hygge' ),
			'description' => esc_html__( 'Choose after how many posts the \'Ad Area in Blog\' Widget is displayed (repeating). Configure this widget in Appearance -> Widgets.', 'hygge' ),
			'section' => 'blogging_settings_archive',
			'settings' => 'archive_ads',
			'type' => 'select',
			'choices' => array(
				0 => esc_html__( '0 - Off', 'hygge'),
				1 => esc_html__( '1', 'hygge'),
				2 => esc_html__( '2', 'hygge' ),
				3 => esc_html__( '3', 'hygge' ),
				4 => esc_html__( '4', 'hygge' ),
				5 => esc_html__( '5', 'hygge' ),
				6 => esc_html__( '6', 'hygge' ),
				7 => esc_html__( '7', 'hygge' ),
				8 => esc_html__( '8', 'hygge' )
			),
			'priority' => 40,
		)
	)
);



/* Single Post Settings
 *------------------------------------*/

$wp_customize->add_section( 'blogging_settings_single', array(
	'title' => esc_html__('Single Posts', 'hygge'),
	'panel'	 	=> 'blogging_settings',
	'priority' => 30,
));

// Cover Style
$wp_customize->add_setting( 'post_cover_style', array(
	'default' => 'notebook',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_select'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'post_cover_style',
		array(
			'label' => esc_html__( 'Cover Style', 'hygge' ),
			'section' => 'blogging_settings_single',
			'settings' => 'post_cover_style',
			'type' => 'select',
			'choices' => array(
				'notebook' => esc_html__( 'Notebook', 'hygge' ),
				'overlay' => esc_html__( 'Colored Overlay', 'hygge' ),
				'basic' => esc_html__( 'Basic', 'hygge' ),
			),
			'priority' => 5,
		)
	)
);

// Post Excerpt
$wp_customize->add_setting( 'post_excerpt', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'post_excerpt',
		array(
			'label' => esc_html__( 'Show Post Excerpt', 'hygge' ),
			'section' => 'blogging_settings_single',
			'settings' => 'post_excerpt',
			'type' => 'checkbox',
			'priority' => 10,
		)
	)
);

// Show Author in meta
$wp_customize->add_setting( 'post_meta_author', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
	'post_meta_author',
	array(
		'label' => esc_html__( 'Show Author details in post meta.', 'hygge' ),
		'section' => 'blogging_settings_single',
		'settings' => 'post_meta_author',
		'type' => 'checkbox',
		'priority' => 20,
		)
		)
	);

// Hygge Share
$wp_customize->add_setting( 'post_share', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'post_share',
		array(
			'label' => esc_html__( 'Show Post Share', 'hygge' ),
			'section' => 'blogging_settings_single',
			'settings' => 'post_share',
			'type' => 'checkbox',
			'priority' => 30,
		)
	)
);

// Post Related
$wp_customize->add_setting( 'post_related', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'post_related',
		array(
			'label' => esc_html__( 'Show Related Posts', 'hygge' ),
			'section' => 'blogging_settings_single',
			'settings' => 'post_related',
			'type' => 'checkbox',
			'priority' => 50,
		)
	)
);

// Post Related Title
$wp_customize->add_setting( 'post_related_title', array(
	'default' => 'Related Stories',
	'transport' => 'refresh',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'post_related_title',
		array(
			'label' => esc_html__( 'Related posts section title.', 'hygge' ),
			'section' => 'blogging_settings_single',
			'settings' => 'post_related_title',
			'type' => 'text',
			'priority' => 60,
		)
	)
);

// Post Navigation (Previous-Next)
$wp_customize->add_setting( 'post_navigation', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'post_navigation',
		array(
			'label' => esc_html__( 'Show Post Navigation (Previous-Next)', 'hygge' ),
			'section' => 'blogging_settings_single',
			'settings' => 'post_navigation',
			'type' => 'checkbox',
			'priority' => 40,
		)
	)
);

// Post Navigation Category
$wp_customize->add_setting( 'post_navigation_category', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'post_navigation_category',
		array(
			'label' => esc_html__( 'Post Navigation displays posts only from the same category', 'hygge' ),
			'section' => 'blogging_settings_single',
			'settings' => 'post_navigation_category',
			'type' => 'checkbox',
			'priority' => 41,
		)
	)
);




/* Featured Posts
 *------------------------------------*/

$wp_customize->add_section( 'featured', array(
	'title' => esc_html__('Featured Posts', 'hygge'),
	'description' => esc_html__('Setup featured posts. It will automatically be displayed on homepage. It can be displayed on Static Page through it\'s settings on Edit Page screen. By not selecting tag, latest posts will be picked. Disable the featured slider by setting Count to 0.', 'hygge'),
	'panel'	 	=> 'blogging_settings',
	'priority' => 35,
));

// Featured Tag
$wp_customize->add_setting( 'featured_tag', array(
	'default' => '0',
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_number'
));

$wp_customize->add_control(
	new Tag_Dropdown_Control($wp_customize,
		'featured_tag',
		array(
			'label' => esc_html__( 'Tag to use', 'hygge' ),
			'section' => 'featured',
			'settings' => 'featured_tag',
			'priority' => 10,
		)
	)
);

// Featured Count
$wp_customize->add_setting( 'featured_count', array(
	'default' => '4',
	'transport' => 'refresh',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'featured_count',
		array(
			'label' => esc_html__( 'Count (0 = off; -1 = unlimited)', 'hygge' ),
			'section' => 'featured',
			'settings' => 'featured_count',
			'type' => 'text',
			'priority' => 20,
		)
	)
);

// Exclude Featured Posts from the main blog list
$wp_customize->add_setting( 'featured_exclude', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'featured_exclude',
		array(
			'label' => esc_html__( 'Exclude Featured posts from blog', 'hygge' ),
			// 'description' => esc_html__( 'This will exclude the featured posts from Homepage and Archives main blog lists when Featured Slider is displayed', 'hygge'),
			'section' => 'featured',
			'settings' => 'featured_exclude',
			'type' => 'checkbox',
			'priority' => 30,
		)
	)
);

// Featured Interval
$wp_customize->add_setting( 'featured_interval', array(
	'default' => '0',
	'transport' => 'refresh',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'featured_interval',
		array(
			'label' => esc_html__( 'Auto-animation interval in ms (0 = off; 4000 = 4seconds)', 'hygge' ),
			'section' => 'featured',
			'settings' => 'featured_interval',
			'type' => 'text',
			'priority' => 40,
		)
	)
);





/* Hygge Share Settings
 *------------------------------------*/

$wp_customize->add_section( 'blogging_settings_sharing', array(
	'title' => esc_html__('Hygge Share Settings', 'hygge'),
	'description' => esc_html__('Plugin: Hygge Share must be enabled.', 'hygge'),
	'panel' => 'blogging_settings',
	'priority' => 50,
));

// Email
$wp_customize->add_setting( 'sharing_email', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sharing_email',
		array(
			'label' => esc_html__( 'Show Email', 'hygge' ),
			'section' => 'blogging_settings_sharing',
			'settings' => 'sharing_email',
			'type' => 'checkbox',
			'priority' => 10,
		)
	)
);


// Facebook
$wp_customize->add_setting( 'sharing_facebook', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sharing_facebook',
		array(
			'label' => esc_html__( 'Show Facebook', 'hygge' ),
			'section' => 'blogging_settings_sharing',
			'settings' => 'sharing_facebook',
			'type' => 'checkbox',
			'priority' => 20,
		)
	)
);

// Twitter
$wp_customize->add_setting( 'sharing_twitter', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sharing_twitter',
		array(
			'label' => esc_html__( 'Show Twitter', 'hygge' ),
			'section' => 'blogging_settings_sharing',
			'settings' => 'sharing_twitter',
			'type' => 'checkbox',
			'priority' => 30,
		)
	)
);

// Pinterest
$wp_customize->add_setting( 'sharing_pinterest', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sharing_pinterest',
		array(
			'label' => esc_html__( 'Show Pinterest', 'hygge' ),
			'section' => 'blogging_settings_sharing',
			'settings' => 'sharing_pinterest',
			'type' => 'checkbox',
			'priority' => 40,
		)
	)
);

// Google+
$wp_customize->add_setting( 'sharing_google', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sharing_google',
		array(
			'label' => esc_html__( 'Show Google+', 'hygge' ),
			'section' => 'blogging_settings_sharing',
			'settings' => 'sharing_google',
			'type' => 'checkbox',
			'priority' => 50,
		)
	)
);

// Linkedin
$wp_customize->add_setting( 'sharing_linkedin', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sharing_linkedin',
		array(
			'label' => esc_html__( 'Show Linkedin', 'hygge' ),
			'section' => 'blogging_settings_sharing',
			'settings' => 'sharing_linkedin',
			'type' => 'checkbox',
			'priority' => 60,
		)
	)
);

// Tumblr
$wp_customize->add_setting( 'sharing_tumblr', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'sharing_tumblr',
		array(
			'label' => esc_html__( 'Show Tumblr', 'hygge' ),
			'section' => 'blogging_settings_sharing',
			'settings' => 'sharing_tumblr',
			'type' => 'checkbox',
			'priority' => 70,
		)
	)
);





/*------------------------------------
 * Notification Bar Settings
 *------------------------------------*/

$wp_customize->add_section( 'notification', array(
	'title' => esc_html__('Notification Bar', 'hygge'),
	'priority' => 70,
));

// Notif General On/Off
$wp_customize->add_setting( 'notif_use', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'notif_use',
		array(
			'label' => esc_html__( 'Use Notification Bar', 'hygge' ),
			'section' => 'notification',
			'settings' => 'notif_use',
			'type' => 'checkbox',
			'priority' => 10,
		)
	)
);


// Notif Jetpack Subscription On/Off
$wp_customize->add_setting( 'notif_jetpack_subscription', array(
	'default' => false,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'notif_jetpack_subscription',
		array(
			'label' => esc_html__( 'Use Jetpack Subscription form', 'hygge' ),
			'description'=> esc_html__( 'If Jetpack is installed and Subscription module enabled, by checking this field you will display the subscription form.' , 'hygge' ),
			'section' => 'notification',
			'settings' => 'notif_jetpack_subscription',
			'type' => 'checkbox',
			'priority' => 15,
		)
	)
);


// Notif Hideable
$wp_customize->add_setting( 'notif_hideable', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'notif_hideable',
		array(
			'label' => esc_html__( 'Notification Bar can be dismissed', 'hygge' ),
			'description'=> esc_html__( 'Notification will have small \'X\' button, and visitor will be able to dismiss that notification (only for himself). Once you change your notification, it will become visible again.' , 'hygge' ),
			'section' => 'notification',
			'settings' => 'notif_hideable',
			'type' => 'checkbox',
			'priority' => 20,
		)
	)
);


// Notif Text
$wp_customize->add_setting( 'notif_text', array(
	'default' => '',
	'transport' => 'refresh',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'notif_text',
		array(
			'label' => esc_html__( 'Notification Text', 'hygge' ),
			'section' => 'notification',
			'settings' => 'notif_text',
			'type' => 'text',
			'priority' => 30,
		)
	)
);

// Notif Button Text
$wp_customize->add_setting( 'notif_btn_text', array(
	'default' => '',
	'transport' => 'refresh',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'notif_btn_text',
		array(
			'label' => esc_html__( 'Notification Button Text', 'hygge' ),
			'description'=> esc_html__( 'Empty Button Text will hide the button.' , 'hygge' ),
			'section' => 'notification',
			'settings' => 'notif_btn_text',
			'type' => 'text',
			'priority' => 40,
		)
	)
);

// Notif Button Link
$wp_customize->add_setting( 'notif_btn_link', array(
	'default' => '',
	'transport' => 'refresh',
	'sanitize_callback' => 'esc_url_raw'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'notif_btn_link',
		array(
			'label' => esc_html__( 'Notification Button Link', 'hygge' ),
			'description'=> esc_html__( 'Empty Button Link will hide the button.' , 'hygge' ),
			'section' => 'notification',
			'settings' => 'notif_btn_link',
			'type' => 'text',
			'priority' => 50,
		)
	)
);





/*------------------------------------
 * WooCommerce
 *------------------------------------*/

if( class_exists( 'WooCommerce' ) ){

	$wp_customize->add_section( 'woocommerce', array(
		'title' => esc_html__('WooCommerce', 'hygge'),
		'description'=> esc_html__('Settings for WooCommerce pages.', 'hygge'),
		'priority' => 75,
	));

	// Use Sidebar
	$wp_customize->add_setting( 'woocommerce_use_sidebar', array(
		'default' => 'default',
		'transport' => 'refresh',
		'sanitize_callback' => 'hygge_sanitize_checkbox'
	));

	$wp_customize->add_control(
		new WP_Customize_Control($wp_customize,
			'woocommerce_use_sidebar',
			array(
				'label' => esc_html__( 'Use Sidebar', 'hygge' ),
				'section' => 'woocommerce',
				'settings' => 'woocommerce_use_sidebar',
				'type' => 'select',
				'choices' => array(
					'default' => esc_html__( 'Default (Use Layout Setting)', 'hygge' ),
					'true' 		=> esc_html__( 'Yes (Override Layout Setting)', 'hygge' ),
					'false' => esc_html__( 'No (Override Layout Setting)', 'hygge' )
				),
				'priority' => 10,
			)
		)
	);

}





/*------------------------------------
 * Advanced Settings
 *------------------------------------*/

$wp_customize->add_section( 'advanced', array(
	'title' => esc_html__('Advanced', 'hygge'),
	'priority' => 80,
));

// Display pingbacks and trackbacks with comments
$wp_customize->add_setting( 'display_pingbacks', array(
	'default' => true,
	'transport' => 'refresh',
	'sanitize_callback' => 'hygge_sanitize_checkbox'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'display_pingbacks',
		array(
			'label' => esc_html__( 'Display pingbacks', 'hygge' ),
			'description'=> esc_html__( 'Check this field if you wish to display pingbacks and trackbacks along with the comments in the comments area.' , 'hygge' ),
			'section' => 'advanced',
			'settings' => 'display_pingbacks',
			'type' => 'checkbox',
			'priority' => 10,
		)
	)
);

// Google API
$wp_customize->add_setting( 'google_api', array(
	'default' => '',
	'transport' => 'postMessage',
	'sanitize_callback' => 'esc_textarea'
));

$wp_customize->add_control(
	new WP_Customize_Control($wp_customize,
		'google_api',
		array(
			'label' => esc_html__( 'Google API', 'hygge' ),
			'description'=> esc_html__( 'If you have your own Google API key you wish to use for Google features brought in the theme, enter it here.' , 'hygge' ),
			'section' => 'advanced',
			'settings' => 'google_api',
			'type' => 'text',
			'priority' => 20,
		)
	)
);



/*------------------------------------
 * Call Theme Customize Preview Script
 *------------------------------------*/

if ( $wp_customize->is_preview() && !is_admin() ){
	add_action( 'customize_preview_init', 'hygge_theme_customize_preview');
}

function hygge_theme_customize_preview() {
	wp_enqueue_script('theme_customize_preview_script',
		HYGGE_FRAMEWORK_URI . '/theme-customize/theme-customize-preview.js',
		array( 'jquery','customize-preview' ),
		hygge_version(),
		true
	);
}



/*------------------------------------
 * Sanitization functions
 *------------------------------------*/

// Checkbox
function hygge_sanitize_checkbox( $value ) {
	return ( ( isset( $value ) && $value == true ) ? true : false );
}

// Number
function hygge_sanitize_number( $value, $setting ) {

	// Ensure $number is an absolute integer (whole number, zero or greater).
	$value = absint( $value );
	return ( $value ? $value : $setting->default );
}

// Select
function hygge_sanitize_select( $value, $setting ) {

	// Ensure input is a slug.
	$value = sanitize_key( $value );

	// Get list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// If the input is a valid key, return it; otherwise, return the default.
	return ( array_key_exists( $value, $choices ) ? $value : $setting->default );
}


// end hygge_theme_customize
}
