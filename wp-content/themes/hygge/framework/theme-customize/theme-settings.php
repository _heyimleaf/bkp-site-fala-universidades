<?php

/*------------------------------------------------------------
 * Return Global Theme Settings
 *------------------------------------------------------------*/

function hygge_theme_settings_global($clear_cache = false){

	static $theme_settings;

	if( $theme_settings && !$clear_cache ){
		return $theme_settings;
	}



	$theme_settings = array();

	$theme_settings['logo'] = get_theme_mod( 'logo', HYGGE_IMG_URI . '/logo.png' );
	$theme_settings['logo_width'] = get_theme_mod( 'logo_width', '' );
	$theme_settings['ajax_load_pages'] = get_theme_mod( 'ajax_load_pages', true );
	$theme_settings['ajax_loader_post_thumbs'] = get_theme_mod( 'ajax_loader_post_thumbs', true );
	$theme_settings['ajax_loader_img'] = get_theme_mod( 'ajax_loader_img', HYGGE_IMG_URI . '/loader.gif' );
	$theme_settings['copyright'] = get_theme_mod( 'copyright', 'Hygge WordPress Theme by KORRA' );

	$theme_settings['sticky_header_menu'] = get_theme_mod( 'sticky_header_menu', true );
	$theme_settings['use_sidebar'] = get_theme_mod( 'use_sidebar', true );
	$theme_settings['sidebar_width'] = get_theme_mod( 'sidebar_width', '360' );
	$theme_settings['sideblock_width'] = get_theme_mod( 'sideblock_width', '360' );

	$theme_settings['colors']['color_primary'] = get_theme_mod( 'color_primary', '#55b97a' );
	$theme_settings['colors']['color_white'] = get_theme_mod( 'color_white', '#FFFFFF' );
	$theme_settings['colors']['color_grey_pale'] = get_theme_mod( 'color_grey_pale', '#f7f7f7' );
	$theme_settings['colors']['color_grey_light'] = get_theme_mod( 'color_grey_light', '#eeeeee' );
	$theme_settings['colors']['color_grey_label'] = get_theme_mod( 'color_grey_label', '#bababa' );
	$theme_settings['colors']['color_grey'] = get_theme_mod( 'color_grey', '#777777' );
	$theme_settings['colors']['color_black'] = get_theme_mod( 'color_black', '#333333' );

	$theme_settings['bg_body']    = get_theme_mod( 'bg_body', '#ffffff' );
	$theme_settings['bg_header']  = get_theme_mod( 'bg_header', '#f7f7f7' );
	$theme_settings['bg_sidebar'] = get_theme_mod( 'bg_sidebar', '#f7f7f7' );
	$theme_settings['bg_footer']  = get_theme_mod( 'bg_footer', '#f7f7f7' );
	$theme_settings['bg_header_dark']  = get_theme_mod( 'bg_header_dark', false );
	$theme_settings['bg_sidebar_dark'] = get_theme_mod( 'bg_sidebar_dark', false );
	$theme_settings['bg_footer_dark']  = get_theme_mod( 'bg_footer_dark', false );

	$theme_settings['typo']['font_body']        = explode( '|', get_theme_mod( 'font_body', 'Open Sans|regular|16|2|none|') );
	$theme_settings['typo']['font_navigation']  = explode( '|', get_theme_mod( 'font_navigation', 'Poppins|regular|13|1.2|uppercase|0.15') );
	$theme_settings['typo']['font_label']       = explode( '|', get_theme_mod( 'font_label', 'Poppins|500|13|1.8|uppercase|0.16') );
	$theme_settings['typo']['font_label_italic']= explode( '|', get_theme_mod( 'font_label_italic', 'Alegreya|italic|14|1.6|none|0.13') );
	$theme_settings['typo']['font_h1']          = explode( '|', get_theme_mod( 'font_h1', 'Poppins|600|50|1.1|uppercase|0.08') );
	$theme_settings['typo']['font_h2']          = explode( '|', get_theme_mod( 'font_h2', 'Poppins|500|44|1.1|uppercase|0.08') );
	$theme_settings['typo']['font_h3']          = explode( '|', get_theme_mod( 'font_h3', 'Poppins|500|33|1.1|uppercase|0.08') );
	$theme_settings['typo']['font_h4']          = explode( '|', get_theme_mod( 'font_h4', 'Poppins|600|21|1.2|uppercase|0.15') );
	$theme_settings['typo']['font_h5']          = explode( '|', get_theme_mod( 'font_h5', 'Poppins|500|18|1.2|uppercase|0.14') );
	$theme_settings['typo']['font_h6']          = explode( '|', get_theme_mod( 'font_h6', 'Poppins|500|12|1.2|uppercase|0.15') );
	$theme_settings['typo']['font_description'] = explode( '|', get_theme_mod( 'font_description', 'Poppins|regular|22|1.4|uppercase|0.16') );
	$theme_settings['typo']['font_blockquote']  = explode( '|', get_theme_mod( 'font_blockquote', 'Open Sans|300italic|24|1.6|none|') );
	$theme_settings['character_sets']           = get_theme_mod( 'character_sets', 'latin');

	$theme_settings['blog_home_use_sidebar'] = get_theme_mod( 'blog_home_use_sidebar', 'default' );
	$theme_settings['blog_home_display_style'] = get_theme_mod( 'blog_home_display_style', 'grid' );
	$theme_settings['blog_home_pagination'] = get_theme_mod( 'blog_home_pagination', 'load_more' );
	$theme_settings['blog_home_ads'] = get_theme_mod( 'blog_home_ads', 0 );

	$theme_settings['archive_use_sidebar'] = get_theme_mod( 'archive_use_sidebar', 'default' );
	$theme_settings['archive_display_style'] = get_theme_mod( 'archive_display_style', 'grid' );
	$theme_settings['archive_pagination'] = get_theme_mod( 'archive_pagination', 'pagination' );
	$theme_settings['archive_ads'] = get_theme_mod( 'archive_ads', 0 );

	$theme_settings['post_cover_style'] = get_theme_mod( 'post_cover_style', 'notebook' );
	$theme_settings['post_excerpt'] = get_theme_mod( 'post_excerpt', true );
	$theme_settings['post_meta_author'] = get_theme_mod( 'post_meta_author', true );
	$theme_settings['post_share'] = get_theme_mod( 'post_share', true );
	$theme_settings['post_navigation'] = get_theme_mod( 'post_navigation', true );
	$theme_settings['post_navigation_category'] = get_theme_mod( 'post_navigation_category', false );
	$theme_settings['post_related'] = get_theme_mod( 'post_related', true );
	$theme_settings['post_related_title'] = get_theme_mod( 'post_related_title', 'Related Stories' );

	$theme_settings['featured_tag']      = get_theme_mod( 'featured_tag', '0' );
	$theme_settings['featured_count']    = get_theme_mod( 'featured_count', '4' );
	$theme_settings['featured_exclude']  = get_theme_mod( 'featured_exclude', false );
	$theme_settings['featured_interval'] = get_theme_mod( 'featured_interval', '0' );

	$theme_settings['sharing']['email']     = get_theme_mod( 'sharing_email', false );
	$theme_settings['sharing']['facebook']  = get_theme_mod( 'sharing_facebook', true );
	$theme_settings['sharing']['twitter']   = get_theme_mod( 'sharing_twitter', true );
	$theme_settings['sharing']['pinterest'] = get_theme_mod( 'sharing_pinterest', true );
	$theme_settings['sharing']['google']    = get_theme_mod( 'sharing_google', false );
	$theme_settings['sharing']['linkedin']  = get_theme_mod( 'sharing_linkedin', false );
	$theme_settings['sharing']['tumblr']    = get_theme_mod( 'sharing_tumblr', false );

	$theme_settings['notif_use'] = get_theme_mod( 'notif_use', false );
	$theme_settings['notif_jetpack_subscription'] = get_theme_mod( 'notif_jetpack_subscription', false );
	$theme_settings['notif_hideable'] = get_theme_mod( 'notif_hideable', true );
	$theme_settings['notif_text'] = get_theme_mod( 'notif_text', '' );
	$theme_settings['notif_btn_text'] = get_theme_mod( 'notif_btn_text', '' );
	$theme_settings['notif_btn_link'] = get_theme_mod( 'notif_btn_link', '' );

	$theme_settings['woocommerce_use_sidebar'] = get_theme_mod( 'woocommerce_use_sidebar', 'default' );

	$theme_settings['display_pingbacks'] = get_theme_mod( 'display_pingbacks', true );
	$theme_settings['google_api'] = get_theme_mod( 'google_api', '' );





	/*------------------------------------------------------------
	 * Check if any sharing networks are enabled
	 *------------------------------------------------------------*/

	$theme_settings['sharing_count'] = 0;

	foreach( $theme_settings['sharing'] as $share ){
		if( $share ){
			$theme_settings['sharing_count']++;
		}
	}





	return $theme_settings;
}



/*------------------------------------------------------------
 * Return Theme Settings modified by page specific settings
 *------------------------------------------------------------*/

function hygge_theme_settings(){

	static $theme_settings_global;
	static $theme_settings;

	if( $theme_settings ){
		return $theme_settings;
	}

	if(!$theme_settings_global){
		$theme_settings_global = hygge_theme_settings_global();
	}



	// check if $post has been defined or fallback to global settings
	global $post;
	if( !isset($post) ){
		return $theme_settings_global;
	}



	/* Fresh Update of $theme_settings on WP Customize refresh
	 * Else declare $theme_settings same as global
	 *------------------------------------------------------------*/

	global $wp_customize;
	if ( isset( $wp_customize ) ) {
		$theme_settings = hygge_theme_settings_global(true);
	}else{
		$theme_settings = $theme_settings_global;
	}



	/* Override Blog Home settings
	 *------------------------------------------------------------*/

	if( is_home() ){
		// Sidebar ON/OFF
		switch( $theme_settings['blog_home_use_sidebar'] ){
			case 'true':
				$theme_settings['use_sidebar'] = true;
				break;
			case 'false':
				$theme_settings['use_sidebar'] = false;
				break;
		}
	}



	/* Override Archive settings
	 *------------------------------------------------------------*/

	if( is_archive() || is_search() ){
		// Sidebar ON/OFF
		switch( $theme_settings['archive_use_sidebar'] ){
			case 'true':
				$theme_settings['use_sidebar'] = true;
				break;
			case 'false':
				$theme_settings['use_sidebar'] = false;
				break;
		}
	}



	/* Modify $theme_settings per page specific settings
	 *------------------------------------------------------------*/

	if( is_singular() ){

		// Sidebar ON/OFF
		switch( get_post_meta( get_the_ID(), 'use_sidebar', true ) ){
			case 'true':
			case '1':
				$theme_settings['use_sidebar'] = true;
				break;
			case 'false':
			case '0':
				$theme_settings['use_sidebar'] = false;
				break;
		}

	}



	/* Set for static page (impossible to know from withing Loop)
	 *------------------------------------------------------------*/

	$theme_settings['is_page'] = is_page() ? true : false;
	$theme_settings['page_blog_sticky'] = get_post_meta( get_the_ID(), 'enable_sticky_posts', true ) ? true : false;



	/* WooCommerce
	*------------------------------------------------------------*/

	if( class_exists( 'WooCommerce' ) ){

		// shop home
		if( is_shop() ){
			$shop_page_id = get_option( 'woocommerce_shop_page_id' );

			switch( get_post_meta( $shop_page_id, 'use_sidebar', true ) ){
				case 'true':
				case '1':
					$theme_settings['use_sidebar'] = true;
					break;
				case 'false':
				case '0':
					$theme_settings['use_sidebar'] = false;
					break;
			}

		}
	}



	/* Check if sidebar is active (overrides other sidebar settings)
	 *------------------------------------------------------------*/

	if( !is_active_sidebar('widget-area-sidebar-area') ){
		$theme_settings['use_sidebar'] = false;
	}




	return $theme_settings;
}
