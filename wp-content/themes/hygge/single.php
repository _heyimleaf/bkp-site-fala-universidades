<?php
	$theme_settings = hygge_theme_settings();
	get_header();

	if (have_posts()): while (have_posts()) : the_post();

	$format = get_post_format();
	$post_classes = '';
	$post_classes .= ' article-single article-single--post';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $post_classes ); ?> role="main" data-title="<?php echo esc_attr( get_the_title() ); ?>">

	<?php if( function_exists('hygge_share') ){
		echo '<div class="post__share--sticky element-modular-loading">';
		hygge_share();
		echo '</div>';
	} ?>

	<div class="main-content">

		<div class="post__content">

			<?php if ($theme_settings['post_cover_style'] == 'basic') {
				echo "<h1 class='post__title hidden-desktop'>" . get_the_title() . "</h1>";
			} ?>

			<?php $meta_class = '';
				if( get_post_meta( get_the_ID(), 'post_meta_sticky', true ) == '0' ){
					$meta_class = 'hygge-sideblock--non-sticky';
			} ?>

			<div class="hygge-sideblock hygge-sideblock--meta <?php echo $meta_class; ?>">
				<?php get_template_part('post-meta'); ?>
			</div>

			<?php if ($theme_settings['post_cover_style'] == 'basic') {
				echo "<h1 class='post__title visible-desktop'>" . get_the_title() . "</h1>";
			} ?>

			<div class="editor-content">
				<?php
				// Show MANUAL excerpt if enabled in theme settings
				if( $theme_settings['post_excerpt'] && has_excerpt() ){
					echo "<p class='description-heading'>" . get_the_excerpt() . "</p>";
					echo "<hr class='hygge-separator--small' />";
				}

				// If Gallery or video is shown as cover, exclude it from content
				switch( get_post_meta( get_the_ID(), 'cover_media', true ) ){
					case 'gallery':
						$output_content = hygge_match_gallery( get_the_content(), true );
						echo apply_filters( 'the_content', $output_content );
						break;
					case 'video':
						$output_content = hygge_match_first_embed( get_the_content(), true );
						echo apply_filters( 'the_content', $output_content );
						break;
					default:
						the_content();
				}
				?>
			</div>

			<?php if( get_the_tag_list() ): ?>
				<div class="post__tags clearfix cl">
					<div class="label--italic"><?php esc_html_e('Labels', 'hygge')?></div>
					<div class="tagcloud">
						<?php echo get_the_tag_list(); ?>
					</div>
				</div>
			<?php endif; ?>

		</div>

		<?php hygge_link_split_pages(); ?>

	</div>

</article>

<?php if( $theme_settings['post_related'] ): ?>
	<div class="post__related hygge-grey-section grey-mode">
	<div class="max-width-wrapper">
		<?php get_template_part('related-posts') ?>
	</div>
	</div>
<?php endif;

if( $theme_settings['post_navigation'] ): ?>
	<div class="post__navigation">
		<?php get_template_part('post-navigation'); ?>
	</div>
<?php endif; ?>



<?php if(
get_post_meta( get_the_ID(), 'comments_use', true ) != '0' // comments are not disabled on post MUST
&& ( comments_open() || get_comments_number() > 0 ) // comments are open or count > 0 MUST
&& !post_password_required()
){
	comments_template();
}?>





<?php endwhile; endif;

get_footer();
