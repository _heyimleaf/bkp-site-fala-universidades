<?php
$theme_settings = hygge_theme_settings();

$sidebar_class = 'grey-mode';
switch( $theme_settings['bg_sidebar'] ){
	case false:
	case '':
	case '#fff':
	case '#ffffff':
		$sidebar_class = '';
		break;
}

if ($theme_settings['bg_sidebar_dark']) {
	$sidebar_class = 'dark-mode';
}

?>

<div id="sidebar" class="sidebar hygge-layout-sidebar <?php echo $sidebar_class; ?>">
	<?php if( $theme_settings['use_sidebar'] && is_active_sidebar('widget-area-sidebar-area') ): ?>
		<div class="sidebar__content">

			<?php dynamic_sidebar('widget-area-sidebar-area'); ?>

		</div>
	<?php endif; ?>
</div>
