<?php
$theme_settings = hygge_theme_settings();

// get current post tags
$tags_obj = wp_get_post_tags($post->ID);
// if( !$tags_obj ){ return; }

$tags = array();
foreach( $tags_obj as $term ){
	// $tags[] = $term->term_id;
}

$tax_query = array(
	array(
		'taxonomy' => 'post_format'
		,'field' => 'slug'
		,'terms' => array('post-format-link','post-format-aside','post-format-status','post-format-quote')
		,'operator' => 'NOT IN'
	)
);

$args=array(
	'tag__in' => $tags
	,'post__not_in' => array($post->ID)
	,'posts_per_page' => 5
	,'ignore_sticky_posts' => 1
	,'orderby' => 'rand'
	// ,'meta_key' => '_thumbnail_id' // contain featured image
	,'tax_query' => $tax_query
);



$query = new WP_Query($args);

if( $query->have_posts() ):

?>

<?php if( $theme_settings['post_related_title'] ): ?>
	<h3 class="title">
		<?php echo esc_html( $theme_settings['post_related_title'] ); ?>
	</h3>
<?php endif; ?>

<div class="loop-container loop-container--style-related">
<?php while ($query->have_posts()) : $query->the_post();

	get_template_part( 'loop-item', 'related' );

endwhile; ?>

</div>

<?php endif; ?>
<?php wp_reset_postdata(); ?>
