<?php $theme_settings = hygge_theme_settings();

$post_comments_class = have_comments() ? '' : 'post__comments--no-comments';
?>

<div id="comments" class="post__comments max-width-wrapper <?php echo $post_comments_class; ?>">

<h3 class="title">
	<?php comments_number( esc_html__( '0 Comments', 'hygge' ), esc_html__( '1 Comment', 'hygge' ), esc_html__( '% Comments', 'hygge' ) ); ?>
</h3>

<div class="post__comments__inwrap">

<?php
/* Comment Form
 *------------------------------------------------------------*/

// Reorder comment form fields
if( !function_exists( 'hygge_reorder_comment_form_fields') ){
	function hygge_reorder_comment_form_fields( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		return $fields;
	}
}
add_filter( 'comment_form_fields', 'hygge_reorder_comment_form_fields' );

$commenter 	= wp_get_current_commenter();
$req 		= get_option( 'require_name_email' );
$aria_req 	= ( $req ? " aria-required='true'" : '' );

$comments_args = array(
	'class_submit' => '',
	'label_submit' => esc_html__('Comment', 'hygge'),
	'title_reply' => '',
	'title_reply_before' => '',
	'title_reply_after' => '',
	'comment_notes_before' => '',
	'comment_notes_after' => '',
	'fields' => apply_filters( 'comment_form_default_fields',
		array(
			'author' =>
				'<div class="hygge-form-label"><label>'.esc_html__('Name', 'hygge') . ($req ? ' *' : '').'</label><input id="author" name="author" class="' . ($req ? 'required' : '') . '" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . ' /></div>'
			,
			'email' =>
				'<div class="hygge-form-label"><label>' . esc_html__('Email', 'hygge') . ($req ? ' *' : '') . '</label><input id="email" name="email" type="text" class="' . ($req ? 'required' : '') . '" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" ' . $aria_req . ' /></div>'
			,
			'url' =>
					'<div class="hygge-form-label"><label>' . esc_html__('Website', 'hygge').'</label><input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" /></div>'
		)
	),
	'comment_field' =>
		'<div class="hygge-form-label"><label>'.esc_html__('Comment', 'hygge').' *</label><textarea id="comment" class="required" name="comment" aria-required="true"></textarea></div>',
);


if( have_comments() ){
	echo '<div class="comment-respond-main hygge-sideblock">';
}else{
	echo '<div class="comment-respond-main">';
}
	comment_form($comments_args);
echo '</div>';



/* Display Comments
 *------------------------------------------------------------*/

if( have_comments() ): ?>

	<ol class="comments">
		<?php
			if( $theme_settings['display_pingbacks'] ){
				wp_list_comments( array(
					'type' => 'all',
					'avatar_size' => 160,
					'callback' => 'hygge_comments'
				) );
			}else{
				wp_list_comments( array(
					'type' => 'comment',
					'avatar_size' => 160,
					'callback' => 'hygge_comments'
				) );
			}
		?>
	</ol>

	<div class="comment__pager">
		<?php
			//paginate_comments_links();

			if( get_option('default_comments_page') == 'oldest' ){
				next_comments_link( esc_html__('Load More', 'hygge'), $wp_query->max_num_comment_pages );
			}else{
				previous_comments_link( esc_html__('Load More', 'hygge'), $wp_query->max_num_comment_pages );
			}
		?>
	</div>


<?php endif; ?>


</div>
</div>
