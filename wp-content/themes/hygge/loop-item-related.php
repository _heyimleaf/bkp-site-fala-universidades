<?php

/*------------------------------------------------------------
 * This is loop-item-related template.
 * Style: related
 * Title: Related
 *------------------------------------------------------------*/

$color = hygge_get_post_color();
$colorOpacity = $color ? '' : 'color-opacity-low';
?>

<article class="post post--related">
	<div class="post__inwrap">
		<a href="<?php the_permalink(); ?>" class="hygge-media-hover oh has-thumbnail-<?php echo ( has_post_thumbnail() ? 'true' : 'false' ); ?> " title="<?php echo esc_attr( get_the_title() ); ?>" data-loader-img="<?php echo hygge_get_loader_img_url(); ?>">

			<div class="post__media">
				<?php
					echo '<div class="hygge-media-hover__img">';
						if( has_post_thumbnail() ){
							the_post_thumbnail( 'hygge-square-m' );
						}else{
							echo hygge_get_dropcap_media('square');
						}
					echo '</div>';
				?>
				<div class="hygge-media-hover__hover <?php echo esc_attr($colorOpacity); ?>" style="background-color:<?php echo esc_attr($color);?>;"></div>
			</div>

			<div class="post__text">
				<h5 class="post__title">
					<?php echo wp_kses( get_the_title(), hygge_allowed_kses_tags() ); ?>
				</h5>

				<div class="post__meta label--italic">
					<div class="meta--date"><?php echo get_the_date(); ?></div>
				</div>
			</div>

		</a>
	</div>
</article>
