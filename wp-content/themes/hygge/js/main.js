if( typeof hygge === "undefined" ){
	var hygge = {};
}

(function($){
"use strict";



/*------------------------------------------------------------
 * Events
 *------------------------------------------------------------*/

	$(document).ready(function(){
		resolutionUtilities();
		gatherThemeSettings();
		hyggeScrollWatch();
		// hyggeLuminanceMode();
		wpLoopLoadMore();
		ajaxLoadPages();
		slider();
		hyggeCarousel();
		galleryMasonry();
		galleryFluid();
		hyggeFilmstrip();
		galleryLightbox();
		loopMasonry();
		sharer();
		wpCommentsLoadMore();
		hyggeValidateComment();
		animateAppearance();
		hyggeWrapSelect();
		hyggeModularLoading();
		hyggeNav();
		hyggeSmoothAnchorScroll();
		hyggeFullWidth();
		hyggeStickySideblock();
		hyggeLayout();
		hyggeHeaderSearch();
		hyggeNotificationBar();
		hyggeDoubleBlogWidget();
		hyggeStickyHeader();
		hyggeStickySidebar();
		hyggeStickyShare();
		hyggeFormLabel();
		hyggeEvenHeight();
		hyggePreloadImages();
		hyggeFooterLayout();
		hyggeCustomInputNumber();
		hyggeDropdown();
		hyggeMediaDropcapSize();
	});

	$(document).on( 'hygge:ajaxPageLoad', function(){
		// hyggeLuminanceMode();
		wpLoopLoadMore();
		hyggeMejs();
		slider();
		hyggeCarousel();
		galleryMasonry();
		galleryFluid();
		hyggeFilmstrip();
		galleryLightbox();
		loopMasonry();
		sharer();
		animateAppearance();
		hyggeWrapSelect();
		hyggeModularLoading();
		hyggeFullWidth();
		hyggeStickySideblock();
		hyggeLayout();
		hyggeHeaderSearch();
		hyggeDoubleBlogWidget();
		hyggeStickySidebar();
		hyggeStickyShare();
		hyggeFormLabel();
		hyggeEvenHeight();
		hyggePreloadImages();
		hyggeCustomInputNumber();
		hyggeDropdown();
		hyggeMediaDropcapSize();
	});

	$(document).on( 'hygge:resize', function(){
		resolutionUtilities();
		galleryMasonry();
		loopMasonry();
		hyggeLayout();
		hyggeStickySidebar();
		hyggeEvenHeight();
		hyggeFooterLayout();
		hyggeMediaDropcapSize();
	});

	$(document).on( 'hygge:scroll', function(){
	});

	$(document).on( 'hygge:layout', function(){
		hyggeLayout();
		hyggeStickySidebar();
	});

	$(document).on( 'hygge:fullWidth', function(){
		galleryMasonry();
		galleryFluid();
		hyggeEvenHeight();
	});



/* Window width and device
 *------------------------------------------------------------*/

function resolutionUtilities(){

	// get window width & height
	hygge.windowWidth  = $(window).width();
	hygge.windowHeight = $(window).height();

	// get device by window width
	if( hygge.windowWidth <= 599 ){
		hygge.device = 'mobile';
	}

	if( hygge.windowWidth >= 600 && hygge.windowWidth <= 960 ){
		hygge.device = 'tabletV';
	}

	if( hygge.windowWidth >= 961 && hygge.windowWidth <= 1170 ){
		hygge.device = 'tabletH';
	}

	if( hygge.windowWidth >= 1171 ){
		hygge.device = 'desktop';
	}

}



/* FUNCTION: Validate E-mail
 *------------------------------------------------------------*/

function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

/* FUNCTION: Get random number in given range
 *------------------------------------------------------------*/

function getRandomNumber(min,max){
	return Math.floor(Math.random()*(max-min+1)+min);
}



/* Gather theme settings and add important classes
 *------------------------------------------------------------*/

function gatherThemeSettings(){

	hygge.ajaxEnabled = false;
	hygge.stickyHeader = false;

	// Ajax Enabled
	if(
		window.history &&
		window.history.pushState &&
		$('body').hasClass('js-ajax-load-pages')
	){
		hygge.ajaxEnabled = true;
		hygge.ajaxScrollHistory = {};
		history.scrollRestoration = 'manual';
	}

	// Sticky Header Enabled
	if( $('body').hasClass('hygge-sticky-header--true') ){
		hygge.stickyHeader = true;
	}

	if( $('.hygge-notif').length > 0 ){
		hygge.notificationBar = true;

		if( $('.hygge-notif .hygge-notif__hide').length > 0 ){
			hygge.notificationBarHideable = true;
		}
	}

	// WordPress Admin Bar
	if( $('body').hasClass('admin-bar') ){
		hygge.adminBar = true;
	}

	// Browser
	if( hygge.browser ){
		$('body').addClass('browser-'+hygge.browser);
	}

	// RTL
	hygge.dir = $('html').attr('dir') == 'rtl' ? 'rtl' : 'ltr';
}





/*------------------------------------------------------------
 * EVENT: Hygge:Resize
 *------------------------------------------------------------*/

var resizeTimeout;
$(window).resize(function() {

	// don't fire event when address bar disappears
	if( hygge.device == 'mobile' ){
		var w = $(window).width();
		var h = $(window).height();

		if(
			Math.abs(hygge.windowWidth - w) < 80 &&
			Math.abs(hygge.windowHeight - h) < 80
		){
			return;
		}
	}

	requestAnimationFrame(function() {
		clearTimeout(resizeTimeout);
		resizeTimeout = setTimeout(function() {
			$(document).trigger('hygge:resize');
		}, 100);
	});
});





/*------------------------------------------------------------
 * EVENT: Hygge:Scroll
 *------------------------------------------------------------*/

var scrollTimeout;
// var scrollTime = 0;
// var scrollNow = 0;
// var scrollTimeDistance = 0;
var scrollTicking = false;
var previousScrollTop = window.pageYOffset;
hygge.scrollDirection = 'down';

function hyggeScrollRequestTick(){

	if( !scrollTicking ){
		requestAnimationFrame(scrollUpdate);
	}
	scrollTicking = true;

}

function scrollUpdate(){
	scrollTicking = false;

	// Check if scrolled at all or mouse tripping
	if( previousScrollTop - window.pageYOffset === 0 ){
		return;
	}

	// Check for scrolling direction and store it
	hygge.scrollDirection = previousScrollTop > window.pageYOffset ? 'up' : 'down';
	// Update to new scrollTop history point
	previousScrollTop = window.pageYOffset;

	$(document).trigger('hygge:scroll');

	// Trigger the event once after the scrolling stops
	clearTimeout( scrollTimeout );
	scrollTimeout = setTimeout( function(){
		$(document).trigger('hygge:scroll');
	}, 250);
}

function hyggeScrollWatch(){
	$(document).off('.hyggeScrollWatch');
	if( hygge.device != 'mobile' ){
		// currently not firing hygge:scroll event
		$(window).scroll(hyggeScrollRequestTick);
	}
}

$(document).on( 'hygge:ajaxPageLoad.hyggeScrollWatch', hyggeScrollWatch );
$(document).on( 'hygge:resize.hyggeScrollWatch', hyggeScrollWatch );






/*------------------------------------------------------------
 * FUNCTION: Push ajax navigation info to google analytics
 *------------------------------------------------------------*/

function ajaxPushGoogleAnalytics(){
	if( !hygge.ajaxEnabled ){
		return;
	}

	if(typeof _gaq !== "undefined" && _gaq !== null) {
		_gaq.push(['_trackPageview', window.location.pathname]);
	}else if( typeof ga == 'function' ){
		ga('send', 'pageview', window.location.pathname);
	}
}





/*------------------------------------------------------------
 * FUNCTION: Get image luminance
 * returns callback(true/false) for isDark
 *
 * Usage:
 * isImageDark("image.jpg",function(isDark){
 *   // callback with true/false isDark argument
 * });
 *------------------------------------------------------------*/

function isImageDark(imageSrc, callback) {

	// check cache
	if( localStorage.getItem('hyggeImgLum-'+imageSrc) ){
		returnLum( localStorage.getItem('hyggeImgLum-'+imageSrc) );
	}else{
		getImageLum( imageSrc );
	}

	function getImageLum( imageSrc ){
		var img = document.createElement("img");
		img.src = imageSrc;
		img.style.display = "none";
		document.body.appendChild(img);
		img.onload = function() {
			// create canvas
			var canvas = document.createElement("canvas");
			canvas.width = this.width;
			canvas.height = this.height;

			var ctx = canvas.getContext("2d");
			ctx.drawImage(this,0,0);

			var imageData = ctx.getImageData(0,0,canvas.width,canvas.height);
			var data = imageData.data;
			var r,g,b, lum;
			var lumTotal = 0, lumAvg = 0;
			var samples = 16;

			for(var x = 0, len = data.length; x < len; x+=samples) {
				r = data[x];
				g = data[x+1];
				b = data[x+2];

				lum = (r+r+b+g+g+g)/6;
				lumTotal += lum;
			}

			lumAvg = Math.round( lumTotal / (data.length/samples) );

			// Save in cache and remove html object and canvas
			localStorage.setItem('hyggeImgLum-'+imageSrc, lumAvg);
			returnLum( lumAvg);
			img.remove();
			canvas = null;
		};
	}

	function returnLum(lum){
		if (lum < 160){
			callback(true); /* Dark. */
		}else{
			callback(false);  /* Not dark. */
		}
	}
}



/*------------------------------------------------------------
 * FUNCTION: Get color luminance
 * returns callback(true/false) for isDark
 *
 * Usage:
 * isColorDark("#f0f0f0",function(isDark){
 *   // callback with true/false isDark argument
 * });
 *------------------------------------------------------------*/

function isColorDark(color, callback) {

	if( color.substring(0,1) === '#' ){
		color = color.substring(1);
	}

	var rgb = parseInt(color, 16); // convert rrggbb to decimal
	var r = (rgb >> 16) & 0xff; // extract red
	var g = (rgb >>  8) & 0xff; // extract green
	var b = (rgb >>  0) & 0xff; // extract blue

	var lum = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

	if( lum < 215 ){
		callback(true); /* Dark. */
	}else{
		callback(false);  /* Not dark. */
	}
}



/*------------------------------------------------------------
 * FUNCTION: Luminance Mode
 * NOTE: Currently disabled.
 * Colors are checked in php, images are not checked.
 *
 * Automatically check all elements with
 * data-hygge-image-luminance='{$image}'
 * data-hygge-color-luminance='{$color}'
 * and add dark-mode class if {$image} or {$color} is dark
 *------------------------------------------------------------*/

function hyggeLuminanceMode() {
	$('[data-hygge-image-luminance], [data-hygge-color-luminance]').each(function(){
		var $this = $(this);

		// If has image luminance
		if( $this.attr('data-hygge-image-luminance') ){
			var img = $this.attr('data-hygge-image-luminance');
			if( img ){
				isImageDark(img, function(isDark){
					if( isDark ){
						$this.addClass('dark-mode');
					}
				});
			}
		}else if( $this.attr('data-hygge-color-luminance') ){
			// Check color luminance
			var color = $this.attr('data-hygge-color-luminance');
			if( color ){
				isColorDark(color, function(isDark){
					if( isDark ){
						$this.addClass('dark-mode');
					}
				});
			}
		}
	});
}





/*------------------------------------------------------------
 * FUNCTION: Hygge Slider
 *------------------------------------------------------------*/

function slider(){

	$('.hygge-slider:not(.processed)').each(function(e){
		var $el         = $(this).addClass('js-started');
		var type        = $el.hasClass('hygge-slider--blog') ? 'blog' : 'image';
		var $items      = $el.find('.hygge-slider__items');
		var $item       = $el.find('.hygge-slider__item');
		var $first      = $item.first();
		var $last       = $item.last();
		var $active 		= $first;
		var interval;
		var intervalVal = parseInt( $el.attr('data-interval') );
		var $arrowPrev  = $el.find('.js-hygge-ui-arrow--prev');
		var $arrowNext  = $el.find('.js-hygge-ui-arrow--next');
		var $pager      = $el.find('.js-hygge-ui-pager-item');
		var $pagerMarker= $el.find('.hygge-ui-pager__active-marker');
		var dataHeight 	= $el.attr('data-height');

		// Add order ID's if missing
		if( !$item.attr('data-id') ){
			var i = 0;
			$item.each(function(){
				$(this).attr('data-id', i);
				i++;
			});
		}

		// Activate first item
		activate($first, false, true);
		$(document).on('hygge:resize', function(){
			activate($first);
		});

		// Function: Calculate height of the slider
		// Currently goes for average item height
		// used to go for maximum item height
		function sliderHeightEq(){
			var height = 0;
			var avg = 0;
			var maxHeight = parseInt( dataHeight );
			$el.removeClass('processed');
			var i = 0;

			$el.imagesLoaded(function(){
				$item.each(function(){

					var $this = $(this);
					i++;
					var thisHeight = $this.find('.hygge-slider__item-inwrap').height('').innerHeight();

					height = height + thisHeight;
					avg = height / i;

					if( maxHeight && maxHeight < avg ){
						avg = maxHeight;
					}

					if( avg % 2 === 1 ) {
						avg = avg - 1;
					}

					// When last one is done
					if( i == $item.length ){
						$items.height(avg);
						$el.addClass('processed');
						$(document).trigger('hygge:layout');
					}
				});
			});
		}

		sliderHeightEq();
		$(document).one( 'hygge:resize', sliderHeightEq );



		// Function: Activate item, mark animate-out and animate-in
		function activate($activateItem, isPrev, skipAnimate){

			var reverseClass = isPrev === true ? 'animate-reverse' : '';
			skipAnimate = skipAnimate ? true : false;
			var slideId = $activateItem.attr('data-id');

			if( !$activateItem.hasClass('active') ){

				if( skipAnimate ){
					// Only first item is immediately active
					$activateItem.addClass('active').siblings().removeClass('active '+reverseClass);
					$active = $activateItem;
				}else{
					// Standard animation
					// add animate-in class to new active item
					$activateItem.addClass('animate-in '+reverseClass);

					// add animate-out class to last active item
					var $itemOut = $active.addClass('animate-out '+reverseClass);

					setTimeout( function(){
						$itemOut.removeClass('animate-out '+reverseClass);
						$activateItem.removeClass('animate-in '+reverseClass);
						$activateItem.addClass('active').siblings().removeClass('active '+reverseClass);
						$active = $activateItem;
					}, 700 );
				}

				// update pager
				if( $pager.length > 0 ){
					var itemId = $activateItem.attr('data-id');
					$pager.removeClass('active');

					// set timeout to fix initial incorrect value
					setTimeout( function(){
						var pagerLeft = $pager.filter('[data-id="'+itemId+'"]').addClass('active').position().left;
						$pagerMarker.stop().animate({
							left: pagerLeft,
							opacity: 1
						}, 700, 'easeInOutCubic');
					}, 0);
				}
			}
		}



		// Function: Activate next item
		function activateNextItem() {
			var $nextItem;

			// if next item - select it, else jump to start
			if( $active.next('.hygge-slider__item').length > 0 ){
				$nextItem = $active.next('.hygge-slider__item');
			}else{
				$nextItem = $first;
			}

			activate($nextItem);
		}

		// Function: Activate prev item
		function activatePrevItem() {
			var $prevItem;

			// if prev item - select it, else jump to end
			if( $active.prev('.hygge-slider__item').length > 0 ){
				$prevItem = $active.prev('.hygge-slider__item');
			}else{
				$prevItem = $last;
			}

			activate($prevItem, true);
		}



		// Automatic animation
		if( intervalVal && intervalVal != '0'){
			startInterval();

			// events
			$(document).off('.stopInterval');
			$(document).on( 'hygge:ajaxPageLoad.stopInterval', stopInterval );
			$el.hover( stopInterval , startInterval );
		}

		// Function: Stop interval animation
		function stopInterval(){
			clearInterval(interval);
		}

		// Function: Start interval animation
		function startInterval(){

			interval = setInterval( function(){
				activateNextItem();
			}, intervalVal);
		}



		// Arrows functionality
		$arrowNext.click(function(e){
			e.preventDefault();
			activateNextItem();
		});

		$arrowPrev.click(function(e){
			e.preventDefault();
			activatePrevItem();
		});

		// Touch-Swipe functionality
		$el.swipe( {
			swipeRight: function(){
				activatePrevItem();
			},
			swipeLeft: function(){
				activateNextItem();
			},
			threshold:40,
			excludedElements:''
		});

		// Arrow keys
		$(document).keydown(function(e){
			if( e.which == 37 ) {
				activatePrevItem();
			}
			if( e.which == 39 ) {
				activateNextItem();
			}
		});




		// Pager functionality
		$pager.click(function(e){
			e.preventDefault();
			var activeId = $active.attr('data-id');
			var pagerId = $(this).attr('data-id');
			var isPrev = false;

			if( pagerId.replace( /^\D+/g, '') < activeId.replace( /^\D+/g, '') ){
				isPrev = true;
			}
			activate( $items.find('[data-id="'+pagerId+'"]'), isPrev );
		});

	});

}





/*------------------------------------------------------------
 * FUNCTION: Load More of WP Blog Loops
 *------------------------------------------------------------*/

function wpLoopLoadMore(){

	// Event: Load More Button Click
	$('html').off('.load-more--wp-loop');
	$('html').on('click.load-more--wp-loop','.js-load-more--wp-loop', function(e){
		e.preventDefault();
		loadItems( $(this) );
	});

	// Function: Load More
	function loadItems($target){

		var $el     = $('.loop-container--wp');
		var $this   = $target;
		var $pag    = $this.closest('.pagination');
		var href    = $this.attr('href');
		var max     = parseInt($this.attr('data-max'));
		var current = parseInt($this.attr('data-current'));

		if(
			$pag.hasClass('pagination--loading') ||
			$pag.hasClass('pagination--disabled')
		){
			return;
		}

		if(max > current){
			$pag.addClass('pagination--loading');

			if( hygge.xhr ){
				hygge.xhr.abort();
			}

			hygge.xhr = $.ajax({
				type: "GET",
				url: href,
				success: function(data, response, xhr){

					// get, append and animate posts, trigger ajax complete event
					var posts = $(data).find('.loop-container--wp');
					$el.append( posts.html() );

					if( !$el.hasClass('loop-is-masonry') ){
						$pag.removeClass('pagination--loading');
					}

					ajaxPushGoogleAnalytics();

					// update load more button
					current = current + 1;

					// if pretty links ON
					if(/\/page\/[0-9]+/.test(href)){
						href = href.replace(/\/page\/[0-9]+/, '/page/'+ (current+1));
					}else{
						href = href.replace(/paged=[0-9]+/, 'paged='+ (current+1));
					}
					$this.attr('data-current', current);
					$this.attr('href', href);

					// if last available page loaded, remove load more button
					if(current == max){
						$pag.addClass('pagination--disabled');
					}

					// Make a temporary note that load more was the ajaxPageLoad event
					hygge.loopLoadMoreHappened = true;
					setTimeout(function(){
						hygge.loopLoadMoreHappened = false;
					}, 2000);

					$(document).trigger('hygge:ajaxPageLoad');
				},
				fail: function(){
					$pag.removeClass('pagination--loading');
				}

			});

		}
	}

}





/*------------------------------------------------------------
 * FUNCTION: Ajax Load Pages
 *------------------------------------------------------------*/

function ajaxLoadPages(){

	if( hygge.ajaxEnabled === false ) { return; }


	var skipAjax;
	var ajaxLoadPageTime = 0;

	// iOS/Safari Bug: triggers popstate on init page load.
	// Solution: define var and make it true on first popstate/push
	var popped = false;

	// Event: Link clicked
	$('html').on( 'click','a', function(e) {

		// Suppress double clicks
		var now = new Date().getTime();
		var dt = now - ajaxLoadPageTime;
		if ( dt < 700 ) {
			e.preventDefault();
			return;
		}
		ajaxLoadPageTime = now;

		var $link = $(this);



		var href = $link.attr('href');

		if( !href || isExternal(href) ){
			return;
		}

		// Assume that link will be loaded with ajax
		skipAjax = false;

		if (
			( $link.attr('target') == '_blank' ) ||
			( $link.is(".ab-item, .comment-reply-link, #cancel-comment-reply-link, .comment-edit-link, .comment__pager a, .wp-playlist-caption, .js-skip-ajax") ) ||

			// WP links
			( href.indexOf('wp-login.php') > 0 ) ||
			( href.indexOf('/wp-admin/') > 0 ) ||
			( href.indexOf('wp-content/uploads/') > 0 ) ||
			( href == hygge.baseUrl + '/feed/' ) ||
			( href == hygge.baseUrl + '/comments/feed/' ) ||

			// Skip Hashed Links for current page
			// or just plain hashes, both absolute and relative links
			( href.split('#')[1] && href.split('#')[0] == location.href.split('#')[0] ) ||
			( href.match(/^#.*$/) ) ||

			// WPML: on lang change, full page load
			( $link.is('[hreflang]') ) ||
			( $link.parents('#lang_sel').length ) ||
			( $link.parents('#lang_sel_click').length ) ||
			( $link.parents('#lang_sel_list').length ) ||
			( $link.parents('.menu-item-language').length ) ||

			// Disqus: doesn't support ajax
			( typeof DISQUS !== 'undefined' || typeof countVars !== 'undefined' )
		){
			skipAjax = true;
		}else{

			e.preventDefault();
			popped = true;
			push_state(href);

			// Loader image
			$('.hygge-loader--body').find('img').remove();

			var loaderImgUrl = '';
			if( $link.attr('data-loader-img') ){
				// get post thumbnail if available
				loaderImgUrl = $link.attr('data-loader-img');
			}else{
				// try to fallback to default loader image
				loaderImgUrl = hygge.loaderImg;
			}

			if( loaderImgUrl ){
				$('.hygge-loader--body').append( '<img src="' + loaderImgUrl + '">' );
			}

		}

	});



	// Event: Popstate - Location History Back/Forward
	$(window).on('popstate',function(){
		if( !skipAjax && popped ){
			ajaxLoadPage(location.href, true);
		}
		popped = true;
	});



	// Event: Search Form Submitted
	$('html').on('submit','form.search-form',function(e) {
		e.preventDefault();

		var action = $(this).attr('action');
		var term = $(this).find('.textfield').val();

		if( action && term ){
			popped = true;
			push_state( action + '/?s=' + term );
		}
	});



	// Function: PushState and trigger ajax loader
	function push_state(href){

		// store info about current page without the trailing slash
		var ajaxHistoryHref = window.location.href.replace(/\/$/, "");
		hygge.ajaxScrollHistory[ajaxHistoryHref] = window.pageYOffset;


		history.pushState({page: href}, '', href);
		ajaxLoadPage(href);
	}



	// Function: Ajax Load Page
	function ajaxLoadPage(href, scrollHistory) {

		if( hygge.xhr ){
			hygge.xhr.abort();
		}

		var timeStarted = 0;

		// unless back/forward popstate, scrollPosition is 0
		var scrollPosition = 0;
		if( typeof scrollHistory !== 'undefined' && scrollHistory ){
			var ajaxHistoryHref = href.replace(/\/$/, "");
			if( hygge.ajaxScrollHistory[ajaxHistoryHref] ){
				scrollPosition = hygge.ajaxScrollHistory[ajaxHistoryHref];
			}
		}

		$('body').addClass('ajax-loading-start');
		// close header on touch devices, enable header transitions
		$('body').removeClass('touchscreen-header-open sticky-transitions--false');

		// Animate page scroll back to the top
		hyggeSmoothAnchorScroll('#top');

		timeStarted = new Date().getTime();

		hygge.xhr = $.ajax({
			type: "GET",
			url: href,
			success: function(data, response, xhr){

				// Check if css animation had time to finish
				// before new page load animation starts
				var now = new Date().getTime();
				var timeDiff = now - timeStarted;
				if( timeDiff < 700 ) {
					setTimeout( ajaxLoadPageCallback, (700-timeDiff) );
				}else{
					ajaxLoadPageCallback();
				}

				function ajaxLoadPageCallback(){
					var $data = $(data);
					var sidebarChanged = false;

					// Update Page Title in browser window
					var pageTitle = $data.filter('title').text();
					document.title = pageTitle;

					var $newContent = $data.find('#content-wrapper-inside');

					// Update Content
					$('#content-wrapper').html( $newContent );

					// Update Main Navigation
					$('.header__menu .hygge-nav--classic').html( $data.find('.header__menu .hygge-nav--classic > .menu') );


					// Check sidebar content and update if different
					// Approach is to check all the widgets and collect their
					// IDs in a single string
					// if new and old sidebar strings are equal,
					// we expect whole sidebar to also be
					var $newSidebar = $data.find('.sidebar');

					var sidebarIdStringOld = '';
					var sidebarIdStringNew = '';
					$('.sidebar .sidebar__content').children().each(function(){
						sidebarIdStringOld += $(this).attr('id');
					});
					$newSidebar.find('.sidebar__content').children().each(function(){
						sidebarIdStringNew += $(this).attr('id');
					});

					if( sidebarIdStringOld != sidebarIdStringNew ){
						sidebarChanged = true;

						// if whole sidebar should be removed, do so after it animates out
						if( $newSidebar.length == 0 || $newSidebar.html().trim() == '' ){
							setTimeout( function(){
								$('.sidebar').html('');
								$(document).trigger('hygge:resize');
							}, 750 );
						}else{
							// just change the sidebar content. no animation currently
							$('.sidebar').html( $newSidebar.html() );
						}

						// ensure correct layout sizes
						setTimeout( function(){
							$(document).trigger('hygge:resize');
						}, 1000 );
					}



					// Scroll page back to top
					// This is now removed because page is scrolled back
					// while fading-out content
					// $('html,body').stop().scrollTop(0);

					// If scrollPosition from history, scroll to it
					if( scrollPosition !== 0 ){
						hyggeSmoothAnchorScroll( scrollPosition );
					}else{
						hyggeSmoothAnchorScroll( href );
					}

					// Update body classes
					// Keep at the end as it removes 'ajax-loading-start' class automatically
					$('body').attr('class', data.match(/<body.*?class="(.*?)"/)[1] );
					// $('body').removeClass('ajax-loading-start');

					$(document).trigger('hygge:ajaxPageLoad');

					ajaxPushGoogleAnalytics();
				}
			},

			error: function(){
				$('body').removeClass('ajax-loading-start');
			}
		});
	}



	// Function: RegExp: Check if url external
	function isExternal(url) {

		// match solo # or #text
		if( url.match(/^#.*$/) ){
			return false;
		}

		var match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);

		// Check protocol
		if( typeof match[1] === "string" && match[1].length > 0 && match[1].toLowerCase() !== location.protocol ){
			return true;
		}

		// Check hostname
		if( typeof match[2] === "string" && match[2].length > 0 && match[2].replace(new RegExp(":("+{"http:":80,"https:":443}[location.protocol]+")?$"), "") !== location.host ){
			return true;
		}

		// Check if WP baseUrl is exact
		// This enables Ajax to work on subfolder domains
		var regexBaseUrl = new RegExp(hygge.baseUrl+'([^\/]*)');
		match = url.match(regexBaseUrl);

		if( !match || match[1] !== '' ){
			return true;
		}

		return false;
	}

}





/*------------------------------------------------------------
 * FUNCTION: Start Mejs on AJAX load
 * Copied from : /wp-includes/js/mediaelement/wp-mediaelement.js?ver=3.9.1
 *------------------------------------------------------------*/

function hyggeMejs(){

	$('audio.wp-audio-shortcode, video.wp-video-shortcode').each(function(){

		var $el = $(this);

		// check if already processed
		if(
			( $(this).parent().hasClass('mejs-mediaelement') ) ||
			( typeof mejs === 'undefined' )
		){
			return;
		}

		// add mime-type aliases to MediaElement plugin support
		mejs.plugins.silverlight[0].types.push('video/x-ms-wmv');
		mejs.plugins.silverlight[0].types.push('audio/x-ms-wma');

		$(function () {
			var settings = {};

			if ( $( document.body ).hasClass( 'mce-content-body' ) ) {
				return;
			}

			if ( typeof _wpmejsSettings !== 'undefined' ) {
				settings.pluginPath = _wpmejsSettings.pluginPath;
			}

			settings.success = function (mejs) {
				var autoplay = mejs.attributes.autoplay && 'false' !== mejs.attributes.autoplay;
				if ( 'flash' === mejs.pluginType && autoplay ) {
					mejs.addEventListener( 'canplay', function () {
						mejs.play();
					}, false );
				}
			};

			$el.mediaelementplayer( settings );
		});
	});

}





/*------------------------------------------------------------
 * FUNCTION: Gallery Masonry
 *------------------------------------------------------------*/

function galleryMasonry(){
	// if mobile on init, do nothing
	// if mobile on resize, keep masonry and carry on
	if( hygge.device == 'mobile' ){
		if( !$('.js-gallery-masonry').hasClass('gallery-masonry--init-processed') ){
			return;
		}
	}



	$('.js-gallery-masonry').each(function(){

		var el = this;
		var $el = $(this);

		var $image = $el.find('.gallery-item').not('.masonry-processed');
		// var $imageImg = $image.find('img');
		// $imageImg.css('height', '');

		var masonry = new Masonry( el, {
			columnWidth: '.gallery-item',
			itemSelector: '.gallery-item',
			transitionDuration: 0,
			// isInitLayout: false,
			isResizeBound: false
		});

		masonry.on( 'layoutComplete', function() {
			hyggeLayout();
		});

		$image.each(function(){
			var $this = $(this);
			// var $thisImg = $this.find('img');
			// $this.find('img').css('height', '');
			// $thisImg.css('height', $thisImg.height());

			$this.imagesLoaded(function(){
				masonry.layout();
				$this.addClass('masonry-processed');
				// $this.find('img').height($this.height());
			});
		});

		$el.imagesLoaded(function(){
			masonry.layout();

			setTimeout(function(){
				masonry.layout();
			},100);

			setTimeout(function(){
				masonry.layout();
			},500);

			$el.addClass('gallery-masonry--init-processed');
		});

	});
}





/*------------------------------------------------------------
 * FUNCTION: Gallery Fluid
 *------------------------------------------------------------*/

function galleryFluid(){

	$('.gallery--fluid').each(function(){

		var maxHeight = parseInt( $(this).attr('data-height') );
		var height = maxHeight && maxHeight !== 0 ? maxHeight : 1000; //default to 1000px

		// Get the appropriate height
		$('.gallery-item', this).each(function(){
			height = Math.min( height, $(this).attr('data-h') );
		});

		$('.gallery__items', this).flexImages({
			container: '.gallery-item',
			rowHeight: height,
		});

		$(document).trigger( 'hygge:layout');
	});
}





/*------------------------------------------------------------
 * FUNCTION: Hygge Filmstrip
 *------------------------------------------------------------*/

function hyggeFilmstrip(){
	// get rid of excess stuff after ajaxLoad
	$(document).off('.filmstripLayout');
	$.fn.mCustomScrollbar('destroy');

	$('.js-filmstrip').each(function(e){

		var $el         = $(this).addClass('js-started');
		var $elInwrap   = $el.find('.gallery__inwrap');
		var $items      = $el.find('.gallery__items');
		var $item       = $el.find('.gallery-item');

		var elType = $el.hasClass('blog-filmstrip') ? 'blog' : 'gallery';
		if( elType == 'blog' ){
			$elInwrap   = $el.find('.blog-filmstrip-wrapper');
			$items      = $el.find('.loop-container');
			$item       = $el.find('.post, .page');
		}

		var itemPos     = [];
		var $img 				= $item.find('img');
		var itemCount   = $item.length;
		var itemsWidth;
		var elWidth;
		var position    = 0;
		var imgHeight   = 0;
		var heightLimit = $el.attr('data-height');
		var interval;
		var intervalVal = parseInt( $el.attr('data-interval') );


		// Calculate & lay-out elements
		function filmstripLayout(){
			$items.imagesLoaded(function(){

				elWidth = Math.floor( $el.innerWidth() );
				$item.css('max-width', elWidth);

				$items.width('');
				itemsWidth = 0;

				if( elType == 'gallery' ){

					$item.height('');
					$item.width('');
					$img.height('');
					imgHeight = hygge.windowHeight;

					// Equalize img heights to smallest
					$img.each(function(){
						imgHeight = Math.min( imgHeight, $(this).innerHeight() );
					});
					if( heightLimit > 0 ){
						imgHeight = Math.min( heightLimit, imgHeight );
					}
					$img.height( Math.round(imgHeight) );

				}

				// Get total width
				$item.each(function(){
					itemsWidth = itemsWidth + $(this).innerWidth();
				});
				$items.width( itemsWidth );

				// Collect positions for animation
				itemPos = [];
				$item.each(function(){
					if( hygge.dir == 'ltr' ){
						itemPos.push( $(this).position().left + 40 );
					}else{
						itemPos.push( $(this).position().left - 100 );
					}
				});

				initCustomScrollbar();
				$(document).trigger('hygge:layout');
			});
		}

		filmstripLayout();



		// Custom Scrollbar
		function initCustomScrollbar(){

			if( hygge.device != 'desktop' ){
				$elInwrap.mCustomScrollbar('destroy');
				return;
			}

			$elInwrap.mCustomScrollbar({
				axis:"x",
				keyboard: false,
				scrollInertia: 500,
				mouseWheel: {
					enable: false
				},
				callbacks: {
					onScroll: function(){
						position = Math.abs(this.mcs.left);
					}
				}
			});

		}



		// Function: Animate Scrolling
		function animate(position) {
			if( typeof position === 'undefined' ){
				return;
			}

			if( hygge.device != 'desktop' ){
				$elInwrap.stop().animate({
					scrollLeft: position
				}, 700 );
			}else{
				$elInwrap.mCustomScrollbar( 'scrollTo', [0, position], { scrollInertia: 700 } );
			}
		}



		// Function: Scroll to next item
		function scrollNext() {

			if( hygge.device != 'desktop' ){
				position = $elInwrap.scrollLeft();
			}

			if( hygge.dir == 'ltr' ){
				// if at the end
				if( position + elWidth > itemsWidth - 10 ){
					position = 0;
				}else{
					// scroll to next item
					for( var i = 0; i < itemPos.length; i++ ){
						if( position < itemPos[i] - 10 ){
							position = itemPos[i];
							break;
						}
					}
				}
			}else{
				// If RTL
				// if at the end
				if( position < 10 || position - itemPos[itemPos.length-1] < 100 ){
					position = itemsWidth - elWidth;
				}else{
					// scroll to next item
					for( var i = 0; i < itemPos.length; i++ ){
						if( itemPos[i] + 10 < position  ){
							position = itemPos[i];
							break;
						}
					}
				}
			}


			animate(position);
		}

		if( intervalVal ){
			// Automatic animation
			startInterval();
			// events
			$(document).off('.stopInterval');
			$(document).on( 'hygge:ajaxPageLoad.stopInterval', stopInterval );
			$el.hover( stopInterval , startInterval );

			// stop auto scrolling on touch event for handheld
			$elInwrap.on( 'touchstart', stopInterval );
		}

		// Function: Stop interval animation
		function stopInterval(){
			clearInterval(interval);
		}

		// Function: Start interval animation
		function startInterval(){
			interval = setInterval( scrollNext, intervalVal);
		}



		$el.addClass('processed');
		$(document).on( 'hygge:fullWidth.filmstripLayout', filmstripLayout );
		$(document).on( 'hygge:resize.filmstripLayout', filmstripLayout );
	});

}





/*------------------------------------------------------------
 * FUNCTION: Hygge Lightbox
 * Use 'active' for place of active slide in array of slides
 *------------------------------------------------------------*/

function hyggeLightbox( images, active ){

	// Add lightbox html to the page
	if( $('.hygge-lightbox').length === 0 ){
		$('body').append('<div class="hygge-lightbox"><div class="hygge-lightbox__mask"></div><div class="hygge-lightbox__close js-close label">'+hyggeLocalize.close+'</div><div class="hygge-loader"></div><div class="hygge-lightbox__info"><h4 class="hygge-lightbox__title"></h4><div class="hygge-lightbox__nav"><a class="hygge-lightbox__arrow hygge-lightbox__arrow--prev js-arrow-prev"><i class="hygge-icon-chevron-left"></i></a><i class="hygge-slash--big"></i><a class="hygge-lightbox__arrow hygge-lightbox__arrow--next js-arrow-next"><i class="hygge-icon-chevron-right"></i></a></div><div class="hygge-lightbox__details label"><div class="hygge-lightbox__count"></div><div class="hygge-lightbox__img-details"><div class="hygge-lightbox__img-details__title"></div><div class="hygge-lightbox__img-details__caption"></div></div></div></div><div class="hygge-lightbox__media"></div></div>');
	}

	// Init Lightbox
	var $el = $('body').addClass('hygge-lightbox--true').find('.hygge-lightbox').fadeIn(300);
	var $media = $el.find('.hygge-lightbox__media');
	var $h = $el.find('.hygge-lightbox__title');
	var $count = $el.find('.hygge-lightbox__count');
	var $imgDetails = $el.find('.hygge-lightbox__img-details');
	var $imgTitle = $el.find('.hygge-lightbox__img-details__title');
	var $imgCaption = $el.find('.hygge-lightbox__img-details__caption');
	var $loader = $el.find('.hygge-loader');
	var locked = false;
	active = parseInt(active);

	// Set common values/info for all images
	$h.html( $('.main-content h1').first().html() );

	// activate clicked slide
	activate();



	// Activate lightbox slide
	function activate(){

		// update info
		$count.html( (active + 1) + '/' + images.length );
		$imgTitle.html( images[active].title );
		$imgCaption.html( images[active].caption.replace( /\n/g, '<br>' ) + '<br>' + images[active].description.replace( /\n/g, '<br>' ) );

		// animate image
		$media.stop().fadeOut(300, function(){
			$media.html('<img src="'+images[active].url+'">');

			$loader.stop().animate( {'opacity': 1}, 500 );

			$media.imagesLoaded( function(){
				$media.fadeIn(300);
				$loader.stop().animate( {'opacity': 0}, 300 );
			});
		});
	}



	// Activate Next Item
	function activateNext() {

		// if next item in array - select it, else jump to start
		if( active < images.length - 1 ){
			active++;
		}else{
			active = 0;
		}

		activate();
	}

	// Activate Next Item
	function activatePrev() {

		// if next item in array - select it, else jump to start
		if( active > 0 ){
			active = active - 1;
		}else{
			active = images.length - 1;
		}

		activate();
	}

	// Close lightbox
	function closeLightbox(){
		$el.fadeOut(300, function(){
			$el.remove();
			$('body').removeClass('hygge-lightbox--true');
		});
	}

	// Lock Events for a limited time
	var lockTimeout;
	function lockEvents(){
		locked = true;
		clearTimeout(lockTimeout);
		lockTimeout = setTimeout( function(){
			locked = false;
		}, 200);
	}

	// Events
	$el.on( 'click', '.js-arrow-next', activateNext );
	$el.on( 'click', '.js-arrow-prev', activatePrev );
	$el.on( 'click', 'img', function(){
		if( !locked ){
			activateNext();
		}
	});

	// Arrow keys
	$(document).keydown(function(e){
		if( e.which == 37 ) {
			activatePrev();
		}
		if( e.which == 39 ) {
			activateNext();
		}
		if( e.which == 27 ) {
			closeLightbox();
		}
	});

	// Touch-Swipe functionality
	$el.swipe( {
		swipeRight: function(){
			activatePrev();
			lockEvents();
		},
		swipeLeft: function(){
			activateNext();
			lockEvents();
		},
		threshold:40,
		excludedElements:''
	});


	$el.find('.js-close').click(closeLightbox);

}





/*------------------------------------------------------------
 * FUNCTION: Gallery Lightbox trigger
 * Currently bundles all lightbox galleries in the article
 *------------------------------------------------------------*/

function galleryLightbox(){

	$('.main-content').each(function(){

		var $el = $(this);
		var $item = $el.find('.gallery--lightbox .gallery-item');

		// gather an array of imgs and their details for lightbox
		var lightboxArray = [];
		var i = 0;

		$item.each( function(){
			$(this).attr('data-item-id', i);

			var imgUrl = $(this).find('.gallery-icon a').attr('href');
			var title = $(this).attr('data-item-title');
			var caption = $(this).attr('data-item-caption');
			var description = $(this).attr('data-item-description');

			lightboxArray.push({
				url: imgUrl,
				title: title,
				caption: caption,
				description: description
			});

			i++;
		});

		$item.on( 'click', '.gallery-icon a', function(e){
			e.preventDefault();
			var activeId = $(this).closest('.gallery-item').attr('data-item-id');

			hyggeLightbox( lightboxArray, activeId );
		});

	});

}





/*------------------------------------------------------------
 * FUNCTION: Loop Masonry
 * NOTE: keep masonry function after other layout-changers
 *------------------------------------------------------------*/

function loopMasonry(){
	// if Mobile on init, do nothing
	// if mobile on resize, keep masonry and continue
	if( hygge.device == 'mobile' ){
		if( !$('.js-loop-is-masonry').hasClass('loop-is-masonry--init-processed') ){
			return;
		}
	}



	$('.js-loop-is-masonry').each(function(){

		var el = this;
		var $el = $(this).addClass('loop-is-masonry');
		var $post = $el.find('.post, .page').not('.masonry-processed');

		// Define situation
		var sit = 'init';
		if( $el.hasClass('loop-is-masonry--init-processed') ){
			sit = $post.length === 0 ? 'done' : 'appended';
		}



		var masonry = new Masonry( el, {
			columnWidth: '.post:not(.hygge-sticky), .page',
			itemSelector: '.post, .page',
			transitionDuration: 0,
			// isInitLayout: false,
			isResizeBound: false
		});



		masonry.on( 'layoutComplete', function() {
			hyggeLayout();
		});


		if( sit == 'done' ){
			masonry.layout();
			return;
		}



		if( sit == 'appended' ){
			var elems = [];
			$post.each(function(){
				elems.push( $(this) );
			});

			masonry.appended( elems );

			$post.each(function(){
				var $this = $(this);
				$this.imagesLoaded(function(){
					masonry.layout();
					$this.addClass('masonry-processed');
				});
			});

			$el.imagesLoaded(function(){

				setTimeout(function(){
					masonry.layout();
				},150);

				setTimeout(function(){
					masonry.layout();
				},700);

				$el.next('.pagination').removeClass('pagination--loading');
			});

			return;
		}



		$post.each(function(){
			var $this = $(this);
			$this.imagesLoaded(function(){
				masonry.layout();
				$this.addClass('masonry-processed');
			});
		});

		$el.imagesLoaded(function(){
			// masonry.layout();
			// make loop visible after the initial masonry layout
			setTimeout(function(){
				masonry.layout();
			},150);

			setTimeout(function(){
				masonry.layout();
			},700);

			$el.addClass('loop-is-masonry--init-processed');
		});

	});
}





/*------------------------------------------------------------
 * FUNCTION: Share Block
 *------------------------------------------------------------*/

function sharer(){

	$('body').on('click', '.js-sharer', function(e){
		e.preventDefault();
		var link = $(this).attr('href');
		var windowFeatures = "height=320, width=640, status=no,resizable=yes,toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
		window.open(link,'Sharer', windowFeatures);
	});

}





/*------------------------------------------------------------
 * FUNCTION: Load More of WP Comments
 *------------------------------------------------------------*/

function wpCommentsLoadMore(){

	$('html').on('click','.comment__pager a', function(e){
		e.preventDefault();
		var $el     = $('.comment__list');
		var $this   = $(this);
		var href    = $this.attr('href');

		$this.addClass('button--disabled');

		$.get(href, function(data, status, xhr){
			// Success!

			// get, append and animate comments, trigger ajax complete event
			var comments = $(data).find('.comment__list .comments');
			comments.find('.comment').addClass('hygge-animate-appearance');
			$el.find('.comments').append( comments.html() );

			$el.find('.hygge-animate-appearance').addClass('hygge-animate-appearance--start');

			// switch button with new
			var btn = $(data).find('.comment__list .comment__pager');
			$this.parent().html( btn.html() );

		})
		.fail(function(){
			$this.text('Error');
			$this.removeClass('button--loading');
		});
	});

}





/*------------------------------------------------------------
 * FUNCTION: Validate comments on front-end before posting
 *------------------------------------------------------------*/

function hyggeValidateComment(){

	var $form = $('#commentform');
	var $btn = $form.find('#submit');
	var url = $form.attr('action');

	// on submit click
	$btn.click(function(e){
		// validate form
		var formValid = true;
		$form.find('[aria-required="true"]').each(function(){
			if( !$.trim( $(this).val() ) ){
				formValid = false;
				$(this).addClass('error');
			}else{
				$(this).removeClass('error');
			}

			// e-mail check
			if( $(this).attr('name') == 'email' ){
				if( !validateEmail( $(this).val() ) ){
					formValid = false;
					$(this).addClass('error');
				}else{
					$(this).removeClass('error');
				}
			}
		});

		if( !formValid ) {
			e.preventDefault();
		}
	});
}





/*------------------------------------------------------------
 * FUNCTION: Animate Appearance
 *------------------------------------------------------------*/

function animateAppearance(){

	var $el = $('.hygge-animate-appearance').not('.queued');
	if ( $el.length === 0 ) {
		return;
	}

	var offset = 100;
	if( hygge.adminBar ){
		offset -= 80;
	}

	function animateIfInViewport(){

		if( hygge.device == 'mobile' ){
			return;
		}

		var scrollTop = window.pageYOffset;

		$el.each(function(){
			var $this = $(this);

			requestAnimationFrame(function() {
				if ( $this.hasClass('queued') ){
					return;
				}

				// Elements are loading instantly, without waiting for images
				// $this.imagesLoaded(function(){
					if( $this.offset().top + offset < hygge.windowHeight + scrollTop ){
						$this.addClass('queued');
						// add a small random variation for animation
						// var time = getRandomNumber(1, 500);
						// setTimeout(function(){
							$this.addClass('hygge-animate-appearance--start');
						// }, time);
					}
				// });
			});
		});
	}

	animateIfInViewport();
	$(document).off('.animateIfInViewport');
	$(document).on('hygge:scroll.animateIfInViewport', animateIfInViewport);
	$(document).on('hygge:resize.animateIfInViewport', animateIfInViewport);
}





/*------------------------------------------------------------
 * FUNCTION: Wrap Select for styling
 *------------------------------------------------------------*/

function hyggeWrapSelect(){
	$('select').not('.hygge-skip-wrap-select, .hygge-js-processed, .select2-hidden-accessible, [style*="display: none"], [style*="display:none"], .woocommerce-page select[id="rating"]').each(function(){
		var $el = $(this);

		$el.wrap('<div class="hygge-select-wrap"></div>');
		$el.addClass('hygge-js-processed');
	});

}





/*------------------------------------------------------------
 * Modular loading-appearing
 *------------------------------------------------------------*/

function hyggeModularLoading(){
	var $el = $('.element-modular-loading');

	$el.not('.element-modular-loading--start').each(function(){
		var $this = $(this);

		$this.imagesLoaded( {background: true}, function(){
			$this.addClass('element-modular-loading--start');
		});
	});
}





/*------------------------------------------------------------
 * Main Navigation
 *------------------------------------------------------------*/

function hyggeNav(){

	var $mobileNav = $('.hygge-nav--mobile');

	function closeMobileNav(){
		$('body').removeClass('hygge-nav-open');
	}

	$(document).on( 'hygge:ajaxPageLoad', closeMobileNav);
	$('html').on( 'click','.js-hygge-nav-close', closeMobileNav );

	$('html').on( 'click','.js-hygge-nav-toggle', function(e){
		e.preventDefault();
		$('body').toggleClass('hygge-nav-open');
	});

	$mobileNav.swipe( {
		swipeLeft: closeMobileNav,
		threshold: 40,
		excludedElements: '.search-form'
	});

	// Support current-menu-item state in ajax
	if( hygge.ajaxEnabled ){
		$('html').on( 'click','.hygge-nav--classic a:not([href="#"])', function(e){
			$('.hygge-nav--classic .current-menu-item').removeClass('current-menu-item');
			$('.hygge-nav--classic .current_page_item').removeClass('current_page_item');
			$('.hygge-nav--classic .current-menu-ancestor').removeClass('current-menu-ancestor');
			$('.hygge-nav--classic .current-menu-parent').removeClass('current-menu-parent');

			$(e.target).closest('.menu-item').addClass('current-menu-item');

			if( $(e.target).parents('.sub-menu').length > 0 ){
				$(e.target).closest('.sub-menu').closest('.menu-item').addClass('current-menu-ancestor');
			}
		});
	}
}





/*------------------------------------------------------------
 * Smooth scrolling to # on same page
 *------------------------------------------------------------*/

function hyggeSmoothAnchorScroll( href ){

	// if direct offset passed
	if( parseInt(href) === href && href !== 0 ){
		animate(href);
		return;
	}

	// if href passed
	if( href && typeof href !== 'function' ){
		getOffset(href);
		return;
	}

	// On document ready check for anchors in href
	if( hygge.ajaxEnabled ){
		getOffset( location.href );
	}



	// Event: Href Click
	$('html').on( 'click.hyggeSmoothAnchorScroll','a', function(e) {

		var href = $(this).attr('href');

		if( !href ){
			return;
		}

		var path = href.split('#')[0];
		var hash = href.split('#')[1];
		var pathLoc = location.href.split('#')[0];
		var offset = false;

		// if no hash, exit immediately
		// also if location is not the same (aprove no location for direct #hash)
		if( typeof hash === 'undefined' || ( path && path != pathLoc ) ){
			return;
		}

		// Skip scrolling on WooCommerce Tabs
		if( $(e.target).parents('.wc-tabs').length !== 0 ){
			return;
		}

		getOffset( href, e );
	});



	// Function: Get offset
	function getOffset(href, e){

		var hash = href.split('#')[1];

		if( typeof hash === 'undefined' ){
			return;
		}

		// if dummy hash, prevent default snap to top and exit
		// split('#') removes the '#', but has empty string
		if( hash === '' ){
			if( typeof e !== 'undefined' ){
				e.preventDefault();
			}
			return;
		}

		var offset = false;

		if( hash == 'top' ){
			offset = 0;
		}else{
			if( $('#'+hash).length ){
				offset = $('#'+hash).offset().top - 50;
			}

			if( hygge.adminBar ){ offset -= 32;}
		}

		if( offset !== false ){
			animate(offset);

			if( e ){
				history.pushState({page: href}, '', href);
				e.preventDefault();
			}
		}
	}



	// Function: Animate Scroll
	function animate(offset) {

		// set speed depending on the amount of distance
		// speed must be in 600-1600 ms range
		var dist = Math.abs( window.pageYOffset - offset );
		var speed = Math.round( Math.max( 600, Math.min( 1600, dist/1.7 ) ) );

		$('html,body').stop().animate({
			scrollTop: offset
		}, speed, 'easeInOutCubic');
	}

}





/*------------------------------------------------------------
 * Full Width section
 * Check and set section to exact full width,
 * since vw/vh don't exclude scrollbars
 *------------------------------------------------------------*/

function hyggeFullWidth(){

	var $el = $('.hygge-layout-main .hygge-fullwidth');
	var width;
	var fullWidth;


	if( $el.length === 0 || hygge.device == 'mobile' ){
		return;
	}

	function update(){

		if( hygge.device == 'mobile' || hygge.device == 'tabletV' ){
			$el.css({
				'width': '',
				'margin-left': ''
			});
		}else{
			fullWidth = $('.hygge-layout-main').width();

			$el.each(function(){
				var $this = $(this);
				width = $this.innerWidth();

				if( width != fullWidth ){
					$this.css('width', fullWidth);
				}

				var offset = $this.offset().left;

				if( offset !== 0 ){
					$this.css( 'margin-left', 0 );
					offset = $this.offset().left;
					$this.css( 'margin-left', offset * -1 );
				}
			});
		}


		$(document).trigger( 'hygge:layout');
		$(document).trigger( 'hygge:fullWidth');

	}

	update();
	$(document).on( 'hygge:resize', update );

}





/*------------------------------------------------------------
 * Sticky Sideblock
 *------------------------------------------------------------*/

function hyggeStickySideblock(){

	if( hygge.device == 'mobile' || hygge.device == 'tabletV' ){
		return;
	}

	$(document).off('.sideblocksUpdate');
	$(document).off('.sideblocksCalculate');

	var $selector = $('.post__content .hygge-sideblock:visible, .post__content hr.sideblock-clear, .post__content .hygge-fullwidth, .post__content .hygge-contentwidth')
	.not('.hygge-sideblock .hygge-sideblock, .hygge-sideblock--non-sticky');
	var contentHeight = 0;
	var items = [];
	var scrollTop = 0;
	var $wrapper;

	if( $selector.length === 0 ){
		return;
	}

	var offset = 30;
	if( hygge.stickyHeader ){ offset += 50;}
	if( hygge.adminBar ){ offset += 32;}



	// Full List: Calculate
	function calculate(){

		// If first time, collect the items
		if( items.length === 0 ){

			$selector.each(function(){

				// filter the sideblocks
				var type = 'clear';
				var $placeholder = false;
				if( $(this).is('.hygge-sideblock') ){
					type = 'sideblock';

					// insert placeholder, it will be shown with CSS
					// check for already processed (ajax load more happened)
					if( !$(this).is('.processed') ){
						$placeholder = $('<div class="hygge-sideblock-placeholder"></div>');
						$(this).after($placeholder);
						$(this).addClass('processed');
					}
				}

				var item = {
					el: this,
					type: type,
					placeholder: $placeholder
				};
				items.push(item);
			});
		}


		// reset all sideblocks
		kill();

		// Calculate the numbers and breakpoints
		for( var i = 0; i < items.length; i++ ){
			var $thisEl = $(items[i].el);

			var thisTop = $thisEl.offset().top;
			var thisHeight = $thisEl.outerHeight();
			var thisBottom = thisTop + thisHeight;

			var thisLimit;
			// if this item is not the last
			if( i+1 < items.length ){
				thisLimit = $(items[i+1].el).offset().top;
			}else{
				// if last item, limit it at wrapper's end
				$wrapper = $thisEl.closest('.post__content');
				thisLimit = $wrapper.offset().top + $wrapper.innerHeight();
			}

			items[i].top = Math.round(thisTop);
			items[i].height = Math.round(thisHeight);
			items[i].bottom = Math.round(thisBottom);
			items[i].limit = Math.round(thisLimit - 50);

			// set placeholder's height
			$(items[i].placeholder).css({ height : items[i].height });
		}


		update();
	}

	calculate();
	$(document).on('hygge:layoutFinished.sideblocksCalculate', calculate);



	// Update on Scroll
	function update(){
		scrollTop = window.pageYOffset;
		for( var i = 0; i < items.length; i++ ){
			// only sideblocks are sticked
			if( items[i].type != 'sideblock' ){
				continue;
			}

			// if sideblock has limit too high, don't even try to stick it
			// add a 5px threshold, cause fuck it, that's why
			// just kidding - make sure to not fix the sideblock
			// with immediate sideblock follow up
			if( items[i].limit < items[i].bottom + 5 ){
				continue;
			}

			var $thisEl = $(items[i].el);

			// if sideblock is scrolled over, just ignore it
			if( scrollTop > items[i].limit ){
				if( $thisEl.is('.hygge-sideblock--sticked') ){
					kill($thisEl);
				}
				continue;
			}

			// stick element that is scrolled to but not over it's limit
			if(
				scrollTop >= items[i].top - offset &&
				scrollTop < items[i].limit
			){

				if( !$thisEl.is('.hygge-sideblock--sticked') ){
					$thisEl.addClass('hygge-sideblock--sticked');
				}

				// tween the element position
				// when it should be pushed out
				var translate = 0;
				if( scrollTop > items[i].limit - items[i].height - offset ){
					translate = items[i].limit - scrollTop - offset - items[i].height;
				}

				$thisEl.css({
					'transform' : 'translateY('+ translate + 'px)',
					'-webkit-transform' : 'translateY('+ translate +'px)'
				});

			}else{
				// leave items that are not scrolled to the top
				if( $thisEl.is('.hygge-sideblock--sticked') ){
					kill($thisEl);
				}
			}
		}
	}

	$(document).on('hygge:scroll.sideblocksUpdate', update);



	// Clear alterations on elements
	function kill( $el, what ){

		if( $el ){
			if( what != 'style' ){
				$el.removeClass('hygge-sideblock--sticked');
			}

			$el.css({
				'transform' : '',
				'-webkit-transform' : ''
			});

		}else{

			// kill all
			for( var i = 0; i < items.length; i++ ){
				if( items[i].type == 'sideblock' ){
					$(items[i].el).removeClass('hygge-sideblock--sticked')
						.css({
							'transform' : '',
							'-webkit-transform' : ''
						});
				}
			}
		}
	}
}





/*------------------------------------------------------------
 * FUNCTION: Hygge Layout Changed
 * Keep at the most bottom of script file
 *------------------------------------------------------------*/

hygge.mainContentHeight = 0;
var hyggeLayoutTimeout;
var hyggeLayoutTime = 0;
var hyggeLayoutNow = 0;
var hyggeLayoutTimeDistance = 0;
function hyggeLayout( force ){

	if( hygge.device == 'mobile' || hygge.device == 'tabletV' ){
		return;
	}

	if( force === true ){
		hyggeLayoutFire(true);
		return;
	}

	// Limit the triggers in time
	hyggeLayoutNow = new Date().getTime();
	hyggeLayoutTimeDistance = hyggeLayoutNow - hyggeLayoutTime;
	if ( hyggeLayoutTimeDistance < 100 ) { return; }
	hyggeLayoutTime = hyggeLayoutNow;

	function hyggeLayoutFire(force){

		// if height hasn't changed, skip it
		if(
			hygge.contentWrapperHeight === $('.content-wrapper').height() &&
			force !== true
		){
			return;
		}

		// Update the content height with the latest value
		hygge.contentWrapperHeight = $('.content-wrapper').height();
		$(document).trigger('hygge:layoutFinished');
	}

	hyggeLayoutFire();
	$('.content-wrapper').imagesLoaded( hyggeLayoutFire );
	$(window).load( hyggeLayoutFire );

}





/*------------------------------------------------------------
 * Header Search
 *------------------------------------------------------------*/

function hyggeHeaderSearch(){
	var $btn = $('.header__search__toggle');

	$btn.each(function(){
		var $thisBtn = $(this);
		var $el = $thisBtn.nextAll('.header__search');
		var $input = $el.find('input[name="s"]');

		$thisBtn.click(function(){
			$el.addClass('on');
			$thisBtn.addClass('on');

			// Wait for element to become visible or focus won't work
			setTimeout(function(){
				$input.focus();
			},300);
		});

		$input.blur(function(){
			$el.removeClass('on');
			$thisBtn.removeClass('on');
		});

		// Esc
		$(document).keydown(function(e){
			if( e.which == 27 ) {
				$el.removeClass('on');
				$thisBtn.removeClass('on');
			}
		});

	});
}





/*------------------------------------------------------------
 * Notification Bar
 *------------------------------------------------------------*/

function hyggeNotificationBar(){
	if( !hygge.notificationBar ){
		return;
	}

	var $el = $('.hygge-notif');
	var id = 'hygge-notif-hide-';
	id += $el.find('.hygge-notif__text').text();
	id += $el.find('.hygge-notif__btn').attr('href');

	// check cache
	var hidden = localStorage.getItem(id) ? true : false;

	if( !hidden || !hygge.notificationBarHideable ){
		show();
	}

	function hide(){
		$('body').attr('hygge-notif', '');
		// save to cache
		if( !hidden ){
			localStorage.setItem(id, true);
		}
	}

	function show(){
		$('body').attr('hygge-notif', 'true');
	}

	$el.find('.hygge-notif__hide').click(function(){
		hide();
	});
}





/*------------------------------------------------------------
 * Double Blog Widget
 *------------------------------------------------------------*/

function hyggeDoubleBlogWidget(){
	var $el = $('.widget_hygge_blog_widget');

	$el.each(function(){
		$el = $(this);
		var $title = $el.find('.widget__title');
		var $item = $el.find('.wrap > .item');

		if( $title.find('span[data-item="2"]').length > 0 ) {
			$title.click(function(){
				$title.find('>span').toggleClass('hygge--on');
				toggleItem();
			});
		}

		function toggleItem(){
			var $current = $item.filter('.hygge--on');

			$current.fadeOut(250, function(){
				$current.removeClass('hygge--on');
				var $new = $item.filter('[data-item="'+$title.find('>span.hygge--on').attr('data-item')+'"]');
				$new.fadeIn(250, function(){
					$new.addClass('hygge--on');
				});
			});
		}

	});
}





/*------------------------------------------------------------
 * FUNCTION: Hygge Carousel
 *------------------------------------------------------------*/

function hyggeCarousel(){

	// assume not featured
	document.body.className = document.body.className.replace('hygge-featured-visible--true', 'hygge-featured-visible--false');

	$('.js-hygge-carousel:not(.processed)').each(function(e){

		var $el         = $(this).addClass('js-started');
		var $elInwrap   = $el.find('.hygge-carousel__inwrap');
		var $items      = $el.find('.hygge-carousel__items');
		var $itemsGroup = $el.find('.hygge-carousel__items-group');
		var $item       = $el.find('.hygge-carousel__item');
		var $first      = $item.first();
		var $active     = $first;
		var $activeGroup= $itemsGroup;
		var interval;
		var intervalVal = $el.attr('data-interval');
		var itemWidth;
		var itemsWidth  = 0;
		var elWidth;
		var position    = 0;
		var itemsHeight = 0;
		var availHeight = hygge.windowHeight;
		var $arrowPrev  = $el.find('.js-hygge-ui-arrow--prev');
		var $arrowNext  = $el.find('.js-hygge-ui-arrow--next');
		var $pager      = $el.find('.js-hygge-ui-pager-item');
		var $pagerMarker= $el.find('.hygge-ui-pager__active-marker');
		var locked      = false;

		var textElements = '.post__text';

		// Add body class if featured is used
		if( $el.hasClass('featured') ){
			document.body.className = document.body.className.replace('hygge-featured-visible--false', 'hygge-featured-visible--true');
		}

		// Add IDs to all items
		$item.each(function(i){
			$(this).attr( 'data-id', i );
		});

		// Duplicate all items before and after originals
		if( $item.length > 1 ){
			$itemsGroup.clone().prependTo( $items );
			$itemsGroup.clone().appendTo( $items );
		}

		// Update $itemsGroup var with clonned items
		// And give theme IDs
		$itemsGroup = $el.find('.hygge-carousel__items-group');
		$itemsGroup.each(function(i){
			$(this).attr( 'data-id', 'group-' + i );
		});

		// Update $item var with clonned items
		$item = $items.find('.hygge-carousel__item');



		// Calculate & lay-out elements
		function carouselLayout(){

			elWidth = Math.floor( $el.innerWidth() );
			$item.css('max-width', elWidth);

			// Get available height for shorter screens
			// window height - header - pagination
			// availHeight = window.innerHeight - $el.offset().top - 50;

			function calculateSizes(){

				// Width
				$items.width('');
				itemsWidth = 0;

				$item.each(function(){
					itemsWidth = itemsWidth + $(this).outerWidth(true);
				});

				$items.width( itemsWidth );



				// Height
				// Get largest image or content to make sure that everything is displayed
				// Keep image-height restrained, unless content demands higher space
				itemsHeight = 0;
				$item.each( function(){
					itemsHeight = Math.max(
						itemsHeight,
						$(this).find('.post__text').outerHeight(true),
						Math.min( $(this).innerHeight(), 550 )
					);
				});

				// Set all elements to even height value
				// to prevent half-pixel font anti-alias
				hyggeEvenHeight( $(textElements, this) );

				itemsHeight = Math.min(itemsHeight, 700);

				$item.height( itemsHeight );
				$items.height( itemsHeight );



				// Reset scroll position
				scrollTo($first, false);

			}

			// calculateSizes();

			$el.imagesLoaded(function(){

				calculateSizes();

				$el.addClass('processed');
				// force hygge layout
				hyggeLayout( true );
			});

			$(document).one( 'hygge:resize', carouselLayout );
		}

		carouselLayout();



		// Function: Scroll To - 'next', 'prev', or $item
		function scrollTo( $target, animate ) {

			if( $item.length == 1 ){
				return;
			}

			if( typeof $target === 'undefined' ){
				return;
			}

			if( typeof animate === 'undefined' ){
				animate = true;
			}

			// Skip if same item
			// Don't skip on init activation
			// Don't skip on prev/next
			if( animate && $target.length == 1 ){
				if( $active && $target.attr('data-id') == $active.attr('data-id') ){
					return;
				}
			}


			// Find the item for animation if prev/next used
			if( $target == 'next' ){

				// Select next in exists in this group
				// or go to next group and select first one
				if( $active.next('.hygge-carousel__item').length == 1 ){
					$target = $active.next('.hygge-carousel__item');
				}else{
					$target = $activeGroup
								.next('.hygge-carousel__items-group')
								.find('.hygge-carousel__item')
								.first();
				}
			}

			if( $target == 'prev' ){

				// Select prev in exists in this group
				// or go to prev group and select last one
				if( $active.prev('.hygge-carousel__item').length == 1 ){
					$target = $active.prev('.hygge-carousel__item');
				}else{
					$target = $activeGroup
								.prev('.hygge-carousel__items-group')
								.find('.hygge-carousel__item')
								.last();
				}
			}

			updatePosition( $target );

			if( animate ){
				$elInwrap.stop().animate({
					scrollLeft: position
				}, 600, 'easeInOutCubic');
			}else{
				// just jump-scroll to the item
				$elInwrap.scrollLeft( position );
			}

			// Set active, prev and next classes
			$item.removeClass('hygge-carousel__item--active hygge-carousel__item--next hygge-carousel__item--prev');
			$target.addClass('hygge-carousel__item--active');

			if( $target.next('.hygge-carousel__item').length == 1 ){
				$target.next('.hygge-carousel__item')
					.addClass('hygge-carousel__item--next');
			 }else{
				$activeGroup.next('.hygge-carousel__items-group').find('.hygge-carousel__item').first()
					.addClass('hygge-carousel__item--next');
			}

			if( $target.prev('.hygge-carousel__item').length == 1 ){
				$target.prev('.hygge-carousel__item')
					.addClass('hygge-carousel__item--prev');
			}else{
				$activeGroup.prev('.hygge-carousel__items-group').find('.hygge-carousel__item').last()
					.addClass('hygge-carousel__item--prev');
			}


			updateInfo( $target );
		}



		// Update Position
		function updatePosition( $target ){

			// Check if $target is not in $activeGroup
			if( !$target.parent().is( $activeGroup ) ){

				// Change $activeGroup
				$activeGroup = $target.parent();

				// Update $itemsGroup var with latest order
				$itemsGroup = $el.find('.hygge-carousel__items-group');

				// Check for the location of $activeGroup in current order
				// and make it middle
				if( $activeGroup.next('.hygge-carousel__items-group').length == 1 ){
					$items.prepend( $itemsGroup[2] );
				}else{
					$items.append( $itemsGroup[0] );
				}

				// Jump-scroll to $active item instantly
				// To compensate for the change when groups changed places
				position = $active.position().left - (elWidth/2) + ($active.innerWidth()/2);
				$elInwrap.scrollLeft( position );
			}



			position = $target.position().left - (elWidth/2) + ($target.innerWidth()/2);
		}



		// Update info/pagers
		function updateInfo( $target ){

			// update active item
			$active = $target;

			// update pager
			if( $pager.length ){
				var itemId = $target.attr('data-id');
				$pager.removeClass('active');
				var pagerLeft = $pager.filter('[data-id="'+itemId+'"]').addClass('active').position().left;
				$pagerMarker.stop().animate({
					left: pagerLeft,
					opacity: 1
				}, 600, 'easeInOutCubic');
			}
		}



		// Automatic animation on interval
		if( intervalVal && intervalVal != '0'){
			startInterval();

			// events
			$(document).off('.stopInterval');
			$(document).on( 'hygge:ajaxPageLoad.stopInterval', stopInterval );
			$el.hover( stopInterval , startInterval );

			// stop auto scrolling on touch event for handheld
			$elInwrap.on( 'touchstart', stopInterval );
		}

		// Function: Stop interval animation
		function stopInterval(){
			clearInterval(interval);
		}

		// Function: Start interval animation
		function startInterval(){
			clearInterval(interval);

			interval = setInterval( function(){
				scrollTo('next');
			}, intervalVal);
		}

		// Lock Events for a limited time
		var lockTimeout;
		function lockEvents(){
			locked = true;
			clearTimeout(lockTimeout);
			lockTimeout = setTimeout( function(){
				locked = false;
			}, 200);
		}

		// Pager functionality
		$pager.click(function(e){
			e.preventDefault();
			var pagerId = $(this).attr('data-id');

			scrollTo( $activeGroup.find('[data-id="'+pagerId+'"]') );
		});

		// Touch-Swipe functionality
		$el.swipe( {
			swipeRight: function(){
				scrollTo('prev');
				lockEvents();
			},
			swipeLeft: function(){
				scrollTo('next');
				lockEvents();
			},
			threshold:40,
			excludedElements:''
		});

		// Arrow keys
		$(document).keydown(function(e){
			if( e.which == 37 ) {
				scrollTo('prev');
				startInterval();
			}
			if( e.which == 39 ) {
				scrollTo('next');
				startInterval();
			}
		});

		// Click on item scrolls to it
		$item.click(function(){
			if( !locked ){
				scrollTo( $(this) );
				stopInterval();
			}
		});



	});

}





 /*------------------------------------------------------------
 * Sticky Header
 *------------------------------------------------------------*/

function hyggeStickyHeader(){

	if( !hygge.stickyHeader || hygge.device == 'mobile' || hygge.device == 'tabletV' ){
		return;
	}

	var $el = $('#header');
	var $placeholder = $('<div class="header-placeholder"></div>');
	var scrollTop = 0;
	var elOffset = $el.offset().top;
	var elWidth = $el.outerWidth();
	var elHeight = $el.outerHeight();
	var threshold = elOffset + elHeight + 500; // scroll height when menu actually appears
	var translateValue = 0;
	// cache positions while continuous scroll
	var scrollTopHistoryDown = false;
	var scrollTopHistoryUp = false;

	if( $el.length === 0 ){
		return;
	}

	var offset = 0;
	if( hygge.adminBar ){ offset = 32;}

	// Get the original sizes
	$placeholder.height(elHeight);
	$placeholder.width(elWidth);

	// Calculate
	function calculate(){

		// insert placeholder, it will be shown with CSS
		// check for already processed (ajax load more happened)
		if( !$el.is('.processed') ){
			$el.width(elWidth);
			$el.after( $placeholder );
			$el.addClass('processed');
		}else{
			$el.removeClass('processed');
			$el.css({ width: '', height: '', position: 'relative' });
			elWidth = $el.outerWidth();
			elHeight = $el.outerHeight();
			$el.css({ position: '' });
			$el.width(elWidth);
			$placeholder.height(elHeight);
			$placeholder.width(elWidth);
			$el.addClass('processed');
		}

		update();
	}

	calculate();
	$(document).on( 'hygge:resize', calculate );



	// Update on Scroll
	function update(){
		scrollTop = window.pageYOffset;

		// Scrolling down
		if( hygge.scrollDirection == 'down' ){

			// Scrolling DOWN has action only for already sticked menu by scrolling UP
			if( $el.is('.header--sticked') ){

				// reset scroll UP information so that it knows to start fresh
				scrollTopHistoryUp = false;

				// If first tick of scrolling down prepare calculations
				// else just skip to translation based on previous calculation
				if( !scrollTopHistoryDown ){
					scrollTopHistoryDown = scrollTop;

					// If el is in mid-translation, continue from it
					// by offsetting the point from which calculation starts
					if( translateValue < 0 && translateValue > elHeight * -1 ){
						scrollTopHistoryDown = scrollTopHistoryDown + translateValue;
					}
				}

				translateValue = 0 - Math.min( elHeight, scrollTop - scrollTopHistoryDown );

				if( translateValue == elHeight * -1 ){
					kill();
				}else{
					translate( translateValue );
				}
			}
		}

		// Scrolling back up - show sticky header
		if( hygge.scrollDirection == 'up' ){
			// reset scroll DOWN information so that it knows to start fresh
			scrollTopHistoryDown = false;

			// If element is in position to be (un)sticked, do it
			// Especially important for scrolling back to top and unsticking right in position
			if( scrollTop > elOffset - offset ){

				// If first tick of scrolling up prepare calculations
				// else just skip to translation based on previous calculation
				if( !scrollTopHistoryUp ){
					scrollTopHistoryUp = scrollTop;

					// If el is in mid-translation, continue from it
					// by offsetting the point from which calculation starts
					if( translateValue < 0 && translateValue > elHeight * -1 ){
						scrollTopHistoryUp = scrollTopHistoryUp + elHeight + translateValue;
					}
				}

				if( $el.is('.header--sticked') ){
					translateValue = Math.min( elHeight, scrollTopHistoryUp - scrollTop ) - elHeight;
					translate( translateValue );
				}else{
					if( scrollTop > threshold ){
						$el.addClass('header--sticked');
						// alter the (sticked) height size for the background and calcs
						elHeight = $el.outerHeight();
						translateValue = Math.min( elHeight, scrollTopHistoryUp - scrollTop ) - elHeight;
						translate( translateValue );
					}
				}
			}else{
				kill();
			}
		}
	}

	$(document).on('hygge:scroll', update);



	// Translate elements
	function translate( value ){
		value = value ? value : 0;
		// Ensure that translate value is not higher then the 100% el
		if( value < elHeight * -1 ){
			value = elHeight * -1;
		}

		$el.css({
			'transform' : 'translateY('+ value + 'px)',
			'-webkit-transform' : 'translateY('+ value +'px)'
		});
	}



	// Clear alterations on elements
	function kill(){
		scrollTopHistoryDown = false;
		scrollTopHistoryUp = false;
		$el.removeClass('header--sticked');
		$el.css({
			'transform' : '',
			'-webkit-transform' : ''
		});
	}
}





 /*------------------------------------------------------------
 * Sticky Sidebar
 *------------------------------------------------------------*/

function hyggeStickySidebar(){
	var offset = 0;
	if( hygge.adminBar ){
		offset += 32;
	}

	$('.hygge-layout-wrapper').imagesLoaded(function(){
		if (hygge.device == 'desktop') {
			$('.sidebar__content').stick_in_parent({ offset_top: offset });
		} else {
			$('.sidebar__content').trigger('sticky_kit:detach');
		}
	});
}





 /*------------------------------------------------------------
 * Sticky Post Share
 *------------------------------------------------------------*/

function hyggeStickyShare(){
	var offset = 40;
	if( hygge.stickyHeader ){ offset += 40; }
	if( hygge.adminBar ){ offset += 32; }

	if( hygge.device == 'mobile' ){
		offset = 20;
	}

	var $share = $('.post__share--sticky');

	$share.stick_in_parent({
		offset_top: offset
	}).on("sticky_kit:bottom", function(e) {
		$(this).addClass('is_bottom');
	}).on("sticky_kit:unbottom", function(e) {
		$(this).removeClass('is_bottom');
	});
}





 /*------------------------------------------------------------
 * Hygge Form Label
 *------------------------------------------------------------*/

function hyggeFormLabel(){
	$('.hygge-form-label input, .hygge-form-label textarea').each(function(){
		if( $(this).val() != '' ){
			$(this).closest('.hygge-form-label').addClass('focused');
		}
	});

	$('body').on( 'focus', '.hygge-form-label input, .hygge-form-label textarea', function(){
		$(this).closest('.hygge-form-label').addClass('focused');
	});
	$('body').on( 'blur', '.hygge-form-label input, .hygge-form-label textarea', function(){
		if( $(this).val() == '' ){
			$(this).closest('.hygge-form-label').removeClass('focused');
		}
	});
}





 /*------------------------------------------------------------
 * Hygge Form Label
 * Set all elements to even height value
 * to prevent half-pixel font anti-alias
 *
 * Height value is applied also in the case of even number
 * because jquery doesn't read the sub-pixel values
 * that chrome actually uses.
 *------------------------------------------------------------*/

function hyggeEvenHeight($el){

	if( !$el || $el.length == 0 ){
		$el = $('.js-hygge-even-height');
	}

	$el.each(function(){
		var $this = $(this);
		$this.imagesLoaded( function() {
			var height = $this.height('').height();

			if( hygge.device != 'desktop' ){
				return;
			}

			if( height % 2 === 1 ) {
				$this.height( height + 1 );
			}else{
				$this.height( height );
			}
		});
	});

}




/*------------------------------------------------------------
* Hygge Preload Images
* Preloads loader images after all regular images are loaded
*------------------------------------------------------------*/

function hyggePreloadImages(){
	// keep history of already loaded images
	hygge.preloadedImages = hygge.preloadedImages || [];

	$('body').imagesLoaded( function(){
		$('body').find('a[data-loader-img]').each(function(){
			var img = $(this).attr('data-loader-img');

			// check if img exists and if it is not already loaded
			if( img && !hygge.preloadedImages.includes(img) ){
				var imageObject = new Image();
				imageObject.src = img;
				hygge.preloadedImages.push(img);
			}
		});
	});
}





/*------------------------------------------------------------
* Hygge Footer Layout
* Move footer into the main content by negative-offsetting sidebar
*------------------------------------------------------------*/

function hyggeFooterLayout(){
	var $footer = $('.footer.hygge-layout-wrapper');
	var $sidebar = $('#sidebar.hygge-layout-sidebar');

	function calc(){
		var footerHeight = hygge.device == 'desktop' || hygge.device == 'tabletH' ? $footer.innerHeight() * -1 : 0;
		$sidebar.css( 'margin-bottom', footerHeight );
	}

	calc();
	$footer.imagesLoaded( calc );
}





/*------------------------------------------------------------
* Hygge Custom Input Number
*------------------------------------------------------------*/

function hyggeCustomInputNumber(){

	$('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');

	$('.quantity').each(function() {
		var spinner = $(this),
			input = spinner.find('input[type="number"]'),
			btnUp = spinner.find('.quantity-up'),
			btnDown = spinner.find('.quantity-down'),
			min = input.attr('min') || 0,
			max = input.attr('max') || 999999;

		btnUp.click(function() {
			var oldValue = parseFloat(input.val());
			if (oldValue >= max) {
				var newVal = oldValue;
			} else {
				var newVal = oldValue + 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("change");
		});

		btnDown.click(function() {
			var oldValue = parseFloat(input.val());
			if (oldValue <= min) {
				var newVal = oldValue;
			} else {
				var newVal = oldValue - 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("change");
		});
	});
}





/*------------------------------------------------------------
 * Dropdown
 *------------------------------------------------------------*/

function hyggeDropdown(){
	var $el = $('.hygge-dropdown');

	$el.each(function(){
		var $btn = $(this).find('.hygge-dropdown__handle');
		var $menu = $(this).find('.hygge-dropdown__menu');

		$btn.click(function(){
			$(document).off('.hyggeDropdown');
			$el.toggleClass('on');

			$(document).on('click.hyggeDropdown', function(e){
				if( !$el.get(0).contains( e.target) ){
					$el.removeClass('on');
					$(document).off('.hyggeDropdown');
				}
			});
		});
	});
}



/*------------------------------------------------------------
 * Font Size Adapter
 *------------------------------------------------------------*/

function hyggeMediaDropcapSize(){
	$('.hygge-dropcap-media').each(function(){
		if( $(this).width() < 240 ){
			$('.letter', this).addClass('letter--small');
		}else{
			$('.letter', this).removeClass('letter--small');
		}
	});
}




})(jQuery);
