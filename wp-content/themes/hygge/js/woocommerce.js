if( typeof hygge === "undefined" ){
	var hygge = {};
}

(function($){
"use strict";

$(document).ready( function(){
	hyggeWooCommerceAdditions();
});

$(document).on( 'hygge:ajaxPageLoad', function(){
	hyggeWooCommerceAdditions();
	$( '.wc-tabs-wrapper, .woocommerce-tabs, #rating' ).trigger( 'init' );

});



function hyggeWooCommerceAdditions(){
	if( !$('body').hasClass('woocommerce') ){
		return;
	}

	// Support data tabs changing content
	$('.wc-tabs a').click( function(){
		$(document).trigger( 'hygge:layout' );
	});

	// Orderby augmentation
	$('.woocommerce-ordering .hygge-dropdown__menu a').click( function(){
		$(this)
			.closest('.woocommerce-ordering')
			.find('select')
			.val( $(this).attr('value') )
			.change();
	});

	// Refreshes cart-widget
	$( document.body ).trigger( 'wc_fragment_refresh' );

	// Move Categories from loop into banner area
	var $cats = $('.main-content .woocommerce-loop .products').find('.product-category');
	var $catsBanner = $('.main-content .hygge-wc-categories-banners');
	if ($cats.length > 0 && $catsBanner.length > 0) {
		$catsBanner.html($cats);
	}


	jQuery(function(o){o(".woocommerce-ordering").on("change","select.orderby",function(){o(this).closest("form").submit()}),o("input.qty:not(.product-quantity input.qty)").each(function(){var e=parseFloat(o(this).attr("min"));e>=0&&parseFloat(o(this).val())<e&&o(this).val(e)}),jQuery(".woocommerce-store-notice__dismiss-link").click(function(){Cookies.set("store_notice","hidden",{path:"/"}),jQuery(".woocommerce-store-notice").hide()}),"hidden"===Cookies.get("store_notice")?jQuery(".woocommerce-store-notice").hide():jQuery(".woocommerce-store-notice").show()});

	// Price Slider
	jQuery(function(e){function r(){e("input#min_price, input#max_price").hide(),e(".price_slider, .price_label").show();var r=e(".price_slider_amount #min_price").data("min"),i=e(".price_slider_amount #max_price").data("max"),c=e(".price_slider_amount #min_price").val(),o=e(".price_slider_amount #max_price").val();e(".price_slider:not(.ui-slider)").slider({range:!0,animate:!0,min:r,max:i,values:[c,o],create:function(){e(".price_slider_amount #min_price").val(c),e(".price_slider_amount #max_price").val(o),e(document.body).trigger("price_slider_create",[c,o])},slide:function(r,i){e("input#min_price").val(i.values[0]),e("input#max_price").val(i.values[1]),e(document.body).trigger("price_slider_slide",[i.values[0],i.values[1]])},change:function(r,i){e(document.body).trigger("price_slider_change",[i.values[0],i.values[1]])}})}if("undefined"==typeof woocommerce_price_slider_params)return!1;e(document.body).bind("price_slider_create price_slider_slide",function(r,i,c){e(".price_slider_amount span.from").html(accounting.formatMoney(i,{symbol:woocommerce_price_slider_params.currency_format_symbol,decimal:woocommerce_price_slider_params.currency_format_decimal_sep,thousand:woocommerce_price_slider_params.currency_format_thousand_sep,precision:woocommerce_price_slider_params.currency_format_num_decimals,format:woocommerce_price_slider_params.currency_format})),e(".price_slider_amount span.to").html(accounting.formatMoney(c,{symbol:woocommerce_price_slider_params.currency_format_symbol,decimal:woocommerce_price_slider_params.currency_format_decimal_sep,thousand:woocommerce_price_slider_params.currency_format_thousand_sep,precision:woocommerce_price_slider_params.currency_format_num_decimals,format:woocommerce_price_slider_params.currency_format})),e(document.body).trigger("price_slider_updated",[i,c])}),r(),"undefined"!=typeof wp&&wp.customize&&wp.customize.selectiveRefresh&&wp.customize.widgetsPreview&&wp.customize.widgetsPreview.WidgetPartial&&wp.customize.selectiveRefresh.bind("partial-content-rendered",function(){r()})});



}



})(jQuery);
