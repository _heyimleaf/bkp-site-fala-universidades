"use strict";
if( typeof hygge === "undefined" ){
	var hygge = {};
}

document.documentElement.className = document.documentElement.className.replace('no-js','js');



/* UserAgent
 *------------------------------------------------------------*/

function detectUserAgent(){

	hygge.userAgent = '';

	if( navigator.userAgent.match(/iPad/i) !== null ){
		hygge.userAgent = 'iPad';
	}

	if( navigator.userAgent.match(/iPhone/i) !== null ){
		hygge.userAgent = 'iPhone';
	}

	if( navigator.userAgent.match(/Android/i) !== null ){
		hygge.userAgent = 'Android';
	}

	hygge.platform = '';
	if(
		navigator.platform.match(/Mac/i)  !== null &&
		navigator.userAgent.match(/10_6/i) === null &&
		navigator.userAgent.match(/10.6/i) === null &&
		navigator.userAgent.match(/10_5/i) === null &&
		navigator.userAgent.match(/10.5/i) === null
	){
		hygge.platform = 'Mac';
	}

	hygge.browser = '';

	if(
		navigator.appName.match(/Microsoft/i) !== null ||
		navigator.appName.match(/Netscape/i) !== null
	){
		hygge.browser = 'ie';
	}

	if( navigator.userAgent.match(/Firefox/i) !== null ){
		hygge.browser = 'firefox';
	}
	if( navigator.userAgent.match(/Opera Mini/i) !== null ){
		hygge.browser = 'operamini';
	}
	if( navigator.userAgent.match(/Safari/i) !== null ){
		hygge.browser = 'safari';
	}
	if( navigator.userAgent.match(/Chrome/i) !== null ){
		hygge.browser = 'chrome';
	}

}

detectUserAgent();
