/*------------------------------------------------------------
 * jQuery Easing Extend
 *------------------------------------------------------------*/

jQuery.easing.jswing = jQuery.easing.swing;

jQuery.extend(jQuery.easing, {
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	}
});
