<?php

$theme_settings = hygge_theme_settings();
$loop_classes = '';
$i = 0;
$ads = 0;
$ads_type = 'wide';

// get display style
if( is_home() ){
	$display_style = esc_attr( $theme_settings['blog_home_display_style'] );
	$ads = $theme_settings['blog_home_ads'];
}

if( is_archive() || is_search() ){
	$display_style = $theme_settings['archive_display_style'];
	$ads = $theme_settings['archive_ads'];
}

$loop_classes .= ' loop-container--style-'.$display_style;

// add classes for masonry
if( $display_style == 'masonry' ){
	$loop_classes .= ' loop-container--masonry js-loop-is-masonry';
}



// Ads: Set ads type
if( $display_style == 'masonry' || $display_style == 'grid' || $display_style == 'grid-3-2' || $display_style == 'grid-3-1' || $display_style == 'widget-list' || $display_style == 'widget-slider' ){
	$ads_type = 'square';
}

// Ads: Check if paged, and count posts for all pages
if( $wp_query->query_vars['paged'] ){
	$i = ( $wp_query->query_vars['paged'] - 1 ) * $wp_query->query_vars['posts_per_page'];
}

if (have_posts()):
?>


	<div class="hygge-blog hygge-blog--style-<?php echo esc_attr($display_style); ?>">

		<div class="loop-container loop-container--wp <?php echo esc_attr($loop_classes); ?>">

			<?php while (have_posts()) : the_post();

				get_template_part( 'loop-item', $display_style );

				$i++;
				if( $ads && $i % $ads == 0 ){
					if( is_active_sidebar("widget-area-ad-{$ads_type}") ){
						echo "<article class='widget-area--blog widget-area--blog--{$ads_type} hygge-animate-appearance post'>";
						echo "<div class='post__inwrap'>";
						dynamic_sidebar("widget-area-ad-{$ads_type}");
						echo "</div>";
						echo "</article>";
					}
				}

			endwhile; ?>

		</div>

		<?php get_template_part('pagination'); ?>

	</div>

<?php endif; ?>
