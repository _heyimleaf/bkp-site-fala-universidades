<?php
$theme_settings = hygge_theme_settings();

if( $theme_settings['notif_use'] ){
	echo '<div class="hygge-notif dark-mode label">';

	if( $theme_settings['notif_jetpack_subscription'] && shortcode_exists('jetpack_subscription_form') ){

		$text = $theme_settings['notif_text'] ? $theme_settings['notif_text'] : esc_html__('Subscribe', 'hygge');
		$btn = $theme_settings['notif_btn_text'] ? 'subscribe_button="'.$theme_settings['notif_btn_text'].'"' : '';
		echo do_shortcode('[jetpack_subscription_form subscribe_text="'. esc_html($text) .'" '. esc_html($btn) .' title=""]');

	}else{

		if( $theme_settings['notif_text'] ){
			echo '<div class="hygge-notif__text">'. esc_html( $theme_settings['notif_text'] ) .'</div>';
		}

		if( $theme_settings['notif_btn_text'] && $theme_settings['notif_btn_link'] ){
			echo '<a class="hygge-notif__btn button button--small" href="'. esc_url( $theme_settings['notif_btn_link'] ) .'">'. esc_html( $theme_settings['notif_btn_text'] ) .'</a>';
		}

	}

	if( $theme_settings['notif_hideable'] ){
		echo '<a class="hygge-notif__hide"><i class="hygge-icon-cross"></i></a>';
	}

	echo '</div>';
}
