<?php
/*
Used to create blog list on regular page
*/

if( !get_post_meta( get_the_ID(), 'blog_use', true ) ){
	return;
}

$theme_settings = hygge_theme_settings();
$i = 0;
$ads_type = 'wide';

// Gather needed fields
$blog_title = get_post_meta( get_the_ID(), 'blog_title', true );
$display_style = get_post_meta( get_the_ID(), 'blog_display_style', true );
$blog_count = get_post_meta( get_the_ID(), 'blog_count', true );
$blog_category = get_post_meta( get_the_ID(), 'blog_category', true );
$blog_category_exclude = get_post_meta( get_the_ID(), 'blog_category_exclude', true );
$blog_pagination = get_post_meta( get_the_ID(), 'blog_pagination', true );
$blog_more_button_text = get_post_meta( get_the_ID(), 'more_button_text', true );
$blog_more_button_link = get_post_meta( get_the_ID(), 'more_button_link', true );
$blog_ads = get_post_meta( get_the_ID(), 'blog_ads', true );
$blog_ignore_sticky = get_post_meta( get_the_ID(), 'enable_sticky_posts', true ) ? 0 : 1;

$blog_count = $blog_count ? $blog_count : get_option('posts_per_page');

$blog_cats = '';
if( !empty( $blog_category ) ){
	foreach( $blog_category as $blog_cat ){
		$blog_cats .= $blog_cat . ',';
	}
}

if( !empty( $blog_category_exclude ) ){
	foreach( $blog_category_exclude as $blog_cat_exclude ){
		$blog_cats .= '-' . $blog_cat_exclude . ',';
	}
}

// Exclude Featured Posts
$featured_post_ids = '';
if (
	(int)$theme_settings['featured_count'] != 0
	&& get_post_meta( get_the_ID(), 'blog_featured_exclude', true ) == '1'
){

	// WPML convert to translated tag ID
	$featured_tag = apply_filters( 'wpml_object_id', $theme_settings['featured_tag'], 'post_tag', TRUE );

	$featured_post_ids = get_posts( array(
		'numberposts'	=> $theme_settings['featured_count'],
		'tag_id'			=> $featured_tag,
		'meta_key'		=> '_thumbnail_id', // contain featured image
		'fields'			=> 'ids', // Only get post IDs
	));
}



// Loop Classes
$loop_classes = '';
$loop_classes .= ' loop-container--style-'.$display_style;

// add classes for masonry
if( $display_style == 'masonry' ){
	$loop_classes .= ' loop-container--masonry js-loop-is-masonry';
}

// Ads: set ads type
if( $display_style == 'masonry' || $display_style == 'grid' || $display_style == 'grid-3-2' || $display_style == 'grid-3-1' || $display_style == 'widget-list' || $display_style == 'widget-slider' ){
	$ads_type = 'square';
}



// Query

// If static page or static page set as homepage
if( !is_front_page() ){
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
}else{
	$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
}

// Ads: Check if paged, and count posts for all pages
if( $paged > 1 ){
	$i = ( $paged - 1 ) * $blog_count;
}


$temp_query = $wp_query; // backup original query

$meta_key = '';

// Must contain featured images
if( $display_style == 'featured' || $display_style == 'filmstrip' || $display_style == 'gallery' ){
	$meta_key = '_thumbnail_id';
}

global $wp_query;
$wp_query = null;
$wp_query = new WP_Query( array(
	 'posts_per_page' => $blog_count
	,'paged' => $paged
	,'cat' => $blog_cats
	,'ignore_sticky_posts' => $blog_ignore_sticky
	,'meta_key' => $meta_key // bugged and returns one item more
	,'post__not_in' => $featured_post_ids
));

if ( $wp_query->have_posts() ):

	echo '<div class="hygge-blog hygge-blog--page hygge-blog--style-'.$display_style.'">';

		if( $blog_title ){
			echo( do_shortcode("<h2 class='align-center'>{$blog_title}</h2>") );
		}

		echo '<div class="loop-container loop-container--wp '.$loop_classes.'">';

		// pass info to loop items / feature heads
		$wp_query->hygge_page_blog_display = $display_style;
		$wp_query->hygge_page_blog_pagination = $blog_pagination;
		$wp_query->hygge_more_button_text = $blog_more_button_text;
		$wp_query->hygge_more_button_link = $blog_more_button_link;

		while ( $wp_query->have_posts() ) : $wp_query->the_post();

			get_template_part( 'loop-item', $display_style );

			$i++;
			if( $blog_ads && $i % $blog_ads == 0 ){
				if( is_active_sidebar("widget-area-ad-{$ads_type}") ){
					echo "<article class='widget-area--blog widget-area--blog--{$ads_type} hygge-animate-appearance post'>";
					echo "<div class='post__inwrap'>";
					dynamic_sidebar("widget-area-ad-{$ads_type}");
					echo "</div>";
					echo "</article>";
				}
			}

		endwhile;

		echo '</div>';

		get_template_part('pagination');

	echo '</div>';

endif;

$wp_query = $temp_query;
wp_reset_postdata();
