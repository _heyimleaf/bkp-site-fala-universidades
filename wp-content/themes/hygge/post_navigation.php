<?php
$theme_settings = hygge_theme_settings();

// Switch prev-next sides
$prevPost = get_previous_post( $theme_settings['post_navigation_category'] );
$nextPost = get_next_post( $theme_settings['post_navigation_category'] );


// Next Post
if( !empty($nextPost) ){

	$nextClass = '';
	$nextClass = has_post_thumbnail( $nextPost->ID ) ? '' : 'image--false';
	$nextImgID = get_post_thumbnail_id( $nextPost->ID  );
	$nextImg = wp_get_attachment_image_src( $nextImgID, 'hygge-horizontal-l' );

	echo '<a class="post__navigation__item hygge-media-hover next ' . $nextClass . '" href="' . get_permalink( $nextPost->ID ) . '" title="'.$nextPost->post_title.'" data-loader-img="' . hygge_get_loader_img_url($nextPost->ID) . '" style="background-image: url(' . $nextImg[0] . ')">';

		echo '<div class="post__navigation__img hygge-media-hover__img">';
		echo get_the_post_thumbnail( $nextPost->ID, 'hygge-horizontal-l' );
		echo '</div>';

		echo '<div class="hygge-media-hover__hover" style="background-color: ' . hygge_get_post_color( $nextPost->ID ) . ';"></div>';
		echo '<div class="post__navigation__text dark-mode">';
			if( is_rtl() ){
				echo '<span class="label--italic">' . esc_html__('Previous Story', 'hygge') . '</span>';
				echo '<h3 class="post__title">' . wp_kses( $nextPost->post_title, hygge_allowed_kses_tags() ) . '</h3>';
			}else{
				echo '<span class="label--italic">' . esc_html__('Previous Story', 'hygge') . '</span>';
				echo '<h3 class="post__title">' . wp_kses( $nextPost->post_title, hygge_allowed_kses_tags() ) . '</h3>';
			}
		echo '</div>';
	echo '</a>';

}else{

	echo '<a class="post__navigation__item next disabled">';
		echo '<div class="hygge-media-hover__hover"></div>';
		echo '<div class="post__navigation__text">';
			echo '<span class="label--italic">' . esc_html__('Latest Story', 'hygge') . '</span>';
			echo '<h3 class="post__title">' . wp_kses( $post->post_title, hygge_allowed_kses_tags() ) . '</h3>';
		echo '</div>';
	echo '</a>';

}



// Previous Post
if( !empty($prevPost) ){

	$prevClass = '';
	$prevClass = has_post_thumbnail( $prevPost->ID ) ? '' : 'image--false';
	$prevImgID = get_post_thumbnail_id( $prevPost->ID  );
	$prevImg = wp_get_attachment_image_src( $prevImgID, 'hygge-horizontal-l' );


	echo '<a class="post__navigation__item hygge-media-hover prev ' . $prevClass . '" href="' . get_permalink( $prevPost->ID ) . '" title="'.$prevPost->post_title.'" data-loader-img="' . hygge_get_loader_img_url($prevPost->ID) . '" style="background-image: url(' . $prevImg[0] . ')">';

		echo '<div class="post__navigation__img hygge-media-hover__img">';
		echo get_the_post_thumbnail( $prevPost->ID, 'hygge-horizontal-l' );
		echo '</div>';

		echo '<div class="hygge-media-hover__hover" style="background-color: ' . hygge_get_post_color( $prevPost->ID ) . ';"></div>';
		echo '<div class="post__navigation__text dark-mode">';
			if( is_rtl() ){
				echo '<span class="label--italic">'. esc_html__('Next Story', 'hygge') . '</span>';
				echo '<h3 class="post__title">' . wp_kses( $prevPost->post_title, hygge_allowed_kses_tags() ) . '</h3>';
			}else{
				echo '<span class="label--italic">' . esc_html__('Next Story', 'hygge') . '</span>';
				echo '<h3 class="post__title">' . wp_kses( $prevPost->post_title, hygge_allowed_kses_tags() ) . '</h3>';
			}
		echo '</div>';
	echo '</a>';

}else{

	echo '<a class="post__navigation__item prev disabled">';
		echo '<div class="post__navigation__text">';
			echo '<span class="label--italic">' . esc_html__('First Story', 'hygge') . '</span>';
			echo '<h3 class="post__title">' . wp_kses( $post->post_title, hygge_allowed_kses_tags() ) . '</h3>';
		echo '</div>';
	echo '</a>';

}
