<?php

/*------------------------------------------------------------
 * This is loop-item-carousel template.
 * Style: carousel
 * Title: Carousel
 *------------------------------------------------------------*/

	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'hygge-portrait-m' );
	$image = $image[0];
?>

<article <?php post_class( 'hygge-carousel__item' ); ?>>
	<div class="post__inwrap">

		<?php if( has_post_thumbnail() ){
			$imgID = get_post_thumbnail_id();
			$img = wp_get_attachment_image_src( $imgID, 'hygge-xl' );

			echo '<div class="post__media" style="background-image: url('.$img[0].');">';
				the_post_thumbnail( 'hygge-xl' );
			echo '</div>';
		} ?>

		<div class="post__text">

			<?php echo hygge_category(1); ?>

			<h3 class="post__title">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" data-loader-img="<?php echo hygge_get_loader_img_url(); ?>">
					<?php echo wp_kses( get_the_title(), hygge_allowed_kses_tags() ); ?>
				</a>
			</h3>

			<div class="post__excerpt--wrapper">
				<?php hygge_wp_excerpt(); ?>
			</div>

			<?php hygge_continue_reading(true, false, true); ?>

		</div>

	</div>
</article>
