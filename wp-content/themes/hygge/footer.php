<?php

$theme_settings = hygge_theme_settings();
$footer_class = '';

if( empty( $theme_settings['bg_footer'] ) ){
	$footer_class .= ' no-background';
}

if ($theme_settings['bg_footer_dark']) {
	$footer_class = 'dark-mode';
}

if( is_active_sidebar('widget-area-after-content') ){
	echo '<div class="widget-area--content widget-area--after-content">';
	echo '<div class="max-width-wrapper">';
		dynamic_sidebar('widget-area-after-content');
		echo '</div>';
	echo '</div>';
}



echo '</div>'; // content wrapper inside
echo '</div>'; // content wrapper

echo '</div>';
if( class_exists( 'WooCommerce' ) ){
	/**
	 * woocommerce_sidebar hook.
	 *
	 * @hooked woocommerce_get_sidebar - 10
	 */
	do_action( 'woocommerce_sidebar' );
}else{
	get_sidebar();
}

echo '</div>';
echo '<div class="footer hygge-layout-wrapper">';
echo '<div class="hygge-layout-main">';

	if( is_active_sidebar('footer-area-instagram') ){
		echo '<div class="footer__instagram">';
			dynamic_sidebar('footer-area-instagram');
		echo '</div>';
	}

	if( $theme_settings['copyright'] || is_active_sidebar('footer-area') ){
		echo '<div class="footer__bottom '.esc_attr($footer_class).'">';
		echo '<div class="max-width-wrapper">';

			if( is_active_sidebar('footer-area') ){
				echo '<div class="fl">';
			}

			// Copyright
			if( $theme_settings['copyright'] ){
				echo '<div class="copyright label">';
				echo wpautop( do_shortcode( wp_kses( html_entity_decode($theme_settings['copyright']), array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'small' => array(),
					'br' => array(),
					'strong' => array(),
					'b' => array(),
					'em' => array(),
					'i' => array()
				) ) ) );
				echo '</div>';
			}

			if( is_active_sidebar('footer-area') ){
				echo '</div>';
				echo '<div class="fr">';
					dynamic_sidebar('footer-area');
				echo '</div>';
			}


		echo '</div>';
		echo '</div>';
	}
echo '</div>';
echo '<div class="hygge-layout-sidebar"></div>';
echo '</div>';

wp_footer();
?>

</body>
</html>
