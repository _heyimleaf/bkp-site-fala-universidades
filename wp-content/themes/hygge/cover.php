<?php
$theme_settings = hygge_theme_settings();

// Defaults
$showCover = true;
$showCoverMedia = true;
$showCategory = false;
$type = 'archive';
$title = '';
$description = '';
$label = '';
$img = false;
$gallery = false;
$video = false;
$coverBg = '';
$output_el = '';
$overlay = true;
$blurredImg = false;
$color = false;
$colorOpacity = false;



// Single Post and Page
if( is_singular() ){
	$type = $theme_settings['post_cover_style'];
	$showCategory = $theme_settings['post_cover_style'] == 'notebook';

	switch( get_post_meta( get_the_ID(), 'cover_media', true ) ){
		case 'none':
			return;
			break;

		case 'title':
			if ($theme_settings['post_cover_style'] == 'basic') {
				return;
			}
			$showCoverMedia = true;
			$coverBg = $theme_settings['colors']['color_grey_pale'];
			break;

		case 'gallery':
			$gallery = hygge_match_gallery( $post->post_content );
			if( $gallery ){
				$gallery = do_shortcode( $gallery );
			}else{
				$gallery = false;
			}

			$showCoverMedia = $gallery ? true : false;
			break;

		case 'video':
			$video = hygge_match_first_embed( $post->post_content );
			if( !empty( $video ) ){
				$video = '<div class="hygge-embed-container domain-youtube">' . do_shortcode( $video ) . '</div>';
			}else{
				$video = false;
			}

			$showCoverMedia = $video ? true : false;
			break;

		case 'custom':
			$coverBg = get_post_meta( get_the_ID(), 'custom_cover_image', true );
			$showCoverMedia = $coverBg ? true : false;

			// Match image url from custom cover and try to get the blurred loader
			preg_match_all('/((?>http|https|\/\/).*?)(\.jpg|\.jpeg|\.gif|\.png)/', $coverBg, $matches, PREG_SET_ORDER);
			$coverBgUrl = end( $matches );
			if( $coverBgUrl[1] && $coverBgUrl[2] ){
				$blurredImg = $coverBgUrl[1].'-62x62'.$coverBgUrl[2];
			}

			break;

		case 'featured':
		case false:
		case '':
		default:
			if( has_post_thumbnail() ){
				$imgID = get_post_thumbnail_id();
				$img = wp_get_attachment_image_src( $imgID, 'hygge-xl' );
				$blurredImg = wp_get_attachment_image_src( $imgID, 'hygge-loader' );
				if( !empty( $blurredImg ) ){
					$blurredImg = $blurredImg[0];
				}
			}else{
				$coverBg = $theme_settings['colors']['color_grey_pale'];
			}

			$showCoverMedia = true;
	}

	$title = get_the_title();
	$description = get_post_meta( get_the_ID(), 'cover_description', true );

	if ($theme_settings['post_cover_style'] == 'basic') {
		$title = '';
		$description = '';
	}

	if ($theme_settings['post_cover_style'] == 'overlay' && $showCoverMedia && !$gallery && !$video) {
		if( hygge_get_post_color() ){
			$color = hygge_get_post_color();
		}else{
			$color = $theme_settings['colors']['color_black'];
			$colorOpacity = 'low';
		}
	}
}

// Archive - Fallback
if( is_archive() ){
	$showCoverMedia = false;
	$title = esc_html__('Archives', 'hygge');
}

// Day
if( is_day() ){
	$showCoverMedia = false;
	$title = get_the_date();
	$label = esc_html__('Daily Archive', 'hygge');
}

// Month
if( is_month() ){
	$showCoverMedia = false;
	$title = get_the_date( _x( 'F Y', 'monthly archives date format', 'hygge' ) );
	$label = esc_html__('Monthly Archive', 'hygge');
}

// Year
if( is_year() ){
	$showCoverMedia = false;
	$title = get_the_date( _x( 'Y', 'yearly archives date format', 'hygge' ) );
	$label = esc_html__('Yearly Archive', 'hygge');
}

// Format - Tax
if( is_tax() ){
	$showCoverMedia = false;
	$title = single_cat_title( '' ,false );
	$label = esc_html__('Post Format Archive', 'hygge');
}

// Main Taxonomies
if( is_category() || is_tag() ){
	$queried_object = get_queried_object();
	if( is_category() && function_exists('get_field') ){
		$img = wp_get_attachment_image_src( get_field( 'image', $queried_object), 'hygge-xl' );
		$blurredImg = wp_get_attachment_image_src( get_field( 'image', $queried_object), 'hygge-loader' );
		$blurredImg = !empty( $blurredImg ) ? $blurredImg[0] : '';
	}

	$showCoverMedia = $img ? true : false;
	if( $img ){
		if( hygge_get_category_color( $queried_object->term_id ) ){
			$color = hygge_get_category_color( $queried_object->term_id );
		}else{
			$color = $theme_settings['colors']['color_black'];
			$colorOpacity = 'low';
		}
	}

	$title = single_cat_title('', false);
	$description = $queried_object->description ? $queried_object->description : '';
	$label = is_category() ? esc_html__('Category', 'hygge') : esc_html__('Tag', 'hygge');
}

// Author
if( is_author() ){
	$showCoverMedia = false;
	$title = get_the_author_meta( 'display_name' );
	$description = get_the_author_meta('description') ? get_the_author_meta('description') : '';
	$label = esc_html__('Author Archive', 'hygge');
}

// Blog Home - Index
if( is_home() ){
	return;
}

// Search
if( is_search() ){
	$showCoverMedia = false;
	$title = get_search_query();
	$label = esc_html__('Search Results', 'hygge');
}

// 404
if( is_404() ){
	$showCoverMedia = false;
	$title = esc_html__('404', 'hygge');
	$label = esc_html__('Page not found', 'hygge');
}

// WooCommerce
if ( class_exists( 'WooCommerce' ) ){

	if( is_shop() ){
		$shop_page_id = get_option( 'woocommerce_shop_page_id' );
		$post = get_post( $shop_page_id );

		switch( get_post_meta( get_the_ID(), 'cover_media', true ) ){
			case 'none':
				return;
				break;

			case 'title':
				$showCoverMedia = false;
				break;

			case 'gallery':
				$gallery = hygge_match_gallery( $post->post_content );
				if( $gallery ){
					$gallery = do_shortcode( $gallery );
				}else{
					$gallery = false;
				}

				$showCoverMedia = $gallery ? true : false;
				break;

			case 'video':
				$video = hygge_match_first_embed( $post->post_content );
				if( !empty( $video ) ){
					$video = '<div class="hygge-embed-container domain-youtube">' . do_shortcode( $video ) . '</div>';
				}else{
					$video = false;
				}

				$showCoverMedia = $video ? true : false;
				break;

			case 'custom':
				$coverBg = get_post_meta( get_the_ID(), 'custom_cover_image', true );
				$showCoverMedia = $coverBg ? true : false;

				// Match image url from custom cover and try to get the blurred loader
				preg_match_all('/((?>http|https|\/\/).*?)(\.jpg|\.jpeg|\.gif|\.png)/', $coverBg, $matches, PREG_SET_ORDER);
				$coverBgUrl = end( $matches );
				if( $coverBgUrl[1] && $coverBgUrl[2] ){
					$blurredImg = $coverBgUrl[1].'-62x62'.$coverBgUrl[2];
				}

				break;

			case 'featured':
			case false:
			case '':
			default:
				if( has_post_thumbnail() ){
					$imgID = get_post_thumbnail_id();
					$img = wp_get_attachment_image_src( $imgID, 'hygge-xl' );
					$blurredImg = wp_get_attachment_image_src( $imgID, 'hygge-loader' );
					if( !empty( $blurredImg ) ){
						$blurredImg = $blurredImg[0];
					}
				}
				$showCoverMedia = $img ? true : false;

		}

		if ( apply_filters( 'woocommerce_show_page_title', true ) ){
			$title = woocommerce_page_title(false);
		}

		$description = get_post_meta( get_the_ID(), 'cover_description', true );

	}

	if( is_product_category() || is_product_tag() ){
		$queried_object = get_queried_object();
		$title = woocommerce_page_title(false);
		$description = $queried_object->description ? $queried_object->description : '';

		if( is_product_category() ){
			$label = esc_html__( 'Category', 'hygge' );
		}elseif( is_product_tag() ){
			$label = esc_html__( 'Tag', 'hygge' );
		}

		$img_id = get_woocommerce_term_meta( $queried_object->term_id, 'thumbnail_id', true );
		$img = wp_get_attachment_image_src( $img_id, 'hygge-xl' );
		$blurredImg = wp_get_attachment_image_src( $img_id, 'hygge-loader' );

		$blurredImg = !empty( $blurredImg ) ? $blurredImg[0] : '';

		$showCoverMedia = $img ? true : false;

		if( $img ){
			if( hygge_get_category_color( $queried_object->term_id ) ){
				$color = hygge_get_category_color( $queried_object->term_id );
			}else{
				$color = $theme_settings['colors']['color_black'];
				$colorOpacity = 'low';
			}
		}
	}

	if( is_product() ){
		$showCover = false;
	}
}




/*------------------------------------------------------------
 * Process data and output html
 *------------------------------------------------------------*/

if( !$showCover ){
	return;
}

if( $coverBg ){
	// Prioritize over image
	$coverBg = "background:{$coverBg};";
}elseif( $img ){
	$coverBg = "background-image:url({$img[0]});";
	$output_el = "<img src='{$img[0]}' alt='{$title}'>";
}elseif( $gallery ){
	$output_el = $gallery;
}elseif( $video ){
	$output_el = $video;
}

$media_style = '';
$media_style .= esc_attr($coverBg);

if( !$showCoverMedia || $gallery || $video ){
	$overlay = false;
}

$class = '';
$class .= 'hygge-cover--type-' . $type;
$class .= $gallery ? ' hygge-cover--gallery' : '';
$class .= $video ? ' hygge-cover--video' : '';
$class .= $overlay ? ' hygge-cover--overlay' : '';
$class = esc_attr( $class );

echo "<div class='max-width-wrapper'>";
echo "<div class='hygge-cover {$class}'>";

	if( $showCoverMedia ){
		if( $blurredImg ){
			echo "<div class='hygge-cover__blurred' style='background-image:url({$blurredImg});'></div>";
		}
		echo "<div class='hygge-cover__media element-modular-loading' style='{$media_style}'>";
			echo $output_el;
		echo "</div>";
	}

	if( $color ){
		echo "<div class='hygge-cover__color color-opacity-{$colorOpacity}' style='background-color:{$color};'></div>";
	}

	echo "<div class='hygge-cover__text js-hygge-even-height " . hygge_is_color_dark($color, true) . "'>";
		if( $showCategory ){
			echo hygge_category(20);
		}
		if( $label ){
			echo "<div class='label--italic'>{$label}</div>";
		}
		if( $title ){
			if( $type == 'archive' && !$showCoverMedia && !$description ){
				echo "<h1 class='h4 post__title'>". wp_kses( $title, hygge_allowed_kses_tags() ) ."</h1>";
			}else{
				echo "<h1 class='post__title'>". wp_kses( $title, hygge_allowed_kses_tags() ) ."</h1>";
			}
		}

		if( $description ){
			echo "<p class='description-heading'>" . do_shortcode( nl2br( esc_html($description) ) )."</p>";
		}

		if( $type == 'archive' && !$showCoverMedia ){
			echo '<hr class="hygge-separator--small align-center" />';
		}

	echo "</div>";

echo "</div>";
echo "</div>";
