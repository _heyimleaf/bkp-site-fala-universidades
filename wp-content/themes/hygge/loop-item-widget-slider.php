<?php

/*------------------------------------------------------------
 * This is loop-item-widget-slider template.
 * Style: widget-slider
 * Title: Widget Slider
 *------------------------------------------------------------*/
?>

<article <?php post_class( 'hygge-slider__item' ); ?>>
	<div class="post__inwrap hygge-slider__item-inwrap">

			<div class="post__media">
				<?php if( has_post_thumbnail() ){
					the_post_thumbnail( 'hygge-horizontal-m' );
				}else{
					echo hygge_get_dropcap_media('horizontal');
				}?>
			</div>

		<div class="post__text">

			<h6 class="post__title">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" data-loader-img="<?php echo hygge_get_loader_img_url(); ?>">
					<?php echo wp_kses( get_the_title(), hygge_allowed_kses_tags() ); ?>
				</a>
			</h6>

			<div class="label--italic">
				<?php echo '<div class="item">' . get_the_date() . '</div>'; ?>
			</div>

		</div>

	</div>
</article>
