<?php

function hygge_version(){
	// Ensure parent theme version
	return wp_get_theme( wp_get_theme()->get( 'Template' ) )->get( 'Version' );
}

/*------------------------------------------------------------
 * Define paths
 *------------------------------------------------------------*/

define( "HYGGE_DIR", get_parent_theme_file_path() );
define( "HYGGE_DIR_URI", get_parent_theme_file_uri() );

define( "HYGGE_STYLESHEET", HYGGE_DIR . "/stylesheet" );
define( "HYGGE_STYLESHEET_URI", HYGGE_DIR_URI . "/stylesheet" );
define( "HYGGE_LESS", HYGGE_DIR . "/stylesheet/less" );
define( "HYGGE_LESS_URI", HYGGE_DIR_URI . "/stylesheet/less" );
define( "HYGGE_JS_URI", HYGGE_DIR_URI . "/js" );
define( "HYGGE_ICONS", HYGGE_DIR . "/icons" );
define( "HYGGE_ICONS_URI", HYGGE_DIR_URI . "/icons" );
define( "HYGGE_IMG_URI", HYGGE_DIR_URI . "/img" );
define( "HYGGE_PATTERNS", HYGGE_DIR . "/img/patterns" );
define( "HYGGE_PATTERNS_URI", HYGGE_DIR_URI . "/img/patterns" );

define( "HYGGE_FRAMEWORK", HYGGE_DIR . "/framework" );
define( "HYGGE_FRAMEWORK_URI", HYGGE_DIR_URI . "/framework" );
define( "HYGGE_FUNCTIONS", HYGGE_FRAMEWORK . "/functions" );
define( "HYGGE_FUNCTIONS_URI", HYGGE_FRAMEWORK_URI . "/functions" );
define( "HYGGE_ADMIN", HYGGE_FRAMEWORK . "/admin" );
define( "HYGGE_ADMIN_URI", HYGGE_FRAMEWORK_URI . "/admin" );
define( "HYGGE_PLUGINS", HYGGE_FRAMEWORK . "/plugins" );

define( "HYGGE_BG_CONTROL", HYGGE_ADMIN . "/bg-control" );
define( "HYGGE_BG_CONTROL_URI", HYGGE_ADMIN_URI . "/bg-control" );
define( "HYGGE_ICON_PICKER", HYGGE_ADMIN . "/icon-picker" );
define( "HYGGE_ICON_PICKER_URI", HYGGE_ADMIN_URI . "/icon-picker" );
define( "HYGGE_SHORTCODE_GENERATOR", HYGGE_ADMIN . "/shortcode-generator" );
define( "HYGGE_SHORTCODE_GENERATOR_URI", HYGGE_ADMIN_URI . "/shortcode-generator" );

define( "HYGGE_CUSTOMIZE", HYGGE_FRAMEWORK . "/theme-customize" );
define( "HYGGE_CUSTOMIZE_URI", HYGGE_FRAMEWORK_URI . "/theme-customize" );



/*------------------------------------------------------------
 * External Modules/Files
 *------------------------------------------------------------*/

add_action( 'after_setup_theme', 'hygge_include_files' );

function hygge_include_files(){
	include_once( HYGGE_FUNCTIONS . '/class-tgm-plugin-activation.php' );
	include_once( HYGGE_FUNCTIONS . '/theme-functions.php' );
	include_once( HYGGE_FUNCTIONS . '/navigation-menus.php' );
	include_once( HYGGE_FUNCTIONS . '/sidebars.php' );
	include_once( HYGGE_FUNCTIONS . '/gallery.php' );
	include_once( HYGGE_CUSTOMIZE . '/theme-customize.php' );
	include_once( HYGGE_CUSTOMIZE . '/theme-settings.php' );
	include_once( HYGGE_FRAMEWORK . '/custom-fields/custom-fields-type-background.php' );
	include_once( HYGGE_FRAMEWORK . '/custom-fields/custom-fields-import.php' );
	include_once( HYGGE_FUNCTIONS . '/custom-comments.php' );
	include_once( HYGGE_FRAMEWORK . '/tinymce-extend/tinymce-extend.php' );
	include_once( HYGGE_FUNCTIONS . '/megamenu.php' );

	include_once( HYGGE_STYLESHEET. '/customized-style.php' );



	if( is_admin() ){
		add_action( 'in_admin_footer', 'hygge_admin_includes' );

		function hygge_admin_includes(){
			include_once( HYGGE_ICONS . '/icons.php' );
			include_once( HYGGE_ICON_PICKER . '/icon-picker-lightbox.php' );
			include_once( HYGGE_BG_CONTROL . '/bg-control-lightbox.php' );
			include_once( HYGGE_SHORTCODE_GENERATOR . '/sc-gen-lightbox.php' );
		}
	}
}



/*------------------------------------------------------------
 * Load Scripts and Styles
 *------------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', 'hygge_load_styles_and_scripts' );
add_action( 'admin_enqueue_scripts', 'hygge_load_admin_assets' );
add_action( 'after_setup_theme', 'hygge_editor_styles' );



// localize strings for translataion or usage in .js
// both front-end and admin scripts
function hygge_localization_array(){
	return array (
		'comment1' => esc_html__('1 Comment', 'hygge'),
		'chooseImage' => esc_html__('Choose Image', 'hygge'),
		'close' => esc_html__('Close', 'hygge'),
	);
}



function hygge_load_styles_and_scripts() {

	// Enqueue google fonts
	wp_enqueue_style( 'hygge-google-fonts', hygge_get_google_fonts_embed_url(), array(), hygge_version(), 'all' );

	// Icons
	wp_enqueue_style( 'hygge-icons', HYGGE_ICONS_URI . '/style.css', array(), hygge_version(), 'all' );

	if ( class_exists('WPLessPlugin') ){
		// Main WP-LESS styles
		wp_enqueue_style( 'hygge-less', HYGGE_LESS_URI . '/main.less', array(), hygge_version(), 'all' );
	}else{
		// Compiled style fallback + customized styles
		wp_enqueue_style( 'hygge-compiled', HYGGE_STYLESHEET_URI . '/compiled.css', array(), hygge_version(), 'all' );
		wp_add_inline_style( 'hygge-compiled', hygge_customized_style() );
	}

	// Main style.css
	wp_enqueue_style( 'hygge-main', HYGGE_DIR_URI . '/style.css', array(), hygge_version(), 'all' );

	// RTL
	wp_style_add_data( 'hygge-main', 'rtl', 'replace' );

	// init script loaded as soon as possible
	wp_enqueue_script('hygge-init', HYGGE_JS_URI . '/init.js', array(), hygge_version(), false );

	// custom plugins
	wp_enqueue_script('imagesloaded');
	wp_enqueue_script('jquery-masonry');
	wp_enqueue_script('raf-polyfill', HYGGE_JS_URI . '/plugins/raf.js', array('jquery'), hygge_version(), true );
	wp_enqueue_script('jquery-easing-swing', HYGGE_JS_URI . '/plugins/jquery-easing-swing.js', array('jquery'), hygge_version(), true );
	wp_enqueue_script('jquery-touchswipe', HYGGE_JS_URI . '/plugins/jquery-touchswipe.js', array('jquery'), hygge_version(), true );
	wp_enqueue_script('smoothscroll', HYGGE_JS_URI . '/plugins/smoothscroll.js', array(), hygge_version(), true );
	wp_enqueue_script('nanoscroller', HYGGE_JS_URI . '/plugins/nanoscroller.js', array('jquery'), hygge_version(), true );
	wp_enqueue_script('custom-scrollbar', HYGGE_JS_URI . '/plugins/custom-scrollbar.js', array('jquery'), hygge_version(), true );
	wp_enqueue_script('jquery-fleximages', HYGGE_JS_URI . '/plugins/jquery-fleximages.js', array('jquery'), hygge_version(), true );
	wp_enqueue_script('stickykit', HYGGE_JS_URI . '/plugins/stickykit.js', array('jquery'), hygge_version(), true );

	// main script
	wp_enqueue_script( 'hygge-main-front', HYGGE_JS_URI . '/main.js', array('jquery-easing-swing', 'jquery-touchswipe', 'smoothscroll', 'nanoscroller', 'custom-scrollbar', 'jquery-fleximages', 'stickykit'), hygge_version(), true );
	wp_localize_script( 'hygge-main-front', 'hyggeLocalize', hygge_localization_array() );

	// mejs script/style
	wp_enqueue_style( 'wp-mediaelement' );
	wp_enqueue_script( 'wp-mediaelement' );

	/* Include inline scripts
	*------------------------------------------------------------*/

	$hygge_js_init_inline = "if( typeof hygge === 'undefined' ){ var hygge = {}; }";

	// Pass the base site url to hygge's javascript
	$base_url = get_site_url();
	$hygge_js_init_inline .= "hygge.baseUrl = '{$base_url}';" . "\n";

	// Include theme version
	$version = hygge_version();
	$hygge_js_init_inline .= "hygge.version = '{$version}';" . "\n";

	// Default loader image
	$theme_settings = hygge_theme_settings();
	if( $theme_settings['ajax_loader_img'] ){
		$hygge_js_init_inline .= "hygge.loaderImg = '{$theme_settings['ajax_loader_img']}';" . "\n";
	}

	wp_add_inline_script( 'hygge-init', $hygge_js_init_inline, 'after' );

}



function hygge_load_admin_assets() {

	// admin styles
	wp_enqueue_style( 'hygge-admin', HYGGE_ADMIN_URI . '/assets/css/admin-styles.css', array(), hygge_version(), 'all');

	// icons stylesheet
	wp_enqueue_style( 'hygge-icons', HYGGE_ICONS_URI . '/style.css', array(), hygge_version(), 'all');

	// admin scripts
	wp_enqueue_script('hygge-admin', HYGGE_ADMIN_URI . '/assets/js/admin-scripts.js', array('jquery'), hygge_version(), true);
	wp_localize_script('hygge-admin', 'hyggeLocalize', hygge_localization_array());

	// icon picker script
	wp_enqueue_script( 'hygge-icon-picker', HYGGE_ICON_PICKER_URI.'/icon-picker-script.js', array(), hygge_version(), true );

	// background control script
	wp_enqueue_script( 'hygge-bg-control', HYGGE_BG_CONTROL_URI.'/bg-control-script.js', array('wp-color-picker'), hygge_version(), true );

	// shortcode generator script
	wp_enqueue_script( 'hygge-sc-gen', HYGGE_SHORTCODE_GENERATOR_URI.'/sc-gen-script.js', array(), hygge_version(), true );
	wp_localize_script('hygge-sc-gen', 'hyggeLocalize', hygge_localization_array());


	/* Include inline admin scripts
	*------------------------------------------------------------*/

	$admin_script_inline = '';
	$theme_settings = hygge_theme_settings();
	$hygge_palettes = implode(',', $theme_settings['colors'] );

	$admin_script_inline .= "jQuery(document).ready(function($){ $.wp.wpColorPicker.prototype.options = { hide: true, palettes: '{$hygge_palettes}'.split(',') }; });" . "\n";

	// Get Categories, Tags, Pages and Posts as javascript object
	$all_object = json_encode( hygge_collect_nodes() );

	$admin_script_inline .= "if( typeof hygge === 'undefined' ){ var hygge = {}; } hygge.nodes = {$all_object};" . "\n";

	wp_add_inline_script( 'hygge-admin', $admin_script_inline, 'before' );

}

function hygge_editor_styles() {
	add_editor_style( HYGGE_FRAMEWORK_URI . '/tinymce-extend/tinymce-styles.css' );
}



/*------------------------------------------------------------
 * Theme Support
 *------------------------------------------------------------*/

add_action( 'after_setup_theme', 'hygge_add_theme_support');

function hygge_add_theme_support(){
	if (function_exists('add_theme_support')) {

		if( !isset($content_width) ) {
			$content_width = 1200;
		}

		// Add Thumbnail Theme Support
		add_theme_support('post-thumbnails');

		add_image_size( 'hygge-xl', 1200 ); // content width, highlight grid
		add_image_size( 'hygge-m', 460 ); // masonry blog
		add_image_size( 'hygge-horizontal-xl', 1200, 800, true ); // blog
		add_image_size( 'hygge-horizontal-l', 780, 520, true ); // blog
		add_image_size( 'hygge-horizontal-m', 480, 320, true ); // blog
		add_image_size( 'hygge-horizontal-s', 114, 76, true ); // blog
		add_image_size( 'hygge-square-l', 600, 600, true ); // gallery
		add_image_size( 'hygge-square-m', 390, 390, true ); // gallery
		add_image_size( 'hygge-portrait-m', 495, 585, true ); // much blogs wow, gallery
		add_image_size( 'hygge-portrait-s', 330, 390, true ); // menu, filmstrip, related, gallery
		add_image_size( 'hygge-portrait-xs', 114, 134, true ); // wc widget list
		add_image_size( 'hygge-loader', 62, 62, true ); // ajax loader img



		// Add Thumbnails to WP Admin to choose from
		add_filter( 'image_size_names_choose', 'hygge_custom_image_sizes' );

		function hygge_custom_image_sizes( $sizes ) {

			$sizes = array_merge( $sizes, array(
				'hygge-xl' => esc_html__( 'Content-Width Scale', 'hygge' ),
				'hygge-horizontal-m' => esc_html__( 'Horizontal Crop', 'hygge' ),
				'hygge-square-m' => esc_html__( 'Square Crop', 'hygge' ),
				'hygge-portrait-m' => esc_html__( 'Portrait Crop', 'hygge' ),
			) );

			// Remove native WP sizes
			$sizes = array_diff_key( $sizes, array(
				'thumbnail' => '',
				'medium' => '',
				'large' => '',
			) );

			return $sizes;
		}



		// Enable thumbnail upscale
		function hygge_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
			if ( !$crop ) return null; // let the wordpress default function handle this

			$aspect_ratio = $orig_w / $orig_h;
			$size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

			$crop_w = round($new_w / $size_ratio);
			$crop_h = round($new_h / $size_ratio);

			$s_x = floor( ($orig_w - $crop_w) / 2 );
			$s_y = floor( ($orig_h - $crop_h) / 2 );

			return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
		}

		add_filter( 'image_resize_dimensions', 'hygge_thumbnail_upscale', 10, 6 );



		// Enables post and comment RSS feed links to head
		add_theme_support('automatic-feed-links');

		// Localisation Support
		load_theme_textdomain('hygge', HYGGE_DIR . '/languages');

		// Add Post Formats Support
		add_theme_support( 'post-formats', array( 'quote', 'image', 'gallery', 'video', 'audio' ) );

		// Add Title Tag Support
		add_theme_support( 'title-tag' );

		// Add HTML5 Support
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'widgets' ) );

		// Add WooCommerce Support
		add_theme_support( 'woocommerce' );
		if( class_exists( 'WooCommerce') ){
			include_once( HYGGE_DIR . '/woocommerce/wc_functions.php' );
		}
	}
}




/*------------------------------------------------------------
 * Pages: Support Tags and Excerpt
 *------------------------------------------------------------*/

add_action('init', 'hygge_tags_support_all');
add_action('pre_get_posts', 'hygge_tags_support_query');
add_action( 'init', 'hygge_add_excerpts_to_pages' );

function hygge_tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}

// This will include tagged pages in featured slider,
// but also in archives
function hygge_tags_support_query($wp_query) {
	if( $wp_query->get('tag') ){ $wp_query->set('post_type', 'any'); }
}

function hygge_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
}




/*------------------------------------------------------------
 * Plugins
 *------------------------------------------------------------*/

/* WP Less
 *------------------------------------------------------------*/

add_action( 'init', 'hygge_include_less');

function hygge_include_less(){

	if( class_exists('WPLessPlugin') ){
		include_once( HYGGE_LESS . '/wp-less-config.php' );
		$less = WPLessPlugin::getInstance();
		$less->setVariables( hygge_wp_less_config() );
	}
}



/* Jetpack default modules
 *------------------------------------------------------------*/

add_filter( 'jetpack_get_default_modules', 'hygge_jetpack_default_modules' );

function hygge_jetpack_default_modules() {
	return array( 'widget-visibility' );
}



/* TGM Require plugins
 *------------------------------------------------------------*/

add_action( 'tgmpa_register', 'hygge_register_required_plugins' );

function hygge_register_required_plugins() {
	$plugins = array(

		array(
			'name'               => 'Hygge Shortcodes',
			'slug'               => 'hygge-shortcodes',
			'source'             => HYGGE_PLUGINS . '/hygge-shortcodes.zip',
			'required'           => true,
			'version'            => '1.0.2',
			'force_activation'   => false,
			'force_deactivation' => false,
		),

		array(
			'name'               => 'Hygge Widgets',
			'slug'               => 'hygge-widgets',
			'source'             => HYGGE_PLUGINS . '/hygge-widgets.zip',
			'required'           => true,
			'version'            => '1.0.2',
			'force_activation'   => false,
			'force_deactivation' => false,
		),

		array(
			'name'               => 'Hygge Share',
			'slug'               => 'hygge-share',
			'source'             => HYGGE_PLUGINS . '/hygge-share.zip',
			'required'           => true,
			'version'            => '1.0.2',
			'force_activation'   => false,
			'force_deactivation' => false,
		),

		array(
			'name'      => 'Advanced Custom Fields',
			'slug'      => 'advanced-custom-fields',
			'required'  => true,
		),

		array(
			'name'      => 'Jetpack',
			'slug'      => 'jetpack',
			'required'  => false,
		),

		array(
			'name'      => 'WP Instagram Widget',
			'slug'      => 'wp-instagram-widget',
			'required'  => false,
		),

		array(
			'name'      => 'MCE Editor: Tables',
			'slug'      => 'mce-table-buttons',
			'required'  => false,
		),

	);

	$config = array(
		'is_automatic' => true
	);

	tgmpa( $plugins, $config );
}



/*------------------------------------------------------------
 * Add Filters
 *------------------------------------------------------------*/

// Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'do_shortcode');
// Remove <p> tags in Dynamic Sidebars (better!)
add_filter('widget_text', 'shortcode_unautop');
// Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'shortcode_unautop');
// Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode');
