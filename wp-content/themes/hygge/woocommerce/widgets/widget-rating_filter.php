<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
* Recent Reviews Widget.
*
* @author   WooThemes
* @category Widgets
* @package  WooCommerce/Widgets
* @version  2.3.0
* @extends  WC_Widget_Rating_Filter
*/
class Hygge_WC_Widget_Rating_Filter extends WC_Widget_Rating_Filter {

	function widget( $args, $instance ) {

		global $wp_the_query;

		if ( ! is_post_type_archive( 'product' ) && ! is_tax( get_object_taxonomies( 'product' ) ) ) {
			return;
		}

		if ( ! $wp_the_query->post_count ) {
			return;
		}

		ob_start();

		$found         = false;
		$rating_filter = isset( $_GET['rating_filter'] ) ? array_filter( array_map( 'absint', explode( ',', $_GET['rating_filter'] ) ) ) : array();

		$this->widget_start( $args, $instance );

		echo '<ul>';

		for ( $rating = 5; $rating >= 1; $rating-- ) {
			$count = $this->get_filtered_product_count( $rating );
			if ( empty( $count ) ) {
				continue;
			}
			$found = true;
			$link  = $this->get_page_base_url();

			if ( in_array( $rating, $rating_filter ) ) {
				$link_ratings = implode( ',', array_diff( $rating_filter, array( $rating ) ) );
			} else {
				$link_ratings = implode( ',', array_merge( $rating_filter, array( $rating ) ) );
			}

			$link  = $link_ratings ? add_query_arg( 'rating_filter', $link_ratings ) : remove_query_arg( 'rating_filter' );

			echo '<li class="wc-layered-nav-rating ' . ( in_array( $rating, $rating_filter ) ? 'chosen' : '' ) . '">';

			echo '<a href="' . esc_url( apply_filters( 'woocommerce_rating_filter_link', $link ) ) . '">';

			echo '<div class="hygge-rating-stars">';
			$rating = round( $rating * 2 ) / 2;
			$i = 0;

			while( $i < 5 ){
				$i++;

				if( $i <= $rating ){
					echo '<div class="star star--full hygge-icon-star"></div>';
				}else{
					echo '<div class="star star--empty hygge-icon-star"></div>';
				}
			}

			echo '</div>';

			echo '</a>';
			echo '<span class="count">(' . esc_html( $count ) . ')</span>';

			echo '</li>';
		}

		echo '</ul>';

		$this->widget_end( $args );

		if ( ! $found ) {
			ob_end_clean();
		} else {
			echo ob_get_clean();
		}
	}

}
