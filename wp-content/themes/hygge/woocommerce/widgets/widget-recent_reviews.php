<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
* Recent Reviews Widget.
*
* @author   WooThemes
* @category Widgets
* @package  WooCommerce/Widgets
* @version  2.3.0
* @extends  WC_Widget_Recent_Reviews
*/
class Hygge_WC_Widget_Recent_Reviews extends WC_Widget_Recent_Reviews {

	function widget( $args, $instance ) {
		global $comments, $comment;

		if ( $this->get_cached_widget( $args ) ) {
			return;
		}

		ob_start();

		$number   = ! empty( $instance['number'] ) ? absint( $instance['number'] ) : $this->settings['number']['std'];
		$comments = get_comments( array( 'number' => $number, 'status' => 'approve', 'post_status' => 'publish', 'post_type' => 'product' ) );

		if ( $comments ) {
			$this->widget_start( $args, $instance );

			echo '<ul class="product_list_widget">';

			foreach ( (array) $comments as $comment ) {

				$_product = wc_get_product( $comment->comment_post_ID );

				$rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );

				$rating_html = wc_get_rating_html( $rating );

				?>
				<li><a href=" <?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">

					<div class="post__media">
						<?php
							echo $_product->get_image();

							$attachment_ids = $_product->get_gallery_image_ids();
							if( !empty( $attachment_ids ) ){
								echo wp_get_attachment_image( $attachment_ids[0], 'hygge-portrait-xs' );
							}
						?>
					</div>

					<div class="post__text">

						<h6 class="post__title">
							<?php echo wp_kses_post( $_product->get_name() ); ?>
						</h6>

						<div class="label--italic">
							<?php echo '<div class="item">' . $rating_html . '</div>'; ?>
						</div>

					</div>
				</a></li>
			<?php
			}

			echo '</ul>';

			$this->widget_end( $args );
		}

		$content = ob_get_clean();

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
