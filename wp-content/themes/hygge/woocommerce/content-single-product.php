<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * woocommerce_before_single_product hook.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

echo '<div id="product-' . get_the_ID() . '" class="single-woocommerce ' . implode( ' ', get_post_class() ) . '">';

	echo '<div class="post__content">';
		echo '<div class="hygge-sideblock hygge-sideblock--gallery">';
			echo '<div class="woocommerce-gallery-wrap">';
			/**
			 * woocommerce_before_single_product_summary hook.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 * HYGGE @hooked woocommerce_template_single_meta - 30
			 */
			do_action( 'woocommerce_before_single_product_summary' );
			echo '</div>';
		echo '</div>';

		echo '<div class="summary entry-summary">';

		/**
		 * woocommerce_single_product_summary hook.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * REMOVED @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 * @hooked woocommerce_output_product_data_tabs - 70
		 */
		do_action( 'woocommerce_single_product_summary' );

		echo '</div>';
	echo '</div>';

	/**
	 * woocommerce_after_single_product_summary hook.
	 *
	 * REMOVED @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );

echo '</div>';

do_action( 'woocommerce_after_single_product' );
