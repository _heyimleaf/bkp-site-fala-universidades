<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.2
 */
defined( 'ABSPATH' ) || exit;
// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $post, $product;

$image_ids = '';
$image_ids .= get_post_thumbnail_id( $post->ID );
$attachment_ids = $product->get_gallery_image_ids();

if( !empty( $attachment_ids ) ){
	$image_ids .= ',' . implode( ',', $attachment_ids );
}

echo do_shortcode( '[gallery size="hygge-portrait-m" hygge_style="slider" hygge_interval="0" link="file" ids="'.$image_ids.'"]' );
