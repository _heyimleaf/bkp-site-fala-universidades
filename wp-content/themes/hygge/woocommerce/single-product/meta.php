<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>

<div class="product_meta post__meta--single-product">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<div class="post__meta__item">
			<div class="label--italic">
				<?php esc_html_e( 'SKU:', 'hygge' ); ?>
			</div>
			<div class="post__meta__data label">
				<?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'hygge' ); ?>
			</div>
		</div>
	<?php endif; ?>

	<?php if( wc_get_product_category_list( $product->get_id(), '', '', '' ) ): ?>
		<div class="post__meta__item">
			<div class="label--italic">
				<?php echo _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'hygge' ) ?>
			</div>
			<div class="post__meta__data label">
				<?php echo wc_get_product_category_list( $product->get_id(), '', '<div class="tagcloud"> ', '</div>' ); ?>
			</div>
		</div>
	<?php endif; ?>

	<?php if( wc_get_product_tag_list( $product->get_id(), '', '', '' ) ): ?>
		<div class="post__meta__item">
			<div class="label--italic">
				<?php echo _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'hygge' ) ?>
			</div>
			<div class="post__meta__data label">
				<?php echo wc_get_product_tag_list( $product->get_id(), '', '<div class="tagcloud"> ', '</div>' ); ?>
			</div>
		</div>
	<?php endif; ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
