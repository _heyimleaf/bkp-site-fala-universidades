<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;


$theme_settings = hygge_theme_settings();
get_header( 'shop' );

echo '<div class="main-content">';

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * REMOVED @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
	do_action( 'woocommerce_before_main_content' );

	if ( have_posts() ){

		/**
		 * woocommerce_before_shop_loop hook.
		 *
		 * @hooked wc_print_notices - 10
		 * @hooked hygge_wc_categories_banners - 12
		 * @hooked hygge_wc_before_shop_loop_wrap_start - 15
		 * REMOVED @hooked woocommerce_result_count - 20
		 * @hooked hygge_wc_product_categories - 20
		 * @hooked hygge_wc_product_loop_search - 25
		 * @hooked woocommerce_catalog_ordering - 30
		 * @hooked hygge_wc_before_shop_loop_wrap_end - 50
		 */
		do_action( 'woocommerce_before_shop_loop' );

		woocommerce_product_loop_start();

		// Support WooCommerce < 3.3.0
		if (!function_exists('wc_get_loop_prop')) {
			function wc_get_loop_prop() {
				return true;
			}
		}

		if ( wc_get_loop_prop( 'total' ) ) {
			while ( have_posts() ) {
				the_post();

				/**
				 * Hook: woocommerce_shop_loop.
				 *
				 * @hooked WC_Structured_Data::generate_product_data() - 10
				 */
				do_action( 'woocommerce_shop_loop' );

				wc_get_template_part( 'content', 'product' );
			}
		}

		woocommerce_product_loop_end();

		/**
		 * Hook: woocommerce_after_shop_loop.
		 *
		 * @hooked woocommerce_pagination - 10
		 */
		do_action( 'woocommerce_after_shop_loop' );
	} else {
		/**
		 * Hook: woocommerce_no_products_found.
		 *
		 * @hooked wc_no_products_found - 10
		 */
		do_action( 'woocommerce_no_products_found' );
	}

	/**
	 * Hook: woocommerce_after_main_content.
	 *
	 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
	 */
	do_action( 'woocommerce_after_main_content' );

echo '</div>';

get_footer( 'shop' );
