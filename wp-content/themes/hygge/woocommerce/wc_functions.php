<?php

/*------------------------------------------------------------
 * WooCommerce Functions
 *------------------------------------------------------------*/

// Remove default styles
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


add_action( 'wp_enqueue_scripts', 'hygge_wc_scripts' );

function hygge_wc_scripts(){
	wp_enqueue_script( 'accounting' );
	wp_enqueue_script( 'jquery-ui-slider' );
	wp_enqueue_script( 'wc-jquery-ui-touchpunch' );
	wp_enqueue_script( 'wc-cart' );
	wp_enqueue_script( 'wc-add-to-cart' );
	wp_enqueue_script( 'wc-add-to-cart-variation' );
	wp_enqueue_script( 'wc-checkout' );
	wp_enqueue_script( 'wc-single-product' );

	wp_enqueue_script( 'hygge-main-woocommerce', HYGGE_JS_URI . '/woocommerce.js', array('hygge-main-front'), hygge_version(), true );

	wp_localize_script( 'hygge-main-woocommerce', 'woocommerce_price_slider_params', array(
		'currency_format_num_decimals' => 0,
		'currency_format_symbol'       => get_woocommerce_currency_symbol(),
		'currency_format_decimal_sep'  => esc_attr( wc_get_price_decimal_separator() ),
		'currency_format_thousand_sep' => esc_attr( wc_get_price_thousand_separator() ),
		'currency_format'              => esc_attr( str_replace( array( '%1$s', '%2$s' ), array( '%s', '%v' ), get_woocommerce_price_format() ) ),
	) );
}





// Custom image dimensions
add_action( 'after_switch_theme', 'hygge_wc_image_dimensions', 1 );
if( !function_exists( 'hygge_wc_image_dimensions' ) ){
	function hygge_wc_image_dimensions() {
		update_option( 'shop_catalog_image_size',
		array( 'width' => '495', 'height' => '585', 'crop' => 1 ) );
		update_option( 'shop_single_image_size',
		array( 'width' => '495', 'height' => '585', 'crop' => 1 ) );
		update_option( 'shop_thumbnail_image_size',
		array( 'width' => '114', 'height' => '134', 'crop' => 1 ) );
	}
}



// Remove breadcrumbs
add_action( 'init', 'hygge_wc_remove_breadcrumbs' );
if( !function_exists( 'hygge_wc_remove_breadcrumbs' ) ){
	function hygge_wc_remove_breadcrumbs() {
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
	}
}



// Alter the output of pagination into theme's original
if( !function_exists( 'woocommerce_pagination' ) ){
	function woocommerce_pagination() {
		echo '<div class="pagination pagination__classic label">';
		$pagination = hygge_wp_pagination();
		if( $pagination ){
			echo "<div>{$pagination}</div>";
		}
		echo '</div>';
	}
}



// Change Single product price position
add_action( 'init', 'hygge_wc_change_single_price_position' );
if( !function_exists( 'hygge_wc_change_single_price_position' ) ){
	function hygge_wc_change_single_price_position() {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 7);
	}
}



// Change Single product meta position
add_action( 'init', 'hygge_wc_change_single_meta_position' );
if( !function_exists( 'hygge_wc_change_single_meta_position' ) ){
	function hygge_wc_change_single_meta_position() {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
		add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_meta', 30);
	}
}



// Change Single data tabs position
add_action( 'init', 'hygge_wc_change_data_tabs_position' );
if( !function_exists( 'hygge_wc_change_data_tabs_position' ) ){
	function hygge_wc_change_data_tabs_position() {
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 70);
	}
}



// Change tab names
add_filter( 'woocommerce_product_tabs', 'hygge_wc_product_tabs' );
if( !function_exists( 'hygge_wc_product_tabs' ) ){
	function hygge_wc_product_tabs( $array ){
		if( array_key_exists( 'additional_information', $array ) ){
			$array['additional_information']['title'] = esc_html__( 'Info', 'hygge' );
		}
		return $array;
	}
}



// Change rating style into stars
add_filter( 'woocommerce_product_get_rating_html', 'hygge_wc_filter_rating_stars', 10, 2 );
if( !function_exists( 'hygge_wc_filter_rating_stars' ) ){
	function hygge_wc_filter_rating_stars( $html, $rating ) {
		$output = '';

		if( $rating > 0 ){
			$rating = round( $rating );
			$output .= '<div class="hygge-rating-stars">';
			$i = 0;

			while( $i < 5 ){
				$i++;

				if( $i <= $rating ){
					$output .= '<div class="star star--full hygge-icon-star"></div>';
				}else{
					$output .= '<div class="star star--empty hygge-icon-star"></div>';
				}
			}

			$output .= '</div>';

			return( $output );
		}else{
			return '';
		}
	}
}



// Alter WooCommerce Widgets
add_action( 'widgets_init', 'hygge_override_woocommerce_widgets', 15 );

if( !function_exists( 'hygge_override_woocommerce_widgets' ) ){
	function hygge_override_woocommerce_widgets() {

		if ( class_exists( 'WC_Widget_Recent_Reviews' ) ) {
			unregister_widget( 'WC_Widget_Recent_Reviews' );
			include_once( HYGGE_DIR . '/woocommerce/widgets/widget-recent_reviews.php' );
			register_widget( 'Hygge_WC_Widget_Recent_Reviews' );
		}

		if ( class_exists( 'WC_Widget_Rating_Filter' ) ) {
			unregister_widget( 'WC_Widget_Rating_Filter' );
			include_once( HYGGE_DIR . '/woocommerce/widgets/widget-rating_filter.php' );
			register_widget( 'Hygge_WC_Widget_Rating_Filter' );
		}

	}
}



// Alter Cart buttons
if( !function_exists( 'woocommerce_widget_shopping_cart_button_view_cart' ) ){
	function woocommerce_widget_shopping_cart_button_view_cart() {
		echo '<a href="' . esc_url( wc_get_cart_url() ) . '" class="button button--small button--border wc-forward fl">' . esc_html__( 'View cart', 'hygge' ) . '</a>';
	}
}
if( !function_exists( 'woocommerce_widget_shopping_cart_proceed_to_checkout' ) ){
	function woocommerce_widget_shopping_cart_proceed_to_checkout() {
		echo '<a href="' . esc_url( wc_get_checkout_url() ) . '" class="button button--small button--black button--semiwide checkout wc-forward fr ">' . esc_html__( 'Checkout', 'hygge' ) . '</a>';
	}
}



// Wrap content into hygge-contentwidth
// add_action( 'woocommerce_before_cart', 'hygge_wrap_into_contentwidth_start' );
// add_action( 'woocommerce_after_cart', 'hygge_wrap_into_contentwidth_end' );

if( !function_exists( 'hygge_wrap_into_contentwidth_start' ) ){
	function hygge_wrap_into_contentwidth_start() {
		echo '<div class="hygge-contentwidth">';
	}
}

if( !function_exists( 'hygge_wrap_into_contentwidth_end' ) ){
	function hygge_wrap_into_contentwidth_end() {
		echo '</div>';
	}
}



// Wrap into hygge-sideblock
add_action( 'woocommerce_before_account_navigation', 'hygge_wrap_into_sideblock_start' );
add_action( 'woocommerce_after_account_navigation', 'hygge_wrap_into_sideblock_end' );

if( !function_exists( 'hygge_wrap_into_sideblock_start' ) ){
	function hygge_wrap_into_sideblock_start() {
		echo '<div class="hygge-sideblock">';
	}
}

if( !function_exists( 'hygge_wrap_into_sideblock_end' ) ){
	function hygge_wrap_into_sideblock_end() {
		echo '</div>';
	}
}

// Cart: move cart-totals after the cart table
add_action( 'init', 'hygge_move_cart_totals' );

if( !function_exists( 'hygge_move_cart_totals' ) ){
	function hygge_move_cart_totals() {
		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );
		add_action( 'woocommerce_after_cart_table', 'woocommerce_cart_totals', 10 );
	}
}



// Add categories banners before loop
add_action( 'woocommerce_before_shop_loop', 'hygge_wc_categories_banners', 12 );

if( !function_exists( 'hygge_wc_categories_banners' ) ){
	function hygge_wc_categories_banners() {
		echo '<ul class="hygge-wc-categories-banners"></ul>';
	}
}



// Wrap before loop content
add_action( 'woocommerce_before_shop_loop', 'hygge_wc_before_shop_loop_wrap_start', 15 );
add_action( 'woocommerce_before_shop_loop', 'hygge_wc_before_shop_loop_wrap_end', 50 );

if( !function_exists( 'hygge_wc_before_shop_loop_wrap_start' ) ){
	function hygge_wc_before_shop_loop_wrap_start() {
		echo '<div class="woocommerce-before-loop">';
	}
}

if( !function_exists( 'hygge_wc_before_shop_loop_wrap_end' ) ){
	function hygge_wc_before_shop_loop_wrap_end() {
		echo '</div>';
	}
}



// Remove result count
add_action( 'init', 'hygge_wc_remove_result_count' );
if( !function_exists( 'hygge_wc_remove_result_count' ) ){
	function hygge_wc_remove_result_count() {
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
	}
}



// List Categories as menu above product loop
add_action( 'woocommerce_before_shop_loop', 'hygge_wc_product_categories', 20 );

if( !function_exists( 'hygge_wc_product_categories' ) ){
	function hygge_wc_product_categories() {
		$current_id = get_queried_object_id();
		$parent_cats = get_terms( 'product_cat', array( 'parent' => 0 ) );

		if ( $parent_cats ) {
			echo '<div class="hygge-product-category-menu">';
			echo '<ul class="product-cats">';

				$class_all = is_shop() ? 'active' : '';
				echo '<li class="category"><a class="h5 ' . $class_all . '" href="' . get_permalink( wc_get_page_id( 'shop' ) ) . '"> ' . esc_html__( 'All', 'hygge' ) . ' </a></li>';

				foreach ( $parent_cats as $parent_term ) {
					$class = $parent_term->term_id === $current_id ? 'active' : '';
					echo '<li class="category">';
						echo '<a href="' .  esc_url( get_term_link( $parent_term ) ) . '" class="h5 ' . $parent_term->slug . ' ' . $class . '">' . $parent_term->name . '</a>';

						$child_cats = get_terms( 'product_cat', array( 'parent' => $parent_term->term_id ) );
						if ( $child_cats ) {
							echo '<ul class="product-cats">';
								foreach ( $child_cats as $child_term ) {
									echo '<li class="category">';
										echo '<a href="' .  esc_url( get_term_link( $child_term ) ) . '" class="' . $child_term->slug . '">' . $child_term->name . '</a>';
									echo '</li>';
								}
							echo '</ul>';
						}
					echo '</li>';
				}
			echo '</ul>';
			echo '</div>';
		}

	}
}



// Include product search above loop
add_action( 'woocommerce_before_shop_loop', 'hygge_wc_product_loop_search', 25 );

if( !function_exists( 'hygge_wc_product_loop_search' ) ){
	function hygge_wc_product_loop_search() {
		?>
		<div class="header__search__toggle"><i class="hygge-icon-search"></i></div>
		<div class="header__search">
			<?php get_product_search_form( true ); ?>
		</div>
		<?php
	}
}



// Alter the titles of SortBy ordering filter
if ( !function_exists( 'woocommerce_catalog_ordering' ) ) {
	function woocommerce_catalog_ordering() {
		global $wp_query;

		if ( 1 === (int) $wp_query->found_posts || ! woocommerce_products_will_display() ) {
			return;
		}

		$orderby                 = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
		$show_default_orderby    = 'menu_order' === apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
		$catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', array(
			'menu_order' => esc_html__( 'Default', 'hygge' ),
			'popularity' => esc_html__( 'Popularity', 'hygge' ),
			'rating'     => esc_html__( 'Average rating', 'hygge' ),
			'date'       => esc_html__( 'Newness', 'hygge' ),
			'price'      => esc_html__( 'Price: low to high', 'hygge' ),
			'price-desc' => esc_html__( 'Price: high to low', 'hygge' ),
		) );

		if ( ! $show_default_orderby ) {
			unset( $catalog_orderby_options['menu_order'] );
		}

		if ( 'no' === get_option( 'woocommerce_enable_review_rating' ) ) {
			unset( $catalog_orderby_options['rating'] );
		}

		wc_get_template( 'loop/orderby.php', array( 'catalog_orderby_options' => $catalog_orderby_options, 'orderby' => $orderby, 'show_default_orderby' => $show_default_orderby ) );
	}
}




// Add preload-image to product links
add_action( 'init', 'hygge_wc_change_links_for_product_loop' );
if( !function_exists( 'hygge_wc_change_links_for_product_loop' ) ){
	function hygge_wc_change_links_for_product_loop() {
		remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );

		add_action( 'woocommerce_before_shop_loop_item', 'hygge_wc_add_preload_image_for_product_loop', 10 );
			function hygge_wc_add_preload_image_for_product_loop() {
				echo '<a href="' . esc_url( get_the_permalink() ) . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link" data-loader-img="' . hygge_get_loader_img_url() . '">';
			}
	}
}

// Alter Loop items
add_action( 'init', 'hygge_wc_alter_loop_items' );
if( !function_exists( 'hygge_wc_alter_loop_items' ) ){
	function hygge_wc_alter_loop_items() {
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	}
}

// Alter the output of product title in loop
if( !function_exists( 'woocommerce_template_loop_product_title' ) ){
	function woocommerce_template_loop_product_title() {
		echo '<h5 class="woocommerce-loop-product__title">' . get_the_title() . '</h5>';
	}
}



// Choose correct image preset
function woocommerce_template_loop_product_thumbnail() {
	echo woocommerce_get_product_thumbnail('hygge-portrait-m'); // WPCS: XSS ok.
}



// Choose correct image preset for categories
add_filter( 'subcategory_archive_thumbnail_size', 'hygge_category_thumbnail_size' );
if ( !function_exists( 'hygge_category_thumbnail_size' ) ) {
	function hygge_category_thumbnail_size( $size ) {
		return 'hygge-portrait-m';
	}
}


// Add hover thumbnail image
add_action( 'woocommerce_before_shop_loop_item_title', 'hygge_loop_product_thumbnail_hover', 20 );
if( !function_exists( 'hygge_loop_product_thumbnail_hover' ) ){
	function hygge_loop_product_thumbnail_hover() {
		global $product;
		$attachment_ids = $product->get_gallery_image_ids();
		if( !empty( $attachment_ids ) ){
			echo wp_get_attachment_image( $attachment_ids[0], 'hygge-portrait-m' );
		}
	}
}



// Customize image placeholder for products
add_filter( 'woocommerce_placeholder_img', 'hygge_custom_placeholder' );
if ( !function_exists( 'hygge_custom_placeholder' ) ) {
	function hygge_custom_placeholder( $html ) {
		return hygge_get_dropcap_media( 'vertical' );
	}
}



// Customize image placeholder for categories
if ( ! function_exists( 'woocommerce_subcategory_thumbnail' ) ) {
	function woocommerce_subcategory_thumbnail( $category ) {
		$small_thumbnail_size   = apply_filters( 'subcategory_archive_thumbnail_size', 'shop_catalog' );
		$dimensions             = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id           = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
			$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
		} else {
			// $image        = wc_placeholder_img_src();
			// $image_srcset = $image_sizes = false;

			$letter = mb_substr( $category->name, 0, 1 );
			echo hygge_get_dropcap_media( 'vertical', false, $letter );
			return;
		}

		if ( $image ) {
			// Prevent esc_url from breaking spaces in urls for image embeds
			// Ref: https://core.trac.wordpress.org/ticket/23605
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available
			if ( $image_srcset && $image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" />';
			}
		}
	}
}
