<?php
/**
 * The template for displaying product widget entries
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-widget-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product; ?>

<li>
	<a href="<?php echo esc_url( $product->get_permalink() ); ?>" title="<?php echo $product->get_name(); ?>" data-loader-img="<?php echo hygge_get_loader_img_url(); ?>">

		<div class="post__media">
			<?php
				echo $product->get_image('hygge-portrait-xs');

				$attachment_ids = $product->get_gallery_image_ids();
				if( !empty( $attachment_ids ) ){
					echo wp_get_attachment_image( $attachment_ids[0], 'hygge-portrait-xs' );
				}
			?>
		</div>

		<div class="post__text">

			<h6 class="post__title">
				<?php echo esc_html( $product->get_name() ); ?>
			</h6>

			<div class="price">
				<?php echo $product->get_price_html(); ?>
			</div>

			<?php if ( ! empty( $show_rating ) ) : ?>
				<div class="label--italic">
					<?php echo '<div class="item">' . wc_get_rating_html( $product->get_average_rating() ) . '</div>'; ?>
				</div>
			<?php endif; ?>

		</div>

	</a>
</li>
