<?php
$theme_settings = hygge_theme_settings();

echo '<div class="item">' . get_the_date() . '</div>';
$categories = get_the_category();
$i = 0;

foreach( $categories as $category ) {
	if( $i > 2 ){
		return;
	}

	$i++;

	$img = '';
	if( function_exists('get_field') && $theme_settings['ajax_loader_post_thumbs'] ){
		$imgObj = wp_get_attachment_image_src( get_field( 'image', $category), 'hygge-loader' );
		if( !empty( $imgObj ) ){
			$img = esc_attr( $imgObj[0] );
		}
	}

	echo '<div class="item js-hygge-even-height"><a class="link--underline" href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . $category->name . '" data-loader-img="' . $img . '">' . esc_html( $category->name ) . '</a></div>';
}
