<?php

/*------------------------------------------------------------
 * This is loop-item-list template.
 * Style: list
 * Title: List
 *------------------------------------------------------------*/

$theme_settings = hygge_theme_settings();
$post_format = get_post_format();
$color = hygge_get_post_color();
$colorOpacity = $color ? '' : 'color-opacity-low';

// revert post format for unsupported formats
// helpful for pre-hygge content
if( $post_format == 'status' || $post_format == 'chat' || $post_format == 'link' || $post_format == 'aside' || $post_format == 'image' ){
	$post_format = false;
}

$post_classes = '';
$post_classes .= 'hygge-animate-appearance';

?>



<article <?php post_class( $post_classes ); ?>>
	<div class="post__inwrap">
		<div class="post__media" data-sticky-text="<?php esc_html_e( 'Pinned Post', 'hygge' ); ?>">

			<?php

			if( $post_format == 'video' || $post_format == 'audio' ){

				$embed = hygge_match_first_embed( get_the_content() );

				if( !empty( $embed ) ){

					$tag = substr($embed, 0, 6);
					$wrap = false;

					if( $tag == '<ifram' || $tag == '<objec' || $tag == '<embed' ){
						$wrap = true;
					}

					if( $wrap ){
						// Wrap with youtube domain to ensure responsiveness
						// If it happens that user needs to support twitter or similar
						// non-responsive embed, tweak it via custom CSS
						echo '<div class="hygge-embed-container domain-youtube">';
					}

					echo do_shortcode( $embed );

					if( $wrap ){
						echo '</div>';
					}
				}else{
					// treat the post like a standard format
					$post_format = false;
				}

			}?>



			<?php if( $post_format == 'gallery' ){

				$gallery = hygge_match_gallery( get_the_content() );

				if( !empty( $gallery ) ){

					preg_match('/ids=".*?"/', $gallery, $ids);

					if( !empty($ids) ){
						$gallery = '[gallery size="hygge-horizontal-m" hygge_style="slider" hygge_interval="0" link="none" '.$ids[0].']';

						echo do_shortcode( $gallery );
					}

				}else{
					// treat the post like a standard format
					$post_format = false;
				}

			}?>



			<?php if( ( !$post_format || $post_format == 'quote' ) ): ?>
				<div class="hygge-media-hover has-thumbnail-<?php echo ( has_post_thumbnail() ? 'true' : 'false' ); ?> <?php echo hygge_is_color_dark($color, true, has_post_thumbnail()) ?>">
					<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" data-loader-img="<?php echo hygge_get_loader_img_url(); ?>">
						<div class="hygge-media-hover__img">
							<?php if( has_post_thumbnail() ){
								the_post_thumbnail( 'hygge-horizontal-m' );
							}else{
								echo hygge_get_dropcap_media('horizontal');
							}?>
						</div>
					</a>
					<div class="hygge-media-hover__hover <?php echo esc_attr($colorOpacity); ?>" style="background-color:<?php echo esc_attr($color); ?>;"></div>
					<div class="hygge-media-hover__details label--italic italic-meta js-hygge-even-height">
						<?php get_template_part('post-meta-loop'); ?>
					</div>

				</div>
			<?php endif; ?>

		</div>



		<div class="post__text">
			<?php if( $post_format == 'quote'):
				$quote = hygge_match_blockquote( get_the_content() );
				echo do_shortcode( $quote );
			else: ?>
				<div class="post__text__top">

					<h5 class="post__title">
						<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" data-loader-img="<?php echo hygge_get_loader_img_url(); ?>">
							<?php echo wp_kses( get_the_title(), hygge_allowed_kses_tags() ); ?>
						</a>
					</h5>

					<?php hygge_wp_excerpt(); ?>
				</div>
			<?php endif; ?>

			<div class="post__text__bottom">
				<div class="fl">
					<?php hygge_continue_reading(true, false, true); ?>
				</div>

				<div class="fr">
					<?php if( function_exists('hygge_share') ){
						hygge_share();
					} ?>
				</div>
			</div>

		</div>

	</div>
</article>
