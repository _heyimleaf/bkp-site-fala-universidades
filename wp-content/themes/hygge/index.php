<?php
	$theme_settings = hygge_theme_settings();
	get_header();

	echo '<div class="main-content">';
		get_template_part('loop');
	echo '</div>';

	get_footer();
