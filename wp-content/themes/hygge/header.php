<!doctype html>
<?php $theme_settings = hygge_theme_settings(); ?>

<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<?php wp_head(); ?>
</head>



<body <?php body_class(); ?>>
<div class="hygge-loader hygge-loader--body"></div>

<?php get_template_part('notification-bar'); ?>

<?php
$nav_mobile_class = 'grey-mode';
switch( $theme_settings['bg_header'] ){
	case false:
	case '':
	case '#fff':
	case '#ffffff':
		$nav_mobile_class = '';
		break;
}

$header_class = '';
if ($theme_settings['bg_header_dark']) {
	$header_class = $nav_mobile_class = 'dark-mode';
}
?>

<nav class="hygge-nav--mobile <?php echo esc_attr( $nav_mobile_class ); ?>">
	<div class="nano-content">
	<div class="nano-content-inwrap">
		<a href="#" class="hygge-nav--mobile-btn js-hygge-nav-toggle label"><?php esc_html_e('Close', 'hygge'); ?></a>
		<?php
			hygge_nav_menu_mobile();
			dynamic_sidebar('widget-area-header-area');
			get_template_part('searchform');
		 ?>
	</div>
	</div>
</nav>


<div class="hygge-layout-wrapper">
<div class="hygge-layout-main">

<header id="header" class="header <?php echo $header_class; ?>">

	<div class="header__inwrap ">
		<div class="header__search__toggle"><i class="hygge-icon-search"></i></div>

		<?php dynamic_sidebar('widget-area-header-area'); ?>

		<div class="header__search">
			<?php get_template_part('searchform'); ?>
		</div>

		<div class="header__menu max-width-wrapper">
			<div class="hygge-nav--classic">
				<?php hygge_nav_menu_header(); ?>
			</div>
		</div>

	</div>

</header>

<a href="#" class="hygge-nav--mobile-btn js-hygge-nav-toggle label"><?php esc_html_e('Menu & Search', 'hygge'); ?><i class="has-dropdown-icon"></i></a>

<div class="hygge-logo">
	<a href="<?php echo  esc_url( home_url('/') ) ; ?>">
		<?php if( !$theme_settings['logo'] ): ?>
			<span class="sitename h2"><?php echo bloginfo('name'); ?></span>
		<?php else: ?>
			<img src="<?php echo esc_url($theme_settings['logo']); ?>" alt="<?php echo get_bloginfo('name'); ?>" class="main" style="width: <?php echo $theme_settings['logo_width']; ?>px" />
		<?php endif; ?>
	</a>
</div>



<!-- wrapper -->
<div id="content-wrapper" class="content-wrapper">
	<div id="content-wrapper-inside" class="content-wrapper__inside">

		<?php
			if( is_home() ){
				if( (int)$theme_settings['featured_count'] != 0 ){
					get_template_part('loop', 'featured');
				}
			}

			if( is_page() ){
				if(
					(int)$theme_settings['featured_count'] != 0
					&& get_post_meta( get_the_ID(), 'use_featured', true ) == '1'
				){
					get_template_part('loop', 'featured');
				}
			}

			get_template_part('cover');

			if( is_active_sidebar('widget-area-before-content') ){
				echo '<div class="max-width-wrapper">';
				echo '<div class="widget-area--content widget-area--before-content">';
				dynamic_sidebar('widget-area-before-content');
				echo '</div>';
				echo '</div>';
			}
		?>
