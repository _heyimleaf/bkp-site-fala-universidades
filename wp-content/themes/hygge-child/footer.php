<?php

$theme_settings = hygge_theme_settings();
$footer_class = '';

if( empty( $theme_settings['bg_footer'] ) ){
	$footer_class .= ' no-background';
}

if ($theme_settings['bg_footer_dark']) {
	$footer_class = 'dark-mode';
}

if( is_active_sidebar('widget-area-after-content') ){
	echo '<div class="widget-area--content widget-area--after-content">';
	echo '<div class="max-width-wrapper">';
		dynamic_sidebar('widget-area-after-content');
		echo '</div>';
	echo '</div>';
}



echo '</div>'; // content wrapper inside
echo '</div>'; // content wrapper

echo '</div>';
if( class_exists( 'WooCommerce' ) ){
	/**
	 * woocommerce_sidebar hook.
	 *
	 * @hooked woocommerce_get_sidebar - 10
	 */
	do_action( 'woocommerce_sidebar' );
}else{
	get_sidebar();
}

echo '</div>';
echo '<div class="hygge-layout-wrapper"><a class="button" style="margin: 0px auto 2em;" href="https://falauniversidades.com.br/tags/">Tags mais acessadas</a></div>';
echo '<div class="footer hygge-layout-wrapper">';
echo '<div class="hygge-layout-main">';

?>

<div class="footer__top">
	<div class="max-width-wrapper">
		<?php nav_menu_categories(); ?>
	</div>
</div>

<?php
echo '<div style="background: #f16623;" class="footer__bottom '.esc_attr($footer_class).'">';
echo '<div class="max-width-wrapper">';
echo '<div class="fl">';
nav_menu_footer();
echo '</div>';
echo '<div class="fr">';
echo '<div id="hygge_social_icons-4" class="widget widget--footer widget_hygge_social_icons"><span class="social-nav"><a class="item label js-skip-ajax  icon--true" target="_blank" href="https://www.facebook.com/falauniversidades/"><i class="hygge-icon-facebook-square"></i></a><a class="item label js-skip-ajax  icon--true" target="_blank" href="https://www.linkedin.com/company/falauniversidades/?originalSubdomain=pt"><i class="hygge-icon-linkedin-square"></i></a><a class="item label js-skip-ajax  icon--true" target="_blank" href="https://www.youtube.com/channel/UCHKloiQfykGSkeE42mGHRkA"><i class="hygge-icon-youtube"></i></a></span></div>';
echo '</div>';
echo '</div>';
echo '</div>';
echo '</div>';
echo '<div class="hygge-layout-sidebar"></div>';
echo '</div>';
wp_footer();
?>

<!-- <script type="text/javascript" id="r7barrautil" src="https://barra.r7.com/barra.js">{responsivo:true, banner:false, submenu:false, append:true, blank:true, r7_play:false, acessibilidade:false}</script> -->

<!-- TailTarget Tag Manager TT-9964-3/CT-23 -->
<!-- <script>
    (function(i) {
    var ts = document.createElement('script');
    ts.type = 'text/javascript';
    ts.async = true;
    ts.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tags.t.tailtarget.com/t3m.js?i=' + i;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ts, s);
    })('TT-9964-3/CT-23');
</script> -->
<!-- End TailTarget Tag Manager -->
</body>
</html>
