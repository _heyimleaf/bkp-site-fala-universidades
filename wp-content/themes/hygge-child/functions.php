<?php

/*------------------------------------------------------------
* Include child-theme css styles
*------------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', 'hygge_child_load_styles', 11 );
function hygge_child_load_styles() {
	wp_enqueue_style( 'hygge-child-main', get_theme_file_uri() . '/style.css' );
}

/*------------------------------------------------------------
* Include child-theme language files
*------------------------------------------------------------*/

add_action( 'after_setup_theme', 'hygge_child_load_textdomain' );
function hygge_child_load_textdomain() {
	load_child_theme_textdomain( 'hygge', get_theme_file_path() . '/languages' );
}

/*------------------------------------------------------------
* Custom functions start here
*------------------------------------------------------------*/
add_action('init', 'register_footer_menu');
function register_footer_menu() {
	register_nav_menus(array(
		'categories-menu' => esc_html__('Categories Menu', 'hygge'),
		'footer-menu' => esc_html__('Footer Menu', 'hygge')
	));
}

function nav_menu_footer() {
	wp_nav_menu(array(
		'theme_location'  => 'footer-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'footer-menu',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
	));
}

function nav_menu_categories() {
	wp_nav_menu(array(
		'theme_location'  => 'categories-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'categories-menu',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
	));
}


// function add_vary_header($headers) {
// 	$headers['Vary'] = 'User-Agent';
// 	return $headers;
// 	}
// add_filter('wp_headers', 'add_vary_header');
