<?php

get_header();

/*
    * Template Name: Perguntas Frequentes
    */

?>
<header class="d-flex flex-column justify-content-between">
    <div class="container">
        <?php get_template_part('template', 'nav') ?>
    </div>
</header>
<section id="faq">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="tituloSecao--semTraco">
                    perguntas recorrentes
                </h1>
            </div>
        </div>
        <?php if (have_rows('perguntas_e_respostas')) : ?>
        <?php while (have_rows('perguntas_e_respostas')) : the_row(); ?>
        <div class="row">
            <div class="col-12 faq-pergunta">
                <div class="d-flex flex-colum">

                    <p class="faq-pergunta-texto"><?= get_sub_field('pergunta') ?>
                    </p>

                </div>
                <img class="user-pergunta" src="<?php echo get_template_directory_uri(); ?>/img/user-neosyx.png" alt="" style="">
            </div>
        </div>
        <div class="row">
            <div class="col-12 faq-resposta ">
                <div class="d-flex flex-colum">
                    <p class="faq-resposta-texto"><?= get_sub_field('resposta') ?></p>
                </div>

                <img class="user-resposta" src="<?php echo get_template_directory_uri(); ?>/img/logo-icone.png" alt="" style="">
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
    </div>
</section>
<?php get_footer(); ?>